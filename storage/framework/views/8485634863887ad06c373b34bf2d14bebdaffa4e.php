<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('vendor/laravel-filemanager/js/lfm.js')); ?>"></script>
<script src="<?php echo e(asset('assets/admin/js/admin/employee_custom.js')); ?>"></script>
<script>
    $(document).ready(function () {
        // start form-general
        $('#form-general [name="id"]').val("<?php echo e(!empty($form_employee->id)?$form_employee->id:null); ?> ");
        $('#form-general [name="employee_id"]').val(
            "<?php echo e(!empty($form_employee->employee_id)?$form_employee->employee_id:null); ?>");
        $('#form-general [name="picture_profile"]').val(
            "<?php echo e(!empty($form_employee->picture_profile)?$form_employee->picture_profile:null); ?>");
        $('#form-general img#holder').attr("src",
            "<?php echo e(!empty($form_employee->picture_profile) ? url($form_employee->picture_profile) :null); ?>");
        $('#form-general [name="picture_profile_scan"]').val(
            "<?php echo e(!empty($form_employee->picture_profile_scan)?$form_employee->picture_profile_scan:null); ?>");
        $('#form-general img#holder_scan').attr("src",
            "<?php echo e(!empty($form_employee->picture_profile_scan) ? url($form_employee->picture_profile_scan) :null); ?>"
            );
        $('#form-general [name="cid"]').val("<?php echo e(!empty($form_employee->cid)?$form_employee->cid:null); ?>");
        $('#form-general [name="prename"]').val(
            "<?php echo e(!empty($form_employee->prename)?$form_employee->prename:null); ?>");
        $('#form-general [name="firstname"]').val(
            "<?php echo e(!empty($form_employee->firstname)?$form_employee->firstname:null); ?>");
        $('#form-general [name="lastname"]').val(
            "<?php echo e(!empty($form_employee->lastname)?$form_employee->lastname:null); ?>");
        $('#form-general [name="prename_en"]').val(
            "<?php echo e(!empty($form_employee->prename_en)?$form_employee->prename_en:null); ?>");
        $('#form-general [name="firstname_en"]').val(
            "<?php echo e(!empty($form_employee->firstname_en)?$form_employee->firstname_en:null); ?>");
        $('#form-general [name="lastname_en"]').val(
            "<?php echo e(!empty($form_employee->lastname_en)?$form_employee->lastname_en:null); ?>");
        $('#form-general [name="nickname"]').val(
            "<?php echo e(!empty($form_employee->nickname)?$form_employee->nickname:null); ?>");
        $('#form-general [name="empcode"]').val(
            "<?php echo e(!empty($form_employee->empcode)?$form_employee->empcode:null); ?>");
        $('#form-general [name="birthday"]').val(
            "<?php echo e(!empty($form_employee->birthday)?$form_employee->birthday:null); ?>");
        $('#form-general input[name="gender"][value="<?php echo e(!empty($form_employee->gender)?$form_employee->gender:null); ?>"]')
            .prop("checked", true);

        $('#form-general [name="house_number"]').val(
            "<?php echo e(!empty($form_employee->house_number)?$form_employee->house_number:null); ?>");
        $('#form-general [name="road"]').val("<?php echo e(!empty($form_employee->road)?$form_employee->road:null); ?>");
        var address = "<?php echo e(!empty($form_employee->subdistrict_id)?$form_employee->subdistrict_id:null); ?>";
        if (address != null) {
            redrawaddress(address);
        }
        $('#form-general [name="zipcode"]').val(
            `<?php echo e(!empty($form_employee->zipcode)?$form_employee->zipcode:null); ?>`);

        $('#form-general [name="cr_house_number"]').val(
            "<?php echo e(!empty($form_employee->cr_house_number)?$form_employee->cr_house_number:null); ?>");
        $('#form-general [name="cr_road"]').val(
            "<?php echo e(!empty($form_employee->cr_road)?$form_employee->cr_road:null); ?>");
        var current_address =
            "<?php echo e(!empty($form_employee->cr_subdistrict_id)?$form_employee->cr_subdistrict_id:null); ?>";
        if (current_address != null) {
            redrawcurrentaddress(current_address);
        }
        $('#form-general [name="cr_zipcode"]').val(
            `<?php echo e(!empty($form_employee->cr_zipcode)?$form_employee->cr_zipcode:null); ?>`);
        $('#form-general [name="email"]').val(`<?php echo e(!empty($form_employee->email)?$form_employee->email:null); ?>`);
        $('#form-general [name="mobile"]').val(
            `<?php echo e(!empty($form_employee->mobile)?$form_employee->mobile:null); ?>`);
        $('#form-general [name="telephone"]').val(
            `<?php echo e(!empty($form_employee->telephone)?$form_employee->telephone:null); ?>`);
        $('#form-general [name="branch_id"]').val(
            `<?php echo e(!empty($form_employee->branch_id)?$form_employee->branch_id:null); ?>`);
        $('#form-general [name="department_id"]').val(
            `<?php echo e(!empty($form_employee->department_id)?$form_employee->department_id:null); ?>`);
        $('#form-general [name="level_id"]').val(
            `<?php echo e(!empty($form_employee->level_id)?$form_employee->level_id:null); ?>`);
        $('#form-general [name="employee_status_id"]').val(
            `<?php echo e(!empty($form_employee->employee_status_id)?$form_employee->employee_status_id:null); ?>`);
        $('#form-general [name="startworking_date"]').val(
            `<?php echo e(!empty($form_employee->startworking_date)?$form_employee->startworking_date:null); ?>`);
        $('#form-general [name="resignation_date"]').val(
            `<?php echo e(!empty($form_employee->resignation_date)?$form_employee->resignation_date:null); ?>`);
        $('#form-general [name="group_id"]').val(
            `<?php echo e(!empty($form_employee->group_id)?$form_employee->group_id:null); ?>`);
        $('#form-general [name="education_level_id"]').val(
            `<?php echo e(!empty($form_employee->education_level_id)?$form_employee->education_level_id:null); ?>`);
        $('#form-general [name="company_id"]').val(
            `<?php echo e(!empty($form_employee->company_id)?$form_employee->company_id:null); ?>`);
        $('#form-general [name="employee_level_id"]').val(
            `<?php echo e(!empty($form_employee->employee_level_id)?$form_employee->employee_level_id:null); ?>`);
        <?php if(!empty($form_employee -> remark)): ?>
        $('#form-general [name="remark"]').val(`<?php echo e($form_employee->remark); ?>`);
        <?php endif; ?>
        $('#form-general [name="hospital_id"]').val(
            `<?php echo e(!empty($form_employee->hospital_id)?$form_employee->hospital_id:null); ?>`);
        $('#form-general [name="skill_description"]').val(
            `<?php echo e(!empty($form_employee->skill_description)?$form_employee->skill_description:null); ?>`);
        $('#form-general [name="authority"]').val(
            `<?php echo e(!empty($form_employee->authority)?$form_employee->authority:null); ?>`);
        // end form-general
        $('.ls-select2').select2();

        function redrawcurrentaddress(subdistrict_id) {
            $.ajax({
                type: "get",
                url: rurl + 'getaddress/' + subdistrict_id,
                dataType: "json",
                success: function (response) {
                    $('select#cr_provinces').empty();
                    $('select#cr_provinces').append('<option value="">== จังหวัด ==</option>');
                    $.each(response.provinces, function (index, value) {
                        if (response.province_id == value.id) {
                            $('select#cr_provinces').append('<option value="' + value.id +
                                '" selected>' +
                                value.name_th + '</option>');
                        } else {
                            $('select#cr_provinces').append('<option value="' + value.id +
                                '">' +
                                value.name_th + '</option>');
                        }
                    });
                    $('select#cr_districts').empty();
                    $('select#cr_districts').append('<option value="">== อำเภอ/เขต ==</option>');
                    $.each(response.districts, function (index, value) {
                        if (response.district_id == value.id) {
                            $('select#cr_districts').append('<option value="' + value.id +
                                '" selected>' +
                                value.name_th + '</option>');
                        } else {
                            $('select#cr_districts').append('<option value="' + value.id +
                                '">' +
                                value.name_th + '</option>');
                        }
                    });
                    $('select#cr_subdistricts').empty();
                    $('select#cr_subdistricts').append(
                        '<option value=""> == ตำบล/แขวง == </option>');
                    $.each(response.subdistricts, function (index, value) {
                        if (response.subdistrict_id == value.id) {
                            $('select#cr_subdistricts').append('<option value="' + value
                                .id + '" selected>' +
                                value.name_th + '</option>');
                        } else {
                            $('select#cr_subdistricts').append('<option value="' + value
                                .id + '">' +
                                value.name_th + '</option>');
                        }
                    });
                    $('.ls-select2').select2();
                }
            });
        }
    });
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">ข้อมูลพนักงาน</h5>
    </div>
    <div class="card-body">

        
        <div class="row">
            <div class="col-md-12">
                <div class="card card-transparent">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-linetriangle d-none d-md-flex d-lg-flex d-xl-flex"
                        data-init-reponsive-tabs="dropdownfx">
                        <li class="nav-item">
                            <a class="active show" data-toggle="tab" data-target="#profile"
                                href="#"><span>ข้อมูลทั่วไป</span></a>
                        </li>

                        <li class="nav-item">
                            <a data-toggle="tab" data-target="#levels" href="#"><span>การปรับเลื่อนตำแหน่ง</span></a>
                        </li>

                        <li class="nav-item">
                            <a data-toggle="tab" data-target="#experience" href="#"><span>ประสบการณ์ทำงาน</span></a>
                        </li>

                        <li class="nav-item">
                            <a data-toggle="tab" data-target="#education" href="#"><span>การศึกษา</span></a>
                        </li>

                        <li class="nav-item">
                            <a data-toggle="tab" data-target="#training" href="#"><span>การอบรม</span></a>
                        </li>

                        <li class="nav-item">
                            <a data-toggle="tab" data-target="#leave" href="#"><span>ผู้อนุมัติการลา</span></a>
                        </li>

                        <li class="nav-item">
                            <a data-toggle="tab" data-target="#evaluation" href="#"><span>จัดการการประเมิน</span></a>
                        </li>

                        <li class="nav-item">
                            <a data-toggle="tab" data-target="#warning" href="#"><span>หนังสือเตือน</span></a>
                        </li>

                        <li class="nav-item">
                            <a data-toggle="tab" data-target="#account" href="#"><span>ตั้งค่าบัญชีผู้ใช้</span></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane active show" id="profile">
                            <form id="form-general" class="form-horizontal">
                                <input type="hidden" name="id">

                                <div class="form-group row">
                                    <div class="col-lg-6 text rigth">
                                        <a class="btn btn-sm" data-toggle="modal" data-target="#modelId">
                                            <i class="fas fa-id-card"></i> แสกนบัตรประจำตัวประชาชน
                                        </a>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="picture" class="col-sm-3 col-form-label">ภาพโปรไฟล์(scan)</label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-lg-6 text-center"
                                                style="padding:5px; border:1px dotted rgba(100,100,100,0.1); background:rgba(200,200,200,0.1);">
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="picture_profile_scan" placeholder="ภาพโปรไฟล์">
                                                </div>
                                                <img id="holder_scan" class="img-fluid  text-center"style="margin-top:5px;max-height:150px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="picture" class="col-sm-3 col-form-label">ภาพโปรไฟล์</label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-lg-6 text-center"
                                                style="padding:5px; border:1px dotted rgba(100,100,100,0.1); background:rgba(200,200,200,0.1);">
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail" data-preview="holder"
                                                            class="btn btn-default">
                                                            <i class="fa fa-picture-o"></i> อัพโหลด
                                                        </a>
                                                    </span>
                                                    <input id="thumbnail" class="form-control" type="text"
                                                        name="picture_profile" placeholder="ภาพโปรไฟล์">
                                                </div>
                                                <img id="holder" class="img-fluid  text-center"
                                                    style="margin-top:5px;max-height:150px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">เลขที่บัตรประชาขน</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="เลขที่บัตรประชาขน"
                                                    name="cid">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">ชื่อ นามสกุล(ไทย)</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" placeholder="คำนำหน้าชื่อ"
                                                    name="prename">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" placeholder="ชื่อ"
                                                    name="firstname">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" placeholder="นามสกุล"
                                                    name="lastname">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">ชื่อ นามสกุล(อังกฤษ)</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" placeholder="คำนำหน้าชื่อ"
                                                    name="prename_en">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" placeholder="ชื่อ"
                                                    name="firstname_en">
                                            </div>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" placeholder="นามสกุล"
                                                    name="lastname_en">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">ชื่อเล่น</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" placeholder="ชื่อเล่น"
                                                    name="nickname">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">รหัสพนักงาน</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input class="form-control" type="text" name="empcode">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">วันเกิด</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input class="form-control" type="date" name="birthday">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">เพศ</label>
                                    <div class="col-md-9">
                                        <div class="radio radio-success">
                                            <input type="radio" value="M" name="gender" id="male">
                                            <label for="male">ชาย</label>
                                            <input type="radio" value="F" name="gender" id="female">
                                            <label for="female">หญิง</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">ที่อยู่ตามบัตรประชาชน</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>เลขที่</label>
                                                <input type="text" name="house_number" class="form-control"
                                                    placeholder="เลขที่">
                                            </div>
                                            <div class="col-md-6">
                                                <label>ถนน</label>
                                                <input type="text" name="road" class="form-control" placeholder="ถนน">
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>จังหวัด</label>
                                                <select id="provinces" class="form-control ls-select2"
                                                    name="province_id">
                                                    <option value="">== จังหวัด ==</option>
                                                    <?php $__currentLoopData = $provinces; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name_th); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>อำเภอ/เขต</label>
                                                <select class="form-control ls-select2" id="districts"
                                                    name="district_id">
                                                    <option value="">== อำเภอ/เขต ==</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>ตำบล/แขวง</label>
                                                <select class="form-control ls-select2" id="subdistricts"
                                                    name="subdistrict_id">
                                                    <option value="">== ตำบล/แขวง ==</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>รหัสไปรษณีย์</label>
                                                <input type="text" id="zipcode" name="zipcode" class="form-control"
                                                    placeholder="รหัสไปรษณีย์">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">ที่อยู่ปัจจุบัน</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>เลขที่</label>
                                                <input type="text" name="cr_house_number" class="form-control"
                                                    placeholder="เลขที่">
                                            </div>
                                            <div class="col-md-6">
                                                <label>ถนน</label>
                                                <input type="text" name="cr_road" class="form-control"
                                                    placeholder="ถนน">
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>จังหวัด</label>
                                                <select id="cr_provinces" class="form-control ls-select2"
                                                    name="cr_province_id">
                                                    <option value="">== จังหวัด ==</option>
                                                    <?php $__currentLoopData = $provinces; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name_th); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>อำเภอ/เขต</label>
                                                <select class="form-control ls-select2" id="cr_districts"
                                                    name="cr_district_id">
                                                    <option value="">== อำเภอ/เขต ==</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>ตำบล/แขวง</label>
                                                <select class="form-control ls-select2" id="cr_subdistricts"
                                                    name="cr_subdistrict_id">
                                                    <option value="">== ตำบล/แขวง ==</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>รหัสไปรษณีย์</label>
                                                <input type="text" id="cr_zipcode" name="cr_zipcode"
                                                    class="form-control" placeholder="รหัสไปรษณีย์">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-3 col-form-label">บริษัท</label>
                                    <div class="col-md-9">

                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                                <select class="form-control ls-select2" name="company_id">
                                                    <option value="">== บริษัท ==</option>
                                                    <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>

                                            <div class="col-md-6">
                                                
                                                <select class="form-control ls-select2" name="branch_id">
                                                    <option value="">== สาขา ==</option>
                                                    <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->branch_name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                                <select class="form-control ls-select2" name="group_id">
                                                    <option value="">== ฝ่าย ==</option>
                                                    <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                
                                                <select class="form-control ls-select2" name="department_id">
                                                    <option value="">== แผนก ==</option>
                                                    <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                                <select class="form-control ls-select2" name="level_id">
                                                    <option value="">== ตำแหน่ง ==</option>
                                                    <?php $__currentLoopData = $levels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                
                                                <select class="form-control ls-select2" name="employee_level_id">
                                                    <option value="">== ระดับ ==</option>
                                                    <?php $__currentLoopData = $employeelevel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-3 col-form-label">รายละเอียดงาน</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>หน้าที่</label>
                                                <textarea class="form-control" name="skill_description"></textarea>
                                            </div>
                                            <div class="col-md-6">
                                                <label>อำนาจอนุมัติในการบริหาร</label>
                                                <textarea class="form-control" name="authority"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-3 col-form-label">ประสังคม</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control ls-select2" name="hospital_id">
                                                    <option value="">== ประสังคม ==</option>
                                                    <?php $__currentLoopData = $hospitals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-3 col-form-label">ระดับการศึกษา</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                                <select class="form-control ls-select2" name="education_level_id">
                                                    <option value="">== ระดับการศึกษา ==</option>
                                                    <?php $__currentLoopData = $educationlevel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-3 col-form-label">ติดต่อ</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="email" class="form-control" id="email" placeholder="อีเมล"
                                                    name="email">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control"
                                                    placeholder="เบอร์โทรศัพท์มือถือ" name="mobile" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-3 col-form-label">สถานะพนักงาน</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>สถานะพนักงาน</label>
                                                <select class="form-control ls-select2" name="employee_status_id">
                                                    <option value="">== สถานะ ==</option>
                                                    <?php $__currentLoopData = $employeestatus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>วันที่เริ่มงาน</label>
                                                <input type="date" name="startworking_date" id="" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-3 col-form-label">การออกจากงาน</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>วันที่</label>
                                                <input type="date" name="resignation_date" id="" class="form-control">
                                            </div>
                                            <div class="col-md-4">
                                                <label>เหตุผล</label>
                                                <select class="form-control ls-select2" name="resign_reason_id">
                                                    <option value="">== เหตุผล ==</option>
                                                    <?php $__currentLoopData = $resignreason; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>หมายเหตุ</label>
                                                <textarea class="form-control" id="name" placeholder="หมายเหตุ"
                                                    name="resign_reason_remark" aria-invalid="false"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <button class="btn btn-success" type="submit">บันทึก</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane" id="account">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form method="post" action="" style="width: 100%;" id="form-account">
                                        <input type="hidden" class="form-control" name="id"
                                            value="<?php echo e(!empty($form_admin_user->id)?$form_admin_user->id:''); ?>">
                                        <input type="hidden" class="form-control" name="employee_id"
                                            value="<?php echo e($employee_id); ?>">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>ชื่อผู้ใช้</label>
                                                    <input type="text" placeholder="ชื่อผู้ใช้" class="form-control"
                                                        name="email"
                                                        value="<?php echo e(!empty($form_admin_user->email)?$form_admin_user->email:''); ?>">
                                                </div>

                                                <div class="col-md-6">
                                                    <label>รหัสผ่าน</label>
                                                    <br>
                                                    <a href="#" class="btn-newpassword">ตั้งรหัสผ่านใหม่</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group d-none newpassword">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>รหัสผ่านใหม่</label>
                                                    <input type="password" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary legitRipple">บันทึก</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="levels">

                            <div class="row">
                                <div class="col-lg-12">
                                    <p>การปรับเลื่อนตำแหน่ง
                                        <button class="btn btn-default btn-xs card-add">+</button>
                                    </p>
                                    <form id="form-preferment" role="form" autocomplete="off" novalidate="novalidate">
                                        <div class="container-clone">
                                            <?php for($big = count($form_employee_preferment)-1; $big>=0; $big--): ?>
                                            <?php
                                            $value = $form_employee_preferment[$big];
                                            ?>
                                            <div data-pages="card" class="card card-default" data-id="<?php echo e($big); ?>"
                                                data-table="preferment">
                                                <div class="card-header">
                                                    <div class="card-title">การปรับเลื่อนตำแหน่ง <span
                                                            class="card-title-id">#<?php echo e(($big+1)); ?></span>
                                                    </div>
                                                    <div class="card-controls">
                                                        <ul>
                                                            </li>
                                                            <li><a data-toggle="refresh" class="card-clone" href="#"><i
                                                                        class="pg-plus"></i></a>
                                                            </li>
                                                            <li><a class="card-remove" href="#"><i
                                                                        class="card-icon card-icon-close"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-group-attached">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <input type="hidden" name="id[]" value="<?php echo e($value->id); ?>">
                                                                <input type="hidden" name="employee_id[]"
                                                                    value="<?php echo e($employee_id); ?>">
                                                                <div class="form-group form-group-default">
                                                                    <label>สาขา</label>
                                                                    <select name="branch_id[]" class="ls-select2">
                                                                        <option value="">== เลือกสาขา ==</option>
                                                                        <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="<?php echo e($item->id); ?>"
                                                                            <?php echo e(($value->branch_id==$item->id)?'selected':''); ?>>
                                                                            <?php echo e($item->branch_name); ?></option>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group form-group-default">
                                                                    <label>แผนก</label>
                                                                    <select name="department_id[]" class="ls-select2">
                                                                        <option value="">== เลือกแผนก ==</option>
                                                                        <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="<?php echo e($item->id); ?>"
                                                                            <?php echo e(($value->department_id==$item->id)?'selected':''); ?>>
                                                                            <?php echo e($item->name); ?>

                                                                        </option>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group form-group-default">
                                                                    <label>ตำแหน่ง</label>
                                                                    <select name="level_id[]" class="ls-select2">
                                                                        <option value="">== เลือกตำแหน่ง ==</option>
                                                                        <?php $__currentLoopData = $levels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="<?php echo e($item->id); ?>"
                                                                            <?php echo e(($value->level_id==$item->id)?'selected':''); ?>>
                                                                            <?php echo e($item->name); ?>

                                                                        </option>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>ตั้งแต่</label>
                                                                    <input type="date" class="form-control"
                                                                        name="start_date[]"
                                                                        value="<?php echo e($value->start_date); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>ถึง</label>
                                                                    <input type="date" class="form-control"
                                                                        name="end_date[]" value="<?php echo e($value->end_date); ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endfor; ?>
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary legitRipple">บันทึก</button>
                                        </div>
                                    </form>

                                    <div data-pages="card" class="card card-default d-none" data-table="preferment">
                                        <div class="card-header">
                                            <div class="card-title">การปรับเลื่อนตำแหน่ง <span
                                                    class="card-title-id">#1</span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    </li>
                                                    <li><a data-toggle="refresh" class="card-clone" href="#"><i
                                                                class="pg-plus"></i></a>
                                                    </li>
                                                    <li><a class="card-remove" href="#"><i
                                                                class="card-icon card-icon-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <input type="hidden" name="id[]">
                                            <input type="hidden" name="employee_id[]" value="<?php echo e($employee_id); ?>">
                                            <div class="form-group-attached">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>สาขา</label>
                                                            <select name="branch_id[]" class="ls-select2">
                                                                <option value="">== เลือกสาขา ==</option>
                                                                <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($item->id); ?>">
                                                                    <?php echo e($item->branch_name); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>แผนก</label>
                                                            <select name="department_id[]" class="ls-select2">
                                                                <option value="">== เลือกแผนก ==</option>
                                                                <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($item->id); ?>">
                                                                    <?php echo e($item->name); ?>

                                                                </option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>ตำแหน่ง</label>
                                                            <select name="level_id[]" class="ls-select2">
                                                                <option value="">== เลือกตำแหน่ง ==</option>
                                                                <?php $__currentLoopData = $levels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($item->id); ?>">
                                                                    <?php echo e($item->name); ?>

                                                                </option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>ตั้งแต่</label>
                                                            <input type="date" class="form-control" name="start_date[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>ถึง</label>
                                                            <input type="date" class="form-control" name="end_date[]">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="experience">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>ประสบการณ์ทำงาน
                                        <button class="btn btn-default btn-xs card-add">+</button>
                                    </p>
                                    <form id="form-work" role="form" autocomplete="off" novalidate="novalidate">
                                        <div class="container-clone">
                                            <?php for($big = count($form_employee_work)-1; $big>=0; $big--): ?>
                                            <?php
                                            $value = $form_employee_work[$big];
                                            ?>
                                            <div data-pages="card" class="card card-default" data-id="<?php echo e($big); ?>"
                                                data-table="work">
                                                <div class="card-header">
                                                    <div class="card-title">ประสบการณ์ทำงาน
                                                        <span class="card-title-id">#<?php echo e(($big + 1)); ?></span>
                                                    </div>
                                                    <div class="card-controls">
                                                        <ul>
                                                            </li>
                                                            <li><a data-toggle="refresh" class="card-clone" href="#"><i
                                                                        class="pg-plus"></i></a>
                                                            </li>
                                                            <li><a class="card-remove" href="#"><i
                                                                        class="card-icon card-icon-close"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <input type="hidden" name="id[]" value="<?php echo e($value->id); ?>">
                                                    <input type="hidden" name="employee_id[]" value="<?php echo e($employee_id); ?>">
                                                    <input type="hidden" name="experience_type[]" value="w">
                                                    <div class="form-group-attached">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>บริษัท</label>
                                                                    <input type="text" class="form-control"
                                                                        name="place[]" value="<?php echo e($value->place); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>ตำแหน่ง</label>
                                                                    <input type="text" class="form-control"
                                                                        name="level[]" value="<?php echo e($value->level); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>ตั้งแต่</label>
                                                                    <input type="date" class="form-control"
                                                                        name="start_date[]"
                                                                        value="<?php echo e($value->start_date); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>ถึง</label>
                                                                    <input type="date" class="form-control"
                                                                        name="end_date[]" value="<?php echo e($value->end_date); ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endfor; ?>

                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary legitRipple">บันทึก</button>
                                        </div>
                                    </form>
                                    <div data-pages="card" class="card card-default d-none" data-table="work">
                                        <div class="card-header">
                                            <div class="card-title">ประสบการณ์ทำงาน
                                                <span class="card-title-id">#<?php echo e($big); ?></span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    </li>
                                                    <li><a data-toggle="refresh" class="card-clone" href="#"><i
                                                                class="pg-plus"></i></a>
                                                    </li>
                                                    <li><a class="card-remove" href="#"><i
                                                                class="card-icon card-icon-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <input type="hidden" name="id[]">
                                            <input type="hidden" name="employee_id[]" value="<?php echo e($employee_id); ?>">
                                            <input type="hidden" name="experience_type[]" value="w">
                                            <div class="form-group-attached">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>บริษัท</label>
                                                            <input type="text" class="form-control" name="place[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>ตำแหน่ง</label>
                                                            <input type="text" class="form-control" name="level[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>ตั้งแต่</label>
                                                            <input type="date" class="form-control" name="start_date[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>ถึง</label>
                                                            <input type="date" class="form-control" name="end_date[]">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="education">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>การศึกษา
                                        <button class="btn btn-default btn-xs card-add">+</button>
                                    </p>
                                    <form id="form-education" action="" method="post">
                                        <div class="form-group-attached">
                                            <div class="container-clone">
                                                <?php for($big = count($form_employee_education)-1; $big>=0; $big--): ?>
                                                <?php
                                                $value = $form_employee_education[$big];
                                                ?>
                                                <div data-pages="card" class="card card-default" data-id="<?php echo e($big); ?>"
                                                    data-table="education">
                                                    <div class="card-header">
                                                        <div class="card-title">การศึกษา
                                                            <span class="card-title-id">#<?php echo e((int)($big+1)); ?></span>
                                                        </div>
                                                        <div class="card-controls">
                                                            <ul>
                                                                <li><a data-toggle="refresh" class="card-clone"
                                                                        href="#"><i class="pg-plus"></i></a>
                                                                </li>
                                                                <li><a class="card-remove" href="#"><i
                                                                            class="card-icon card-icon-close"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <input type="hidden" name="id[]" value="<?php echo e($value->id); ?>">
                                                        <input type="hidden" name="employee_id[]"
                                                            value="<?php echo e($employee_id); ?>">
                                                        <input type="hidden" name="experience_type[]" value="e">
                                                        <div class="form-group-attached">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>สถาบัน</label>
                                                                        <select class="form-control ls-select2"
                                                                            name="institute_id[]">
                                                                            <option value="">== สถาบัน ==</option>
                                                                            <?php $__currentLoopData = $institute; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <option value="<?php echo e($item->id); ?>"
                                                                                <?php echo e($value->institute_id==$item->id?'selected':''); ?>>
                                                                                <?php echo e($item->name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>ตั้งแต่</label>
                                                                        <input type="date" class="form-control"
                                                                            name="start_date[]"
                                                                            value="<?php echo e($value->start_date); ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>ถึง</label>
                                                                        <input type="date" class="form-control"
                                                                            name="end_date[]"
                                                                            value="<?php echo e($value->end_date); ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>ระดับการศึกษา</label>
                                                                        <select class="form-control ls-select2"
                                                                            name="education_level[]">
                                                                            <option value="">== ระดับการศึกษา ==
                                                                            </option>
                                                                            <?php $__currentLoopData = $educationlevel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <option value="<?php echo e($item->id); ?>"
                                                                                <?php echo e($value->education_level==$item->id?'selected':''); ?>>
                                                                                <?php echo e($item->name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>วุฒิการศึกษา</label>
                                                                        <input type="text" class="form-control"
                                                                            name="level[]" value="<?php echo e($value->level); ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>สาขาวิชา</label>
                                                                        <input type="text" class="form-control"
                                                                            name="subject[]"
                                                                            value="<?php echo e($value->subject); ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary legitRipple">บันทึก</button>
                                        </div>
                                    </form>
                                    <div data-pages="card" class="card card-default d-none" data-table="education">
                                        <div class="card-header">
                                            <div class="card-title">การศึกษา
                                                <span class="card-title-id">#1</span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a data-toggle="refresh" class="card-clone" href="#"><i
                                                                class="pg-plus"></i></a>
                                                    </li>
                                                    <li>
                                                        <a class="card-remove" href="#"><i
                                                                class="card-icon card-icon-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <input type="hidden" name="id[]">
                                            <input type="hidden" name="employee_id[]" value="<?php echo e($employee_id); ?>">
                                            <input type="hidden" name="experience_type[]" value="e">
                                            <div class="form-group-attached">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>สถาบัน</label>
                                                            <select class="form-control ls-select2"
                                                                name="institute_id[]">
                                                                <option value="">== สถาบัน ==</option>
                                                                <?php $__currentLoopData = $institute; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($item->id); ?>"
                                                                    <?php echo e($value->institute_id==$item->id?'selected':''); ?>>
                                                                    <?php echo e($item->name); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>ตั้งแต่</label>
                                                            <input type="date" class="form-control" name="start_date[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>ถึง</label>
                                                            <input type="date" class="form-control" name="end_date[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>ระดับการศึกษา</label>
                                                            <select class="form-control ls-select2"
                                                                name="education_level[]">
                                                                <option value="">== ระดับการศึกษา ==</option>
                                                                <?php $__currentLoopData = $educationlevel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>วุฒิการศึกษา</label>
                                                            <input type="text" class="form-control" name="level[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>สาขาวิชา</label>
                                                            <input type="text" class="form-control" name="subject[]">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="training">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>การอบรม
                                        <button class="btn btn-default btn-xs card-add">+</button>
                                    </p>
                                    <form id="form-training" action="" method="post">
                                        <div class="form-group-attached">
                                            <div class="container-clone">
                                                <?php for($big = count($form_employee_training)-1; $big>=0; $big--): ?>
                                                <?php
                                                $value = $form_employee_training[$big];
                                                ?>
                                                <div data-pages="card" class="card card-default" data-id="<?php echo e($big); ?>"
                                                    data-table="education">
                                                    <div class="card-header">
                                                        <div class="card-title">การอบรม
                                                            <span class="card-title-id">#<?php echo e((int)($big+1)); ?></span>
                                                        </div>
                                                        <div class="card-controls">
                                                            <ul>
                                                                <li><a data-toggle="refresh" class="card-clone"
                                                                        href="#"><i class="pg-plus"></i></a>
                                                                </li>
                                                                <li><a class="card-remove" href="#"><i
                                                                            class="card-icon card-icon-close"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <input type="hidden" name="id[]" value="<?php echo e($value->id); ?>">
                                                        <input type="hidden" name="employee_id[]"
                                                            value="<?php echo e($employee_id); ?>">
                                                        <input type="hidden" name="experience_type[]" value="t">
                                                        <div class="form-group-attached">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>สถาบัน</label>
                                                                        <input type="text" class="form-control"
                                                                            name="place[]" value="<?php echo e($value->place); ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>หลักสูตรการอบรม</label>
                                                                        <input type="text" class="form-control"
                                                                            name="level[]" value="<?php echo e($value->level); ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>ประเภทการอบรม</label>
                                                                        <select class="form-control" name="subject[]">
                                                                            <option value="">== ประเภท ==</option>
                                                                            <option value="ภายใน"
                                                                                <?php echo e($value->subject == "ภายใน" ? 'selected' : ''); ?>>
                                                                                ภายใน</option>
                                                                            <option value="ภายนอก"
                                                                                <?php echo e($value->subject  == "ภายนอก" ? 'selected' : ''); ?>>
                                                                                ภายนอก</option>
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group form-group-default">
                                                                        <label>ตั้งแต่</label>
                                                                        <input type="date" class="form-control"
                                                                            name="start_date[]"
                                                                            value="<?php echo e($value->start_date); ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group form-group-default">
                                                                        <label>ถึง</label>
                                                                        <input type="date" class="form-control"
                                                                            name="end_date[]"
                                                                            value="<?php echo e($value->end_date); ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary legitRipple">บันทึก</button>
                                        </div>
                                    </form>
                                    <div data-pages="card" class="card card-default d-none" data-table="education">
                                        <div class="card-header">
                                            <div class="card-title">การอบรม
                                                <span class="card-title-id">#1</span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a data-toggle="refresh" class="card-clone" href="#"><i
                                                                class="pg-plus"></i></a>
                                                    </li>
                                                    <li>
                                                        <a class="card-remove" href="#"><i
                                                                class="card-icon card-icon-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <input type="hidden" name="id[]">
                                            <input type="hidden" name="employee_id[]" value="<?php echo e($employee_id); ?>">
                                            <input type="hidden" name="experience_type[]" value="t">
                                            <div class="form-group-attached">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>สถาบัน</label>
                                                            <input type="text" class="form-control" name="place[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>หลักสูตรการอบรม</label>
                                                            <input type="text" class="form-control" name="level[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>ประเภทการอบรม</label>
                                                            <select class="form-control" name="subject[]">
                                                                <option value="">== ประเภท ==</option>
                                                                <option value="ภายใน">ภายใน</option>
                                                                <option value="ภายนอก">ภายนอก</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>ตั้งแต่</label>
                                                            <input type="date" class="form-control" name="start_date[]">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>ถึง</label>
                                                            <input type="date" class="form-control" name="end_date[]">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="leave">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="form-leave-approvers" action="" method="post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" name="id"
                                                    value="<?php echo e(!empty($form_employee_leave_approver->id)?$form_employee_leave_approver->id:''); ?>">
                                                <input type="hidden" name="employee_id" value="<?php echo e($employee_id); ?>">
                                                <label>ผู้อนุมัติการลา</label><br>
                                                <?php
                                                $multiple =
                                                !empty($form_employee_leave_approver->approvers)?json_decode($form_employee_leave_approver->approvers):[];
                                                ?>
                                                <select name="approvers[]" class="ls-select2" multiple="multiple">
                                                    <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($item->id); ?>"
                                                        <?php echo e(in_array($item->id,$multiple)?'selected':''); ?>>
                                                        <?php echo e($item->firstname); ?> <?php echo e($item->lastname); ?>

                                                    </option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary legitRipple">บันทึก</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="evaluation">
                            <div class="row">
                                <div class="col-lg-12">

                                    <p>จัดการการประเมิน
                                        <button class="btn btn-default btn-xs card-add">+</button>
                                    </p>
                                    <form id="form-evaluation" action="" method="post">
                                        <div class="form-group-attached">
                                            <div class="container-clone">
                                                <?php for($key = count($form_employee_evaluation)-1; $key>=0; $key--): ?>
                                                <?php
                                                $value = $form_employee_evaluation[$key];
                                                ?>
                                                <div data-pages="card" class="card card-default" data-id="<?php echo e($key); ?>"
                                                    data-table="evaluation">
                                                    <div class="card-header">
                                                        <div class="card-title">ความสามารถในการประเมิน
                                                            <span class="card-title-id">#<?php echo e((int)($key+1)); ?></span>
                                                        </div>
                                                        <div class="card-controls">
                                                            <ul>
                                                                <li><a data-toggle="refresh" class="card-clone"
                                                                        href="#"><i class="pg-plus"></i></a>
                                                                </li>
                                                                <li><a class="card-remove" href="#"><i
                                                                            class="card-icon card-icon-close"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <input type="hidden" name="id[<?php echo e($key); ?>]"
                                                                value="<?php echo e(($value->id)); ?>">
                                                            <input type="hidden" name="employee_id[<?php echo e($key); ?>]"
                                                                value="<?php echo e(($employee_id)); ?>">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>ชื่อ นามสกุล</label>
                                                                    <select name="employee_target_id[<?php echo e($key); ?>]"
                                                                        class="ls-select2">
                                                                        <option value="">== ชื่อ นามสกุล ==</option>
                                                                        <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <option value="<?php echo e($item->id); ?>"
                                                                            <?php echo e($value->employee_target_id==$item->id?'selected':''); ?>>
                                                                            <?php echo e($item->firstname); ?> <?php echo e($item->lastname); ?>

                                                                        </option>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>ความสามารถในการประเมิน <?php echo e($key+1); ?></label>
                                                                    <?php
                                                                    $ability =
                                                                    !empty($value->evaluation_ability)?json_decode($value->evaluation_ability):[];
                                                                    ?>
                                                                    <?php $__currentLoopData = $evaluationtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <div class="checkbox check-success">
                                                                        <input type="checkbox" value="<?php echo e($item->id); ?>"
                                                                            id="checkbox-<?php echo e($key); ?>-<?php echo e($item->id); ?>"
                                                                            name="evaluation_ability[<?php echo e($key); ?>][]"
                                                                            <?php echo e(in_array($item->id,$ability)?'checked':''); ?>>
                                                                        <label
                                                                            for="checkbox-<?php echo e($key); ?>-<?php echo e($item->id); ?>"><?php echo e($item->name); ?></label>
                                                                    </div>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary legitRipple">บันทึก</button>
                                        </div>
                                    </form>
                                    <div data-pages="card" class="card card-default d-none" data-table="evaluation">
                                        <div class="card-header">
                                            <div class="card-title">ความสามารถในการประเมิน
                                                <span class="card-title-id">#1</span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li><a data-toggle="refresh" class="card-clone" href="#"><i
                                                                class="pg-plus"></i></a>
                                                    </li>
                                                    <li><a class="card-remove" href="#"><i
                                                                class="card-icon card-icon-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <input type="hidden" name="id[]">
                                                <input type="hidden" name="employee_id[]" value="<?php echo e($employee_id); ?>">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>ชื่อ นามสกุล</label>
                                                        <select name="employee_target_id[0]" class="ls-select2">
                                                            <option value="">== ชื่อ นามสกุล ==</option>
                                                            <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($item->id); ?>">
                                                                <?php echo e($item->firstname); ?> <?php echo e($item->lastname); ?>

                                                            </option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>ความสามารถในการประเมิน</label>
                                                        <?php $__currentLoopData = $evaluationtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <div class="checkbox check-success">
                                                            <input type="checkbox" value="<?php echo e($item->id); ?>"
                                                                id="checkbox-0-<?php echo e($item->id); ?>"
                                                                name="evaluation_ability[0][]">
                                                            <label
                                                                for="checkbox-0-<?php echo e($item->id); ?>"><?php echo e($item->name); ?></label>
                                                        </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="warning">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>หนังสือเตือน
                                        <button class="btn btn-default btn-xs card-add">+</button>
                                    </p>
                                    <form id="form-warning" action="" method="post">
                                        <div class="form-group-attached">
                                            <div class="container-clone">
                                                <?php for($big = count($form_employee_warning)-1; $big>=0; $big--): ?>
                                                <?php
                                                $value = $form_employee_warning[$big];
                                                ?>
                                                <div data-pages="card" class="card card-default" data-id="<?php echo e($big); ?>"
                                                    data-table="warning">
                                                    <div class="card-header">
                                                        <div class="card-title">หนังสือเตือน
                                                            <span class="card-title-id">#<?php echo e((int)($big+1)); ?></span>
                                                        </div>
                                                        <div class="card-controls">
                                                            <ul>
                                                                <li><a data-toggle="refresh" class="card-clone"
                                                                        href="#"><i class="pg-plus"></i></a>
                                                                </li>
                                                                <li><a class="card-remove" href="#"><i
                                                                            class="card-icon card-icon-close"></i></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <input type="hidden" name="id[]" value="<?php echo e($value->id); ?>">
                                                        <input type="hidden" name="employee_id[]"
                                                            value="<?php echo e($employee_id); ?>">
                                                        <div class="form-group-attached">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>โทษทางวินัย</label>
                                                                        <select class="form-control ls-select2"
                                                                            name="warning_id[]">
                                                                            <option value="">== โทษทางวินัย ==</option>
                                                                            <?php $__currentLoopData = $warning; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <option value="<?php echo e($item->id); ?>"
                                                                                <?php echo e($value->warning_id==$item->id?'selected':''); ?>>
                                                                                <?php echo e($item->name); ?></option>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>ตั้งแต่</label>
                                                                        <input type="date" class="form-control"
                                                                            name="start_date[]"
                                                                            value="<?php echo e($value->start_date); ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-default">
                                                                        <label>ถึง</label>
                                                                        <input type="date" class="form-control"
                                                                            name="end_date[]"
                                                                            value="<?php echo e($value->end_date); ?>">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group form-group-default">
                                                                        <label>เรื่อง</label>
                                                                        <input type="text" class="form-control" name="detail[]"
                                                                            cols="30" rows="10"
                                                                            value="<?php echo e($value->detail); ?>"></input>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endfor; ?>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary legitRipple">บันทึก</button>
                                        </div>
                                    </form>
                                    <div data-pages="card" class="card card-default d-none" data-table="education">
                                        <div class="card-header">
                                            <div class="card-title">การศึกษา
                                                <span class="card-title-id">#1</span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a data-toggle="refresh" class="card-clone" href="#"><i
                                                                class="pg-plus"></i></a>
                                                    </li>
                                                    <li>
                                                        <a class="card-remove" href="#"><i
                                                                class="card-icon card-icon-close"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <input type="hidden" name="id[]">
                                            <input type="hidden" name="employee_id[]" value="<?php echo e($employee_id); ?>">
                                            <div class="form-group-attached">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>โทษทางวินัย</label>
                                                            <select class="form-control ls-select2" name="warning_id[]">
                                                                <option value="">== โทษทางวินัย ==</option>
                                                                <?php $__currentLoopData = $warning; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>ตั้งแต่</label>
                                                            <input type="date" class="form-control" name="start_date[]">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group form-group-default">
                                                            <label>ถึง</label>
                                                            <input type="date" class="form-control" name="end_date[]">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-default">
                                                            <label>เรื่อง</label>
                                                            <input type="text" class="form-control" name="detail[]" cols="30"
                                                                rows="10"></input>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade slide-up show" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content-wrapper">
                    <form class="hawkeye">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">อ่านข้อมูลจากบัตรประชาชน</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <br />
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">ภาพถ่าย(scan)</label>
                                    <div class="col-sm-8">
                                        <textarea name="picture_profile_scan" id="summernote-editor"
                                            class="form-control input-sm" placeholder="เนื้อหา"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">ภาพถ่าย</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="picture" placeholder="ภาพถ่าย"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">เลขบัตรประชาชน</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="cid" placeholder="เลขบัตรประชาชน"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">คำนำหน้าชื่อ</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="prename" placeholder="คำนำหน้าชื่อไทย"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">ชื่อ</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="firstname" placeholder="ชื่อ"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">นามสกุล</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="lastname" placeholder="นามสกุล"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name"
                                        class="col-sm-4 col-form-label">คำนำหน้าชื่อ(อังกฤษ)</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="prename_en" placeholder="คำนำหน้าชื่อ(อังกฤษ)"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">ชื่อ(อังกฤษ)</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="firstname_en" placeholder="ชื่อ(อังกฤษ)"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">นามสกุล(อังกฤษ)</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="lastname_en" placeholder="นามสกุล(อังกฤษ)"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">วันเกิด</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="birthday" placeholder="วันเกิด"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">เพศ</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="gender" placeholder="เพศ"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">บ้านเลขที่</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="house_number" placeholder="บ้านเลขที่"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">หมู่ที่</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="village_no" placeholder="สาขา"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">ถนน</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="road" placeholder="ถนน" class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">จังหวัด</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="province" placeholder="จังหวัด"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">อำเภอ</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="district" placeholder="อำเภอ"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">ตำบล</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="subdistrict" placeholder="ตำบล"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-sm-4 col-form-label">ไปรษณีย์</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="zipcode" placeholder="ไปรษณีย์"
                                            class="form-control input-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                                <button type="submit" class="btn btn-primary">กรอกข้อมูลอัตโนมัติ</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/form_personal_information.blade.php ENDPATH**/ ?>