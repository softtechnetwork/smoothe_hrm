<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('assets/admin/js/admin/adminusers.js')); ?>"></script>
<script>
    $(document).on('click', '.btn-newpassword', function () {
        $('.newpassword').toggleClass('d-none');
            // console.log($('.newpassword.d-none').length);
        if ($('.newpassword.d-none').length == 1) {
            $('.newpassword input[type="password"]:first').removeAttr('name');
        } else {
            $('.newpassword input[type="password"]:first').attr('name', 'password');
        }
    });

    $(document).on('click', '.checkall', function () {
        var checkall = $(this).is(':checked');
        if(checkall){
            $('input[name="access_menu[]"]').prop("checked", true);
        }else{
            $('input[name="access_menu[]"]').prop("checked", false);
        }
    });

    $(document).on('change', '.main_menu', function () {
        var checkbox = $(this);
        if (checkbox.is(':checked')) {
            $("[data-group='" + checkbox.val() + "']").prop("checked", true);
        } else {
            $("[data-group='" + checkbox.val() + "']").prop("checked", false);
        }
    });

    $(document).on('change', 'input[name="access_menu[]"]', function () {
        var l = $('input[name="access_menu[]"]:not(:checked)').length;
        if(l!=0){
            $('.checkall').prop('checked',false);
        }else{
            $('.checkall').prop('checked',true);
        }
    });
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <h5 class="pull-left"><?php echo e(isset($menu) ? $menu : ''); ?></h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + <?php echo e(isset($menu) ? $menu : ''); ?>

        </button>
    </div>
    <div class="card-body">
        <table id="adminusers" class="table table-xs table-hover table-bordered table-striped dataTable no-footer"
            cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ชื่อผู้ใช้ (user)</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th></th>
                    <th></th>
                    <th>วันที่</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form class="validateForm" >
    <div class="modal fade slide-up" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-full">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5><?php echo e(isset($menu) ? $menu : ''); ?></h5>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
                        <div class="row">
                            <label for="firstname" class="col-md-2 col-form-label">ชื่อ-นามสกุล</label>
                            <div class="col-md-10">
                                <select class="ls-select2" name="employee_id">
                                    <option value="">== ชื่อ-นามสกุล ==</option>
                                    <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($item->id); ?>"><?php echo e($item->firstname); ?> <?php echo e($item->lastname); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label for="email" class="col-md-2 col-form-label">ชื่อผู้ใช้ (username)</label>
                            <div class="col-md-10">
                                <input type="text" name="email" placeholder="ชื่อผู้ใช้ (username)" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="row">
                            <label for="password" class="col-md-2 col-form-label">รหัสผ่าน (password)</label>
                            <div class="col-md-10">
                                <a class="btn btn-default btn-newpassword" href="#">ตั้งรหัสผ่าน</a>
                                <div class="newpassword d-none" style="margin-top:5px;">
                                    <input type="password" placeholder="password" class="form-control input-sm">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label for="meu_name" class="col-md-2 col-form-label">สิทธิ์การเข้าถึงเมนู</label>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                             
                                            <div class="checkbox check-success">
                                                <input type="checkbox" id="checkall" class="checkall">
                                                <label for="checkall">ทั้งหมด</label>
                                            </div>
                                            
                                            <div class="row">
                                                <br/>
                                                <?php
                                                    $am = !empty($form_admin_user->access_menu)?json_decode($form_admin_user->access_menu):[];
                                                ?>
                                                <?php $__currentLoopData = $adminmenus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($item->main_menu==0 && $item->main_menu!=null): ?>
                                                <div class="col-lg-3 col-md-4">
                                                    <div class="checkbox check-success">
                                                        <input type="checkbox" value="<?php echo e($item->id); ?>"
                                                            id="checkbox<?php echo e($item->id); ?>" name="access_menu[]"
                                                            class="main_menu"
                                                            <?php echo e(in_array($item->id,$am)?"checked":""); ?>>
                                                        <label for="checkbox<?php echo e($item->id); ?>"><?php echo e($item->menu_name); ?>

                                                            (เมนูหลัก)</label>
                                                    </div>
                                                </div>

                                                <?php $__currentLoopData = $adminmenus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($value->main_menu==$item->id && $value->id!=$item->id &&
                                                $value->main_menu!=0 && $value->main_menu!=null): ?>
                                                <div class="col-lg-3 col-md-4">
                                                    <div class="checkbox check-success">
                                                        <input type="checkbox" value="<?php echo e($value->id); ?>"
                                                            id="checkbox<?php echo e($value->id); ?>" name="access_menu[]"
                                                            data-group="<?php echo e($item->id); ?>"
                                                            <?php echo e(in_array($value->id,$am)?"checked":""); ?>>
                                                        <label for="checkbox<?php echo e($value->id); ?>"><?php echo e($value->menu_name); ?></label>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>

                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="clearfix">สิทธิ์การใช้เครื่องมือ</label>
                                            <div class="row">
                                                <br />
                                                <?php $__currentLoopData = $adminmenus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($item->main_menu==null): ?>
                                                <div class="col-md-3">
                                                    <div class="checkbox check-success">
                                                        <input type="checkbox" value="<?php echo e($item->id); ?>" id="checkbox<?php echo e($item->id); ?>" name="access_menu[]" <?php echo e(in_array($item->id,$am)?"checked":""); ?>>
                                                        <label for="checkbox<?php echo e($item->id); ?>"><?php echo e($item->menu_name); ?></label>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/adminusers.blade.php ENDPATH**/ ?>