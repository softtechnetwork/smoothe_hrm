<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(config('app.name', 'PSI_HRM')); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="<?php echo e(asset('pages/ico/60.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('pages/ico/76.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('pages/ico/120.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('pages/ico/152.png')); ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="PSI HRM Backend" name="description" />
    <meta content="Napat Osaklang" name="author" />
    <?php echo $__env->yieldContent('meta'); ?>
    <?php echo $__env->yieldContent('font'); ?>
    <!-- Styles -->

    
    <link href="<?php echo e(asset('assets/plugins/pace/pace-theme-flash.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/plugins/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo e(asset('assets/plugins/switchery/css/switchery.min.css')); ?>" rel="stylesheet" type="text/css" media="screen" />
    

    
    <link href="<?php echo e(asset('template/condensed/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('template/condensed/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('template/condensed/assets/plugins/datatables-responsive/css/datatables.responsive.css')); ?>" rel="stylesheet" type="text/css" media="screen" />

    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/bootstrap-sweetalert-master/dist/sweetalert.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/fontawesome-free-5.9.0-web/css/all.css')); ?>">
    
    
    <?php echo $__env->yieldContent('style'); ?>
    <link href="<?php echo e(asset('pages/css/pages.css')); ?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo e(asset('pages/css/pages-icons.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('css/admin/custom.css')); ?>" rel="stylesheet" type="text/css" />

    
    <link href="https://fonts.googleapis.com/css?family=K2D&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">

    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
      var OneSignal = window.OneSignal || [];
      OneSignal.push(function(){
        OneSignal.init({
          appId: "828752d9-0826-4e12-9453-80f76dfba0ac",
          notifyButton:{
            enable: true,
          },
          subdomainName: "hrmsystem-psis",
        });
      });
    </script>
</head>

<body class="fixed-header">
    <!-- Navs -->
    <?php echo $__env->make('admin.layouts.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
        <?php echo $__env->make('admin.layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- START PAGE CONTENT WRAPPER -->
        <div class="page-content-wrapper">
            <!-- START PAGE CONTENT -->
            <div class="content">
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid padding-25 sm-padding-10">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <?php echo $__env->yieldContent('content'); ?>
                <!-- END PLACE PAGE CONTENT HERE -->
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        <?php echo $__env->make('admin.layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->

    
    <script src="<?php echo e(asset('assets/plugins/pace/pace.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery/jquery-3.2.1.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/modernizr.custom.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/popper/umd/popper.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/bootstrap/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery/jquery-easy.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery-unveil/jquery.unveil.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery-ios-list/jquery.ioslist.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery-actual/jquery.actual.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/plugins/select2/js/select2.full.min.js')); ?>"></script>

    <script src="<?php echo e(asset('template/condensed/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/condensed/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/condensed/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('template/condensed/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo e(asset('template/condensed/assets/plugins/datatables-responsive/js/datatables.responsive.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('template/condensed/assets/plugins/datatables-responsive/js/lodash.min.js')); ?>"></script>
    

    
    <script src="<?php echo e(asset('assets/plugins/jquery-validation1191/jquery.validate.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/jquery-validation1191/additional-methods.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/validate_th.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/bootstrap-sweetalert-master/dist/sweetalert.min.js')); ?>"></script>
    

    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo e(asset('pages/js/pages.min.js')); ?>"></script>
    <!-- END CORE TEMPLATE JS -->

    <script src="<?php echo e(asset('assets/admin/js/admin/custom.js')); ?>"></script>

    <script>
        surl = "<?php echo e(url('')); ?>";
        rurl = "<?php echo e(asset('')); ?>";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var menu = "<?php echo e(!empty($menu) ? $menu : ''); ?>";
        var database = "psi_hrm";
        var checked = null;
    </script>

    <!-- Scripts -->
    <?php echo $__env->yieldContent('script'); ?>
</body>

</html><?php /**PATH C:\xampp\htdocs\smoothe_hrm\resources\views/admin/layouts/app.blade.php ENDPATH**/ ?>