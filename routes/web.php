<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', function () {
    return redirect('admin');
});

Route::get('push', 'API\NotificationController@Push');

Route::get('projectassign', function () {
    $query =
    \DB::select("
		SELECT 
		p.id,p.created_by
		FROM 
		project p;
	");
    $result = array();
    $insert = ['211','30','203'];
    $str = '';
    foreach ($query as $key => $item) {
        foreach ($insert as $k => $i) {
            $query_in = \DB::select("select pa.employee_id from project_assign pa where pa.employee_id=".$i." and pa.project_id = ".($item->id));
            if (!$query_in) {
                $str .= "INSERT INTO [project_assign]([project_id], [employee_id], [role], [created_at]) VALUES (".($item->id).", ".$i.", NULL, '2020-05-08 09:50:50.000');</br>";
            }
        }
    }
    return $str;
});

Route::get('/clear', function () {
    \Artisan::call('cache:clear');
    \Artisan::call('config:cache');
    \Artisan::call('view:clear');
    // \Artisan::call('route:cache');
    return "Successfully";
});

Route::get('/getdistrict/{province_id}', 'FunctionController@GetDistrinct');
Route::get('/getsubdistrict/{district_id}', 'FunctionController@GetSubdistrinct');
Route::get('/getaddress/{subdistrict_id}', 'FunctionController@GetAddress');
Route::get('/getzipcode/{subdistrict_id}', 'FunctionController@GetZipcode');
Route::post('/getidaddress', 'FunctionController@convertAddress');
Route::post('/convertImage', 'FunctionController@convertImage');

Route::get('/evaluation_calculate/{employee_id}', 'Admin\EvaluationtypeController@calculate_result');

//Install CRUD
Route::get('/install/detailvalue', 'InstallController@detailvalue');
Route::post('/install/createmvc', 'InstallController@createmvc');

Route::get('/install/list', 'InstallController@list');
Route::post('/install/column', 'InstallController@column');
Route::resource('/install', 'InstallController');

// START ADMIN
Route::get('/admin/logout', 'Auth\AdminController@logout');
Route::get('/admin/login', 'Auth\AdminController@login');
Route::post('/admin/login', 'Auth\AdminController@authenticate');
Route::group(['middleware' => ['admin'] , 'prefix' => 'admin'], function () {
    //Install CRUD
    Route::get('/install/detailvalue', 'InstallController@detailvalue');
    Route::post('/install/createmvc', 'InstallController@createmvc');

    Route::get('/install/list', 'InstallController@list');
    Route::post('/install/column', 'InstallController@column');
    Route::resource('/install', 'InstallController');

    Route::get('/', 'Auth\AdminController@index');

    Route::get('/employeecustom', 'Admin\EmployeeCustomController@index');

    Route::get('/organizationalstructure', 'Admin\StructureController@index');
    Route::get('/organizationalstructure/json', 'Admin\StructureController@get_organize');
    Route::get('/organizationalstructure/chart', 'Admin\StructureController@orgranizational_chart');
	Route::get('/organizationalstructure/chart_json', 'Admin\StructureController@orgranizational_chart_json');
	Route::get('/organizationalstructure/chart_json/{evaluaiton_id}', 'Admin\StructureController@orgranizational_chart_json');
    Route::post('/organizationalstructure/getlistemployee', 'Admin\StructureController@get_list');
    Route::post('/organizationalstructure', 'Admin\StructureController@post_organize');

    Route::post('organizationalstructure/structure_evaluation/{evaluation_id}', 'Admin\StructureController@structure_evaluation');

    Route::get('/evaluationform', function () {
        return view('admin.evaluation_form');
    });

    Route::get('laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
    
    Route::get('/department/list', 'Admin\DepartmentController@list');
    Route::resource('/department', 'Admin\DepartmentController');

    Route::get('/level/list', 'Admin\LevelController@list');
    Route::resource('/level', 'Admin\LevelController');

    Route::get('/leavetype/list', 'Admin\LeavetypeController@list');
    Route::resource('/leavetype', 'Admin\LeavetypeController');

    Route::get('/holiday/list', 'Admin\HolidayController@list');
    Route::resource('/holiday', 'Admin\HolidayController');

    Route::get('/customer/list', 'Admin\CustomerController@list');
    Route::resource('/customer', 'Admin\CustomerController');

    Route::get('/provinces/list', 'Admin\ProvincesController@list');
    Route::resource('/provinces', 'Admin\ProvincesController');

    Route::get('/districts/list', 'Admin\DistrictsController@list');
    Route::resource('/districts', 'Admin\DistrictsController');

    Route::get('/subdistricts/list', 'Admin\SubdistrictsController@list');
    Route::resource('/subdistricts', 'Admin\SubdistrictsController');

    Route::get('/branch/list', 'Admin\BranchController@list');
    Route::resource('/branch', 'Admin\BranchController');

    Route::get('/geographies/list', 'Admin\GeographiesController@list');
    Route::resource('/geographies', 'Admin\GeographiesController');

    Route::get('/leaveduration/list', 'Admin\LeavedurationController@list');
    Route::resource('/leaveduration', 'Admin\LeavedurationController');

    Route::get('/evaluation/list', 'Admin\EvaluationController@list');
    Route::resource('/evaluation', 'Admin\EvaluationController');

    Route::get('news/list', 'Admin\NewsController@list');
    Route::resource('news', 'Admin\NewsController');

    Route::get('corporateinformation/list', 'Admin\CorporateinformationController@list');
    Route::resource('corporateinformation', 'Admin\CorporateinformationController');

    Route::get('evaluationtype/list', 'Admin\EvaluationtypeController@list');
    Route::resource('evaluationtype', 'Admin\EvaluationtypeController');

    Route::post('evaluationresult/list', 'Admin\EvaluationresultController@list');
    Route::resource('evaluationresult', 'Admin\EvaluationresultController');
    Route::get('evaluationresult/export', 'Admin\EvaluationresultController@export_evaluationresult');

    Route::get('employeestatus/list', 'Admin\EmployeestatusController@list');
    Route::resource('employeestatus', 'Admin\EmployeestatusController');
    
    Route::post('employee/preferment', 'Admin\EmployeeController@store_preferment');
    Route::post('employee/experience', 'Admin\EmployeeController@store_experience');
    Route::post('employee/leaveapprovers', 'Admin\EmployeeController@store_leaveapprovers');
    Route::post('employee/evaluation', 'Admin\EmployeeController@store_evaluation');
    Route::post('employee/adminuser', 'Admin\EmployeeController@store_adminuser');
    Route::post('employee/warning', 'Admin\EmployeeController@store_warning');
    Route::post('employee/create', 'Admin\EmployeeController@create');
    Route::post('employee/delete', 'Admin\EmployeeController@destroy_card');
    Route::post('employee/list', 'Admin\EmployeeController@list');
    Route::resource('employee', 'Admin\EmployeeController');
    
    Route::get('employeeleave_employee', 'Admin\EmployeeleaveController@index_employee');
    Route::get('employeeleave_approver', 'Admin\EmployeeleaveController@index_approver');
    Route::get('employeeleave/approvers/{id}', 'Admin\EmployeeleaveController@show_approvers');
    Route::get('employeeleave/list_employee', 'Admin\EmployeeleaveController@list_employee');
    Route::get('employeeleave/list_approver', 'Admin\EmployeeleaveController@list_approver');
    Route::post('employeeleave/list', 'Admin\EmployeeleaveController@list');
    Route::get('employeeleave/detail/{id}', 'Admin\EmployeeleaveController@show_detail');
    Route::resource('employeeleave', 'Admin\EmployeeleaveController');

    Route::post('employeeregistration/list', 'Admin\EmployeeregistrationController@list');
    Route::resource('employeeregistration', 'Admin\EmployeeregistrationController');
    Route::get('groups/list', 'Admin\GroupsController@list');
    Route::resource('groups', 'Admin\GroupsController');
    
    Route::get('workdaygroup/list', 'Admin\WorkdaygroupController@list');
    Route::resource('workdaygroup', 'Admin\WorkdaygroupController');
    Route::get('workday/list', 'Admin\WorkdayController@list');
    Route::resource('workday', 'Admin\WorkdayController');
    Route::get('company/list', 'Admin\CompanyController@list');
    Route::resource('company', 'Admin\CompanyController');
    
    Route::get('resignreason/list', 'Admin\ResignreasonController@list');
    Route::resource('resignreason', 'Admin\ResignreasonController');
    
    Route::get('educationlevel/list', 'Admin\EducationlevelController@list');
    Route::resource('educationlevel', 'Admin\EducationlevelController');
    Route::get('employeelevel/list', 'Admin\EmployeelevelController@list');
    Route::resource('employeelevel', 'Admin\EmployeelevelController');
    Route::get('hospitals/list', 'Admin\HospitalsController@list');
    Route::resource('hospitals', 'Admin\HospitalsController');
    Route::get('warning/list', 'Admin\WarningController@list');
    Route::resource('warning', 'Admin\WarningController');
    Route::get('institutes/list', 'Admin\InstitutesController@list');
    Route::resource('institutes', 'Admin\InstitutesController');

    Route::get('report/attendance', 'Admin\ReportController@attendance');
    Route::post('report/attendance/employee', 'Admin\ReportController@attendance_employee');

    Route::get('report/attendance_all', 'Admin\ReportController@attendance_all');
    Route::post('report/attendance_all', 'Admin\ReportController@attendance_all_list');

    Route::get('report/evaluation_check', 'Admin\ReportController@evaluation_check');
    Route::post('report/evaluation_check/data', 'Admin\ReportController@evaluation_check_list');

    Route::get('import/evaluation', 'Admin\ImportController@get_evaluation');
    Route::post('import/evaluation', 'Admin\ImportController@post_evaluation');
    Route::post('employeeconfirm/list', 'Admin\EmployeeconfirmController@list');
    Route::resource('employeeconfirm', 'Admin\EmployeeconfirmController');
    Route::get('adminusers/list', 'Admin\AdminusersController@list');
    Route::resource('adminusers', 'Admin\AdminusersController');

    Route::get('importevaluation/list', 'Admin\ImportevaluationController@list');
    Route::resource('importevaluation', 'Admin\ImportevaluationController');
    Route::get('shift/list', 'Admin\ShiftController@list');
    Route::resource('shift', 'Admin\ShiftController');
    Route::get('manage_shift', 'Admin\ShiftController@manage_index');
    Route::get('manage_shift/list', 'Admin\ShiftController@management_list');
    Route::get('manage_shift/{id}', 'Admin\ShiftController@show_shift_management');
    Route::post('manage_shift', 'Admin\ShiftController@post_shift_management');
    Route::post('manage_shift/{id}', 'Admin\ShiftController@update_shift_management');
    Route::delete('manage_shift/{id}', 'Admin\ShiftController@delete_shift_management');

    Route::get('/setting/get', 'Admin\SettingController@get_setting');
    Route::resource('/setting', 'Admin\SettingController');
    
    Route::get('employeeexperience/train', 'Admin\EmployeeexperienceController@index_train');
    Route::post('employeeexperience/list', 'Admin\EmployeeexperienceController@list');
    Route::get('employeeexperience/warning', 'Admin\EmployeeexperienceController@index_warning');
    Route::post('employeeexperience/list_warning', 'Admin\EmployeeexperienceController@list_warning');
    Route::get('employeeexperience/position', 'Admin\EmployeeexperienceController@index_position');
    Route::post('employeeexperience/list_preferment', 'Admin\EmployeeexperienceController@list_preferment');
    Route::resource('employeeexperience', 'Admin\EmployeeexperienceController');
    Route::get('project/list', 'Admin\ProjectController@list');
    Route::resource('project', 'Admin\ProjectController');
    Route::get('project/assign/{id}', 'Admin\ProjectController@get_project_assign');
    Route::post('project/assign', 'Admin\ProjectController@post_project_assign');
    Route::get('todolist/list', 'Admin\TodolistController@list');
    Route::resource('todolist', 'Admin\TodolistController');
    /*==Addition_Route==*/

    Route::get('report/evaluation', 'Admin\ReportController@report_evaluation_view');
    Route::post('report/evaluation/data', 'Admin\ReportController@report_evaluation_data');
});
// END ADMIN

Route::group(['prefix' => 'export'], function () {
    Route::get('employee_conclusion', 'PDFController@employee_conclusion');
    Route::get('leave_conclusion', 'Admin\ReportController@leave_conclusion');
    Route::get('employee_leave', 'PDFController@employee_leave');

    Route::get('experience/train', 'Admin\EmployeeexperienceController@report_train');
    Route::get('experience/warn', 'Admin\EmployeeexperienceController@report_warning');
    Route::get('experience/position', 'Admin\EmployeeexperienceController@report_position');

    Route::get('employee_longevity', 'PDFController@employee_longevity');
    Route::get('employee_age', 'PDFController@employee_age');
    Route::get('employee_resign', 'PDFController@employee_resign');
    Route::get('employee_tain', 'PDFController@employee_tain');
    Route::get('employee_current', 'PDFController@employee_current');
    Route::get('employee_hospital', 'PDFController@employee_hospital');
    Route::get('employee_contact', 'PDFController@employee_contact');
});
