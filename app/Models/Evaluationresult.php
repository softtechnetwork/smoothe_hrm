<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluationresult extends Model
{
    protected $table = 'evaluation_result';
    public $timestamps = true;
}