<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shiftmanagement extends Model
{
    protected $table = 'shift_management';
    public $timestamps = true;
}