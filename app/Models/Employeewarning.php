<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeewarning extends Model
{
    protected $table = 'employee_warning';
    public $timestamps = true;
}