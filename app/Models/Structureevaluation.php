<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Structureevaluation extends Model
{
    protected $table = 'structure_evaluation';
    public $timestamps = true;
}
