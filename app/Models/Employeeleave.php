<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeleave extends Model
{
    protected $table = 'employee_leave';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}