<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
class Projecttask extends Model
{
    // use SoftDeletes;
    protected $table = 'project_task';
    public $timestamps = true;
    public $fillable = ['status'];
}