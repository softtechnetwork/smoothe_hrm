<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employeeconfirm extends Model
{
    protected $table = 'employee_confirm';
    public $timestamps = true;
}