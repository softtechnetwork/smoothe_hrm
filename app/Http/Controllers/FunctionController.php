<?php

namespace App\Http\Controllers;

use App\Models\Holiday;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Employee;
use Illuminate\Support\Arr;

class FunctionController extends Controller // App\Http\Controllers\FunctionController
{
    public static function GetPK($table)
    {
        $table = "$table";
        $getcolumn = \DB::select('SHOW COLUMNS FROM '.$table.';');
        // return response()->json($getcolumn);
        foreach ($getcolumn as $key => $value) {
            if ($value->Key == 'PRI') {
                $result[] = $value->Field;
            }
        }
        return "'$table.*','$table.$result[0] as $table$result[0]'";
    }

    public static function GetTablePrimary($table)
    {
        $table = "$table";
        $getcolumn = \DB::select('SHOW COLUMNS FROM '.$table.';');
        foreach ($getcolumn as $key => $value) {
            if ($value->Key == 'PRI') {
                $result[] = $value->Field;
            }
        }
        return $table.$result[0];
    }

    public static function StringConcat($str, $break)
    {
        $result = '';
        foreach ($str as $key => $value) {
            $result .= $value;
            $key < count($str)-1 ? $result.=',' : '';
        }
        return '$model->'."select([$result]);";
    }

    public static function GetDistrinct($province_id)
    {
        return \App\Models\Districts::where('province_id', $province_id)->get();
    }

    public static function GetSubdistrinct($district_id)
    {
        return \App\Models\Subdistricts::where('district_id', $district_id)->get();
    }

    public static function GetZipcode($subdistrict_id)
    {
        return \App\Models\Subdistricts::find($subdistrict_id)->zip_code;
    }

    public static function GetAddress($subdistrict_id)
    {
        $district_id = \App\Models\Subdistricts::find($subdistrict_id)->district_id;
        $province_id = \App\Models\Districts::find($district_id)->province_id;
        $provinces = \App\Models\Provinces::get();
        $districts = \App\Models\Districts::where('province_id', $province_id)->get();
        $subdistricts = \App\Models\Subdistricts::where('district_id', $district_id)->get();
        $data['subdistrict_id'] = $subdistrict_id;
        $data['district_id'] = $district_id;
        $data['province_id'] = $province_id;
        $data['provinces'] = $provinces;
        $data['districts'] = $districts;
        $data['subdistricts'] = $subdistricts;
        return $data;
    }

    public static function GetPictureProfile($employee_id)
    {
        return \App\Models\Employee::find($employee_id);
    }

    public static function GetNameMenu($url)
    {
        if ($first = \App\AdminMenu::where('menu_url', $url)->first()) {
            $result[] = $first;
            if ($first->main_menu!=0) {
                $x = null;
                $i=0;
                do {
                    if ($i==0) {
                        $value = \App\AdminMenu::find($first->main_menu);
                    } else {
                        $value = \App\AdminMenu::find($value->main_menu);
                    }
                    $i++;
                    $x = $value->main_menu;
                    $result[] = $value;
                } while ($x = 0);
            }
            krsort($result);
            return $result;
        }
    }

    public static function convertAddress(Request $request)
    {
        return \App\Models\Subdistricts::where([
            'provinces.name_th' => $request->province,
            'districts.name_th' => $request->district,
            'subdistricts.name_th' => $request->subdistrict,
        ])
        ->leftjoin('districts', 'districts.id', 'subdistricts.district_id')
        ->leftjoin('provinces', 'provinces.id', 'districts.province_id')
        ->select(\DB::raw('provinces.id as provincesid'), \DB::raw('districts.id as districtsid'), \DB::raw('subdistricts.id as subdistrictsid'), 'subdistricts.zip_code')
        ->first();
    }

    /**
     * @param $start_date
     * @param Carbon $workingTimeEnd
     * @return Carbon|null
     */
    private static function endOfFirstDay($start_date, Carbon $workingTimeEnd): Carbon
    {
        return Carbon::make($start_date)
            ->setTime($workingTimeEnd->hour, $workingTimeEnd->minute, $workingTimeEnd->second);
    }

    /**
     * @param $end_date
     * @param Carbon $workingTimeStart
     * @return Carbon|null
     */
    private static function startOfLastDay($end_date, Carbon $workingTimeStart): Carbon
    {
        return Carbon::make($end_date)
            ->setTime($workingTimeStart->hour, $workingTimeStart->minute, $workingTimeStart->second);
    }

    /**
     * @param Carbon|null $startDate
     * @param Carbon|null $endOfFirstDay
     * @return int[]
     */
    private static function diffFirstDate(?Carbon $startDate, ?Carbon $endOfFirstDay): array
    {
        $diffFirstDay = $startDate->diffInMinutes($endOfFirstDay);
        $minutes = $diffFirstDay % 60;
        $diffHoursFirstDay = (int)($diffFirstDay / 60);

        if ($startDate->hour < 12) {
            $diffHoursFirstDay -= 1;
        }
        return array($minutes, $diffHoursFirstDay);
    }

    /**
     * @param Carbon|null $endDate
     * @param Carbon|null $startOfLastDay
     * @param int $minutes
     * @return int[]
     */
    private static function diffLastDate(?Carbon $endDate, ?Carbon $startOfLastDay, int $minutes): array
    {
        $diffLastDay = $endDate->diffInMinutes($startOfLastDay);
        $minutes += $diffLastDay % 60;
        $diffHoursLastDay = (int)($diffLastDay / 60);
        if ($endDate->hour > 13) {
            $diffHoursLastDay -= 1;
        }
        return array($minutes, $diffHoursLastDay);
    }

    /**
     * @param int $minutes
     * @param int $allHours
     * @return int[]
     */
    private static function getMinutes(int $minutes, int $allHours): array
    {
        if ($minutes > 60) {
            $allHours += (int)($minutes / 60);
            $minutes = $minutes % 60;
        }
        return array($allHours, $minutes);
    }

    /**
     * @param int $allHours
     * @param int $hours
     * @return int[]
     */
    private static function getHours(int $allHours, int $hours): array
    {
        $days = (int)($allHours / 8);
        if ($allHours % 8 !== 0) {
            $hours = $allHours % 8;
        }
        return array($days, $hours);
    }

    /**
     * @param int $days
     * @param string $response
     * @param int $hours
     * @param int $minutes
     * @return string
     */
    private static function getDisplay(int $days, string $response, int $hours, int $minutes): string
    {
        $response .= $days !== 0 ? "{$days}(ว)" : '';
        $response .= $hours !== 0 ? "{$hours}(ช)" : '';
        $response .= $minutes !== 0 ? "{$minutes}(น)" : '';
        return $response;
    }

    public function convertImage(Request $request)
    {
        $request['picture_profile_scan'] = $this->get_string_between($request['picture_profile_scan'], 'src="', '"');
        return $request->picture_profile_scan;
    }

    public static function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public static function convert_minute($value)
    {
        $value = intval($value);
        if (!empty($value)) {
            // $value = -720;
            // $day = $value<0 ? ceil($value/60/8) : floor($value/60/8);
            // $hr = $value<0 ? ceil( ( $value - ($day*8*60) ) / 60 ) : floor( ( $value - ($day*8*60) ) / 60 );
            $day = $value<0 ? "00" : floor($value/60/8.50);
            $hr = $value<0 ? "00" : floor(($value - ($day*8.50*60)) / 60);
            $minute = $value - ($day*8.50*60) - ($hr*60) ;
            $obj = new \stdClass();
            $obj->d = sprintf('%01d', $day);
            $obj->h = sprintf('%02d', $hr);
            $obj->m = sprintf('%02d', $minute);
            return $obj;
        } else {
            $day = floor(0);
            $hr = floor(0);
            $minute = floor(0);
            $obj = new \stdClass();
            $obj->d = sprintf('%01d', $day);
            $obj->h = sprintf('%02d', $hr);
            $obj->m = sprintf('%02d', $minute);
            return $obj;
        }
    }

    public static function convert_minute_to_hr($value)
    {
        $value = intval($value);
        if (!empty($value)) {
            $hr = $value<0 ? ceil(($value) / 60) : floor(($value) / 60);
            $minute = $value - ($hr*60) ;
            $obj = new \stdClass();

            $obj->h = sprintf('%02d', $hr);
            $obj->m = sprintf('%02d', $minute);
            return $obj;
        } else {
            $day = floor(0);
            $hr = floor(0);
            $minute = floor(0);
            $obj = new \stdClass();
            $obj->h = sprintf('%02d', $hr);
            $obj->m = sprintf('%02d', $minute);
            return $obj;
        }
    }

    public static function convert_minute_hr($value)
    {
        $value = intval($value);
        if (!empty($value)) {
            // $day = floor( $value/60/8 );
            $hr = floor($value / 60);
            $minute = $value - ($hr*60) ;
            // $d = !empty($day) ? sprintf('%01d',$day).'วัน ' : '';
            $h = !empty($hr) ? sprintf('%01d', $hr).'ชั่วโมง ' : '';
            $m = !empty($minute) ? sprintf('%01d', $minute).'นาที ' : '';
            $string = $h.$m;
            return $string;
        } else {
            return null;
        }
    }

    public static function convert_minute_day($value)
    {
        //for smoothe only 
       
        $value = intval($value);
      
       
        if (!empty($value)) {
         
            $day = floor(($value/60)/8.50);
           
            $hr = floor(($value - ($day*8.50*60)) / 60);
           
            $minute = $value - ($day*8.50*60) - ($hr*60) ;
            
            $d = !empty($day) ? sprintf('%01d', $day).'วัน' : '';
            
            $h = !empty($hr) ? sprintf('%01d', $hr).'ชั่วโมง' : '';
            $m = !empty($minute) ? sprintf('%01d', $minute).'นาที' : '';
            $string = $d.$h.$m;
          
            return $string;
        } else {
            return "-";
        }
    }

    public static function null_to_empty($data)
    {
        $new = array();
        foreach (json_decode($data) as $key => $value) {
            $new[$key] = empty($value) ? '' : $value;
        }
        return $new;
    }

    public static function deg2rad($deg)
    {
        $rad = $deg * pi()/180; // radians = degrees * pi/180
        return $rad;
    }
    
    public static function calculate_distance($lat1, $lat2, $lon1, $lon2)
    {
        $R = 6371.0088; // metres
        $lat1 = deg2rad($lat1);
        $lat2 = deg2rad($lat2);
        $lon1 = deg2rad($lon1);
        $lon2 = deg2rad($lon2);

        $dlon = $lon2 - $lon1;
        $dlat = $lat2 - $lat1;
        $a = pow(sin($dlat/2), 2) + cos($lat1) * cos($lat2) * pow(sin($dlon/2), 2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        // return $R * $c * 1000;
        return $d = ROUND($R * $c * 1000); //(where R is the radius of the Earth)
    }

    public static function date_format_date($str)
    {
        if ($date=date_create($str)) {
            return date_format($date, "Y-m-d");
        } else {
            return "";
        }
    }

    public static function date_format_time($str)
    {
        if ($date=date_create($str)) {
            return date_format($date, "H:i:s");
        } else {
            return "";
        }
    }

    public static function date_format_datetime($str)
    {
        if (isset($str)) {
            $date=date_create($str);
            return date_format($date, "Y-m-d H:i:s");
        } else {
            return "";
        }
    }

    public static function date_format_custom($str, $format="Y-m-d")
    {
        if ($date=date_create($str)) {
            return date_format($date, "$format");
        } else {
            return "";
        }
    }

    public static function array_leave_picture($str)
    {
        $result = json_decode($str);
        $return = array();
        if (is_array($result)) {
            foreach ($result as $key => $value) {
                $return[] = url('photos/images_leave/'.$value);
            }
            $return = $return;
        }
        return $return;
    }
    public static function check_date($start_date, $end_date)
    {
        $start_date = $post_leave['start_date'];
        $end_date = $post_leave['end_date'];
        $date1 = new \DateTime($end_date);
        $date2 = $date1->diff(new \DateTime($start_date));
        return $date2;
    }

    public static function DateThai($strDate)
    {
        $strYear		=		date("Y", strtotime($strDate))+543;
        $strMonth=		date("n", strtotime($strDate));
        $strDay=		date("j", strtotime($strDate));
        $strHour=		date("H", strtotime($strDate));
        $strMinute=		date("i", strtotime($strDate));
        $strSeconds=	date("s", strtotime($strDate));
        $strMonthCut = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
        $strMonthLong = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
        $strMonthThai=$strMonthCut[$strMonth];
        $strYearCut = substr($strYear, 2, 2); //เอา2ตัวท้ายของปี .พ.ศ.
        return "$strDay $strMonthThai $strYearCut";
    } //end function DateThai

    public static function convert_day_thai($name=null)
    {
        $array=array("Sunday"=>"อาทิตย์","Monday"=>"จันทร์","Tuesday"=>"อังคาร","Wednesday"=>"พุธ","Thursday"=>"พฤหัสบดี","Friday"=>"ศุกร์","Saturday"=>"เสาร์");
        $result = $array[$name];
        return isset($result) ? $result : $name ;
    }

    public static function thai_date($time, $type=null)
    {
        $time = strtotime($time);
        $thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
        $thai_month_arr=array(
            "0"=>"",
            "1"=>"มกราคม",
            "2"=>"กุมภาพันธ์",
            "3"=>"มีนาคม",
            "4"=>"เมษายน",
            "5"=>"พฤษภาคม",
            "6"=>"มิถุนายน",
            "7"=>"กรกฎาคม",
            "8"=>"สิงหาคม",
            "9"=>"กันยายน",
            "10"=>"ตุลาคม",
            "11"=>"พฤศจิกายน",
            "12"=>"ธันวาคม"
        );

        $thai_date_return = "";

        switch ($type) {
            case 'w':
                $thai_date_return .= $thai_day_arr[date("w", $time)];
                break;

            case 'j':
                $thai_date_return .= "ที่ ".date("j", $time);
                break;

            case 'n':
                $thai_date_return .=" เดือน".$thai_month_arr[date("n", $time)];
                break;

            case 'Y':
                $thai_date_return .= " พ.ศ.".(date("Y", $time)+543);
                break;

            case 'H:i':
                $thai_date_return .= "  ".date("H:i", $time)." น.";
                break;
            
            default:
                $thai_date_return .= "วัน".$thai_day_arr[date("w", $time)];
                $thai_date_return .= "ที่ ".date("j", $time);
                $thai_date_return .=" เดือน".$thai_month_arr[date("n", $time)];
                $thai_date_return .= " พ.ศ.".(date("Y", $time)+543);
                $thai_date_return .= "  ".date("H:i", $time)." น.";
                break;
        }

        return $thai_date_return;
    }

    // คำนวณเกรดรวมสุดท้าย
    public static function total_score($obj=[])
    {
        $result = null;
        $ratio = 0;
        $score = 0;
        foreach($obj as $key => $value){
            $ratio += $value->ratio;
            $score += ($value->score_float)*($value->ratio);
        }
        $result = ($score)/($ratio);
        return $result;
    }

    // แปลงจากคะแนนดิบเป็นตัวอักษร F - A
    public static function grade($value)
    {
        $value = (int)(ROUND((float)$value * 20));
        if ($value>=1 && $value<=40) {
            return 'E';
        } else if ($value>=41 && $value<=55) {
            return 'D';
        } else if ($value>=56 && $value<=69) {
            return 'C';
        } else if ($value>=70 && $value<=85) {
            return 'B';
        } else if ($value>=86 && $value<=100) {
            return 'A';
        } else {
            return 'F';
        }
    }

    // แปลงจากคะแนนดิบเป็นดาวที่ต้องวาด
    public static function star($value)
    {
        $value = ROUND( (float)$value * 20 );
        if ($value>=1 && $value<=40) {
            return '1';
        } else if ($value>=41 && $value<=55) {
            return '2';
        } else if ($value>=56 && $value<=69) {
            return '3';
        } else if ($value>=70 && $value<=85) {
            return '4';
        } else if ($value>=86 && $value<=100) {
            return '5';
        } else {
            return '0';
        }
    }

    public static function DateDiff($strDate1, $strDate2)
    {
        return (strtotime($strDate2) - strtotime($strDate1))/  (60 * 60 * 24);  // 1 day = 60*60*24
    }
    public static function TimeDiff($strTime1, $strTime2)
    {
        return (strtotime($strTime2) - strtotime($strTime1))/  (60); // 1 Min =  60
    }
    public static function DateTimeDiff($strDateTime1, $strDateTime2)
    {
        return (strtotime($strDateTime2) - strtotime($strDateTime1))/  (60 * 60); // 1 Hour =  60*60
    }
    public static function find_change_shift($datetimeone, $datetimetwo)
    {
        $diff = (FunctionController::TimeDiff($datetimeone, $datetimetwo))/2;
        $plus = new \DateTime($datetimeone);

        $c = explode('.', $diff);
        $str = "";
        if (count($c)>1) {
            // $str .= FLOOR( ($c[0]/60)/24 )."D";
            $str .= FLOOR($c[0]/(60))."H";
            $str .= ($c[0]%60)."M";
            $str .= "$c[1]S";
        } else {
            $str .= FLOOR($diff/60)."H";
            $str .= ($diff%60)."M";
        }
        // return $str;
        $result = $plus->add(new \DateInterval('PT'.$str));
        return $result->format('Y-m-d H:i:s');
    }


    public static function get_leave_time($employee_id)
    { // หาช่วงเวลาทำงาน
        $result = Employee::select(['branch.working_time_start','branch.working_time_end'])
        ->leftjoin('branch', 'branch.id', 'employee.branch_id')
        ->where('employee.id', $employee_id)
        ->first();

        if ($result) {
            $return['start'] = str_replace('.0000000', '', $result->working_time_start);
            $return['end'] = str_replace('.0000000', '', $result->working_time_end);
        } else {
            // $return = ['start'=>'08:00:00','end'=>'17:00:00'];
            $return = ['start'=>'08:00:00','end'=>'17:00:00'];
        }

        //gprint_r($return);exit();

        return $return;
    }

    public static function leave_diff($start_date, $end_date, $employee_id)
    {
        $getLeaveTime = FunctionController::get_leave_time($employee_id);
        //print_r($getLeaveTime);exit();
        $workingTimeStart = Carbon::createFromFormat('H:i:s', data_get($getLeaveTime, 'start'));
        $workingTimeEnd = Carbon::createFromFormat('H:i:s', data_get($getLeaveTime, 'end'));
        $startDate = Carbon::make($start_date);
        $endDate = Carbon::make($end_date);
        $daysForExtraCoding = $startDate->diffInDaysFiltered(function(Carbon $date) use ($startDate, $endDate) {
            if ($date->year == 2020) {
                return !$date->isWeekend();
            }

            $start = clone $startDate;
            $end = clone $endDate;
            $holiday = Holiday::query()->where(['status' => 'T'])->whereBetween(
                'holiday_date',
                [
                    $start->startOfYear()->format('Y-m-d'),
                    $end->endOfYear()->format(
                        'Y-m-d'
                    )
                ]
            )->pluck('holiday_date')->toArray();

            $formatDay = $date->format('Y-m-d');
            if (in_array($formatDay, $holiday)) {
                return false;
            }
            return $date;
        }, $endDate);

        $response = '';

        if ($daysForExtraCoding > 1) {
            $endOfFirstDay = self::endOfFirstDay($start_date, $workingTimeEnd);
            $startOfLastDay = self::startOfLastDay($end_date, $workingTimeStart);

            list($minutes, $diffHoursFirstDay) = self::diffFirstDate($startDate, $endOfFirstDay);
            list($minutes, $diffHoursLastDay) = self::diffLastDate($endDate, $startOfLastDay, $minutes);

            $dateWithinDuration = $daysForExtraCoding - 2;
            if ($dateWithinDuration  > 0) {
                $dateWithinDuration *= 8;
            }


            $allHours = $diffHoursFirstDay + $diffHoursLastDay + $dateWithinDuration;
            $hours = 0;

            list($allHours, $minutes) = self::getMinutes($minutes, $allHours);
            list($days, $hours) = self::getHours($allHours, $hours);

            $response = self::getDisplay($days, $response, $hours, $minutes);
        } else {
            if ($startDate->day !== $endDate->day) {
                $endOfFirstDay = self::endOfFirstDay($startDate->format(Carbon::DEFAULT_TO_STRING_FORMAT), $workingTimeEnd);
                $startOfLastDay = self::startOfLastDay($endDate->format(Carbon::DEFAULT_TO_STRING_FORMAT), $workingTimeStart);

                list($minutes, $diffHoursFirstDay) = self::diffFirstDate($startDate, $endOfFirstDay);
                list($minutes, $diffHoursLastDay) = self::diffLastDate($endDate, $startOfLastDay, $minutes);

                $allHours = $diffHoursFirstDay + $diffHoursLastDay;
                $hours = 0;

                list($allHours, $minutes) = self::getMinutes($minutes, $allHours);
                list($days, $hours) = self::getHours($allHours, $hours);

                $response = self::getDisplay($days, $response, $hours, $minutes);
            } else {
                $diffInDay = $startDate->diffInMinutes($endDate);
                $hours = (int)($diffInDay / 60);
                $minutes = $diffInDay % 60;

                if ($startDate->hour < 12 && $endDate->hour > 13) {
                    $hours -= 1;
                }

                $response = self::getDisplay(0, $response, $hours, $minutes);
            }
        }

        return ($response);
//        $loopstart = date_format(date_create($start_date), "d");
//        $startdateonly = (date_format(date_create($start_date), "Y-m-d"));
//        $enddateonly = date_format(date_create($end_date), "Y-m-d");
//        $startdate = new \DateTime($start_date);
//        $enddate = (new \DateTime($end_date));
//        $datediff = $startdate->diff($enddate);
//        $result = null;
//
//        $leavetime = FunctionController::get_leave_time($employee_id);
//
//        if ($datediff->d==0) {
//            $result = '';
//            if (strtotime($start_date)<strtotime($startdateonly." 12:00:00") && ($end_date)>($enddateonly." 13:00:00")) {
//                $result .= !empty($datediff->h) ? (string)($datediff->h - 1)."(ช)" : '';
//                $result .= !empty($datediff->i) ? ':'.(string)$datediff->i."(น)":'';
//            } else {
//                $result .= !empty($datediff->h) ? (string)$datediff->h."(ช)" : '';
//                $result .= !empty($datediff->i) ? ':'.(string)$datediff->i."(น)":'';
//            }
//        } else {
//            $value = array('h'=>0,'i'=>0);
//            // ลูปวันแรกถึงวันสุดท้าย
//            for ($i=$loopstart , $j=0; $i <= ($loopstart+$datediff->d); $j++,$i++) {
//                $currentdate = substr(date('Y-m-d', strtotime($start_date."+".$j." day")), 1, 10);
//                $currentdatestart = new \DateTime($currentdate.' '.$leavetime['start']);
//                $currentdateend = new \DateTime($currentdate.' '.$leavetime['end']);
//                if ($startdate->diff($currentdateend)->d == 0) {
//                    if (strtotime($start_date)<strtotime($currentdate.' 12:00:00')) {
//                        $value['h'] += ($startdate->diff($currentdateend)->h - 1);
//                        $value['i'] += ($startdate->diff($currentdateend)->i);
//                    } else {
//                        $value['h'] += ($startdate->diff($currentdateend)->h);
//                        $value['i'] += ($startdate->diff($currentdateend)->i);
//                    }
//                } elseif ($currentdatestart->diff($enddate)->d == 0) {
//                    if (strtotime($end_date)>strtotime($currentdate.' 13:00:00')) {
//                        $value['h'] += ($currentdatestart->diff($enddate)->h - 1);
//                        $value['i'] += ($currentdatestart->diff($enddate)->i);
//                    } else {
//                        $value['h'] += ($currentdatestart->diff($enddate)->h);
//                        $value['i'] += ($currentdatestart->diff($enddate)->i);
//                    }
//                } elseif (($startdate->diff($currentdatestart)->d != 0) && ($currentdatestart->diff($enddate)->d == 0)) {
//                    $value['h'] += ($currentdatestart->diff($currentdateend)->h - 1);
//                }
//            }
//            $value['d'] = FLOOR(($value['h'] + FLOOR($value['i']/60)) /8);
//            $value['h'] = FLOOR(($value['h'] + FLOOR($value['i']/60))%8);
//            $value['i'] = $value['i']%60;
//            $v = '';
//            $v .= ($value['d']!=0) ? $value['d'].'(ว)' : '';
//            $v .= ($value['h']!=0) ? $value['h'].'(ช)' : '';
//            $v .= ($value['i']!=0) ? $value['i'].'(น)' : '';
//            $result = ($v);
//        }
//        return ($result);
    }

    public static function monthtoyear($value=0)
    { //$value = จำนวนเดือน
        $str = '';
        $year = FLOOR($value/12);
        $month = $value%12;
        $str .= ($year>0) ? "$year ปี " : '';
        $str .= ($month>0) ? "$month เดือน " : '';
        return $str;
    }

    public static function gender($variable)
    { //$value = จำนวนเดือน
        switch ($variable) {
            case 'M':
                return 'ชาย';
                break;

            case 'F':
                return 'หญิง';
                break;
            
            default:
                return '';
                break;
        }
    }
}
