<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FunctionController as FC;
use Validator;

use App\AdminUser;
use App\Models\Employee;
use App\Models\Employeeregistration;
use App\Models\Employeeleave;
use App\Models\Employeeleaveapprovers;
use App\Models\Employeeevaluation;
use App\Models\News;
use App\Models\Corporateinformation;
use App\Models\Employeeconfirm;
use App\Models\Employeeconfirmregistration;
use App\Models\Employeeconfirmleave;
use App\Models\Branch;
use App\Models\Structurepivot;
use App\Models\Evaluationresult;
use App\Models\Evaluationtype;
use App\Models\Evaluation;
use App\Models\Newstraffic;
use App\Models\Leavetype;
use App\Models\Project;
use App\Models\Projectpicture;
use App\Models\Projectassign;
use App\Models\Projecttask;
use App\Models\Projectstatushistory;
use App\Models\Shift;
use App\Models\Shiftmanagement;
use App\Models\Uuid;
use App\Models\Notification;
use App\Models\Todolist;
use Image;
use stdClass;
use Carbon\Carbon;
use App\Http\Controllers\Admin\EvaluationtypeController as ET;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\API\NotificationController as Noti;
use App\Http\Controllers\Traits\CanLeaveTrait;
use Throwable;
class APIController extends Controller
{

    use CanLeaveTrait;
    public $successStatus = 200;
    
    public function login(Request $request)
    {
        if (Auth::guard('admin')->attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::guard('admin')->user();
            Auth::guard('admin')->user()->AauthAcessToken()->delete();
            $success['auth'] = $user->employee_id;
            $success['token'] =  $user->createToken('Mobile')->accessToken;
            $request['employee_id'] = $user->employee_id;
            if (isset($request['token_device'])) {
                $adddevice = Noti::addDevice($request);
                // return $adddevice->success;
                if ($adddevice->success) {
                    $request['uuid'] = "$adddevice->id";
                    $success['uuid'] = $request['uuid'];
                    $success['uuid_update'] = $this->uuid($request);
                } else {
                    $success['result_code'] = '001';
                    $success['onesignal_exception'] = $adddevice->errors;
                }
            }
            $success['result_code'] = '000';
            $success['result_desc'] = 'เข้าสู่ระบบสำเร็จ';
            $success['message'] = 'เข้าสู่ระบบสำเร็จ';
            $success['status'] = true;
            return response()->json($success, $this-> successStatus);
        } else {
            $data['message'] = 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง';
            $data['result_code'] = '001';
            $data['result_desc'] = 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง';
            $data['status'] = false;
            return response()->json($data, 401);
        }
    }

    public function uuid(Request $request)
    {
        if ($request['uuid']==null) {
            return false;
        }

        Uuid::where([
            ['uuid',$request['uuid']]
        ])->delete();

        $result = Uuid::where([
            ['employee_id',$request['employee_id']]
        ])->first();

        if (empty($result)) {
            $insert['employee_id'] = $request['employee_id'];
            $insert['uuid'] = $request['uuid'];
            $insert['token_device'] = $request['token_device'];
            $insert['os'] = $request['os'];
            $insert['created_at'] = date('Y-m-d H:i:s');
            return (Uuid::insert($insert))?true:false;
        } else {
            $insert['employee_id'] = $request['employee_id'];
            $insert['uuid'] = $request['uuid'];
            $insert['token_device'] = $request['token_device'];
            $insert['os'] = $request['os'];
            $insert['updated_at'] = date('Y-m-d H:i:s');
            return (Uuid::where('employee_id', $request['employee_id'])->update($insert))?true:false;
        }
    }

    public function formlogin()
    {
        return view('api.login');
    }

    public function logout(Request $request)
    {
        if (Auth::guard('api')->check()) {
            $employee_id = Auth::guard('api')->user()->employee_id;
            if (Auth::guard('api')->user()->AauthAcessToken()->delete()) {
                Uuid::where('employee_id', $employee_id)->delete();
                return response()->json([
                    'result_code' => '000'
                    ,'result_desc' => 'ออกจากระบบสำเร็จ'
                    ,'message' => 'ออกจากระบบสำเร็จ'
                    ,'status' => true
                ]);
            } else {
                return response()->json([
                    'result_code' => '005'
                    ,'result_desc' => 'ออกจากระบบไม่สำเร็จ'
                    ,'message' => 'ออกจากระบบไม่สำเร็จ'
                    ,'status' => false
                ]);
            }
        } else {
            $data['result_code'] = '001';
            $data['result_desc'] = 'กรุณาลงชื่อเข้าใช้งาน';
            $data['message'] = "กรุณาลงชื่อเข้าใช้งาน";
            $data['status'] = false;
            return response()->json($data, 401);
        }
    }

    public function change_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ]);
        if (!$validator->fails()) {
            $user = Auth::guard('api')->user();
            if (Auth::guard('admin')->attempt(['email' => $user->email, 'password' => $request->current_password ])) {
                $input['password'] = bcrypt($request->new_password);
                if (AdminUser::where('id', $user->id)->update($input)) {
                    return response()->json([
                        'result_code' => '000'
                        ,'result_desc' => 'เปลี่ยนรหัสผ่านสำเร็จ','message'=>"เปลี่ยนรหัสผ่านสำเร็จ",'status'=>true
                    ], $this->successStatus);
                } else {
                    return response()->json([
                        'result_code' => '004'
                        ,'result_desc' => 'เปลี่ยนรหัสผ่านไม่สำเร็จ','message'=>"เปลี่ยนรหัสผ่านไม่สำเร็จ",'status'=>false
                    ], $this->successStatus);
                }
            } else {
                return response()->json([
                    'result_code' => '009'
                    ,'result_desc' => 'กรอกรหัสผ่านเดิมไม่ถูกต้อง','message'=>"กรอกรหัสผ่านเดิมไม่ถูกต้อง",'status'=>false
                ], $this->successStatus);
            }
        } else {
            return response()->json([
                'result_code' => '009'
                ,'result_desc' => 'รหัสผ่านไม่ถูกต้อง','message'=>"รหัสผ่านไม่ถูกต้อง",'status'=>false
            ], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = AdminUser::create($input);
        if ($user) {
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['name'] =  $user->name;
            $success['status'] = true;
            return response()->json(['ลงทะเบียนสำเร็จ'=>$success], $this-> successStatus);
        } else {
            $success['token'] =  '';
            $success['name'] =  '';
            $success['status'] = false;
            return response()->json(['ลงทะเบียนไม่สำเร็จ'=>$success], $this-> successStatus);
        }
    }

    public function details()
    {
        if (Auth::user()) {
            $data['success'] = Auth::user();
            $data['status'] = true;
        } else {
            $data['success'] = [];
            $data['status'] = false;
        }
        return response()->json(['success' => $data], $this-> successStatus);
    }

    public function personal_details()
    {
        $executionStartTime = microtime(true);

        $employee_id = Auth::user()->employee_id;
        $model = Employee::query();
        $model->leftjoin('branch', 'employee.branch_id', 'branch.id');
        $model->leftjoin('provinces', 'employee.province_id', 'provinces.id');
        $model->leftjoin('groups', 'employee.group_id', 'groups.id');
        $model->leftjoin('districts', 'employee.district_id', 'districts.id');
        $model->leftjoin('subdistricts', 'employee.subdistrict_id', 'subdistricts.id');
        $model->leftjoin('department', 'employee.department_id', 'department.id');
        $model->leftjoin('level', 'employee.level_id', 'level.id');
        $model->leftjoin('employee_status', 'employee.employee_status_id', 'employee_status.id');
        $model->leftjoin('employee_level', 'employee_level.id', 'employee.employee_level_id');
        $model->leftjoin('employee_leave_approvers', 'employee_leave_approvers.employee_id', 'employee.id');
        $model->select([
            // 'branch.*',
            'branch.branch_name as branch_name',
            'branch.id as branchid',
            'branch.latitude as branchlatitude',
            'branch.longitude as branchlongitude',
            'branch.distance as branchdistance',
            'branch.working_time_start',
            'branch.working_time_end',
            // 'provinces.*',
            'provinces.id as provincesid',
            // 'districts.*',
            'districts.id as districtsid',
            // 'subdistricts.*',
            'subdistricts.id as subdistrictsid',
            'groups.id as groupsid',
            'groups.name as groupsname',
            // 'department.*',
            'department.id as departmentid',
            'department.name as departmentname',
            // 'level.*',
            'level.id as levelid',
            'level.name as levelname',
            // 'employee_status.*',
            'employee.*',
            \DB::raw("'".url('')."'+employee_level.picture as employee_level_picture"),
            'employee_level.id as employee_level_id',
            \DB::raw("('".url('')."'+employee.picture_profile) as picture_profile"),
            'employee.id as employeeid',
            \DB::raw("
                CASE (SELECT COUNT(*) FROM employee_leave_approvers WHERE approvers LIKE '%\"".$employee_id."\"%')
                WHEN 0 THEN 'F'
                ELSE 'T'
                END as approver
            "),
            \DB::raw('employee_leave_approvers.approvers as leader_id')
        ]);
        $personal = $model->find($employee_id);

        $get_registration = $this->registration_type($employee_id, $personal); //fixing

        $data['registration_today'] = $get_registration['registration'];
        $data['registration_type'] = isset($get_registration['type'])?$get_registration['type']:'';
        $data['registration_period'] = isset($get_registration['period']) ? $get_registration['period'] : '';
        $data['date_used'] = isset($get_registration['date']) ? $get_registration['date'] : '';
        
        if (!empty($get_registration['registration'])) {
            $get_registration['registration']->in_time = isset($get_registration['registration']->in_time) ? str_replace('.0000000', '', $get_registration['registration']->in_time) : null;
            $get_registration['registration']->out_time = isset($get_registration['registration']->out_time) ? str_replace('.0000000', '', $get_registration['registration']->out_time) : null;
        } else {
            $get_registration['registration'] = null;
        }

        $personal->leader_id = "";
        if ($personal) {
            if ($e = Structurepivot::where('employee_id', $employee_id)->first()) {
                if ($find_leader = Structurepivot::where('box_id', $e->box_top_id)->first()) {
                    $personal->leader_id = $find_leader->employee_id;
                    $personal->leader_id = (Structurepivot::where('box_top_id', $e->box_id)->count()==0)?$personal->leader_id:$employee_id;
                }
            }
        }
        $ceo_id = \App\Http\Controllers\Admin\SettingController::show('ceo_id');
        $personal->ceo_id = (string)(($ceo_id) ? $ceo_id : '');
        $personal->is_projectmanager = (Employee::where('id', $employee_id)->whereIn('employee_level_id', [1,2,3])->count()>0); //ความสามารถในการสร้าง  project
        $data['personal'] = $personal;

        $leave_status = false;
        $ela = Employeeleaveapprovers::where('approvers', 'LIKE', '%"'.$employee_id.'"%')->get();
        $get_employee = array();

        if ($ela) {
            foreach ($ela as $key => $value) {
                $get_employee[] = $value->employee_id;
            }
            $leave_status = Employeeleave::where('leave_result', 'N')->whereIn('employee_id', $get_employee)->count();
        }

        $data['leave_status'] = ($leave_status>0) ? true : false;

        
        $obj_grade = new stdClass();

        // คำนวณเกรดแบบเก่า
        // $get_calculate_value = ET::calculate_result($employee_id);
        // $obj_grade->evaluation_type = $get_calculate_value['evaluation_type'];
        // $obj_grade->total_rate = $get_calculate_value['grade'];
        
        //คำนวณเกรดแบบใหม่
        $get_grade = ET::query_get_value($employee_id, null);
        $obj_grade->evaluation_type = $get_grade;
        $obj_grade->total_score = FC::total_score($get_grade);
        $obj_grade->total_rate = FC::grade($obj_grade->total_score);

        // $obj_grade->total_grade = FC::grade($obj_grade->total_rate);
        $obj_grade->has_under = false;

        $data['grade'] = $obj_grade;

        if ($box_id = Structurepivot::where('employee_id', $employee_id)->select('box_id')->first()) {
            $under_count = Structurepivot::where('box_top_id', $box_id['box_id'])->select('employee_id')->count();
            $obj_grade->has_under = $under_count!=0 ? true : false ;
        }
        
        if ($data['personal']!=null || $data['registration_today']!=null) {
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงข้อมูลผู้ใช้ระบบสำเร็จ';
            $data['message'] = 'ดึงข้อมูลผู้ใช้ระบบสำเร็จ';
            $data['status'] = true;
        } else {
            $data['personal'] = null;
            $data['registration_today'] = null;
            $data['result_code'] = '002';
            $data['result_desc'] = 'ไม่พบข้อมูล';
            $data['message'] = 'ไม่พบข้อมูล';
            $data['status'] = false;
        }

        $executionEndTime = microtime(true);
        $data['executetime'] = ROUND($executionEndTime - $executionStartTime, 3);

        return response()->json($data, $this-> successStatus);
    }

    public function get_working_time($employee_id)
    {
        $employee = Employee::find($employee_id);
        $working_time_start = '';
        $branch = Branch::find($employee->branch_id);
        if (isset($branch)) {
            $working_time_start = isset($branch->working_time_start) ? explode('.', $branch->working_time_start)[0] : '08:00:00';
        }
        return $working_time_start;
    }

    public function registration_list(Request $request)
    {
        $employee_id = Auth::user()->employee_id;

        $confirm_status = false;
        if (isset($request['month']) && isset($request['year'])) {
            $request['month'] = $request['year']."-".$request['month'];
            $request['employee_id'] = $employee_id;

            $year = $_GET['year'];
            $month = sprintf('%02d', $_GET['month']);
            $date_count = date_create($year."-".$month);
            $date_count = date_format($date_count, "t");
            $date_count = date_create($year."-".$month."-".$date_count);
            $date_count = date_format($date_count, "Y-m-d");
            $now = date("Y-m-d");
            $confirm_status = ($date_count<$now) ? true :false;
        } else {
            $request['month'] = date('Y-m');
            $request['employee_id'] = $employee_id;
            $date_count = date('Y-m-t');
            $now = date("Y-m-d");
            $confirm_status = ($date_count<$now) ? true :false;
        }
        $result = ReportController::employee_registration($request);
        // echo '<PRE>';
        // print_r($result);exit();
        if (count($result)!=0) {
            $check_data = array();
            foreach ($result as $key=>$value) {
                $value->out_time = ($value->out_time==null) ? "00:00:00" : $value->out_time;
                $value->hr = $value->hm;
                $check_data[] = $value;
            }
            $data['data']=$check_data;
            $data['confirm_status'] = $confirm_status;
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงรายการลงทะเบียนสำเร็จ';
            $data['message'] = 'ดึงรายการลงทะเบียนสำเร็จ';
            $data['status']=true;
        } else {
            $data['data']=[];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงรายการลงทะเบียนไม่สำเร็จ';
            $data['message'] = 'ดึงรายการลงทะเบียนไม่สำเร็จ';
            $data['status']=false;
        }
        return response()->json($data, $this->successStatus);
    }

    public function registration(Request $request)
    {
        try {
            // กรณีไม่ส่ง latitude longitude
            if (!isset($request->latitude) && !isset($request->longitude)) {
                $data['type'] = "work_time";
                $data['result_code'] = '006';
                $data['result_desc'] = 'DataException';
                $data['message'] = 'กรุณาตรวจสอบสิทธิ์การเข้าถึงตำแหน่งของคุณ';
                $data['status'] = false;
                return $data;
            }
            $data = [];
            $employee_id = Auth::user()->employee_id;
            $branch = Employee::where('employee.id', $employee_id)
            ->select([
                'employee.*'
                ,'branch.*'
            ])
            ->leftjoin('branch', 'branch.id', 'employee.branch_id')->first();
            $branch_id = $branch->branch_id;
            $lat1 = $request->latitude;
            $lon1 = $request->longitude;
            $lat2 = $branch->latitude;
            $lon2 = $branch->longitude;
            $calculate_distance = \App\Http\Controllers\FunctionController::calculate_distance($lat1, $lat2, $lon1, $lon2);
            $distance = empty($branch->distance) ? 50 : $branch->distance;
            $registration = $this->registration_type($employee_id, $branch);

            $registration_type = $registration['type'];
             
            if( ($calculate_distance > $distance) && empty($request->reason) ){
                //echo $calculate_distance;exit();
                
                $text_inorout = $registration_type == "o" ? "ออก" : "เข้า";
                $data['type'] = "work_time";
                $data['result_code'] = '006';
                $data['result_desc'] = "อยู่นอกระยะลงเวลา{$text_inorout}งาน ".$calculate_distance.'(ม.) กรุณาระบุเหตุผล';
                $data['message'] = "อยู่นอกระยะลงเวลา{$text_inorout}งาน ".$calculate_distance.'(ม.) กรุณาระบุเหตุผล';
                $data['status'] = false;
                return response()->json($data, $this->successStatus);
            }
            //else{
            //    echo '5';exit();
           // }

            // if( ($calculate_distance > $distance) && empty($request->reason) ){
            //     $data['type'] = "work_time";
            //     $data['result_code'] = '006';
            //     $data['result_desc'] = 'อยู่นอกระยะลงเวลาเข้างาน '.$calculate_distance.'(ม.) กรุณาระบุเหตุผล';
            //     $data['message'] = 'อยู่นอกระยะลงเวลาเข้างาน '.$calculate_distance.'(ม.) กรุณาระบุเหตุผล';
            //     $data['status'] = false;
            // }else{
            if ($registration_type=='o') {
                $input = [
                        // 'out_date' => date('Y-m-d'),
                        'out_date' => isset($registration['date']) ? $registration['date'] : date('Y-m-d'),
                        'out_time' => date('H:i:s'),
                        'out_latitude' => $request->latitude,
                        'out_longitude' => $request->longitude,
                        'out_reason' => $request->reason
                    ];
                $result = Employeeregistration::where([
                        ['employee_id',$employee_id]
                        ,['in_date',$registration['date']]
                    ])->update($input);
                if ($result) {
                    $data['type'] = "out_time";
                    $data['result_code'] = '000';
                    $data['result_desc'] = 'ลงเวลาออกงานสำเร็จ';
                    $data['message'] = 'ลงเวลาออกงานสำเร็จ';
                    $data['status'] = true;
                } else {
                    $data['type'] = "out_time";
                    $data['result_code'] = '004';
                    $data['result_desc'] = 'ลงเวลาออกงานไม่สำเร็จ';
                    $data['message'] = 'ลงเวลาออกงานไม่สำเร็จ';
                    $data['status'] = false;
                }
            } else {
                $input = [
                        'in_date' => isset($registration['date']) ? $registration['date'] : date('Y-m-d'),
                        'in_time' => date('H:i:s'),
                        'in_latitude' => $request->latitude,
                        'in_longitude' => $request->longitude,
                        'in_reason' => $request->reason,
                        'employee_id' => $employee_id,
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                // $input = [
                //     'in_date' => isset($registration['date']) ? $registration['date'] : date('Y-m-d'),
                //     'in_time' => '2021-01-25',
                //     'in_latitude' => $request->latitude,
                //     'in_longitude' => $request->longitude,
                //     'in_reason' => $request->reason,
                //     'employee_id' => $employee_id,
                //     'created_at' => '2021-01-25'
                // ];
                $result = Employeeregistration::insert($input);
                if ($result) {
                    $data['type'] = "work_time";
                    $data['result_code'] = '000';
                    $data['result_desc'] = 'ลงเวลาเข้างานสำเร็จ';
                    $data['message'] = 'ลงเวลาเข้างานสำเร็จ';
                    $data['status'] = true;
                } else {
                    $data['type'] = "work_time";
                    $data['result_code'] = '003';
                    $data['result_desc'] = 'ลงเวลาเข้างานไม่สำเร็จ';
                    $data['message'] = 'ลงเวลาเข้างานไม่สำเร็จ';
                    $data['status'] = false;
                }
            }
            // }
            return response()->json($data, $this->successStatus);
        } catch (\Throwable $th) {
            $data['type'] = "Exception";
            $data['result_code'] = '004';
            $data['result_desc'] = 'UpdateDataException';
            $data['message'] = 'พบข้อผิดพลาด => '.$th->getMessage();
            $data['exception'] = $th->getTrace();
            $data['status'] = false;
            return response()->json($data, $this->successStatus);
        }
    }

    public static function registration_type($employee_id, $branch)
    {
        $days['yesterday'] = date('Y-m-d', strtotime('-1 day'));
        $days['today'] = date('Y-m-d');
        $days['tomorrow'] = date('Y-m-d', strtotime('+1 day'));

        $data = [];
        foreach ($days as $key => $value) {
            $data[$key] = static::get_time_period($employee_id, $value, $branch);
        }

        $past_end = $data['yesterday']['end'];
        $present_start = $data['today']['start'];
        $present_end = $data['today']['end'];
        $future_start = $data['tomorrow']['start'];

        $period['less'] = FC::find_change_shift($past_end, $present_start);
        $period['then'] = FC::find_change_shift($present_end, $future_start);
        $data['period'] = $period;

        $now = date('Y-m-d H:i:s');

        $result = null;
        switch ($now) {
            case (strtotime($now)<strtotime($period['less'])):
                $result = $days['yesterday'];
                break;
            case (strtotime($now)>=strtotime($period['less']) && strtotime($now)<strtotime($period['then'])):
                $result = $days['today'];
                break;
            case (strtotime($now)>=strtotime($period['then'])):
                $result = $days['tomorrow'];
                break;
        }

        foreach ($days as $key => $value) {
            $data['days'][] = Employeeregistration::where([
                ['employee_id',$employee_id]
                ,['in_date',$result]
            ])
            ->orderBy('created_at', 'DESC')
            ->first();
        }

        $get_registration = Employeeregistration::where([
            ['employee_id',$employee_id]
            ,['in_date',$result]
        ])
        ->orderBy('created_at', 'DESC')
        ->first();
        $data['date'] = $result;
        $data['registration'] = $get_registration;
        $data['type'] = ($get_registration) ? 'o' : 'i';
        return $data;
    }

    public function get_employee_registration($date, $employee)
    {
        return $get_registration = Employeeregistration::where([
            ['employee_id',$employee_id]
            ,['in_date',$result]
        ])
        ->orderBy('created_at', 'DESC')
        ->first();
    }

    public static function get_time_period($employee_id, $date, $branch)
    {
        // $date = "2020-05-12""
        //{
        //     "yesterday": "2020-05-12",
        //     "today": "2020-05-13",
        //     "tomorrow": "2020-05-14"
        // };
        // return ($date);
        $shift = Shiftmanagement::where([
            ['shift_management.employee_id',$employee_id]
            ,['shift_management.date',$date]
        ])
        ->select([
            'shift_management.*'
            ,'shift.name'
            ,'shift.time_start'
            ,'shift.time_end'
        ])
        ->leftjoin('shift', 'shift.id', 'shift_management.shift_id')
        ->first();
        $return = [];
        if (!empty($shift)) { //ถ้ามีกะ
            if (strtotime($shift->time_end) < strtotime($shift->time_start)) {
                $start = date("Y-m-d H:i:s", strtotime("$date $shift->time_start"));
                $end = date("Y-m-d H:i:s", strtotime("$date $shift->time_end + 1 days"));
                $hr = abs(FC::DateTimeDiff($start, $end));
            } else {
                $start = "$date $shift->time_start";
                $end = "$date $shift->time_end";
                $hr = abs(FC::DateTimeDiff($start, $end));
            }
            $start =  str_replace('.0000000', '', $start);
            $return['start'] = $start;
            $return['end'] = date('Y-m-d H:i:s', strtotime("$start + $hr hours"));
        // $return['hr'] = $hr;
        } else {
            $return['start'] = str_replace('.0000000', '', "$date $branch->working_time_start");
            $return['end'] = str_replace('.0000000', '', "$date $branch->working_time_end");
        }
        return $return;
    }

    public function registration_destroy(Request $request)
    {
        $employee_id = Auth::user()->employee_id;
        $result = Employeeregistration::where([
            ['employee_id',$employee_id],
            ['in_date',date('Y-m-d')]
        ])->delete();

        if ($result) {
            $data['type'] = "registration_destroy";
            $data['message'] = 'ลบการลงเวลาทำงานสำเร็จ';
            $data['status'] = true;
        } else {
            $data['type'] = "registration_destroy";
            $data['message'] = 'ลบการลงเวลาทำงานไม่สำเร็จ';
            $data['status'] = false;
        }
        return response()->json($data, $this->successStatus);
    }

    public function news_list()
    {
        $data['data'] = News::select([
            'news.*'
            ,\DB::raw("'".asset('api/news_detail/')."' + '/' + CONVERT(varchar,news.id)  as url")
            ,\DB::raw('(select COUNT(DISTINCT nt.employee_id) from news_traffic nt where nt.news_id=news.id) as traffic_user')
            ,\DB::raw('(select COUNT(*) from news_traffic nt where nt.news_id=news.id) as traffic_total')
            ])->paginate(5);

        if (News::count()!=0) {
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงรายการข่าวสารสำเร็จ';
            $data['message'] = 'ดึงรายการข่าวสารสำเร็จ';
            $data['status'] = true;
        } else {
            $data['data'] = [];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงรายการข่าวสารไม่สำเร็จ';
            $data['message'] = 'ดึงรายการข่าวสารไม่สำเร็จ';
            $data['status'] = false;
        }
        foreach ($data['data'] as $key => $value) {
            $value->short_detail = strip_tags($value->detail);
            $value->short_detail = str_replace('&nbsp;', '', $value->short_detail);
            
            $path = null;
            $news_pic = (explode('/', $value->picture));
            if (count($news_pic) != 0) {
                $news_pic[count($news_pic)] = $news_pic[count($news_pic) - 1];
                $news_pic[count($news_pic) - 2] = "thumbs";
                $news_pic = implode('/', $news_pic);
                $path  = url($news_pic);
            } else {
                $path = "";
            }
            $value->picture_thumbs = $path;
            $value->picture = isset($value->picture) ? url($value->picture) : '';
            unset($value['detail']);
        }
        return response()->json($data, $this->successStatus);
    }

    public function news_detail($id)
    {
        $result = News::select([
            'news.*'
            ,\DB::raw('(select COUNT(DISTINCT nt.employee_id) from news_traffic nt where nt.news_id=news.id) as traffic_user')
            ,\DB::raw('(select COUNT(*) from news_traffic nt where nt.news_id=news.id) as traffic_total')
        ])->find($id);
        // $result->detail = strip_tags($result->detail);
        $result->detail = ($result->detail);
        $data['data'] = $result;
        if ($data['data']) {
            $user = Auth::user();
            $insert['news_id'] = $result->id;
            $insert['client_ip'] = $_SERVER['REMOTE_ADDR'];
            $insert['created_at'] = date('Y-m-d H:i:s');
            $insert['employee_id'] = $user->employee_id;
            Newstraffic::insert($insert);
            
            $data['data']->picture = url($data['data']->picture);
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงรายละเอียดข่าวสำเร็จ';
            $data['message'] = 'ดึงรายละเอียดข่าวสำเร็จ';
            $data['status'] = true;
        } else {
            $data['data'] = [];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงรายละเอียดข่าวไม่สำเร็จ';
            $data['message'] = 'ดึงรายละเอียดข่าวไม่สำเร็จ';
            $data['status'] = false;
        }
        return response()->json($data, $this->successStatus);
    }

    public function corporateinformation($id)
    {
        $data['data'] = Corporateinformation::first();
        if ($data['data']) {
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงข้อมูลองค์กรสำเร็จ';
            $data['message'] = 'ดึงข้อมูลองค์กรสำเร็จ';
            $data['status'] = true;
        } else {
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงข้อมูลองค์กรไม่สำเร็จ';
            $data['message'] = 'ดึงข้อมูลองค์กรไม่สำเร็จ';
            $data['status'] = false;
        }
        return response()->json($data, $this->successStatus);
    }

    public function jobdescription()
    {
        $employee_id = Auth::user()->employee_id;
        $result = Employee::leftjoin('level', 'level.id', 'employee.level_id')->select([
            'level.job_description'
            ,'employee.skill_description'
        ])->find($employee_id);
        $obj = new stdClass();
        $obj->job_description = ($result->skill_description!="" || $result->skill_description!=null) ? $result->skill_description : $result->job_description;
        $data['data'] = $obj;
        if ($data['data']) {
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงหน้าที่งานสำเร็จ';
            $data['message'] = 'ดึงหน้าที่งานสำเร็จ';
            $data['status'] = true;
        } else {
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงหน้าที่งานไม่สำเร็จ';
            $data['message'] = 'ดึงหน้าที่งานไม่สำเร็จ';
            $data['status'] = false;
        }
        return response()->json($data, $this->successStatus);
    }

    public function days_left()
    {
        $employee_id = Auth::user()->employee_id;
        $working_time_start = $this->get_working_time($employee_id);
        $model = Employeeregistration::query();
        $model->leftjoin('employee', 'employee_registration.employee_id', 'employee.id');
        if (env('DB_CONNECTION')=='sqlsrv') {
            $model->select([
                \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.in_date),9,10) as in_date"),
                \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.in_time),1,8) as in_time"),
                \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.out_time),1,8) as out_time"),
                \DB::raw("DATEDIFF(HOUR, employee_registration.in_time,employee_registration.out_time ) as hr"),
                \DB::raw("CASE WHEN DATEDIFF( SECOND, '".$working_time_start."',SUBSTRING(CONVERT(VARCHAR,employee_registration.in_time),1,8) ) > 0 THEN 'สาย' ELSE '' END AS status")
            ]);
        } else {
            $model->select([
                \DB::raw("substr(employee_registration.in_date,9,10) as in_date"),
                \DB::raw("substr(employee_registration.in_time,1,8) as in_time"),
                \DB::raw("substr(employee_registration.out_time,1,8) as out_time"),
                \DB::raw("substr(timediff(employee_registration.out_time,employee_registration.in_time),1,2) as hr"),
                \DB::raw("IF(TIME_TO_SEC(TIMEDIFF(SUBSTRING(employee_registration.in_time,1,8),'".$working_time_start."'))>'0' , 'สาย' , '') AS status"),
            ]);
        }
        if (!empty($_GET['month'])) {
            if ($_GET['month']>=1 && $_GET['month']<=12) {
                $model->where('employee_registration.in_date', 'like', date('Y').'-'.sprintf('%02d', $_GET['month']).'%');
            }
        }
        $result = $model->where('employee_registration.employee_id', $employee_id)->orderBy('employee_registration.in_date', 'asc')->get();
        if ($result) {
            $data['data']=$result;
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงข้อมูลการลาสำเร็จ';
            $data['message'] = 'ดึงข้อมูลการลาสำเร็จ';
            $data['status']=true;
        } else {
            $data['data']=[];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงข้อมูลการลาไม่สำเร็จ';
            $data['message'] = 'ดึงข้อมูลการลาไม่สำเร็จ';
            $data['status']=false;
        }
        return response()->json($data, $this-> successStatus);
    }

    public function report_registration_list($employee_id=null, $month=null)
    {
        $employee_id = $employee_id;
        $working_time_start = $this->get_working_time($employee_id);
        $model = Employeeregistration::query();
        $model->leftjoin('employee', 'employee_registration.employee_id', 'employee.id');
        $model->select([
            \DB::raw("CONVERT(varchar,employee_registration.in_date) as in_date_full"),
            \DB::raw("
                (CASE DATENAME(weekday, employee_registration.in_date)
                    WHEN 'Sunday' THEN 'อาทิตย์'
                    WHEN 'Monday' THEN 'จันทร์'
                    WHEN 'Tuesday' THEN 'อังคาร'
                    WHEN 'Wednesday' THEN 'พุธ'
                    WHEN 'Thursday' THEN 'พฤหัสบดี'
                    WHEN 'Friday' THEN 'ศุกร์'
                    WHEN 'Saturday' THEN 'เสาร์'
                    ELSE '-'
                END) as date_th
            "),
            \DB::raw("CONVERT(nvarchar, DATEPART(day, employee_registration.in_date)) as in_date"),
            \DB::raw("SUBSTRING(CONVERT(nvarchar, employee_registration.in_time),1,8) as in_time"),
            \DB::raw("SUBSTRING(CONVERT(nvarchar, employee_registration.out_time),1,8) as out_time"),
            \DB::raw("
            RIGHT('00' + 
                CONVERT(NVARCHAR,CASE WHEN (employee_registration.in_time < '12:00:00')
                THEN ROUND(CONVERT( INT , (DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time) - 60)/60 ),0)
                ELSE ROUND(CONVERT( INT , DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)/60 ),0)
                END)
            ,2) as hr
            "),
            \DB::raw("
            RIGHT('00' + 
                CONVERT(NVARCHAR,CASE WHEN employee_registration.in_time < '12:00:00' 
                THEN DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)-(ROUND(CONVERT( INT , DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)/60 ),0)*60)
                ELSE DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)-(ROUND(CONVERT( INT , DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)/60 ),0)*60)
                END)
            ,2) as m
            "),
            \DB::raw("
                CASE WHEN DATEDIFF( SECOND, '".$working_time_start."',employee_registration.in_time ) > 0 THEN 'สาย' ELSE '-' END AS status
            ")
        ]);
        if ($month!=null) {
            $model->where('employee_registration.in_date', 'like', '%'.$month.'%');
        } else {
            $model->where('employee_registration.in_date', 'like', '%'.date('Y-m').'%');
        }
        if ($result = $model->where('employee_registration.employee_id', $employee_id)->orderBy('employee_registration.in_date', 'asc')->get()) {
            $return = array();
            foreach ($result as $key => $value) {
                $value->in_time = ($value->in_time==null) ? '00:00:00' : $value->in_time;
                $value->out_time = ($value->out_time==null) ? '00:00:00' : $value->out_time;
                $value->hr = ($value->hr==null) ? '00' : $value->hr;
                $value->m = ($value->m==null) ? '00' : $value->m;
                $return[] = $value;
            }
            return $return;
        } else {
            return [];
        }
    }

    public function report_leave_list($employee_id=null, $month=null)
    {
        $month = (string)($month);
        $split_month = explode('-', $month);
        // $split_month = array
        // $split_month[0] = "2021";
        // $split_month[1] = "01";
        // print_r($split_month);exit();
        $result = Employeeleave::where('employee_leave.employee_id', $employee_id)
        ->leftjoin('leave_type', 'employee_leave.leave_type_id', 'leave_type.id')
        ->leftjoin('leave_duration', 'employee_leave.leave_duration_id', 'leave_duration.id')
        ->leftjoin(\DB::raw('employee ep1'), 'employee_leave.employee_id', 'ep1.id')
        ->leftjoin(\DB::raw('employee ep2'), 'employee_leave.approver_id', 'ep2.id');
       
        if (count($split_month)==1) {
            //bak
            //$result->where('employee_leave.start_date', 'LIKE', '%'.$month.'%');
            //new
            $result->where(function($q) use ($month){
                $q->where('employee_leave.start_date', 'LIKE', '%'.$month.'%')
                  ->orWhere('employee_leave.approved_at', 'LIKE', '%'.$month.'%');
            });
        } elseif (count($split_month)==2) {
            //bak
            // $result->where([
            //     [\DB::raw('DATEPART(yy, employee_leave.start_date)'),$split_month[0]]
            //     ,[\DB::raw('DATEPART(mm, employee_leave.start_date)'),$split_month[1]]
            // ]);
            //new
            $result->where(function($q) use ($split_month){
                $q->where([
                    [\DB::raw('DATEPART(yy, employee_leave.start_date)'),$split_month[0]]
                    ,[\DB::raw('DATEPART(mm, employee_leave.start_date)'),$split_month[1]]
                ])
                  ->orWhere([
                    [\DB::raw('DATEPART(yy, employee_leave.approved_at)'),$split_month[0]]
                    ,[\DB::raw('DATEPART(mm, employee_leave.approved_at)'),$split_month[1]]
                ]);
            });
           
        } else {
            //bak
            // $result->where('employee_leave.start_date', 'LIKE', '%'.date('Y-m').'%');
            //new
            $date_condition = date('Y-m');
            $result->where(function($q) use ($date_condition){
                $q->where('employee_leave.start_date', 'LIKE', '%'.$date_condition.'%')
                  ->orWhere('employee_leave.approved_at', 'LIKE', '%'.$date_condition.'%');
            });
        }
       
        $result->orderBy('employee_leave.created_at');
        $result->select([
            'employee_leave.id',
            'leave_type.leave_name as leave_name',
            'leave_duration.duration_name as duration_name',
            'employee_leave.leave_result',
            'employee_leave.remark',
            'employee_leave.picture as leave_picture',
            \DB::raw('employee_leave.start_date as start_date'),
            \DB::raw('employee_leave.end_date as end_date'),
            \DB::raw('employee_leave.approved_at as approved_at'),
            \DB::raw('employee_leave.created_at'),
            \DB::raw('ep2.firstname + \' \' + ep2.lastname as approver_name'),
            \DB::raw('ep1.firstname + \' \' + ep1.lastname as employee_name')
        ]);
         $return = $result->get();
        // $return = $result->toSql();
        // echo $return;exit();
        foreach ($return as $key => $value) {
            $return[$key]->leave_picture = \App\Http\Controllers\FunctionController::array_leave_picture($value->leave_picture);
        }
        return $return;
    }

    public function report_registration_leave(Request $request)
    {
        $employee_id = Auth::user()->employee_id;
        $request['employee_id'] = $employee_id;

        $unixtime = strtotime("now - 1 month");
        $unixtime = date("Y-m", $unixtime);

        $month = ($request->month) ? $request->month : (string)$unixtime;
        $request['month'] = $month;
        $result['month'] = $month;

        $registration = ReportController::employee_registration($request);

        $result['registration_list'] = [];

        foreach ($registration as $key => $item) {
            $obj = new stdClass();
            $obj->full_in_date = $item->full_in_date;
            $obj->in_date = $item->in_date;
            $obj->in_time = $item->in_time;
            $obj->out_time = $item->out_time;
            $obj->h = $item->hr;
            $obj->hr = $item->h;
            $obj->m = $item->m;
            $obj->hm = $item->hm;
            $obj->status = $item->status;
            $obj->dayname = $item->dayname;
            $obj->date_th = $item->date_th;
            $result['registration_list'][] = $obj;
        }
        $result['leave_list'] = $this->report_leave_list($employee_id, $month);
        $result['confirm_status'] = false;
        if ($month!=null) {
            $check_confirm = Employeeconfirm::where([
                ['employee_id',$employee_id]
                ,['month',$month]
            ])
            ->whereIn('status', ['T','F'])
            ->count();
            $result['confirm_status'] = ($check_confirm>0) ? true : false;
        }
        if (count($result['registration_list'])!=0 || count($result['leave_list'])!=0) {
            $data['data'] = $result;
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงข้อมูลการลาสำเร็จ';
            $data['message'] = 'ดึงข้อมูลการลาสำเร็จ';
            $data['status']=true;
        } else {
            $data['data']=[];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ไม่พบข้อมูล';
            $data['message'] = 'ไม่พบข้อมูล';
            $data['status']=false;
        }
        return response()->json($data, $this-> successStatus);
    }

    public function leave_employee()
    {
        $employee_id = Auth::user()->employee_id;
        //echo date('Y');exit();
        $result = $this->report_leave_list($employee_id, date('Y'));
       // echo '5';exit
        $history = array();
        foreach ($result as $key => $value) {
            $resultObj = new stdClass();
            $resultObj->id = $value->id;
            $resultObj->leave_name = $value->leave_name;
            $resultObj->duration_name = $value->duration_name;
            $resultObj->leave_result = $value->leave_result;
            $resultObj->remark = $value->remark;
            $resultObj->leave_picture = $value->leave_picture;
            $resultObj->start_date = $value->start_date;
            $resultObj->end_date = $value->end_date;
            $resultObj->approved_at = $value->approved_at;
            $resultObj->approver_name = $value->approver_name;
            $resultObj->employee_name = $value->employee_name;
        }

        if ($result) {
            $data['data'] = $result;
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงประวัติการลาสำเร็จ';
            $data['message'] = 'ดึงประวัติการลาสำเร็จ';
            $data['status'] = true;
        } else {
            $data['data'] = [];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงประวัติการลาไม่สำเร็จ';
            $data['message'] = 'ดึงประวัติการลาไม่สำเร็จ';
            $data['status'] = false;
        }
        return response()->json($data, $this->successStatus);
    }

    // public function leave_detail($id)
    // {
    //     $data['data'] = Employeeleave::select([
    //         'employee_leave.leave_type_id'
    //         ,\DB::raw("
    //             (SELECT
    //                 (lt.amount * 8 * 60)-(
    //                 SELECT
    //                     SUM(
    //                         CASE WHEN DATEDIFF(MINUTE, el.start_date , el.end_date)<=0
    //                         THEN
    //                                 (
    //                                         CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
    //                                         THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
    //                                         ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
    //                                         END
    //                                 )
    //                         ELSE 
    //                                 (
    //                                         CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
    //                                         THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
    //                                         ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
    //                                         END
    //                                 )
    //                                 +
    //                                 (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8 * 60 )
    //                                 +
    //                                 (
    //                                         CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
    //                                         THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
    //                                         ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
    //                                         END
    //                                 )
    //                         END
    //                     )
    //                 FROM employee_leave el
    //                 WHERE el.leave_type_id=lt.id 
    //                 AND el.employee_id = employee_leave.employee_id 
    //                 AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday') 
    //                 AND el.leave_result='T'
    //                 AND el.start_date <= employee_leave.start_date
    //                 )
    //             FROM leave_type lt WHERE lt.id=employee_leave.leave_type_id) as balance
    //         ")
    //         ,\DB::raw('CONVERT(DATE,employee_leave.start_date) as start_date')
    //         ,\DB::raw('CONVERT(DATE,employee_leave.end_date) as end_date')
    //         ,\DB::raw('CONVERT(TIME,employee_leave.start_date) as start_time')
    //         ,\DB::raw('CONVERT(TIME,employee_leave.end_date) as end_time')
    //         ,'employee_leave.remark'
    //         ,\DB::raw('e.firstname+\' \'+e.lastname as name')
    //         ,\DB::raw('l.name as position_name')
    //         ,\DB::raw('g.name as group_name')
    //         ,\DB::raw("'".url('/')."'+e.picture_profile as employee_img")
    //         ,\DB::raw("employee_leave.picture")
    //     ])
    //     ->leftjoin(\DB::raw('employee e'), 'e.id', 'employee_leave.employee_id')
    //     ->leftjoin(\DB::raw('level l'), 'l.id', 'e.level_id')
    //     ->leftjoin(\DB::raw('groups g'), 'g.id', 'e.group_id')
    //     ->find($id);

    //     $obj = new stdClass();
    //     $obj = $data['data'];
    //     $return = array();
    //     $pic = array();
    //     foreach (json_decode($data['data']->picture) as $key => $value) {
    //         $pic[] = url('photos/images_leave/'.$value);
    //     }
    //     $data['data']->picture = $pic;
        
    //     if ($data['data']) {
    //         $obj->balance_string = \App\Http\Controllers\FunctionController::convert_minute_day($data['data']->balance);
    //         $obj->balance = \App\Http\Controllers\FunctionController::convert_minute($data['data']->balance);
    //         $data['data'] = $obj;
    //         $data['result_code'] = '000';
    //         $data['result_desc'] = 'ดึงข้อมูลการลาสำเร็จ';
    //         $data['message'] = 'ดึงข้อมูลการลาสำเร็จ';
    //         $data['status'] = true;
    //     } else {
    //         $data['data'] = null;
    //         $data['result_code'] = '002';
    //         $data['result_desc'] = 'ดึงข้อมูลการลาไม่สำเร็จ';
    //         $data['message'] = 'ดึงข้อมูลการลาไม่สำเร็จ';
    //         $data['status'] = false;
    //     }
    //     return $data;
    // }

    public function leave_detail($id)
    {
        $data['data'] = Employeeleave::select([
            'employee_leave.leave_type_id',
            'employee_leave.employee_id'
            ,\DB::raw("
                (SELECT
                    (lt.amount * 8.50 * 60)-(
                    SELECT
                        SUM(
                            CASE WHEN DATEDIFF(MINUTE, el.start_date , el.end_date)<=0
                            THEN
                                    (
                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
                                            END
                                    )
                            ELSE 
                                    (
                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
                                            END
                                    )
                                    +
                                    (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8.50 * 60 )
                                    +
                                    (
                                            CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
                                            THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
                                            ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
                                            END
                                    )
                            END
                        )
                    FROM employee_leave el
                    WHERE el.leave_type_id=lt.id 
                    AND el.employee_id = employee_leave.employee_id 
                    AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday') 
                    AND el.leave_result='T'
                    AND el.start_date <= employee_leave.start_date
                    AND el.employee_id = {$id}
                    )
                FROM leave_type lt WHERE lt.id=employee_leave.leave_type_id) as balance
            ")
            ,\DB::raw('CONVERT(DATE,employee_leave.start_date) as start_date')
            ,\DB::raw('CONVERT(DATE,employee_leave.end_date) as end_date')
            ,\DB::raw('CONVERT(TIME,employee_leave.start_date) as start_time')
            ,\DB::raw('CONVERT(TIME,employee_leave.end_date) as end_time')
            ,'employee_leave.remark'
            ,\DB::raw('e.firstname+\' \'+e.lastname as name')
            ,\DB::raw('l.name as position_name')
            ,\DB::raw('g.name as group_name')
            ,\DB::raw("'".url('/')."'+e.picture_profile as employee_img")
            ,\DB::raw("employee_leave.picture")
        ])
        ->leftjoin(\DB::raw('employee e'), 'e.id', 'employee_leave.employee_id')
        ->leftjoin(\DB::raw('level l'), 'l.id', 'e.level_id')
        ->leftjoin(\DB::raw('groups g'), 'g.id', 'e.group_id')->find($id);
        //print_r($data);exit();
    
        $request = request();
        // dd(DB::getQueryLog());exit();
        $month = Carbon::make($data['data']->end_date)->endOfYear()->format('Y-m');

        $request->request->add(
            [
                "employee_id" => $data['data']->employee_id,
                "month" => $month,
            ]
        );
        $leaveUsedDay = collect(ReportController::leave_conclusion($request))->filter(function($leave) use ($data) {
            return $leave->id == $data['data']->leave_type_id;
        })->first();


        if ($leaveUsedDay->used_day == '-') {
            $data['data']->balance = $this->getTotalMinute($leaveUsedDay->amount_day);
        } else {
            $data['data']->balance = $this->getTotalMinute($leaveUsedDay->balance_day);
        }

        $obj = new stdClass();
        $obj = $data['data'];
        $return = array();
        $pic = array();
        if ($data['data']->picture) {
            foreach (json_decode($data['data']->picture) as $key => $value) {
                $pic[] = url('photos/images_leave/' . $value);
            }
        }
        $data['data']->picture = $pic;
        
        if ($data['data']) {
            $obj->balance_string = \App\Http\Controllers\FunctionController::convert_minute_day($data['data']->balance);
            $obj->balance = \App\Http\Controllers\FunctionController::convert_minute($data['data']->balance);
            $data['data'] = $obj;
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงข้อมูลการลาสำเร็จ';
            $data['message'] = 'ดึงข้อมูลการลาสำเร็จ';
            $data['status'] = true;
        } else {
            $data['data'] = null;
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงข้อมูลการลาไม่สำเร็จ';
            $data['message'] = 'ดึงข้อมูลการลาไม่สำเร็จ';
            $data['status'] = false;
        }
        return $data;
    }


    public function leave_approver($type)
    {
        
        $result = Employeeleave::where(\DB::raw('au.approvers'), 'LIKE', '%"'.\AUTH::user()->employee_id.'"%')
            ->leftjoin(\DB::raw('employee_leave_approvers au'), 'employee_leave.employee_id', 'au.employee_id')
            ->leftjoin('leave_type', 'employee_leave.leave_type_id', 'leave_type.id')
            ->leftjoin('leave_duration', 'employee_leave.leave_duration_id', 'leave_duration.id')
            ->leftjoin(\DB::raw('employee ep1'), 'employee_leave.employee_id', 'ep1.id')
            ->leftjoin(\DB::raw('employee ep2'), 'employee_leave.approver_id', 'ep2.id')
            ->leftjoin('groups', 'groups.id', \DB::raw('ep1.group_id'))
            ->leftjoin('level', 'level.id', \DB::raw('ep1.level_id'))
            ->leftjoin('employee_level', 'employee_level.id', \DB::raw('ep1.employee_level_id'));

        $result->select([
            'employee_leave.id',
            'employee_leave.employee_id',
            'employee_leave.leave_type_id',
            'leave_type.leave_name as leave_name',
            'leave_duration.duration_name as duration_name',
            'employee_leave.leave_result',
            'employee_leave.remark',
            'employee_leave.picture as leave_picture',
            'level.name as lname',
            'groups.name as gname',
            \DB::raw('employee_leave.start_date as start_date'),
            \DB::raw('employee_leave.end_date as end_date'),
            \DB::raw('employee_leave.approved_at as approved_at'),
            \DB::raw("'".url('/')."' + employee_level.picture as picture"),
            \DB::raw('ep2.firstname + \' \' + ep2.lastname as approver_name'),
            \DB::raw('ep1.firstname + \' \' + ep1.lastname as employee_name'),
            \DB::raw("'".url('/')."'+ep1.picture_profile as employee_img")
            ,\DB::raw("
                ISNULL((SELECT
                    (lt.amount * 8.50 * 60) - (
                    SELECT
                        SUM(
                            CASE WHEN DATEDIFF(MINUTE, el.start_date , el.end_date)<=0
                            THEN
                                    (
                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
                                            END
                                    )
                            ELSE 
                                    (
                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
                                            END
                                    )
                                    +
                                    (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8.50 * 60 )
                                    +
                                    (
                                            CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
                                            THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
                                            ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
                                            END
                                    )
                            END
                        )
                    FROM employee_leave el
                    WHERE el.leave_type_id=lt.id 
                    AND el.employee_id = employee_leave.employee_id 
                    AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday') 
                    AND el.leave_result='T'
                    AND el.start_date <= employee_leave.end_date
                    )
                FROM leave_type lt WHERE lt.id=employee_leave.leave_type_id
                ), 
                (SELECT (lt.amount * 8.50 * 60) FROM leave_type lt WHERE lt.id=employee_leave.leave_type_id)
                ) as balance
            ")
        ]);
        if (!empty($type)) {
            $condition = null;
            switch ($type) {
                case 'new':
                    $condition = 'N';
                    break;
                case 'approved':
                    $condition = 'T';
                    break;
                case 'not':
                    $condition = 'F';
                    break;
            }
            $result->where('employee_leave.leave_result', $condition);
        }
        $result = $result->get();

        if ($result) {
            $resultObj = new stdClass();
           
            foreach ($result as $key => $value) {
                $value->leave_picture = \App\Http\Controllers\FunctionController::array_leave_picture($value->leave_picture);

                $resultObj = $value;
                $resultObj->leave_name = ($resultObj->leave_name==null)? "" : $resultObj->leave_name;
                $resultObj->duration_name = ($resultObj->duration_name==null)? "" : $resultObj->duration_name;
                $resultObj->leave_result = ($resultObj->leave_result==null)? "" : $resultObj->leave_result;
                // $resultObj->leave_picture = ($resultObj->leave_picture==null) ? json_decode($resultObj->leave_picture) : [];
                $resultObj->remark = ($resultObj->remark==null)? "" : $resultObj->remark;
                $resultObj->lname = ($resultObj->lname==null)? "" : $resultObj->lname;
                $resultObj->gname = ($resultObj->gname==null)? "" : $resultObj->gname;
                $resultObj->start_date = ($resultObj->start_date==null)? "" : $resultObj->start_date;
                $resultObj->end_date = ($resultObj->end_date==null)? "" : $resultObj->end_date;
                $resultObj->approved_at = ($resultObj->approved_at==null)? "" : $resultObj->approved_at;
                $resultObj->picture = ($resultObj->picture==null)? "" : $resultObj->picture;
                $resultObj->approver_name = ($resultObj->approver_name==null)? "" : $resultObj->approver_name;
                $resultObj->employee_name = ($resultObj->employee_name==null)? "" : $resultObj->employee_name;
                $resultObj->employee_img = ($resultObj->employee_img==null)? "" : $resultObj->employee_img;

                $request = request();
                $month = Carbon::make($resultObj->end_date)->endOfYear()->format('Y-m');
                
                $request->request->add(
                    [
                        "employee_id" => $resultObj->employee_id,
                        "month" => $month,
                    ]
                );
                $leaveUsedDay = collect(ReportController::leave_conclusion($request))->filter(function($leave) use ($resultObj) {
                    return $leave->id == $resultObj->leave_type_id;
                })->first();


                if ($leaveUsedDay->used_day == '-') {
                    $resultObj->balance = $this->getTotalMinute($leaveUsedDay->amount_day);
                } else {
                    $resultObj->balance = $this->getTotalMinute($leaveUsedDay->balance_day);
                }
                $resultObj->balance_string = \App\Http\Controllers\FunctionController::convert_minute_day($resultObj->balance);
                $resultObj->balance = \App\Http\Controllers\FunctionController::convert_minute($resultObj->balance);
                $data['data'][] = $resultObj;
            }
           // echo '<PRE>';
           // print_r($data);exit();
            if (count($result)==0) {
                $data['data']=[];
            }
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงรายการขอลาสำเร็จ';
            $data['message'] = 'ดึงรายการขอลาสำเร็จ';
            $data['status'] = true;
        } else {
            $data['data'] = [];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ไม่มีข้อมูล';
            $data['message'] = 'ไม่มีข้อมูล';
            $data['status'] = false;
        }
        return response()->json($data, $this->successStatus);
    }
    // public function leave_approver($type)
    // {
    //     $result = Employeeleave::where(\DB::raw('au.approvers'), 'LIKE', '%"'.\AUTH::user()->employee_id.'"%')
    //         ->leftjoin(\DB::raw('employee_leave_approvers au'), 'employee_leave.employee_id', 'au.employee_id')
    //         ->leftjoin('leave_type', 'employee_leave.leave_type_id', 'leave_type.id')
    //         ->leftjoin('leave_duration', 'employee_leave.leave_duration_id', 'leave_duration.id')
    //         ->leftjoin(\DB::raw('employee ep1'), 'employee_leave.employee_id', 'ep1.id')
    //         ->leftjoin(\DB::raw('employee ep2'), 'employee_leave.approver_id', 'ep2.id')
    //         ->leftjoin('groups', 'groups.id', \DB::raw('ep1.group_id'))
    //         ->leftjoin('level', 'level.id', \DB::raw('ep1.level_id'))
    //         ->leftjoin('employee_level', 'employee_level.id', \DB::raw('ep1.employee_level_id'));

    //     $result->select([
    //         'employee_leave.id',
    //         'leave_type.leave_name as leave_name',
    //         'leave_duration.duration_name as duration_name',
    //         'employee_leave.leave_result',
    //         'employee_leave.remark',
    //         'employee_leave.picture as leave_picture',
    //         'level.name as lname',
    //         'groups.name as gname',
    //         \DB::raw('employee_leave.start_date as start_date'),
    //         \DB::raw('employee_leave.end_date as end_date'),
    //         \DB::raw('employee_leave.approved_at as approved_at'),
    //         \DB::raw("'".url('/')."' + employee_level.picture as picture"),
    //         \DB::raw('ep2.firstname + \' \' + ep2.lastname as approver_name'),
    //         \DB::raw('ep1.firstname + \' \' + ep1.lastname as employee_name'),
    //         \DB::raw("'".url('/')."'+ep1.picture_profile as employee_img")
    //         ,\DB::raw("
    //             ISNULL((SELECT
    //                 (lt.amount * 8 * 60) - (
    //                 SELECT
    //                     SUM(
    //                         CASE WHEN DATEDIFF(MINUTE, el.start_date , el.end_date)<=0
    //                         THEN
    //                                 (
    //                                         CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
    //                                         THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
    //                                         ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
    //                                         END
    //                                 )
    //                         ELSE 
    //                                 (
    //                                         CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
    //                                         THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
    //                                         ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
    //                                         END
    //                                 )
    //                                 +
    //                                 (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8 * 60 )
    //                                 +
    //                                 (
    //                                         CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
    //                                         THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
    //                                         ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
    //                                         END
    //                                 )
    //                         END
    //                     )
    //                 FROM employee_leave el
    //                 WHERE el.leave_type_id=lt.id 
    //                 AND el.employee_id = employee_leave.employee_id 
    //                 AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday') 
    //                 AND el.leave_result='T'
    //                 AND el.start_date <= employee_leave.end_date
    //                 )
    //             FROM leave_type lt WHERE lt.id=employee_leave.leave_type_id
    //             ), 
    //             (SELECT (lt.amount * 8 * 60) FROM leave_type lt WHERE lt.id=employee_leave.leave_type_id)
    //             ) as balance
    //         ")
    //     ]);
    //     if (!empty($type)) {
    //         $condition = null;
    //         switch ($type) {
    //             case 'new':
    //                 $condition = 'N';
    //                 break;
    //             case 'approved':
    //                 $condition = 'T';
    //                 break;
    //             case 'not':
    //                 $condition = 'F';
    //                 break;
    //         }
    //         $result->where('employee_leave.leave_result', $condition);
    //     }
      
    //     $result = $result->orderBy('employee_leave.id', 'DESC')->get();
        
    //     if ($result) {
    //         $resultObj = new stdClass();
    //         foreach ($result as $key => $value) {
    //             $value->leave_picture = \App\Http\Controllers\FunctionController::array_leave_picture($value->leave_picture);

    //             $resultObj = $value;
    //             $resultObj->leave_name = ($resultObj->leave_name==null)? "" : $resultObj->leave_name;
    //             $resultObj->duration_name = ($resultObj->duration_name==null)? "" : $resultObj->duration_name;
    //             $resultObj->leave_result = ($resultObj->leave_result==null)? "" : $resultObj->leave_result;
    //             // $resultObj->leave_picture = ($resultObj->leave_picture==null) ? json_decode($resultObj->leave_picture) : [];
    //             $resultObj->remark = ($resultObj->remark==null)? "" : $resultObj->remark;
    //             $resultObj->lname = ($resultObj->lname==null)? "" : $resultObj->lname;
    //             $resultObj->gname = ($resultObj->gname==null)? "" : $resultObj->gname;
    //             $resultObj->start_date = ($resultObj->start_date==null)? "" : $resultObj->start_date;
    //             $resultObj->end_date = ($resultObj->end_date==null)? "" : $resultObj->end_date;
    //             $resultObj->approved_at = ($resultObj->approved_at==null)? "" : $resultObj->approved_at;
    //             $resultObj->picture = ($resultObj->picture==null)? "" : $resultObj->picture;
    //             $resultObj->approver_name = ($resultObj->approver_name==null)? "" : $resultObj->approver_name;
    //             $resultObj->employee_name = ($resultObj->employee_name==null)? "" : $resultObj->employee_name;
    //             $resultObj->employee_img = ($resultObj->employee_img==null)? "" : $resultObj->employee_img;
                
    //             $resultObj->balance_string = \App\Http\Controllers\FunctionController::convert_minute_day($resultObj->balance);
    //             $resultObj->balance = \App\Http\Controllers\FunctionController::convert_minute($resultObj->balance);
    //             $data['data'][] = $resultObj;
    //         }
    //         if (count($result)==0) {
    //             $data['data']=[];
    //         }
    //         $data['result_code'] = '000';
    //         $data['result_desc'] = 'ดึงรายการขอลาสำเร็จ';
    //         $data['message'] = 'ดึงรายการขอลาสำเร็จ';
    //         $data['status'] = true;
    //     } else {
    //         $data['data'] = [];
    //         $data['result_code'] = '002';
    //         $data['result_desc'] = 'ไม่มีข้อมูล';
    //         $data['message'] = 'ไม่มีข้อมูล';
    //         $data['status'] = false;
    //     }
    //     return response()->json($data, $this->successStatus);
    // }

   
    protected function getTotalMinute($value): int
    {
       
        $explodeDay = explode('วัน', $value);
        $day = 0;
        if (count($explodeDay) > 1) {
            $day = (int)head($explodeDay);
        }
        $explodeHour = explode('ชั่วโมง', last($explodeDay));
      
        $hour = 0;
        if (count($explodeHour) > 1) {
            $hour = (int)head($explodeHour);
        }
        $explodeMinute = explode('นาที', last($explodeHour));
        $minute = 0;
        if (count($explodeMinute) > 1) {
            $minute = (int)head($explodeMinute);
        }
        
        //echo ($day * 9 * 60);exit();
        //echo ($day * 8 * 60) + ($hour * 60) + $minute;exit();
        
        return ($day * 8.50 * 60) + ($hour * 60) + $minute;
    }
//     public function leave_report()
//     {
//         $userid = AUTH::user()->employee_id;
//         request()->request->add(
//             [
//                 'month' => now()->endOfYear()->format('Y-m'),
//                 'employee_id' => $userid,
//             ]
//         );

//         $data = ReportController::leave_conclusion(request());

//         foreach ($data as $key => $value) {
//             $amount = $this->getTotalMinute($value->amount_day);
//             $leaved = $this->getTotalMinute($value->used_day);
//             $balance = $this->getTotalMinute($value->balance_day);

//             $value->amount = $amount;
//             $value->leaved = $leaved;
//             $value->balance = $balance;
//             $value->leave_detail = isset($value->detail) ? $value->detail : "";
//         }
// //        $data = \DB::select("
// //        SELECT
// //            lt.leave_name,lt.detail as leave_detail,(lt.amount * 8 * 60) as amount
// //            ,(
// //            SELECT
// //                    SUM(
// //                            CASE WHEN DATEDIFF(DAY, el.start_date , el.end_date)<=0
// //                            THEN
// //                                    (
// //                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
// //                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
// //                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
// //                                            END
// //                                    )
// //                            ELSE
// //                                    (
// //                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
// //                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
// //                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
// //                                            END
// //                                    )
// //                                    +
// //                                    (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8 * 60 )
// //                                    +
// //                                    (
// //                                            CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
// //                                            THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
// //                                            ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
// //                                            END
// //                                    )
// //                            END
// //                    )
// //            FROM employee_leave el
// //            WHERE el.leave_type_id=lt.id AND el.employee_id = $userid AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday') AND el.leave_result='T'
// //            ) as leaved
// //            ,(lt.amount * 8 * 60)-(
// //                SELECT
// //                        SUM(
// //                                CASE WHEN DATEDIFF(DAY, el.start_date , el.end_date)<=0
// //                                THEN
// //                                        (
// //                                                CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
// //                                                THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
// //                                                ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
// //                                                END
// //                                        )
// //                                ELSE
// //                                        (
// //                                                CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
// //                                                THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
// //                                                ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
// //                                                END
// //                                        )
// //                                        +
// //                                        (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8 * 60 )
// //                                        +
// //                                        (
// //                                                CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
// //                                                THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
// //                                                ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
// //                                                END
// //                                        )
// //                                END
// //                        )
// //                FROM employee_leave el
// //                WHERE el.leave_type_id=lt.id AND el.employee_id = $userid AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday') AND el.leave_result='T'
// //                ) as balance
// //        FROM leave_type lt
// //        GROUP BY lt.id,lt.leave_name,lt.amount,lt.detail;
// //        ");

//         $check_data = array();
//         foreach ($data as $key => $value) {
//             $value->amount = $value->amount==null ? 0 : $value->amount;
//             $value->leaved = $value->leaved==null ? 0 : $value->leaved;
//             $value->balance = $value->balance==null ? 0 : $value->balance;
//             $check_data[] = $value;
//         }
//         $data = $check_data;
//         foreach ($data as $key => $value) {
//             $new_value = new stdClass();
//             $new_value->leave_name = $value->leave_name;
//             $new_value->leave_detail = empty($value->leave_detail) ? '' : $value->leave_detail;
//             // $new_value->amount = !empty($value->amount) ? \App\Http\Controllers\FunctionController::convert_minute(intval($value->amount)) : null;
//             // $new_value->leaved = !empty($value->leaved) ? \App\Http\Controllers\FunctionController::convert_minute(intval($value->leaved)) : null;
//             // $new_value->balance = !empty($value->balance) ? \App\Http\Controllers\FunctionController::convert_minute(intval($value->balance)) : \App\Http\Controllers\FunctionController::convert_minute(intval($value->amount));
//             $new_value->amount = \App\Http\Controllers\FunctionController::convert_minute(intval($value->amount));
//             $new_value->leaved = \App\Http\Controllers\FunctionController::convert_minute(intval($value->leaved));
//             $new_value->balance = \App\Http\Controllers\FunctionController::convert_minute(intval($value->balance));
//             $result['data'][$key] = $new_value;
//         }
//         if ($data) {
//             $result['result_code'] = '000';
//             $result['result_desc'] = 'ดึงรายงานการลาสำเร็จ';
//             $result['message'] = 'ดึงรายงานการลาสำเร็จ';
//             $result['status'] = true;
//         } else {
//             $result['result_code'] = '002';
//             $result['result_desc'] = 'ดึงรายงานการลาไม่สำเร็จ';
//             $result['message'] = 'ดึงรายงานการลาไม่สำเร็จ';
//             $result['status'] = false;
//         }
//         return response()->json($result, $this->successStatus);
//     }
public function leave_report()
    {
        $userid = AUTH::user()->employee_id;
        request()->request->add(
            [
                'month' => now()->endOfYear()->format('Y-m'),
                'employee_id' => $userid,
            ]
        );

        $data = ReportController::leave_conclusion(request());
        
        foreach ($data as $key => $value) {
            $amount = $this->getTotalMinute($value->amount_day);
            $leaved = $this->getTotalMinute($value->used_day);
            $balance = $this->getTotalMinute($value->balance_day);
          
            $value->amount = $amount;
            $value->leaved = $leaved;
            $value->balance = $balance;
            $value->leave_detail = $value->detail;
        }
        
//        $data = \DB::select("
//        SELECT
//            lt.leave_name,lt.detail as leave_detail,(lt.amount * 8 * 60) as amount
//            ,(
//            SELECT
//                    SUM(
//                            CASE WHEN DATEDIFF(DAY, el.start_date , el.end_date)<=0
//                            THEN
//                                    (
//                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
//                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
//                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
//                                            END
//                                    )
//                            ELSE
//                                    (
//                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
//                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
//                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
//                                            END
//                                    )
//                                    +
//                                    (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8 * 60 )
//                                    +
//                                    (
//                                            CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
//                                            THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
//                                            ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
//                                            END
//                                    )
//                            END
//                    )
//            FROM employee_leave el
//            WHERE el.leave_type_id=lt.id AND el.employee_id = $userid AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday') AND el.leave_result='T'
//            ) as leaved
//            ,(lt.amount * 8 * 60)-(
//                SELECT
//                        SUM(
//                                CASE WHEN DATEDIFF(DAY, el.start_date , el.end_date)<=0
//                                THEN
//                                        (
//                                                CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
//                                                THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
//                                                ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
//                                                END
//                                        )
//                                ELSE
//                                        (
//                                                CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
//                                                THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
//                                                ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
//                                                END
//                                        )
//                                        +
//                                        (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8 * 60 )
//                                        +
//                                        (
//                                                CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
//                                                THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
//                                                ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
//                                                END
//                                        )
//                                END
//                        )
//                FROM employee_leave el
//                WHERE el.leave_type_id=lt.id AND el.employee_id = $userid AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday') AND el.leave_result='T'
//                ) as balance
//        FROM leave_type lt
//        GROUP BY lt.id,lt.leave_name,lt.amount,lt.detail;
//        ");

        $check_data = array();
        foreach ($data as $key => $value) {
            $value->amount = $value->amount==null ? 0 : $value->amount;
            $value->leaved = $value->leaved==null ? 0 : $value->leaved;
            $value->balance = $value->balance==null ? 0 : $value->balance;
            $check_data[] = $value;
        }
        $data = $check_data;
        foreach ($data as $key => $value) {
            $new_value = new stdClass();
            $new_value->leave_name = $value->leave_name;
            $new_value->leave_detail = empty($value->leave_detail) ? '' : $value->leave_detail;
            // $new_value->amount = !empty($value->amount) ? \App\Http\Controllers\FunctionController::convert_minute(intval($value->amount)) : null;
            // $new_value->leaved = !empty($value->leaved) ? \App\Http\Controllers\FunctionController::convert_minute(intval($value->leaved)) : null;
            // $new_value->balance = !empty($value->balance) ? \App\Http\Controllers\FunctionController::convert_minute(intval($value->balance)) : \App\Http\Controllers\FunctionController::convert_minute(intval($value->amount));
            $new_value->amount = \App\Http\Controllers\FunctionController::convert_minute(intval($value->amount));
            $new_value->leaved = \App\Http\Controllers\FunctionController::convert_minute(intval($value->leaved));
            $new_value->balance = \App\Http\Controllers\FunctionController::convert_minute(intval($value->balance));
            $result['data'][$key] = $new_value;
        }
      
        if ($data) {
            $result['result_code'] = '000';
            $result['result_desc'] = 'ดึงรายงานการลาสำเร็จ';
            $result['message'] = 'ดึงรายงานการลาสำเร็จ';
            $result['status'] = true;
        } else {
            $result['result_code'] = '002';
            $result['result_desc'] = 'ดึงรายงานการลาไม่สำเร็จ';
            $result['message'] = 'ดึงรายงานการลาไม่สำเร็จ';
            $result['status'] = false;
        }
        return response()->json($result, $this->successStatus);
    }
    
    public function conclusion_monthly(Request $request)
    {
        $month = $request->month;
        $year = !empty($request->year) ? $request->year : date('Y');
        $month = sprintf('%02d', $month);
        $date_count = date_create($year."-".$month);
        $date_count = date_format($date_count, "t");

        $time_start = microtime(true);
        $userid = AUTH::user()->employee_id;
        $working_time_start = $this->get_working_time($userid);
        $query = \DB::select("
            SELECT SUBSTRING(CONVERT(NVARCHAR,er.in_date),1,7) AS in_date
            , COUNT(DISTINCT er.in_date) as count_in
            , COUNT(CASE WHEN DATEDIFF( SECOND, '".$working_time_start."',er.in_time ) > 0 THEN er.in_time ELSE null END) as count_late
            FROM employee_registration er 
            WHERE er.employee_id=$userid 
                AND DATENAME(weekday, er.in_date) NOT IN (N'Saturday', N'Sunday')
                AND er.in_date NOT IN (SELECT h.holiday_date FROM holiday h)
                AND er.in_date BETWEEN '".$year."-$month-01' AND '".$year."-$month-$date_count'
            GROUP BY SUBSTRING(CONVERT(NVARCHAR,er.in_date),1,7);
        ");

        $leave_list = Employeeleave::select([
            'leave_type.leave_name','employee_leave.start_date','employee_leave.end_date'
            ,\DB::raw("
            (
                CASE WHEN DATEDIFF(MINUTE, employee_leave.start_date , employee_leave.end_date)<=0
                THEN
                    (
                        CASE WHEN (employee_leave.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,employee_leave.start_date))+' '+'12:00:00') AND employee_leave.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,employee_leave.start_date))+' '+'13:00:00'))
                        THEN (DATEDIFF(MINUTE, CONVERT(datetime,employee_leave.start_date) , CONVERT(datetime,employee_leave.end_date) ) - 60)
                        ELSE (DATEDIFF(MINUTE, CONVERT(datetime,employee_leave.start_date) , CONVERT(datetime,employee_leave.end_date) ))
                        END
                    )
                ELSE 
                    (
                        CASE WHEN (employee_leave.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,employee_leave.start_date))+' '+'12:00:00'))
                        THEN (DATEDIFF(MINUTE, CONVERT(datetime,employee_leave.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,employee_leave.start_date))+' '+'17:00:00') ) - 60)
                        ELSE (DATEDIFF(MINUTE, CONVERT(datetime,employee_leave.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,employee_leave.start_date))+' '+'17:00:00') ))
                        END
                    )
                    +
                    (DATEDIFF(DAY, CONVERT(datetime,employee_leave.start_date) , CONVERT(datetime,employee_leave.end_date) ) * 8.50 * 60 )
                    +
                    (
                        CASE WHEN (employee_leave.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,employee_leave.end_date))+' '+'12:00:00'))
                        THEN DATEDIFF(MINUTE, CONVERT(datetime,employee_leave.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,employee_leave.end_date))+' '+'17:00:00') ) - 60
                        ELSE DATEDIFF(MINUTE, CONVERT(datetime,employee_leave.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,employee_leave.end_date))+' '+'17:00:00') )
                        END
                    )
                END
            ) as leaved
            ")
        ])
        ->leftjoin('leave_type', 'leave_type.id', 'employee_leave.leave_type_id')
        ->where([
            ['employee_id',$userid]
            ,['employee_leave.leave_result','T']
            // ,['']
        ])
        ->whereBetween('employee_leave.start_date', [$year.'-'.$month.'-01' , $year.'-'.$month.'-'.$date_count])
        ->get();

        if (!empty($leave_list)) {
            foreach ($leave_list as $key => $item) {
                $obj = new stdClass();
                $obj->leave_name = $item->leave_name;
                $obj->start_date = $item->start_date;
                $obj->end_date = $item->end_date;
                $obj->leaved = !empty($item->leaved) ? \App\Http\Controllers\FunctionController::convert_minute(intval($item->leaved)) : 0;
                $result['leave_list'][] = $obj;
            }
        } else {
            $result['leave_list'] = [];
        }

        $obj = new stdClass();
        $date = date_create($year."-".sprintf('%02d', $month));
        $obj->month = date_format($date, "Y-m");

        if (!empty($query)) {
            foreach ($query as $key => $item) {
                if ($item->in_date==date_format($date, "Y-m")) {
                    $obj->count_in = !empty($item->count_in) ? $item->count_in : null;
                    $obj->count_late = !empty($item->count_late) ? $item->count_late : null;
                }
            }
        } else {
            $obj->count_in = null;
            $obj->count_late = null;
        }

        $day = 0;
        for ($i=1;$i<=$date_count;$i++) {
            $d = date_create($year."-".sprintf('%02d', $month)."-".sprintf('%02d', $i));
            if (date_format($d, "D")=="Sat" || date_format($d, "D")=="Sun") {
                $day += 1;
            }
        }
        $start = $year."-$month-01";
        $end = $year."-$month-$date_count";
        $holiday_count = \DB::select("select count(*) as count_holiday from holiday where holiday_date between '$start' and '$end'");
        foreach ($holiday_count as $key => $item) {
            $holiday_count = $item->count_holiday;
        }
        $obj->today = (string)(date_format($date, "t") - $day - $holiday_count);
        $result['time_attendance'] = $obj;

        if ($query) {
            $result['result_code'] = '000';
            $result['result_desc'] = 'ดึงสรุปรายเดือนสำเร็จ';
            $result['message'] = 'ดึงสรุปรายเดือนสำเร็จ';
            $result['status'] = true;
        } else {
            $result['result_code'] = '002';
            $result['result_desc'] = 'ดึงสรุปรายเดือนไม่สำเร็จ';
            $result['message'] = 'ดึงสรุปรายเดือนไม่สำเร็จ';
            $result['status'] = false;
        }
        $confirm = Employeeconfirm::where([
            ['employee_id',$userid]
            ,['month',$year."-".$month]
        ])->get();
        $result['confirm'] = count($confirm)==0 ? false : true;
        $time_end = microtime(true);
        $result['excute_time'] = $time_end - $time_start;
        
        return $result;
    }

    public function confirm(Request $request)
    {
        $month = $request->month;
        $year = !empty($request->year) ? $request->year : date('Y');
        $month = sprintf('%02d', $month);
        $date_count = date_create(date('Y')."-".$month);
        $date_count = date_format($date_count, "t");
        $time_start = microtime(true);
        $userid = AUTH::user()->employee_id;
        $working_time_start = $this->get_working_time($userid);
        // start time attendance
        $query = \DB::select("
            SELECT SUBSTRING(CONVERT(NVARCHAR,er.in_date),1,7) AS in_date
            , COUNT(DISTINCT er.in_date) as count_in
            , COUNT(CASE WHEN DATEDIFF( SECOND, '".$working_time_start."',er.in_time ) > 0 THEN er.in_time ELSE null END) as count_late
            FROM employee_registration er
            WHERE er.employee_id=$userid
                AND DATENAME(weekday, er.in_date) NOT IN (N'Saturday', N'Sunday')
                AND er.in_date NOT IN (SELECT h.holiday_date FROM holiday h)
                AND er.in_date BETWEEN '$year-$month-01' AND '$year-$month-$date_count'
            GROUP BY SUBSTRING(CONVERT(NVARCHAR,er.in_date),1,7);
        ");
        $obj = new stdClass();
        $date = date_create($year."-".sprintf('%02d', $month));
        $obj->month = date_format($date, "Y-m");
        $day = 0;
        for ($i=1;$i<=$date_count;$i++) {
            $d = date_create($year."-".sprintf('%02d', $month)."-".sprintf('%02d', $i));
            if (date_format($d, "D")=="Sat" || date_format($d, "D")=="Sun") {
                $day += 1;
            }
        }
        $start = $year."-$month-01";
        $end = $year."-$month-$date_count";
        $holiday_count = \DB::select("select count(*) as count_holiday from holiday where holiday_date between '$start' and '$end'");
        foreach ($holiday_count as $key => $item) {
            $holiday_count = $item->count_holiday;
        }
        $obj->total_day = (int)(date_format($date, "t") - $day - $holiday_count);
        if (!empty($query)) {
            foreach ($query as $key => $item) {
                if ($item->in_date==date_format($date, "Y-m")) {
                    $obj->work_day = (int)(!empty($item->count_in) ? $item->count_in : null);
                    $obj->late_day = (int)(!empty($item->count_late) ? $item->count_late : null);
                    $obj->absence_day = (int)(!empty($obj->total_day) && !empty($item->count_in) ? $obj->total_day - $item->count_in : 0);
                }
            }
        }
        $obj->status = $request->status;
        $obj->created_at = date('Y-m-d H:i:s');

        foreach ($obj as $key=>$value) {
            $time_attendance[$key] = $value;
        }
        $time_attendance['employee_id'] = $userid;

        $confirm = Employeeconfirm::where([
            ['employee_id',$userid]
            ,['month',$year."-".$month]
        ]);

        if (($confirm->count())==0) {
            // insert employee_confirm
            $employee_confirm_id = Employeeconfirm::insertGetId($time_attendance);
        } else {
            // update employee_confirm
            $employee_confirm_id = $confirm->first()->id;
            unset($time_attendance['id']);
            Employeeconfirm::where('id', $employee_confirm_id)->update($time_attendance);
            Employeeconfirmregistration::where('employee_confirm_id', $employee_confirm_id)->delete();
            Employeeconfirmleave::where('employee_confirm_id', $employee_confirm_id)->delete();
        }

        $registration = \DB::select("
                SELECT 
                $employee_confirm_id AS employee_confirm_id
                ,SUBSTRING(CONVERT(NVARCHAR,er.in_date),1,10)+' '+SUBSTRING(CONVERT(NVARCHAR,er.in_time),1,8) AS in_datetime
                ,SUBSTRING(CONVERT(NVARCHAR,er.out_date),1,10)+' '+SUBSTRING(CONVERT(NVARCHAR,er.out_time),1,8) AS out_datetime
                FROM employee_registration er
                WHERE er.employee_id=$userid
                    AND DATENAME(weekday, er.in_date) NOT IN (N'Saturday', N'Sunday')
                    AND er.in_date NOT IN (SELECT h.holiday_date FROM holiday h)
                    AND er.in_date BETWEEN '".$year."-$month-01' AND '".$year."-$month-$date_count'
                ORDER BY er.in_date;
            ");
        if (COUNT($registration)!=0) {
            foreach ($registration as $key=>$item) {
                $registration_insert[] = array(
                        'employee_confirm_id' => $item->employee_confirm_id
                        ,'in_datetime' => isset($item->in_datetime) ? $item->in_datetime : null
                        ,'out_datetime' => isset($item->out_datetime) ? $item->out_datetime : null
                    );
            }
            $insert = Employeeconfirmregistration::insert($registration_insert);
        }
    
        $leave_report = \DB::select("
                SELECT 
                    $employee_confirm_id AS employee_confirm_id
                    ,lt.id AS leave_type_id
                    ,lt.amount
                    ,(
                    SELECT
                        SUM(
                            CASE WHEN DATEDIFF(MINUTE, el.start_date , el.end_date)<=0
                            THEN
                                (
                                    CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
                                    THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
                                    ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
                                    END
                                )
                            ELSE 
                                (
                                    CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
                                    THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
                                    ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
                                    END
                                )
                                +
                                (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8.50 * 60 )
                                +
                                (
                                    CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
                                    THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
                                    ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
                                    END
                                )
                            END
                        )
                    FROM employee_leave el
                    WHERE el.leave_type_id=lt.id AND el.employee_id = $userid AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday')
                    AND el.start_date BETWEEN '".$year."-$month-01' AND '".$year."-$month-$date_count' AND el.leave_result = 'T'
                    ) as leaved_minute
                FROM leave_type lt
                GROUP BY lt.id,lt.leave_name,lt.amount;
            ");
            
        if (!empty($leave_report)) {
            foreach ($leave_report as $key=>$item) {
                $leave_report_insert[] = array(
                        'employee_confirm_id'=>$item->employee_confirm_id
                        ,'leave_type_id'=>$item->leave_type_id
                        ,'leaved_minute'=>$item->leaved_minute
                        ,'balance_minute'=> ((isset($item->amount)?$item->amount:0) * (8.50) * 60)-($item->leaved_minute)
                        ,'created_at'=>date('Y-m-d H:i:s')
                    );
            }
            $insert = Employeeconfirmleave::insert($leave_report_insert);
        }

        if ($insert) {
            $result['result_code'] = '000';
            $result['result_desc'] = 'ยืนยันการลงเวลาสำเร็จ';
            $result['message'] = 'ยืนยันการลงเวลาสำเร็จ';
            $result['status'] = true;
        } else {
            $result['result_code'] = '003';
            $result['result_desc'] = 'ยืนยันการลงเวลาไม่สำเร็จ';
            $result['message'] = 'ยืนยันการลงเวลาไม่สำเร็จ';
            $result['status'] = false;
        }

        $time_end = microtime(true);
        $result['excute_time'] = $time_end - $time_start;
        
        return $result;
    }

    public function leave_test(Request $request)
    {
        $employee_id = Auth::user()->employee_id;
        $request->request->add(
            [
                'employee_id' => $employee_id,
            ]
        );
        // leave_type_id
        // start_date
        // end_date
        $post_leave = $request->all();

        // id
        // leave_name
        // sex
        // amount
        // before_date
        // before_unit
        // minimum_leave
        // minimum_leave_unit
        // longevity
        // status
        $leave_type = Leavetype::find($request->leave_type_id);

        $employee =  Employee::select([
            'employee.firstname',
            'employee.lastname',
            'employee.gender',
            'employee.startworking_date',
        ])->find($employee_id);

        $message = [];
        $data['validate'] = true;

        if (isset($leave_type->sex)) {
            if (in_array($employee->gender, json_decode($leave_type->sex))) {
            } else {
                $data['validate'] = false;
                $message[] = "เพศของคุณ";
            }
        }
        if (isset($leave_type->before_date)) {
            $start_date = $post_leave['start_date'];
            $date1 = new \DateTime(date('Y-m-d H:i:s'));
            $date2 = $date1->diff(new \DateTime($start_date));
            if (((int)$date2->{$leave_type->before_unit} + 1) >= (int)$leave_type->before_date) {
            } else {
                $data['validate'] = false;
                $message[] = "วันที่แจ้งก่อนการลา";
            }
        }
        if (isset($leave_type->minimum_leave)) {
            $start_date = $post_leave['start_date'];
            $end_date = $post_leave['end_date'];
            $date1 = new \DateTime($end_date);
            $date2 = $date1->diff(new \DateTime($start_date));
            if (((int)$date2->{$leave_type->minimum_leave_unit} + 1)>=(int)$leave_type->minimum_leave) {
            } else {
                $data['validate'] = false;
                $message[] = "ระยะเวลาการลา";
            }
        }
        if (isset($leave_type->longevity)) {
            $employee->startworking_date;
            $date1 = new \DateTime($employee->startworking_date);
            $date2 = $date1->diff(new \DateTime(date('Y-m-d')));
            $result = ($date2->y * 12) + ($date2->m) + 1;
            if ((int)$result >= (int)$leave_type->longevity) {
            } else {
                $data['validate'] = false;
                $message[] = "อายุงาน";
            }
        }

        /** ============================================================================
         *  check employee leave more than this leave amount then return validate = false
         * ============================================================================
         */
            //$leave_amount = Employeeleave::leftjoin('leave_type', 'leave_type.id', 'employee_leave.leave_type_id')->find($employee_id);
            $isWork = optional($leave_type)->is_work ?: 0;
            if ($request->has('id')) {
                $isWork = Employeeleave::query()->find($request->id)->is_work ?: 0;
            }
          // echo $isWork;exit();
            try {
                if (!$this->isApprove($request)) {
                    $this->isEmployeeConfirmLeave($request);
                  
                }
                if (!$isWork) {
                   // $this->checkLeaveInWorkingTime($request);
                    $this->canLeave($request);
                  
                   
                }
            } catch (Throwable $exception) {
                $data['validate'] = false;
                $message[] = $exception->getMessage();
            }
           // echo '<PRE>';
           // print_r($message);exit();
         /** ============================================================================
          *  eof 
          *  ============================================================================
          */

        if ($data['validate']==false) {
            $data['message'] = (is_array($message))?implode(',', $message)."\nไม่ถูกต้องตามเงื่อนไข":'';
        } else {
            $data['message'] = 'Complete!';
        }
        return ($data);
    }

    private function isApprove(Request $request): bool
    {
        $only = [
            'leave_result',
            'employee_id',
            'id'
        ];
       // echo '<PRE>';
        //print_r($request->all());exit();
  
        foreach ($request->all() as $key => $value) {
            if (!in_array($key, $only)) {
                return false;
            }
        }
     
        return true;
    }


    public function leave(Request $request)
    {
        // wait app to store
        $check = $this->leave_test($request);
        //echo '<PRE>';
        //print_r($check);
        if ($check['validate']==false) {
            $data['type'] = "Employee leave";
            $data['result_code'] = '003';
            $data['result_desc'] = $check['message'];
            $data['message'] = $check['message'];
            $data['status'] = $check['validate'];
            return $data;
        }

        $employee_id = Auth::user()->employee_id;
        $upload = array();
        if ($request->hasfile('picture')) {
            foreach ($request->file('picture') as $image) {
                //Upload file
                $name = uniqid().'_'.$image->getClientOriginalName();
                $name = $name;
                $image->move(public_path().'/photos/images_leave/', $name);
                $upload[] = $name;

                //Resize image here
                $thumbnailpath = public_path('/photos/images_leave/'.$name);
                $img = Image::make($thumbnailpath)->resize(1000, 1000, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($thumbnailpath);
            }
        }
        
        $picture = count($upload)!=0 ? json_encode($upload) : json_encode([]);
        if (empty($request->id)) {
            $input = [
                'leave_type_id' => $request->leave_type_id,
                'leave_duration_id' => $request->leave_duration_id,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'picture' => $picture,
                'remark' => $request->remark,
                'employee_id' => $employee_id,
                'created_at' => date('Y-m-d H:i:s')
            ];
            $result = Employeeleave::insertGetId($input);

            if ($result) {
                // Start Push Notification
                // start get uuid array
                $uuid_array = array();
                if ($insert_detail = Employeeleave::leftjoin('employee_leave_approvers', 'employee_leave_approvers.employee_id', 'employee_leave.employee_id')->find($result)) {
                    if (json_decode($insert_detail->approvers)) {
                        if ($uuid = Uuid::select('uuid')->whereIn('employee_id', json_decode($insert_detail->approvers))->get()) {
                            foreach ($uuid as $value) {
                                $uuid_array[] = $value->uuid;
                            }
                            $params = array();
                            $params['type'] = 'leave_request';
                            $params['userId'] = $uuid_array;
                            $params['employee_id'] = json_decode($insert_detail->approvers);
                            $params['created_by'] = $employee_id;
                            $data['id']=$result;
                            $data['type']=$params['type'];
                            if (isset($request['project_id'])) {
                                $data['project_id']=$request['project_id'];
                            };
                            $params['data'] = $data;
                            $data['notification'] = Noti::pushNotification($params);
                        }
                    }
                }
                // end get uuid array
                // End Push Notification

                $data['type'] = "Employee leave";
                $data['result_code'] = '000';
                $data['result_desc'] = 'บันทึกการลาสำเร็จ';
                $data['message'] = "บันทึกการลาสำเร็จ";
                $data['status'] = true;
            } else {
                $data['type'] = "Employee leave";
                $data['result_code'] = '003';
                $data['result_desc'] = 'บันทึกการลาไม่สำเร็จ';
                $data['message'] = "บันทึกการลาไม่สำเร็จ";
                $data['status'] = false;
            }
        } else {
            $input = [
                'leave_result' => $request->leave_result,
                'approver_id' => $employee_id,
                'approved_at' => date('Y-m-d H:i:s')
            ];
            $result = Employeeleave::where('id', $request->id)->update($input);
            if ($result) {
                $get_employee_leave = Employeeleave::select('uuid.*')
                ->where('employee_leave.id', $request->id)
                ->leftjoin('uuid', 'uuid.employee_id', 'employee_leave.employee_id')
                ->first();

                $get_uuid = $get_employee_leave->uuid;

                $get_uuid = is_array($get_uuid) ? $get_uuid : array((string)$get_uuid);

                $uuid_array = array();
                $uuid_array = $get_uuid;
                $params = array();
                $params['type'] = 'leave_result';
                $params['userId'] = $uuid_array;
                $params['employee_id'] = array($get_employee_leave->employee_id);
                $params['created_by'] = $employee_id;
                $data['id']=$request->id;
                $data['type']=$params['type'];
                if (isset($request['project_id'])) {
                    $data['project_id']=$request['project_id'];
                };
                $params['data'] = $data;
                $data['notification'] = Noti::pushNotification($params);

                $data['type'] = "Approver leave";
                $data['result_code'] = '000';
                $data['result_desc'] = 'อัพเดทข้อมูลการลาสำเร็จ';
                $data['message'] = "อัพเดทข้อมูลการลาสำเร็จ";
                $data['status'] = true;
            } else {
                $data['type'] = "Approver leave";
                $data['result_code'] = '004';
                $data['result_desc'] = 'อัพเดทข้อมูลการลาไม่สำเร็จ';
                $data['message'] = "อัพเดทข้อมูลการลาไม่สำเร็จ";
                $data['status'] = false;
            }
        }
        return response()->json($data, $this->successStatus);
    }

    public function leave_type()
    {
        $data['data'] = \App\Models\Leavetype::active()->get();
        if ($data['data']) {
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงประเภทการลาสำเร็จ';
            $data['message'] = 'ดึงประเภทการลาสำเร็จ';
            $data['status']=true;
        } else {
            $data['data'] = [];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงประเภทการลาไม่สำเร็จ';
            $data['message'] = 'ดึงประเภทการลาไม่สำเร็จ';
            $data['status']=false;
        }
        return $data;
    }

    public function leave_duration()
    {
        $data['data'] = \App\Models\Leaveduration::active()->get();
        if ($data['data']) {
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงช่วงเวลาการลาสำเร็จ';
            $data['message'] = 'ดึงช่วงเวลาการลาสำเร็จ';
            $data['status']=true;
        } else {
            $data['data'] = [];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงช่วงเวลาการลาไม่สำเร็จ';
            $data['message'] = 'ดึงช่วงเวลาการลาไม่สำเร็จ';
            $data['status']=false;
        }
        return $data;
    }

    public function employee_level()
    {
        $data['data']=\App\Models\Employeelevel::active()
        ->select([
            'employee_level.id'
            ,'employee_level.name'
            ,\DB::raw("'".url('/')."' + employee_level.picture as picture")])
        ->get();
        if ($data['data']) {
            $data['result_code'] = '000';
            $data['result_desc'] = 'ดึงช่วงเวลาการลาสำเร็จ';
            $data['message'] = 'ดึงช่วงเวลาการลาสำเร็จ';
            $data['status']=true;
        } else {
            $data['data'] = [];
            $data['result_code'] = '002';
            $data['result_desc'] = 'ดึงช่วงเวลาการลาไม่สำเร็จ';
            $data['message'] = 'ดึงช่วงเวลาการลาไม่สำเร็จ';
            $data['status']=false;
        }
        return $data;
    }

    public function structure_top()
    {
        $ceo_id = \App\Http\Controllers\Admin\SettingController::show('ceo_id');
        return $this->structure($ceo_id);
    }

    public function structure_main(Request $request)
    {
        $employee_id = !empty($request->employee_id) ? $request->employee_id : AUTH::user()->employee_id;
        return $this->structure($employee_id);
    }

    public function structure($employee_id=null)
    {
        $query = Structurepivot::where('employee_id', $employee_id);
        if (!empty($request->box_id)) {
            $query->where('box_id', $request->box_id);
        }
        $query->orderBy('box_id', 'asc');
        $get_box = $query->get();
        $result = array();
        foreach ($get_box as $key => $value) {
            $container = array();
            $employee_obj = new stdClass();
            $employee_obj->has_additional_nodes = !empty($this->get_add_node((string)$value->box_id)) ? true : false;
            $employee_obj->MainNode = $this->get_main_node($value->employee_id);
            $employee_obj->BranchNode = $this->get_branch_node((string)$value->box_id);
            $employee_obj->AdditionalNodes = $this->get_add_node((string)$value->box_id);
            $result[] = $employee_obj;
        }
        $obj = new stdClass();

        if ($get_box) {
            $obj->status = true;
            $obj->ChartData = $result;
            $obj->result_code = '000';
            $obj->result_desc = 'Success';
        } else {
            $obj->status = false;
            $obj->ChartData = [];
            $obj->result_code = '002';
            $obj->result_desc = 'NotFoundDataException';
        }
        return response()->json($obj);
    }

    public function get_main_node($employee_id)
    {
        $employee = Employee::find($employee_id);
        $result = Employee::select([
            'level.job_description as job_description'
            ,'employee.id as employee_id'
            ,'employee.empcode as employee_code'
            ,'employee.cid as employee_card_number'
            ,'employee.firstname as employee_firstname'
            ,'employee.lastname as employee_lastname'
            ,'level.name as employee_position'
            ,\DB::raw("'".url('')."'+employee.picture_profile as employee_picture_profile")
            ,'branch.id as branch_id'
            ,'branch.branch_name as branch_name'
            ,'employee_level.id as level_id'
            ,'employee_level.name as level_name'
            ,'employee.skill_description as skill_description'
            ,'employee.authority as authority'
        ])
        ->leftjoin('level', 'employee.level_id', 'level.id')
        ->leftjoin('branch', 'employee.branch_id', 'branch.id')
        ->leftjoin('employee_level', 'employee.employee_level_id', 'employee_level.id')
        ->find($employee_id);
        
        $leader_id = "";
        if ($leader=Structurepivot::select('box_top_id')->where('employee_id', $employee_id)->first()) {
            if ($leader_id=Structurepivot::select('employee_id')->where('box_id', $leader->box_top_id)->first()) {
                $leader_id=$leader_id->employee_id;
            }
        }
        $has_underling="";
        if ($underling=Structurepivot::select('box_id')->where('employee_id', $employee_id)->first()) {
            $has_underling=Structurepivot::select('employee_id')->where('box_top_id', $underling->box_id)->count();
        }

        $has_leader = ($leader_id) ? true : false;
        $has_underling = ($has_underling!=0) ? true : false;
        $active_status = ($employee->resignation_date == null) ? true : false;
        $leader_employee_code = Employee::find((int)$leader_id);
        $result->authority = ($result->authority!=null)? $result->authority : "" ;
        $jobdescription = ($result->skill_description!=null)? $result->skill_description : $result->job_description ;
        $result->skill_description = ($result->skill_description==null) ? '' : $result->skill_description;
        $result->job_description = ($jobdescription==null) ? '' : $jobdescription;
        $result->has_leader = $has_leader;
        $result->has_underling = $has_underling;
        $result->active_status = $active_status;
        $result->leader_id = $leader_id;
        $result->leader_employee_code = ($leader_employee_code) ? $leader_employee_code->empcode:'';
        return $result;
    }

    public function get_branch_node($box_id=null)
    {
        $query = Structurepivot::select(['box_id','box_top_id','employee_id']);
        $query->where('primary', 'T');
        $query->orderBy('created_at', 'ASC');
        $query->orderBy('primary', 'DESC');
        if ($box_id!=null) {
            $query->where('box_top_id', (string)$box_id); //find children
        }
        $getbox = $query->get();
        foreach ($getbox as $key => $value) {
            $result[] = $this->get_main_node($value->employee_id);
        }
        return !empty($result)?$result:[];
    }

    public function get_add_node($box_id=null)
    {
        $query = Structurepivot::select(['box_id','box_top_id','employee_id']);
        $query->where('primary', 'F');
        $query->orderBy('created_at', 'ASC');
        $query->orderBy('primary', 'DESC');
        if ($box_id!=null) {
            $query->where('box_top_id', (string)$box_id); //find children
        }
        $getbox = $query->get();
        foreach ($getbox as $key => $value) {
            $result[] = $this->get_main_node($value->employee_id);
        }
        return !empty($result)?$result:[];
    }

    public function get_evaluation()
    {
        $data = new stdClass();
        $employee_id = !empty($request->employee_id) ? $request->employee_id : AUTH::user()->employee_id;
        $query = Employeeevaluation::leftjoin('employee', 'employee.id', 'employee_evaluation.employee_target_id')
        ->leftjoin('level', 'level.id', 'employee.level_id')
        ->leftjoin('branch', 'branch.id', 'employee.branch_id')
        ->leftjoin('department', 'department.id', 'employee.department_id')
        ->leftjoin('groups', 'groups.id', 'employee.group_id')
        ->leftjoin('employee_level', 'employee_level.id', 'employee.employee_level_id')
        ->where('employee_evaluation.employee_id', $employee_id)
        ->select([
            'employee.id',
            \DB::raw('employee.firstname+\' \'+employee.lastname as fullname'),
            'employee.firstname',
            'employee.lastname',
            \DB::raw("'".url('')."' + employee.picture_profile as picture_profile"),
            'level.name as job',
            'employee_level.picture as employee_level_picture',
            'branch.branch_name',
            'employee_evaluation.evaluation_ability',
            'department.name as department',
            'groups.name as groupsname'
        ])
        ->get();
        //echo $query;exit();
        $result = array();
        foreach ($query as $key => $value) {
            $obj = new stdClass();
            $obj->id = $value->id;
            $obj->fullname = $value->fullname;
            $obj->firstname = $value->firstname;
            $obj->lastname = $value->lastname;
            $obj->picture_profile = $value->picture_profile;
            $obj->employee_level_picture = $value->employee_level_picture;
            $obj->job = ($value->job==null)?'':$value->job;
            $obj->department = ($value->department==null)?'':$value->department;
            $obj->branch_name = ($value->branch_name==null)?'':$value->branch_name;
            $obj->department_branch = $value->department."($value->branch_name)";
            $obj->groupsname = ($value->groupsname==null)?'':$value->groupsname;
            $obj->evaluation_ability = $this->get_evaluation_last((int)$employee_id, (int)$value->id);
            $evaluation_ability = $obj->evaluation_ability;
            $check_evaluation = true;
            foreach ($evaluation_ability as $key => $v) {
                if ($v->is_show==true && $v->score=="") {
                    $check_evaluation = false;
                }
                else{
                    if(isset($v->score)){
                        if(!empty($v->score)){
                            $v->score = round($v->score);
                        }
                    }
                }
            }
            $evaluation = $obj->evaluation_ability;
            if ($obj->evaluation_ability==null) {
                $obj->evaluation_ability = [];
            }
            $obj->evaluation_completed = $check_evaluation;
            $result[] = $obj;
        }
        $data->data = $result;
        //bak up before update
        $data->evaluation_active = (Evaluation::where([
            [ 'evaluation_start' , '<=' , \DB::raw('CONVERT(date, \''.date('Y-m-d').'\')') ],
            [ 'evaluation_end' , '>=' , \DB::raw('CONVERT(date, \''.date('Y-m-d').'\')') ]
        ])->count())==0 ? false : true ;
        
       // dd(\DB::getQueryLog());exit();
        $data->notice = "หากข้อมูลรายชื่อที่มีสิทธิ์ประเมิณไม่ถูกต้องสามารถติดต่อสอบถามได้ที่ ฝ่าย HRD หรือ ฝ่ายทรัทพยากรบุคคล หรือ ทาง Line กลุ่ม HRM";
        if (count($query)!=0) {
            $data->status = true;
            $data->result_code = '000';
            $data->result_desc = 'Complete';
        } else {
            $data->status = true;
            $data->result_code = '002';
            $data->result_desc = 'NotFoundDataException';
        }
        return response()->json($data);
    }

    public function get_evaluation_detail($employee_id=null, $target_id=null)
    {
        $employee = Employee::find($employee_id);
        $query = Employeeevaluation::where([
            ['employee_target_id',$target_id],
            ['employee_id',$employee_id]
        ])
        ->select([
            'evaluation_ability'
        ])
        ->first(); //null

        $item = array();

        if ($query!=null) {
            $result = [];
            if (json_decode($query->evaluation_ability)==null) {
                $result = \DB::select("
                SELECT
                et.name
                ,et.description
                ,et.id as evaluation_type_id
                ,".$target_id." as employee_target_id
                ,(SELECT AVG(er.score) FROM evaluation_result er WHERE er.employee_id=".$employee_id." AND er.employee_target_id=".$target_id." AND er.evaluation_type_id=et.id) as score
                ,'F' as is_show
                FROM 
                evaluation_type et;
            ");
            } else {
                $result = \DB::select("
                SELECT
                et.name
                ,et.description
                ,et.id as evaluation_type_id
                ,".$target_id." as employee_target_id
                ,(SELECT AVG(er.score) FROM evaluation_result er WHERE er.employee_id=".$employee_id." AND er.employee_target_id=".$target_id." AND er.evaluation_type_id=et.id) as score
                ,(
                    CASE WHEN et.id IN (".implode(',', json_decode($query->evaluation_ability)).") THEN 'T'
                    ELSE 'F' END
                ) as is_show
                FROM 
                evaluation_type et;
            ");
            }

            foreach ($result as $key => $value) {
                $obj = new stdClass();
                $obj->name = $value->name;
                $obj->description = $value->description;
                $obj->evaluation_type_id = $value->evaluation_type_id;
                $obj->employee_target_id = $value->employee_target_id;
                $obj->score = ($value->score==null)?'':$value->score;
                $obj->is_show = ($value->is_show=='T')?true:false;
                $obj->evaluation_status = ($value->score==null)?false:true;
                $item[] = $obj;
            }
        } else {
            $item = [];
        }
        return $item;
    }

    public static function get_evaluation_last($employee_id=null, $target_id=null)
    {
        if (isset($employee_id) && isset($target_id)) {
            $employee_id = (int)($employee_id);
            $target_id = (int)($target_id);
        }

        $count_evaluation = Evaluation::where([
            [\DB::raw("CONVERT(datetime , CONVERT(nvarchar,evaluation_start)+' '+'00:00:00')"),'<=',\DB::raw('GETDATE()')]
            ,[\DB::raw("CONVERT(datetime , CONVERT(nvarchar,evaluation_end)+' '+'23:59:59')"),'>=',\DB::raw('GETDATE()')]
        ]);

        $evaluation = $count_evaluation->get();

        $return = [];

        $current_evaluation = $count_evaluation->select([
            \DB::raw("CONVERT(nvarchar,evaluation_start) + ' ' + '00:00:00' as evaluation_start")
            ,\DB::raw("CONVERT(nvarchar,evaluation_end) + ' ' + '23:59:59' as evaluation_end")
        ])->latest('id')->first();

        $evaluation_start = null;
        $evaluation_end = null;

        if ($current_evaluation) {
            $evaluation_start = $current_evaluation->evaluation_start;
            $evaluation_end = $current_evaluation->evaluation_end;
        }

        $results = Evaluationtype::select([
            'evaluation_type.name'
            ,'evaluation_type.description'
            ,'evaluation_type.id as evaluation_type_id'
            ,'evaluation_result.employee_id'
            ,'evaluation_result.employee_target_id'
            ,'evaluation_result.score'
            ,'evaluation_result.reason'
            ,\DB::raw('CONVERT(datetime,\''.($evaluation_start).'\') as evaluation_start')
            ,\DB::raw('CONVERT(datetime,\''.($evaluation_end).'\') as evaluation_end')
            ,'evaluation_result.created_at'
            ,\DB::raw('
                    (
                        CASE
                            WHEN (
                                    SELECT COUNT( * ) 
                                    FROM employee_evaluation 
                                    WHERE employee_evaluation.employee_id='.$employee_id.'
                                    AND employee_evaluation.employee_target_id='.$target_id.'
                                    AND employee_evaluation.evaluation_ability LIKE \'%"\'+CONVERT(varchar,evaluation_type.id)+\'"%\'
                            )>0 THEN \'true\' ELSE \'false\'
                        END
                    ) as is_show
                ')
            ,\DB::raw('
                    (
                        CASE
                            WHEN (evaluation_result.score is NULL) THEN \'false\' ELSE \'true\'
                        END
                    ) as evaluation_status
                ')
        ]);
        $results->leftjoin('evaluation_result', function ($query) use ($evaluation_start,$evaluation_end,$employee_id,$target_id) {
            $query->on('evaluation_result.evaluation_type_id', '=', 'evaluation_type.id');
            $query->where([
                [ 'evaluation_result.created_at' , '>=' , \DB::raw('CONVERT(datetime,\''.($evaluation_start).'\')') ]
                ,[ 'evaluation_result.created_at' , '<=' , \DB::raw('CONVERT(datetime,\''.($evaluation_end).'\')') ]
            ]);
            $query->where('evaluation_result.employee_id', $employee_id);
            $query->where('evaluation_result.employee_target_id', $target_id);
        });
        $results = $results->get();
        
        foreach ($results as $key => $result) {
            $obj = new stdClass();
            $obj->evaluation = $evaluation;
            $obj->parameter_employee_id = $employee_id;
            $obj->parameter_target_id = $target_id;
            $obj->name = ($result->name==null)? '' : $result->name ;
            $obj->description = ($result->description==null)? '' : $result->description ;
            $obj->evaluation_start = ($result->evaluation_start==null)? '' : $result->evaluation_start ;
            $obj->evaluation_end = ($result->evaluation_end==null)? '' : $result->evaluation_end ;
            $obj->evaluation_type_id = ($result->evaluation_type_id==null)? '' : $result->evaluation_type_id ;
            $obj->employee_target_id = ($result->employee_target_id==null)? "$target_id" : $result->employee_target_id ;
            $obj->score = ($result->score==null)? '' : $result->score ;
            $obj->reason = ($result->reason==null)? '' : $result->reason ;
            $obj->is_show = ($result->is_show=='true')? true : false ;
            $obj->evaluation_status = ($result->evaluation_status=="false")? false : true ;
            $obj->created_at = ($result->created_at==null)? '' : $result->created_at ;
            $return[] = $obj;
        }

        return $return;
    }

    public function post_evaluation(Request $request)
    {
        $evaluation_id = null;
        $result_evaluation = Evaluation::where([
            ['evaluation.status','T'],
        ])->select('evaluation.*')->orderBy('evaluation.updated_at', 'desc')->first();
        // return json_encode($result_evaluation);
        if (($result_evaluation)!=null) {
            $evaluation_id = $result_evaluation['id'];
        } else {
            $obj = new stdClass();
            $obj->status = false;
            $obj->result_code = '002';
            $obj->result_desc = 'ไม่อยู่ในช่วงการประเมิน';
            $obj->message = 'ไม่อยู่ในช่วงการประเมิน';
            return response()->json($obj);
        }
        // return $evaluation_id;
        $employee_id = !empty($request->employee_id) ? $request->employee_id : AUTH::user()->employee_id;
        $insert = array();
        $form_data = $request->all();
        \DB::beginTransaction();
        $obj = new stdClass();
        foreach ($form_data as $key => $value) {
            if ($value['score']!=0 && $value['score']!=3 && ($value['reason']==''||$value['reason']==null)) {
                $obj->status = false;
                $obj->result_code = '003';
                $obj->result_desc = 'กรุณาระบุเหตุผลการประเมิน';
                return response()->json($obj);
            }
            if ($value['score']>=1 && $value['score']<=5) {
                $value['employee_id'] = $employee_id;
                $value['created_at'] = date('Y-m-d H:i:s');
                $value['evaluation_id'] = $evaluation_id;
                $insert[] = $value;
            }
        }
        try {
            $result = Evaluationresult::insert($insert);
            if ($result) {
                \DB::commit();
                $obj->status = true;
                $obj->result_code = '000';
                $obj->result_desc = 'Completed';
            } else {
                \DB::rollBack();
                $obj->status = false;
                $obj->result_code = '003';
                $obj->result_desc = 'InsertDataException';
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            $obj->status = false;
            $obj->result_code = '005';
            $obj->result_desc = 'RequestDataException';
            $obj->exeption_detail = $e;
        }
        return response()->json($obj);
    }

    public function check_post_evaluation()
    {
        return Evaluationresult::select([
            'evaluation_result.*'
            ,\DB::raw('e1.firstname + \' \' + e1.lastname as e1name')
            ,\DB::raw('e2.firstname + \' \' + e2.lastname as e2name')
        ])
        ->leftjoin(\DB::raw('employee e1'), 'evaluation_result.employee_id', 'e1.id')
        ->leftjoin(\DB::raw('employee e2'), 'evaluation_result.employee_target_id', 'e2.id')
        ->get();
    }

    public function evaluation_grade_calculate($employee_id)
    {
        $result = Structurepivot::where('employee_id', $employee_id)->get();
        $return = array();

        foreach ($result as $key => $value) {
            $obj = new stdClass();
            $obj->employee_id = $employee_id;
            $value->box_top_id;
            if ($value->box_top_id!=null) {
                $obj->evaluation_get_uppper = $this->evaluation_get_uppper($value->box_top_id);
            } else {
                $obj->evaluation_get_uppper = [];
            }
            $obj->evaluation_get_lowwer = $this->evaluation_get_lowwer($value->box_id);
            $return[] = $obj;
        }
        return response()->json($return);
    }

    public function evaluation_get_uppper($box_top_id=null)
    {
        $result = array();
        $upper = array();
        $data = Structurepivot::where('box_id', $box_top_id)->first();
        $result[] = $data->employee_id;
        $value = null;
        if ($data->box_top_id != null) {
            $upper = $this->evaluation_get_uppper($data->box_top_id);
            $result = array_merge($result, $upper);
        }
        return ($result);
    }

    public function evaluation_get_lowwer($box_top_id=null)
    {
        $result = array();
        $data = \DB::table(\DB::raw('structure_pivot s'))->where('s.box_top_id', $box_top_id)->select([
            \DB::raw('s.box_id')
            ,\DB::raw('s.employee_id')
            ,\DB::raw('(SELECT COUNT(*) FROM structure_pivot sp WHERE sp.box_top_id=s.box_id) as count_child')
        ])->get();
        foreach ($data as $key => $value) {
            $obj = new stdClass();
            $obj->employee_id = $value->employee_id;
            $obj->count_child = $value->count_child;
            if ($value->count_child > 0) {
                $obj->children = $this->evaluation_get_lowwer($value->box_id);
            }
            $obj->score = $this->get_score($value->employee_id);
            $result[] = $obj;
        }
        return $result;
    }

    public function get_score($employee_id)
    {
        //รอคำนวณจริง
        $score = \DB::select("
            SELECT
                et.name
                ,
                ( 
                    SELECT AVG( er.score )
                    FROM evaluation_result er
                    WHERE er.evaluation_type_id = et.id AND er.employee_target_id = ".$employee_id."
                ) AS avg_score
            FROM
                evaluation_type et
            GROUP BY
                et.name,et.id
        ");
        $score_array = array();
        foreach ($score as $key => $value) {
            $obj_score = new stdClass();
            $obj_score->name = $value->name;
            $obj_score->score = ($value->avg_score!=null) ? (string)$value->avg_score:'';
            $obj_score->status = ($value->avg_score!=null) ? true:false;
            $score_array[] = $obj_score;
        }
        return $score_array;
    }

    public function get_score_history($employee_id=null)
    {
        // สร้างขึ้นมาใหม่ => ปรับประสิทธิภาพลดปัญหาคอขวด query แต่ใช้ cpu เพิ่มขึ้น
        // สร้างขึ้นมาสำหรับทำการคำนวณทันที หลังจากบุคคลใดบุคคลหนึ่งกดประเมิน
        $employee_id = ($employee_id) ? $employee_id : (Auth::user()->employee_id);
        $executionStartTime = microtime(true);
        $obj_return = new stdClass();
        $data = array();
        try {
            $eva = Evaluation::get();
            foreach ($eva as $key => $value) {
                $score = ET::query_get_value($employee_id, $value->id);
                $obj = new stdClass();
                $obj->evaluation_id = $value->id;
                $obj->evaluation_name = $value->evaluation_name;
                $obj->evaluation_start = $value->evaluation_start;
                $obj->evaluation_end = $value->evaluation_end;
                $obj->date = date_format(date_create($value->evaluation_start), "d/m/Y")." - ".date_format(date_create($value->evaluation_end), "d/m/Y");
                $obj->evaluation_total_score = (string) ROUND(FC::total_score($score), 2);
                $obj->evaluation_total_rate = FC::grade($obj->evaluation_total_score);
                $obj->evaluation_history = $score;
                $data[] = $obj;
            }
            $obj_return->result_code = "000";
            $obj_return->result_desc = "Completed";
            $obj_return->status = true;
            $obj_return->data = $data;
        } catch (\Throwable $th) {
            $obj_return->result_code = "002";
            $obj_return->result_desc = "NotFoundDataException";
            $obj_return->status = false;
            $obj_return->data = [];
            $obj_return->exception = $th->getTrace();
        }
        $executionEndTime = microtime(true);
        $obj_return->executetime = ROUND($executionEndTime - $executionStartTime, 3);
        return response()->json($obj_return);
    }

    public function get_score_history_old($employee_id=null)
    {
        $executionStartTime = microtime(true);
        $eva = Evaluation::get();
        $return = array();
        $obj_return = new stdClass();
        if ($eva) {
            foreach ($eva as $key => $value) {
                $obj = new stdClass();
                $obj->evaluation_id = $value->id;
                $obj->evaluation_name = $value->evaluation_name;
                $obj->evaluation_start = $value->evaluation_start;
                $obj->evaluation_end = $value->evaluation_end;
                $obj->date = date_format(date_create($value->evaluation_start), "d/m/Y")." - ".date_format(date_create($value->evaluation_end), "d/m/Y");

                // ฟังก์ชันดึงคะแนนการประเมิน ตามช่วงเวลาการประเมิน (ไตรมาส)
                $get_calculate_value = ET::calculate_result($employee_id, $value->evaluation_start, $value->evaluation_end, $value->id);
                
                // $obj->evaluation_total_item = empty($get_calculate_value['rawScore']) ? [] : $get_calculate_value['rawScore'];
                $obj->evaluation_total_score = empty($get_calculate_value['score']) ? '0' : $get_calculate_value['score'];
                $obj->evaluation_total_rate = empty($get_calculate_value['grade']) ? 'F' : $get_calculate_value['grade'];
                $obj->evaluation_history = empty($get_calculate_value['evaluation_type']) ? [] : $get_calculate_value['evaluation_type'];
                $return[] = $obj;
            }
            $obj_return->result_code = "000";
            $obj_return->result_desc = "Completed";
            $obj_return->status = true;
            $obj_return->data = $return;
        } else {
            $obj_return->result_code = "002";
            $obj_return->result_desc = "NotFoundDataException";
            $obj_return->status = false;
            $obj_return->data = [];
        }
        $executionEndTime = microtime(true);
        $obj_return->executetime = ROUND($executionEndTime - $executionStartTime, 3);
        return response()->json($obj_return);
    }

    public function post_image(Request $request)
    {
        if ($request->hasfile('picture')) {
            foreach ($request->file('picture') as $image) {
                //Upload file
                $name = uniqid().'_'.$image->getClientOriginalName();
                $name = $name;
                $image->move(public_path().'/photos/images_leave/', $name);
                $data[] = $name;
            }
        }
        return response()->json($data);
    }

    public function get_under()
    {
        $data = new stdClass();
        $employee_id = !empty($request->employee_id) ? $request->employee_id : AUTH::user()->employee_id;
        $employee_id_under = [];
        if ($box_id = Structurepivot::where('employee_id', $employee_id)->select('box_id')->first()) {
            if ($under_id = Structurepivot::where('box_top_id', $box_id['box_id'])->select('employee_id')->get()) {
                foreach ($under_id as $key => $value) {
                    $employee_id_under[] = $value->employee_id;
                }
            }
        }
        $data->employee_id_under = $employee_id_under;

        $query = Employee::whereIn('employee.id', $employee_id_under)
        ->leftjoin('level', 'level.id', 'employee.level_id')
        ->leftjoin('branch', 'branch.id', 'employee.branch_id')
        ->leftjoin('department', 'department.id', 'employee.department_id')
        ->leftjoin('employee_level', 'employee_level.id', 'employee.employee_level_id')
        ->select([
            'employee.id',
            \DB::raw('employee.firstname+\' \'+employee.lastname as fullname'),
            'employee.firstname',
            'employee.lastname',
            \DB::raw("'".url('')."' + employee.picture_profile as picture_profile"),
            'level.name as job',
            'level.job_description as job_description',
            'employee.skill_description as skill_description',
            'branch.branch_name',
            'employee.employee_level_id as employee_level_id',
            'employee_level.picture as employee_level_picture',
            'department.name as department'
        ])
        ->get();

        $data->query = $query;

        $result = array();
        
        if ($query) {
            foreach ($query as $key => $value) {
                $obj = new stdClass();
                $obj->id = $value->id;
                $obj->fullname = $value->fullname;
                $obj->firstname = $value->firstname;
                $obj->lastname = $value->lastname;
                $obj->picture_profile = $value->picture_profile;
                $obj->employee_level_picture = ($value->employee_level_picture==null) ? "" : $value->employee_level_picture ;
                $obj->job = ($value->job==null)?'':$value->job;
                $obj->job_description = ($value->job_description==null)?'':$value->job_description;
                $obj->department = ($value->department==null)?'':$value->department;
                $obj->branch_name = ($value->branch_name==null)?'':$value->branch_name;
                $obj->department_branch = $value->department."($value->branch_name)";
                $get_evaluation_detail = $this->get_evaluation_detail($employee_id, $value->id);
                $obj->evaluation_ability = $get_evaluation_detail;
                $check_evaluation = true;
                foreach ($get_evaluation_detail as $key => $v) {
                    if ($v->score==null||$v->score!="") {
                        $check_evaluation = false;
                    }
                }
                $obj->evaluation_completed = $check_evaluation;
    
                // // start fake grade
                // $fake_grade = Evaluationtype::select([
                //     'name',
                //     \DB::raw('0 as score'),
                //     \DB::raw('\'F\' as grade')
                // ])->get();
                // $obj_grade = new stdClass();
                // $get_calculate_value = ET::calculate_result($value->id);
                // $obj_grade->evaluation_type = $get_calculate_value['evaluation_type'];
                // $obj_grade->total_rate = $get_calculate_value['grade'];
                // $obj->grade = $obj_grade;
                // // end fake grade

                //คำนวณเกรดแบบใหม่
                $obj_grade = new stdClass();
                $get_grade = ET::query_get_value($value->id, null);
                $obj_grade->evaluation_type = $get_grade;
                $obj_grade->total_score = FC::total_score($get_grade);
                $obj_grade->total_rate = FC::grade($obj_grade->total_score);
                $obj->grade = $obj_grade;

                $get_score_history = $this->get_score_history($value->id);
                if (!empty($get_score_history)) {
                    $obj->score_history = $get_score_history->original->data;
                } else {
                    $obj->score_history = [];
                }
                
                $result[] = $obj;
            }
        }

        $data->data = $result;
        $data->evaluation_active = (Evaluation::where([
            [ 'evaluation_start' , '<=' , \DB::raw('CONVERT(date, \''.date('Y-m-d').'\')') ],
            [ 'evaluation_end' , '>=' , \DB::raw('CONVERT(date, \''.date('Y-m-d').'\')') ]
        ])->count())==0 ? false : true ;
        if (count($query)!=0) {
            $data->status = true;
            $data->result_code = '000';
            $data->result_desc = 'Complete';
        } else {
            $data->status = false;
            $data->result_code = '002';
            $data->result_desc = 'NotFoundDataException';
        }
        return response()->json($data);
    }

    public function check_data(Request $request)
    {
        return $request;
    }

    // START PROJECT

    // start get employee
    public function employee($employee_id=null, $project_id=null)
    {
        $query = Employee::select([
            \DB::raw(" '".url('/')."'+employee.picture_profile as picture_profile")
            ,'employee.id as employee_id'
            ,'employee.empcode as employee_code'
            ,\DB::raw("employee.firstname+' '+employee.lastname as name")
            ,'groups.name as group_name'
            ,'department.name as department_name'
            ,'level.name as position_name'
        ]);
        $query->leftjoin('groups', 'employee.group_id', 'groups.id');
        $query->leftjoin('department', 'employee.department_id', 'department.id');
        $query->leftjoin('level', 'employee.level_id', 'level.id');
        $query->active();
        if ($employee_id!=null) {
            if (is_array($employee_id)) {
                $query->whereIn('employee.id', $employee_id);
            } else {
                $query->where('employee.id', $employee_id);
            }
        }
        $result = $query->get();

        $return = array();
        foreach ($result as $key => $value) {
            $obj = new stdClass();
            $obj->picture_profile = ($value->picture_profile==null) ? '' : $value->picture_profile;
            $obj->employee_id = ($value->employee_id==null) ? '' : $value->employee_id;
            $obj->employee_code = ($value->employee_code==null) ? '' : $value->employee_code;
            $obj->name = ($value->name==null) ? '' : $value->name;
            $obj->group_name = ($value->group_name==null) ? '' : $value->group_name;
            $obj->department_name = ($value->department_name==null) ? '' : $value->department_name;
            $obj->position_name = ($value->position_name==null) ? '' : $value->position_name;
            $laststatus = '';
            if (isset($project_id)) {
                $projecttask = Projecttask::where([
                    ['project_task.employee_id','=',$value->employee_id]
                    ,['project_task.project_id','=',$project_id]
                ])
                ->orderBy('project_task.id', 'DESC')->first();
                if ($projecttask) {
                    $laststatus = $projecttask->status;
                    $laststatus = (date('Y-m-d')>$projecttask->due_date && $projecttask->status!='C')?'U':$laststatus;
                }
            }
            $obj->last_status = $laststatus;
            $return[] = $obj;
        }
        return $return;
    }
    // end get employee

    // start get get_employee
    public function get_employee()
    {
        $result = $this->employee();
        if ($result) {
            $data['data'] = $result;
            $data['record'] = count($result);
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'Complete';
        } else {
            $data['data'] = [];
            $data['record'] = 0;
            $data['status'] = false;
            $data['result_code'] = '002';
            $data['result_desc'] = 'NotFoundDataException';
        }
        return $data;
    }
    // end get get_employee

    // start post project_create
    public function project_create(Request $request)
    {
        // return $request;
        $employee_id = AUTH::user()->employee_id;
        $insert['name'] = $request->name;
        $insert['detail'] = $request->detail;
        $insert['due_date'] = $request->due_date;
        $insert['status'] = $request->status;
        $insert['created_by'] = $employee_id;
        $insert['created_at'] = date('Y-m-d H:i:s');
        $data = array();
        \DB::beginTransaction();
        try {
            if ($insert_id = Project::insertGetId($insert)) {

                //start insert history
                $history_insert = array();
                $history_insert['project_id'] = $insert_id;
                $history_insert['status'] = $request->status;
                $history_insert['created_at'] = date('Y-m-d H:i:s');
                $history_insert['created_by'] = AUTH::user()->employee_id;
                Projectstatushistory::insert($history_insert);
                //end insert history

                if ($request->hasfile('project_picture')) {
                    // $insert_picture = array();
                    foreach ($request->file('project_picture') as $image) {
                        //Upload file
                        $name = uniqid().'_'.$image->getClientOriginalName();
                        $name = $name;
                        $image->move(public_path().'/photos/project/'.$insert_id.'/', $name);
                        // $image->move(public_path().'/photos/project/', $name);
                        $value['name'] = $name;
                        $value['project_id'] = $insert_id;
                        $value['created_at'] = date('Y-m-d H:i:s');
                        $insert_picture[] = $value;
                        // $insert_picture[] = '/photos/project/'.$insert_id.'/'.$name;
        
                        //Resize image here
                        $thumbnailpath = public_path('/photos/project/'.$insert_id.'/'.$name);
                        // $thumbnailpath = public_path('/photos/project/'.$name);
                        $img = Image::make($thumbnailpath)->resize(1000, 1000, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $img->save($thumbnailpath);
                    }

                    if (!empty($insert_picture)) {
                        Projectpicture::insert($insert_picture);
                    }
                }

                if (!empty($request->project_assign)) {
                    $insert_employee = array();
                    foreach ($request->project_assign as $key => $value) {
                        if ($value!='' || $value!=null) {
                            $v['project_id'] = $insert_id;
                            $v['employee_id'] = $value;
                            $v['created_at'] = date('Y-m-d H:i:s');
                            $insert_employee[] = $v;
                        }
                    }
    
                    if (!empty($insert_employee)) {
                        if (Projectassign::insert($insert_employee)) {
                            if ($uuid = Uuid::select('uuid')->whereIn('employee_id', $request->project_assign)->get()) {
                                $uuid_array = array();
                                foreach ($uuid as $value) {
                                    $uuid_array[] = $value->uuid;
                                }
                                $params = array();
                                $params['type'] = 'project_create';
                                $params['userId'] = $uuid_array;
                                $params['employee_id'] = $request->project_assign;
                                $params['created_by'] = $employee_id;
                                $data['id']=$insert_id;
                                $data['type']=$params['type'];
                                if (isset($request['project_id'])) {
                                    $data['project_id']=$request['project_id'];
                                };
                                $params['data'] = $data;
                                $data['notification'] = Noti::pushNotification($params);
                            }
                        }
                    }
                }
            }
            \DB::commit();
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'บันทึกข้อมูลสำเร็จ';
            $data['message'] = 'บันทึกข้อมูลสำเร็จ';
        } catch (\Throwable $th) {
            \DB::rollBack();
            $data['status'] = false;
            $data['result_code'] = '005';
            $data['result_desc'] = 'RequestDataException';
            $data['message'] = 'รูปแบบข้อมูลไม่ถูกต้อง';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }
    // end post project_create

    // start get project
    public function project($project_id=null)
    {
        if ($project_id==null) {
            return null;
        }
        $employee_id = AUTH::user()->employee_id;
        $result = Project::select([
            'project.*'
            ,'employee.picture_profile'
            ,\DB::raw("employee.firstname+' '+employee.lastname as employee_name")
        ])
        ->leftjoin('employee', 'employee.id', 'project.created_by')
        ->find($project_id);
        if ($result) {
            $result['id'] = (string)$result['id'];
            if (isset($result['picture_profile'])) {
                $result['picture_profile'] = (string)url($result['picture_profile']);
            }
            if (isset($result['due_date'])) {
                if ($result['status']!='C') {
                    $result['status'] = (strtotime(date('Y-m-d')) > strtotime($result['due_date']))?'U':$result['status'];
                }
            }
            $is_edit = ((int)$employee_id==(int)($result['created_by'])) ? true : false;
            $result['is_edit'] = $is_edit;

            // start get project_picture
            $project_picture = Projectpicture::select(
                \DB::raw("'".url('/')."/photos/project/' + CONVERT(varchar,project_id) + '/' + name as url"),
                'project_id',
                'created_at'
            )
            ->where('project_id', $project_id)
            ->get();
            $result['project_picture'] = $project_picture;
            //end get project_picture

            // start get project_assign
            $project_assign = Projectassign::select('employee_id')->where('project_id', $project_id)->get();
            if (count($project_assign)!=0) {
                $employee = array();
                foreach ($project_assign as $key => $value) {
                    $employee[] = (int)($value->employee_id);
                }
                $project_assign = $this->employee($employee, $project_id);
            } else {
                $project_assign = [];
            }
            $result['project_assign'] = $project_assign;
            // start end project_assign
            
            if (isset($_GET['employee_id'])) {
                $project_assign = $this->employee($_GET['employee_id'], $project_id);
                $result['employee'] = (count($project_assign)==1)?$project_assign[0]:'';
                $result['project_task'] = $this->project_task($project_id, $_GET['employee_id']);
            }
            $data['data'] = $result;
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'บันทึกข้อมูลสำเร็จ';
            $data['message'] = 'บันทึกข้อมูลสำเร็จ';
        } else {
            $data['data'] = [];
            $data['status'] = false;
            $data['result_code'] = '002';
            $data['result_desc'] = 'NotFoundDataException';
            $data['message'] = 'ไม่มีข้อมูล';
        }
        return $data;
    }

    public function project_update(Request $request, $project_id)
    {
        // return $request;
        $employee_id = AUTH::user()->employee_id;
        $id = $project_id;
        $insert['name'] = $request->name;
        $insert['detail'] = $request->detail;
        $insert['due_date'] = $request->due_date;
        $insert['status'] = $request->status;
        
        $data = array();
        \DB::beginTransaction();
        try {
            $project = Project::where('id', $id)->update($insert);

            $get_project_detail = Project::where('id', $id)->first();

            if ($project) {
                if (isset($request->project_picture)) {
                    if ($request->hasfile('project_picture')) {
                        $insert_picture = array();
                        foreach ($request->file('project_picture') as $image) {
                            //Upload file
                            $name = uniqid().'_'.$image->getClientOriginalName();
                            $name = $name;
                            $image->move(public_path().'/photos/project/'.$id.'/', $name);
                            // $image->move(public_path().'/photos/project/', $name);
                            $value['name'] = $name;
                            $value['project_id'] = $id;
                            $value['created_at'] = date('Y-m-d H:i:s');
                            $insert_picture[] = $value;
            
                            //Resize image here
                            $thumbnailpath = public_path('/photos/project/'.$id.'/'.$name);
                            // $thumbnailpath = public_path('/photos/project/'.$name);
                            $img = Image::make($thumbnailpath)->resize(1000, 1000, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                            $img->save($thumbnailpath);
                        }
    
                        //start delete files
                        if ($projectpicture = Projectpicture::where('project_id', $id)->get()) {
                            foreach ($projectpicture as $key => $value) {
                                $file = public_path('/photos/project/'.$id.'/'.$value->name);
                                if (\File::exists(public_path($file))) {
                                    \File::delete(public_path($file));
                                }
                            }
                            Projectpicture::where('project_id', $id)->delete();
                        }
                        //end delete files
                        if (!empty($insert_picture)) {
                            Projectpicture::insert($insert_picture);
                        }
                    } else {
                        Projectpicture::where('project_id', $id)->delete();
                    }
                }

                $get_employee = array();
                if (isset($request->project_assign)) {
                    Projectassign::where('project_id', $id)->delete();
                    Projecttask::where('project_id', $project_id)
                    ->whereNotIn('employee_id', $request->project_assign)
                    ->delete();
                    $insert_employee = [];
                    foreach ($request->project_assign as $key => $value) {
                        if ($value!='' || $value!=null) {
                            $v['project_id'] = $id;
                            $v['employee_id'] = $value;
                            $v['created_at'] = date('Y-m-d H:i:s');
                            $insert_employee[] = $v;
                        }
                    }
                    if (!empty($insert_employee)) {
                        Projectassign::insert($insert_employee);
                    }

                    //start get_employee for notification
                    $get_employee = $request->project_assign;
                //end get_employee for notification
                } else {
                    //start get_employee for notification
                    $project_assign = Projectassign::where('project_id', $project_id)->get();
                    foreach ($project_assign as $key => $value) {
                        $get_employee[] = $value->employee_id;
                    }
                    // return $get_employee;
                    //end get_employee for notification
                }

                if ($uuid = Uuid::select('uuid')->whereIn('employee_id', $get_employee)->get()) {
                    $uuid_array = array();
                    foreach ($uuid as $value) {
                        $uuid_array[] = $value->uuid;
                    }
                    if (count($uuid)!=0) {
                        $params = array();
                        $params['type'] = 'project_update';
                        $params['content'] = $request['name'];
                        $params['userId'] = $uuid_array;
                        $params['employee_id'] = $get_employee;
                        $params['created_by'] = $employee_id;
                        $data['id']=$project_id;
                        $data['type']=$params['type'];
                        if (isset($request['project_id'])) {
                            $data['project_id']=$request['project_id'];
                        };
                        $params['data'] = $data;
                        $data['notification'] = Noti::pushNotification($params);
                    }
                }

                if (isset($request['status'])) {
                    if ($get_project_detail['status'] != $request['status']) {
                        $history_insert = array();
                        $history_insert['project_id'] = $project_id;
                        $history_insert['status'] = $request['status'];
                        $history_insert['created_at'] = date('Y-m-d H:i:s');
                        $history_insert['created_by'] = $employee_id;
                        Projectstatushistory::insert($history_insert);
                    }
                }
            }
            \DB::commit();
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'อัพเดทข้อมูลสำเร็จ';
            $data['message'] = 'อัพเดทข้อมูลสำเร็จ';
        } catch (\Exception $th) {
            \DB::rollBack();
            $data['status'] = false;
            $data['result_code'] = '005';
            $data['result_desc'] = 'RequestDataException';
            $data['message'] = 'รูปแบบข้อมูลไม่ถูกต้อง';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }

    // start get project_task
    public function project_task($project_id=null, $employee_id=null)
    {
        $login_user = AUTH::user()->employee_id;
        $return = array();
        if ($project_id!=null&&$employee_id!=null) {
            $result = Projecttask::select([
                'project_task.employee_id'
                ,'project_task.id'
                ,'project_task.detail'
                ,'project_task.comment'
                ,'project_task.note'
                ,'project_task.due_date'
                ,'project_task.status'
                ,'project_task.created_by'
                ,'project_task.created_at'
                ,'project_task.updated_at'
            ])
            ->where([
                ['project_task.project_id',$project_id]
                ,['project_task.employee_id',$employee_id]
            ])
            ->orderBy('project_task.id', 'ASC')
            ->get();
            if ($result) {
                foreach ($result as $key => $value) {
                    $obj = new stdClass();
                    $obj->employee_id = ($value->employee_id==null)?'':(string)$value->employee_id;
                    $obj->id = ($value->id==null)?'':(string)$value->id;
                    $obj->detail = ($value->detail==null)?'':(string)$value->detail;
                    $obj->comment = ($value->comment==null)?'':(string)$value->comment;
                    $obj->note = ($value->note==null)?'':(string)$value->note;
                    $obj->due_date = ($value->due_date==null)?'':(string)$value->due_date;
                    $obj->status = ($value->status==null)?'':(string)$value->status;
                    $obj->show_submit_button = ($value->employee_id==AUTH::user()->employee_id && $value->status=='N')?true:false;
                    $obj->task_owner = $login_user==$employee_id ? true : false;
                    $obj->created_by = ($value->created_by==null)?'':(string)$value->created_by;
                    $obj->created_at = ($value->created_at==null)?'':(string)$value->created_at;
                    $obj->updated_at = ($value->updated_at==null)?'':(string)$value->updated_at;
                    $return[] = $obj;
                }
            }
        }
        return $return;
    }
    // end get project_task

    // start get project_list
    public function project_list(Request $request)
    {
        $employee_id = AUTH::user()->employee_id;

        $query = Project::select([
            'project.*'
        ]);
        $query->whereIn('project.id', function ($q) use ($employee_id) {
            $q->select('project_id')
            ->from(with(new Projectassign)->getTable())
            ->where('employee_id', $employee_id)
            ->orWhere('project.created_by', $employee_id);
        });
        if (isset($request->status)) {
            if ($request->status == 'U') {
                $query->whereRaw('(project.status = \'U\' OR (project.status != \'C\' AND project.due_date<\''.date('Y-m-d').'\'))');
            } else {
                if ($request->status != 'C') {
                    $query->where('project.due_date', '>=', date('Y-m-d'));
                }
                $query->where('project.status', $request->status);
            }
        }
        $query->orderBy('project.id', 'DESC');
        $result = $query->get();

        if (COUNT($result)>0) {
            $return = [];
            foreach ($result as $key => $value) {
                $obj_return = $this->project((int)($value->id));
                $return[] = $obj_return['data'];
            }
            $data['data'] = $return;
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'Complete';
        } else {
            $data['data'] = [];
            $data['status'] = false;
            $data['result_code'] = '002';
            $data['result_desc'] = 'ไม่มีงานที่มอบหมาย';
            $data['messege'] = 'ไม่มีงานที่มอบหมาย';
        }
        $data['is_projectmanager'] = (Employee::where('id', $employee_id)->whereIn('employee_level_id', [1,2,3])->count()>0); //ความสามารถในการสร้าง  project
        return $data;
    }
    // end get project_list

    // start post project_task_create
    public function project_task_create(Request $request)
    {
        $insert['employee_id'] = $request['employee_id'];
        $insert['project_id'] = $request['project_id'];
        $insert['due_date'] = $request['due_date'];
        $insert['detail'] = $request['detail'];
        $insert['comment'] = $request['comment'];
        $insert['status'] = isset($request['status']) ? $request['status'] : 'N' ;
        $insert['created_by'] = AUTH::user()->employee_id;
        $insert['created_at'] = date('Y-m-d H:i:s');

        $employee_id = AUTH::user()->employee_id;

        \DB::beginTransaction();
        $project_name = Project::find($request['project_id'])->name;
        try {
            if ($result = Projecttask::insertGetId($insert)) {

                // start insert history
                $insert_history = array();
                $insert_history['project_id'] = $request['project_id'];
                $insert_history['project_task_id'] = $result;
                $insert_history['status'] = $request['status'];
                $insert_history['created_at'] = date('Y-m-d H:i:s');
                $insert_history['created_by'] = AUTH::user()->employee_id;
                Projectstatushistory::insertGetId($insert_history);
                // end insert history

                if ($uuid = Uuid::select('uuid')->whereIn('employee_id', array($request['employee_id']))->get()) {
                    $uuid_array = array();
                    foreach ($uuid as $value) {
                        $uuid_array[] = $value->uuid;
                    }
                    $params = array();
                    $params['type'] = 'project_task_create';
                    $params['userId'] = $uuid_array;
                    $params['content'] = $project_name;
                    $params['task'] = isset($request->detail) ? $request->detail : '';
                    $params['employee_id'] = array($request['employee_id']);
                    $params['created_by'] = $employee_id;
                    // $params['project_id'] = $request['project_id'];
                    $data['id'] = (string)$result;
                    $data['type']=$params['type'];
                    if (isset($request['project_id'])) {
                        $data['project_id']=$request['project_id'];
                    };
                    $params['data'] = $data;
                    $data['notification'] = Noti::pushNotification($params);
                }

                \DB::commit();
                $data['status'] = true;
                $data['result_code'] = '000';
                $data['result_desc'] = 'บันทึกข้อมูลสำเร็จ';
                $data['message'] = 'บันทึกข้อมูลสำเร็จ';
            }
        } catch (\Throwable $th) {
            \DB::rollBack();
            $data['status'] = false;
            $data['result_code'] = '005';
            $data['result_desc'] = 'RequestDataException';
            $data['message'] = 'รูปแบบข้อมูลไม่ถูกต้อง';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }
    // end post project_task_create

    // start post project_task_update
    public function project_task_update(Request $request, $id)
    {
        // return $request->all();
        \DB::beginTransaction();
        try {
            $result = Projecttask::select([
                'project_task.*',
                'project.name as project_name'
            ])
            ->leftjoin('project', 'project.id', 'project_task.project_id')
            ->find($id);
            
            // if( isset( $request->due_date ) ){
            //     $date = date_create($request->due_date);
            //     $request->due_date = date_format($date,"Y/m/d H:i:s.000");
            // }

            $check_update = Projecttask::where('id', $id)->update($request->all());

            if ($check_update) {
                $check_value = Projecttask::where('id', $id)->first();
                if ($result->status=="P") {
                    $employee_id = $result->created_by;
                } else {
                    $employee_id = AUTH::user()->employee_id;
                }

                if (isset($request->status)) {
                    if ($check_value['status'] != $request->status) {
                        // start insert history
                        $insert_history = array();
                        $insert_history['project_id'] = $result->project_id;
                        $insert_history['project_task_id'] = $id;
                        $insert_history['status'] = $request['status'];
                        $insert_history['created_at'] = date('Y-m-d');
                        $insert_history['created_by'] = $employee_id;
                        Projectstatushistory::insert($insert_history);
                        // end insert history
                    }
                }

                if ($uuid = Uuid::select('uuid')->whereIn('employee_id', array($result->employee_id))->get()) {
                    $uuid_array = array();
                    foreach ($uuid as $value) {
                        $uuid_array[] = $value->uuid;
                    }
                    $params = array();
                    $params['type'] = 'project_task_update';
                    $params['userId'] = $uuid_array;
                    $params['content'] = $result->project_name;
                    // return $params['task'] = '';
                    $params['task'] = isset($result->detail) ? $result->detail : '';
                    $data['id']=$result->id;
                    $data['type']=$params['type'];
                    if (isset($result->project_id)) {
                        $data['project_id']=$result->project_id;
                    };
                    $params['data'] = $data;
                    $params['employee_id'] = array($result->employee_id);
                    $params['created_by'] = $employee_id;
                    // return $params;
                    $data['notification'] = Noti::pushNotification($params);
                }
            }

            \DB::commit();

            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'อัพเดทข้อมูลสำเร็จ';
            $data['message'] = 'อัพเดทข้อมูลสำเร็จ';
        } catch (\Throwable $th) {
            \DB::rollBack();
            $data['status'] = false;
            $data['result_code'] = '005';
            $data['result_desc'] = 'RequestDataException';
            $data['message'] = 'อัพเดทข้อมูลไม่สำเร็จ';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }
    // end post project_task_update

    // start post project_task_update
    public function project_delete($id)
    {
        \DB::beginTransaction();
        try {
            Project::find($id)->delete();
            \DB::commit();
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'ลบข้อมูลสำเร็จ';
            $data['message'] = 'ลบข้อมูลสำเร็จ';
        } catch (\Throwable $th) {
            \DB::rollBack();
            $data['status'] = false;
            $data['result_code'] = '005';
            $data['result_desc'] = 'RequestDataException';
            $data['message'] = 'ลบข้อมูลไม่สำเร็จ';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }
    // end post project_task_update
    // END PROJECT

    // START TODO_LIST
    public function todolist_get(Request $request)
    {
        $return = array();
        try {
            $employee_id = AUTH::user()->employee_id;
            $query = Todolist::query();
            $query->where('created_by', $employee_id);
            if (isset($request->date)) {
                $query->where('date', $request->date);
            }
            if (isset($request->month)) {
                $query->where('date', 'LIKE', "$request->month%");
            }
            $result = $query->get();
            if (empty($result)) {
                new exception;
            } else {
                foreach ($result as $key => $value) {
                    $value->time = str_replace('.0000000', '', $value->time);
                    $return[] = $value;
                }
            }
            $data['data'] = $return;
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'Success';
            $data['message'] = 'Success';
        } catch (\Throwable $th) {
            $data['status'] = false;
            $data['result_code'] = '002';
            $data['result_desc'] = 'NotFoundDataException';
            $data['messege'] = 'ไม่พบข้อมูล';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }


    // START TODO_LIST
     public function todolist_get_under_employee(Request $request)
     {
         
         $return = array();
         try {
             //$employee_id = AUTH::user()->employee_id;
            
             $query = Todolist::query();
             if (isset($request->employee_id)) {
                $query->where('created_by', '=', $request->employee_id);
            }
             if (isset($request->month)) {
                $query->where('date', 'LIKE', "$request->month%");
            }
            if (isset($request->date)) {
                $query->where('date', 'LIKE', "$request->date%");
            }
            $result = $query->get();
            $num_row = $result->count();
            // echo '<PRE>';
            // print_r($result);exit();
             if ($num_row == 0) {
                
                  $data['data'] = array();
                  $data['status'] = true;
                  $data['result_code'] = '000';
                  $data['result_desc'] = 'Success';
                  $data['message'] = 'Success';
                  return $data;
		        
                  //$data['exception'] = ($th->getTrace());

             } else {
                 foreach ($result as $key => $value) {
                     $value->time = str_replace('.0000000', '', $value->time);
                     $return[] = $value;
                 }
             }
             $data['data'] = $return;
             $data['status'] = true;
             $data['result_code'] = '000';
             $data['result_desc'] = 'Success';
             $data['message'] = 'Success';
         } catch (\Throwable $th) {
             $data['status'] = false;
             $data['result_code'] = '002';
             $data['result_desc'] = 'NotFoundDataException';
             $data['messege'] = 'ไม่พบข้อมูล';
             $data['exception'] = ($th->getTrace());
         }
         return $data;
     }

    public function todolist_show($id)
    {
        try {
            $result = Todolist::find($id);
            if ($result==null) {
                new exception;
            } else {
                $result->time = str_replace('.0000000', '', $result->time);
            }
            $data['data'] = $result;
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'Success';
            $data['message'] = 'Success';
        } catch (\Throwable $th) {
            $data['data'] = [];
            $data['status'] = false;
            $data['result_code'] = '002';
            $data['result_desc'] = 'NotFoundDataException';
            $data['messege'] = 'ไม่พบข้อมูล';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }

    public function todolist_create(Request $request)
    {
        \DB::beginTransaction();
        try {
            unset($request['id']);
            $request['created_at'] = date('Y-m-d');
            $request['created_by'] = AUTH::user()->employee_id;
            $status = Todolist::insert($request->all());
            \DB::commit();
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'Success';
            $data['message'] = 'บันทึกข้อมูลสำเร็จ';
        } catch (\Exception $e) {
            \DB::rollBack();
            $data['status'] = false;
            $data['result_code'] = '003';
            $data['result_desc'] = 'InsertDataException';
            $data['message'] = 'บันทึกข้อมูลไม่สำเร็จ';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }

    public function todolist_update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            unset($request['id']);
            $result = Todolist::where('id', $id)->update($request->all());
            \DB::commit();
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'Success';
            $data['message'] = 'อัพเดทข้อมูลสำเร็จ';
        } catch (\Exception $e) {
            \DB::rollBack();
            $data['status'] = false;
            $data['result_code'] = '003';
            $data['result_desc'] = 'InsertDataException';
            $data['message'] = 'บันทึกข้อมูลไม่สำเร็จ';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }

    public function todolist_delete($id)
    {
        \DB::beginTransaction();
        try {
            Todolist::find($id)->delete();
            \DB::commit();
            $data['status'] = true;
            $data['result_code'] = '000';
            $data['result_desc'] = 'ลบข้อมูลสำเร็จ';
            $data['message'] = 'ลบข้อมูลสำเร็จ';
        } catch (\Throwable $th) {
            \DB::rollBack();
            $data['status'] = false;
            $data['result_code'] = '005';
            $data['result_desc'] = 'RequestDataException';
            $data['message'] = 'ลบข้อมูลไม่สำเร็จ';
            $data['exception'] = ($th->getTrace());
        }
        return $data;
    }
    // END TODO_LIST
}
