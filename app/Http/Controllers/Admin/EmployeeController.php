<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FunctionController as FC;

use App\Models\Employee;

use App\Models\Provinces;
use App\Models\Districts;
use App\Models\Subdistricts;
use App\Models\Branch;
use App\Models\Company;
use App\Models\Groups;
use App\Models\Department;
use App\Models\Level;
use App\Models\Employeestatus;
use App\Models\Resignreason;
use App\Models\Employeelevel;
use App\Models\Hospitals;
use App\Models\Warning;
use App\Models\Institutes;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['provinces'] = Provinces::get();
		$data['districts'] = Districts::get();
        $data['subdistricts'] = Subdistricts::get();
        
        $data['companies'] = Company::Active()->get();
        $data['branch'] = Branch::Active()->get();
        $data['groups'] = Groups::Active()->get();
		$data['department'] = Department::Active()->get();
        $data['level'] = Level::Active()->get();
        $data['employee_level'] = Employeelevel::Active()->get();
        $data['resignreason'] = Resignreason::Active()->get();
        $data['employeestatus'] = Employeestatus::Active()->get();
        $data['hospital'] = Hospitals::Active()->get();
        
        $data['menu'] = 'ข้อมูลพนักงาน';
        return view('admin.employee')->with($data); // admin/employee
    }

    public function list(Request $request){
        $model = Employee::query();
        $model->leftjoin('branch','employee.branch_id','branch.id');
        $model->leftjoin('company','employee.company_id','company.id');
        $model->leftjoin('groups','employee.group_id','groups.id');
        $model->leftjoin('department','employee.department_id','department.id');
        $model->leftjoin('level','employee.level_id','level.id');
        $model->leftjoin('employee_status','employee.employee_status_id','employee_status.id');
        $model->leftjoin('employee_level','employee.employee_level_id','employee_level.id');
        $model->leftjoin('hospitals','employee.hospital_id','hospitals.id');
        $model->leftjoin('education_level','employee.education_level_id','education_level.id');
        if(!empty($request->companies)){
            $model->where(\DB::raw('employee.company_id'),$request->companies);
        }
        if(!empty($request->branches)){
            $model->where(\DB::raw('employee.branch_id'),$request->branches);
        }
        if(!empty($request->levels)){
            $model->where(\DB::raw('employee.level_id'),$request->levels);
        }
        if(!empty($request->groups)){
            $model->where(\DB::raw('employee.group_id'),$request->groups);
        }
        if(!empty($request->departments)){
            $model->where(\DB::raw('employee.department_id'),$request->departments);
        }
        if(!empty($request->employee_status_id)){
            $model->where(\DB::raw('employee.employee_status_id'),$request->employee_status_id);
        }
        if(!empty($request->hospital_id)){
            $model->where(\DB::raw('employee.hospital_id'),$request->hospital_id);
        }
        if(!empty($request->employee_level_id)){
            $model->where(\DB::raw('employee.employee_level_id'),$request->employee_level_id);
        }
        $model->select([
            'employee.*',
            'employee.id as employeeid',
            'employee_status.id as employee_statusid',
            'employee_status.name as esname',
            'branch.branch_name as branch_name',
            'branch.id as branchid',
            'department.id as departmentid',
            'department.name as dname',
            'level.id as levelid',
            'level.name as lname',
            'company.id as companyid',
            'company.name as cname',
            'groups.id as groupsid',
            'groups.name as gname',
            'education_level.id as educationlevelid',
            'education_level.name as educationname',
            'hospitals.name as hname',
            'employee_level.name as ename',
            // age calculate
            \DB::raw("DATEDIFF(year, employee.birthday, GETDATE()) as age"),
            // workage_year calculate
            \DB::raw("DATEDIFF(year, employee.startworking_date, GETDATE()) as workage"),
            // workage_month calculate
            \DB::raw("DATEDIFF(month, employee.startworking_date, GETDATE()) as workage_month")
        ]);

        return  \DataTables::eloquent($model)
        ->addColumn('action',function($rec){
            $str = '
            <form method="post" action="'.url("/admin/employee/create").'" class="form">
                '.csrf_field().'
                <input type="hidden" name="id" value="'.$rec->employeeid.'">
                <a class="btn btn-xs btn-warning btn-edit" href="#">
                    <i class="fa fa-edit"></i>
                </a>
                <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->employeeid.'">
                    <i class="fa fa-trash"></i>
                </a>
            </form>
            ';
            return $str;
        })
        ->addColumn('employee_leave_approver',function($rec){ //employee_leave_approver
            $query = \App\Models\Employeeleaveapprovers::where('employee_id',$rec->id)->first();
            $return = "";
            if($query){
                $items = Employee::whereIn('id',json_decode($query->approvers))->get();
                foreach($items as $key => $value){
                    if($key!=0){
                        $return .= ",";
                    }
                    $return .= $value->firstname." ".$value->lastname."\n";
                }
            }
            return $return;
        })
        ->addColumn('address',function($rec){       
            return $this->address($rec->house_number,$rec->road,$rec->subdistrict_id,$rec->district_id,$rec->province_id,$rec->zipcode);
        })
        ->addColumn('address_cr',function($rec){
            return $this->address($rec->cr_house_number,$rec->cr_road,$rec->cr_subdistrict_id,$rec->cr_district_id,$rec->cr_province_id,$rec->cr_zipcode);
        })
        ->editColumn('picture_profile',function($rec){
            return '<img src="'.asset($rec->picture_profile).'" width="150px">';
        })
        ->addColumn('fullname',function($rec){
            return $rec->firstname." ".$rec->lastname;
        })
        ->addColumn('workage_month',function($rec){
            return FC::monthtoyear($rec->workage_month);
        })
        ->addIndexColumn()
        ->rawColumns(['action','picture_profile'])
        ->make('true');
    }

    public function address($house=null , $road=null , $sub=null , $dis=null , $pro=null , $zip=null){
        $return = [];
        if(isset($house)){ $return[] = $house; };
        if(isset($road)){ $return[] = 'ถ.'.$road; };
        if(isset($sub)){ $return[] = 'ต.'.Subdistricts::where('id',$sub)->first()->name_th; };
        if(isset($dis)){ $return[] = 'อ.'.Districts::where('id',$dis)->first()->name_th; };
        if(isset($pro)){ $return[] = 'จ.'.Provinces::where('id',$pro)->first()->name_th; };
        if(isset($zip)){ $return[] = 'ปณ.'.$zip; };
        $return = !empty($return) ? implode(' ',$return) : '';
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['menu'] = 'ข้อมูลพนักงาน';
        $data['branch'] = Branch::get();
        $data['provinces'] = \App\Models\Provinces::get();
        $data['groups'] = \App\Models\Groups::active()->get();
        $data['departments'] = \App\Models\Department::active()->get();
        $data['levels'] = \App\Models\Level::active()->get();
        $data['adminmenus'] = \App\AdminMenu::get();
        $data['employees'] = \App\Models\Employee::get();
        $data['evaluationtype'] = \App\Models\Evaluationtype::active()->get();
        $data['employeestatus'] = \App\Models\Employeestatus::active()->get();
        $data['educationlevel'] = \App\Models\Educationlevel::active()->get();
        $data['companies'] = \App\Models\Company::active()->get();
        $data['resignreason'] = Resignreason::Active()->get();
        $data['employeelevel'] = Employeelevel::Active()->get();
        $data['hospitals'] = Hospitals::Active()->get();
        $data['warning'] = Warning::Active()->get();
        $data['institute'] = Institutes::Active()->get();
        
        $id = $request->id;
        $data['employee_id'] = $request->id;
        $data['form_employee'] = \App\Models\Employee::find($id);
        $data['form_employee_preferment'] = \App\Models\Employeepreferment::where('employee_id',$id)->orderBy('start_date','ASC')->get();
        $data['form_employee_work'] = \App\Models\Employeeexperience::where(['employee_id' => $id , 'experience_type' => 'w'])->orderBy('start_date','ASC')->get();
        $data['form_employee_education'] = \App\Models\Employeeexperience::where(['employee_id' => $id , 'experience_type' => 'e'])->orderBy('start_date','ASC')->get();
        $data['form_employee_training'] = \App\Models\Employeeexperience::where(['employee_id' => $id , 'experience_type' => 't'])->orderBy('start_date','ASC')->get();
        $data['form_employee_leave_approver'] = \App\Models\Employeeleaveapprovers::where('employee_id',$id)->first();
        $data['form_employee_evaluation'] = \App\Models\Employeeevaluation::where('employee_id',$id)->orderBy('id','DESC')->get();
        $data['form_employee_warning'] = \App\Models\Employeewarning::where('employee_id',$id)->orderBy('start_date','ASC')->get();
        $data['form_admin_user'] = \App\AdminUser::where('employee_id',$id)->first();
        
        return view('admin.form_personal_information')->with($data); //  admin/form_personal_information
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Employee::insertGetId($request->all())){
                    \DB::commit();
                    $data['employee_id'] = $result;
                    $data['message'] = "คุณเพิ่มข้อมูลสำเร็จ!";
                    return $data;
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    public function store_preferment(Request $request)
    {
        // for ($i=0; $i < count($request->id); $i++) {
        for ($i = (count($request->id)-1); $i>=0; $i--) {
            foreach($request->all() as $key => $value){
                if(!empty($value[$i])){
                    $values[$i][$key] = $value[$i];
                }
            }
        }
        foreach($values as $key => $item){
            if(empty($item['id'])){
                $item['created_at'] = date("Y-m-d h:i:s");
                $result[] = \App\Models\Employeepreferment::insertGetId($item);
            }else{
                $id = $item['id'];
                unset($item['id']);
                $item['updated_at'] = date("Y-m-d h:i:s");
                $result[] = \App\Models\Employeepreferment::where('id',$id)->update($item);
            }
        }
        $data['form_employee_preferment'] = \App\Models\Employeepreferment::where('employee_id',($request->employee_id)[0])->get();
        $data['text'] = "คุณอัพเดทข้อมูลสำเร็จ!";
        return $data;
    }

    public function store_experience(Request $request)
    {
        // for ($i=0; $i < count($request->id); $i++) {
        for ($i = (count($request->id)-1); $i>=0; $i--) {
            foreach($request->all() as $key => $value){
                if(!empty($value[$i])){
                    $values[$i][$key] = $value[$i];
                }
            }
        }
        foreach($values as $key => $item){
            if(empty($item['id'])){
                $item['created_at'] = date("Y-m-d h:i:s");
                $result[] = \App\Models\Employeeexperience::insert($item);
            }else{
                $id = $item['id'];
                unset($item['id']);
                $item['updated_at'] = date("Y-m-d h:i:s");
                $result[] = \App\Models\Employeeexperience::where('id',$id)->update($item);
            }
        }
        $data['form_employee_experience'] = \App\Models\Employeeexperience::where(['employee_id'=>($request->employee_id)[0],'experience_type'=>($request->experience_type)[0]])->get();
        $data['text'] = "คุณอัพเดทข้อมูลสำเร็จ!";
        return $data;
        // return "อัพเดทข้อมูลสำเร็จ!";
    }

    public function store_leaveapprovers(Request $request)
    {
        $item = $request->all();
        if(!empty($request->approvers)){
            $item['approvers'] = json_encode($item['approvers']);
        }else{
            $item['approvers'] = json_encode([]);
        }
        if(empty($item['id'])){
            unset($item['id']);
            $item['created_at'] = date("Y-m-d h:i:s");
            $result['result'] = \App\Models\Employeeleaveapprovers::insertGetId($item);
            $result['message'] = "เพิ่มข้อมูลสำเร็จ!";
        }else{
            $id = $item['id'];
            unset($item['id']);
            $item['updated_at'] = date("Y-m-d h:i:s");
            $result['result'] = \App\Models\Employeeleaveapprovers::where('id',$id)->update($item);
            $result['message'] = "อัพเดทข้อมูลสำเร็จ!";
        }
        
        return $result;
    }

    public function store_adminuser(Request $request)
    {
        $item = $request->all();
        if($request->access_menu){
            $item['access_menu'] = json_encode($item['access_menu']);
        }else{
            $item['access_menu'] = '[]';
        }
        if($request->password) $item['password'] = \Hash::make($item['password']);
        
        if(empty($item['id'])){
            unset($item['id']);
            $item['created_at'] = date("Y-m-d h:i:s");
            /*
            $result = \App\AdminUser::insertGetId($item);
            $data['adminuser_id'] = $result;
            $data['message'] = "เพิ่มข้อมูลสำเร็จ!";
            return $data;
            */
            $res = \App\AdminUser::where('email',$item['email'])->count();
            if($res == 0){
                $result = \App\AdminUser::insertGetId($item);
                $data['adminuser_id'] = $result;
                $data['status'] = 1;
                $data['message'] = "เพิ่มข้อมูลสำเร็จ!";
                return $data;
            }else{
                $data['status'] = 0;
                $data['message'] = "ชื่อผู้ใช้ ".$item['email']." มีการใช้งานแล้ว";
                return $data;
            }
        }else{
            $res = \App\AdminUser::where('id',$item['id'])->get();
            if($res[0]->email == $item['email']){
                $id = $item['id'];
                unset($item['id']);
                $item['updated_at'] = date("Y-m-d h:i:s");
                $result = \App\AdminUser::where('id',$id)->update($item);
                $data['adminuser_id'] = $id;
                $data['status'] = 1;
                $data['message'] = "อัพเดทข้อมูลสำเร็จ!";
                return $data;
            }else{
                $data['status'] = 0;
                $data['message'] = "ชื่อผู้ใช้ ".$item['email']." มีการใช้งานแล้ว";
                return $data;
            }
        }
    }

    public function store_evaluation(Request $request)
    {
        for ($i=0; $i < count($request->id); $i++) {
        // for ($i = (count($request->id)-1); $i>=0; $i--) {
            foreach($request->all() as $key => $value){
                if(!empty($value[$i])){
                    $values[$i][$key] = $value[$i];
                }
            }
        }
        // return $values;
        foreach($values as $key => $item){
            if(empty($item['id'])){
                $item['created_at'] = date("Y-m-d h:i:s");
                $item['evaluation_ability'] = !empty($item['evaluation_ability'])?json_encode($item['evaluation_ability']):null;
                $result[] = \App\Models\Employeeevaluation::insert($item);
            }else{
                $id = $item['id'];
                unset($item['id']);
                $item['updated_at'] = date("Y-m-d h:i:s");
                $item['evaluation_ability'] = !empty($item['evaluation_ability'])?json_encode($item['evaluation_ability']):null;
                $result[] = \App\Models\Employeeevaluation::where('id',$id)->update($item);
            }
        }
        $data['form_employee_evaluation'] = \App\Models\Employeeevaluation::where(['employee_id'=>$request->employee_id])->get();
        $data['text'] = "คุณอัพเดทข้อมูลสำเร็จ!";
        return $data;
    }

    public function store_warning(Request $request)
    {
        // for ($i=0; $i < count($request->id); $i++) {
        for ($i = (count($request->id)-1); $i>=0; $i--) {
            foreach($request->all() as $key => $value){
                if(!empty($value[$i])){
                    $values[$i][$key] = $value[$i];
                }
            }
        }
        foreach($values as $key => $item){
            if(empty($item['id'])){
                $item['created_at'] = date("Y-m-d h:i:s");
                $result[] = \App\Models\Employeewarning::insertGetId($item);
            }else{
                $id = $item['id'];
                unset($item['id']);
                $item['updated_at'] = date("Y-m-d h:i:s");
                $result[] = \App\Models\Employeewarning::where('id',$id)->update($item);
            }
        }
        $data['form_employee_warning'] = \App\Models\Employeewarning::where('employee_id',($request->employee_id)[0])->get();
        $data['text'] = "คุณอัพเดทข้อมูลสำเร็จ!";
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Employee::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            unset($request['id']);
            if( $result = Employee::where('id',$id)->update($request->all()) ){
                \DB::commit();
                $data['employee_id'] = $id;
                $data['message'] = "คุณอัพเดทข้อมูลสำเร็จ!";
                return $data;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy_card(Request $request)
    {
        $id = $request->id;
        switch ($request->table) {
            case 'preferment':
                $example = \App\Models\Employeepreferment::findOrFail($id);
                break;
            
            case 'work':
                $example = \App\Models\Employeeexperience::findOrFail($id);
                break;
            
            case 'education':
                $example = \App\Models\Employeeexperience::findOrFail($id);
                break;
            case 'evaluation':
                $example = \App\Models\Employeeevaluation::findOrFail($id);
                break;
            case 'warning':
                $example = \App\Models\Employeewarning::findOrFail($id);
                break;
        }

        \DB::beginTransaction();
        
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Employee::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}