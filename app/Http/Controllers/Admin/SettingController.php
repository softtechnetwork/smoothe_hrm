<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'ตั้งค่าระบบ';
        $data['employee'] = \App\Models\Employee::active()->get();
        return view('admin.setting')->with($data); // admin/setting
    }

    public function get_setting(){
        return Setting::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = []; $update = [];
        foreach($request->input() as $key => $value){
            $input_in = array(); $update_in = array();
            if(Setting::where('name',$key)->count()==0){
                $input_in['name'] = $key;
                $input_in['value'] = $value;
                $input_in['created_at'] = date('Y-m-d H:i:s');
                $input[] = $input_in;
            }else{
                $update_in['name'] = $key;
                $update_in['value'] = $value;
                $update[] = $update_in;
            }
        }
        // return $update;
        try{
            if(!empty($input)){
                $request['created_at'] = date("Y-m-d h:i:s");
                \DB::beginTransaction();
                Setting::insert($input);
            }
            if(!empty($update)){
                $this->update($update);
            }
            \DB::commit();
            return "บันทึกข้อมูลสำเร็จ";
        }catch (\Exception $e){
            \DB::rollBack();
            return $e->getMessage();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($id)
    {
        if($result = Setting::where('name',$id)->first()){
            return $result->value;
        }else{
            return '';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($data, $id = NULL)
    {
        foreach($data as $key => $value){
            Setting::where('name',$value['name'])->update($value);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
