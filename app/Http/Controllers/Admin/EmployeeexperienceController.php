<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\ReportController as ReportController;
use App\Models\Employeeexperience;
use App\Models\Employee;
use App\Models\Educationlevel;
use App\Models\Institutes;
use App\Models\Warning;
use App\Models\Employeewarning;
use App\Models\Employeepreferment;
use App\Models\Branch;
use App\Models\Level;
use App\Models\Department;

use PDF;

class EmployeeexperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['employee'] = Employee::get();
		$data['educationlevel'] = Educationlevel::get();
		$data['institutes'] = Institutes::get();

        $data['menu'] = 'Employeeexperience';
        return view('admin.employeeexperience')->with($data);
    }

    public function index_train()
    {
		$data['employee'] = Employee::get();
		$data['educationlevel'] = Educationlevel::active()->get();
        $data['institutes'] = Institutes::active()->get();

        $data['place'] = Employeeexperience::select(['employee_experience.place'])->groupBy('employee_experience.place')->where('employee_experience.experience_type','t')->get();
        $data['level'] = Employeeexperience::select(['employee_experience.level'])->groupBy('employee_experience.level')->where('employee_experience.experience_type','t')->get();
        $data['subject'] = Employeeexperience::select(['employee_experience.subject'])->groupBy('employee_experience.subject')->where('employee_experience.experience_type','t')->get();

        $data['menu'] = 'การฝึกอบรม';
        return view('admin.employeeexperiencetrain')->with($data);
    }

    public function index_position()
    {
		$data['employee'] = Employee::get();

        $data['branch'] = Branch::active()->get();
        $data['level'] = Level::active()->get();
        $data['department'] = Department::active()->get();

        $data['menu'] = 'การเลื่อนตำแหน่ง';
        return view('admin.employeeexperienceposition')->with($data);
    }

    public function index_warning()
    {
		$data['employee'] = Employee::get();
		$data['educationlevel'] = Educationlevel::active()->get();
        $data['institutes'] = Institutes::active()->get();
        $data['warning'] = Warning::active()->get();

        $data['menu'] = 'การเตือน';
        return view('admin.employeeexperiencewarn')->with($data);
    }

    public function list(Request $request){
        $model = $this->report($request);
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                        <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->employee_experienceid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->employee_experienceid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->toJson();
    }

    public function list_warning(Request $request){
        $model = $this->query_warning($request);
        return  \DataTables::eloquent($model)
        ->addIndexColumn()
        ->rawColumns(['action'])
        ->toJson();
    }

    public function query_warning(Request $request){
        $model = Employeewarning::query();
        $model->leftjoin('employee','employee_warning.employee_id','employee.id');
        $model->leftjoin('level','level.id','employee.level_id');
        $model->leftjoin('department','department.id','employee.department_id');
        $model->leftjoin('groups','groups.id','employee.group_id');
        $model->leftjoin('warning','warning.id','employee_warning.warning_id');
        $model->select([
            'employee_warning.*'
            ,'employee_warning.id as employee_warningid'
            ,'employee.prename'
            ,'employee.empcode'
            ,\DB::raw('employee.firstname+\' \'+employee.lastname as employee_name')
            ,'employee.firstname'
            ,'employee.lastname'
            ,'employee.id as employeeid'
            ,'level.name as lname'
            ,'department.name as dname'
            ,'groups.name as gname'
            ,'warning.name as wname'
        ]);
        if(isset($request->warning_id)){$model->where('warning_id',$request->warning_id);}
        if(isset($request->date_start)&&isset($request->date_end)){$model->whereBetween('start_date',[$request->date_start,$request->date_end]);}
        return $model;
    }

    public function list_preferment(Request $request){
        $model = $this->query_preferment($request);
        return  \DataTables::eloquent($model)
        ->addIndexColumn()
        ->rawColumns(['action'])
        ->toJson();
    }

    public function query_preferment(Request $request){
        $model = Employeepreferment::query();
        $model->leftjoin('employee','employee_preferment.employee_id','employee.id');
        $model->leftjoin('level','level.id','employee_preferment.level_id');
        $model->leftjoin('department','department.id','employee_preferment.department_id');
        $model->leftjoin('branch','branch.id','employee_preferment.branch_id');
        // $model->leftjoin('groups','groups.id','employee_preferment.group_id');
        $model->select([
            'employee_preferment.*'
            ,'employee_preferment.id as employee_prefermentid'
            ,'employee.prename'
            ,'employee.empcode'
            ,\DB::raw('employee.firstname+\' \'+employee.lastname as employee_name')
            ,'employee.firstname'
            ,'employee.lastname'
            ,'employee.id as employeeid'
            ,'level.name as lname'
            ,'department.name as dname'
            ,'branch.branch_name as bname'
            // ,'groups.name as gname'
        ]);
        if(isset($request->date_start)&&isset($request->date_end)){$model->whereBetween('start_date',[$request->date_start,$request->date_end]);}
        if(isset($request->branch_id)){$model->where('branch_id',$request->branch_id);}
        if(isset($request->department_id)){$model->where('department_id',$request->department_id);}
        if(isset($request->level_id)){$model->where('level_id',$request->level_id);}
        if(isset($request->employee_id)){$model->where('employee_id',$request->employee_id);}
        
        return $model;
    }

    public function report(Request $request){
        $model = Employeeexperience::query();
        $model->leftjoin('employee','employee_experience.employee_id','employee.id');
        $model->leftjoin('level','level.id','employee.level_id');
        $model->leftjoin('department','department.id','employee.department_id');
        $model->leftjoin('groups','groups.id','employee.group_id');
        $model->leftjoin('education_level','employee_experience.education_level','education_level.id');
        $model->leftjoin('institutes','employee_experience.institute_id','institutes.id');
        $model->select([
            'employee_experience.*'
            ,'employee_experience.id as employee_experienceid'
            ,'employee.prename'
            ,'employee.empcode'
            ,\DB::raw('employee.firstname+\' \'+employee.lastname as employee_name')
            ,'employee.firstname'
            ,'employee.lastname'
            ,'employee.id as employeeid'
            ,'education_level.name as education_level_name'
            ,'institutes.name as institutes_name'
            ,'level.name as lname'
            ,'department.name as dname'
            ,'groups.name as gname'
        ]);
        if(isset($request->experience_type)){$model->where('experience_type',$request->experience_type);}
        if(isset($request->employee_id)){$model->where('employee_id',$request->employee_id);}
        if(isset($request->institute_id)){$model->where('institute_id',$request->institute_id);}
        if(isset($request->education_level)){$model->where('education_level',$request->education_level);}
        if(isset($request->place)){$model->where('place',$request->place);}
        if(isset($request->level)){$model->where('level',$request->level);}
        if(isset($request->subject)){$model->where('subject',$request->subject);}
        if(isset($request->date_start)&&isset($request->date_end)){$model->whereBetween('start_date',[$request->date_start,$request->date_end]);}
        // if(isset($request->date_end)){$model->where('end_date',$request->date_end);}
        return $model;
    }

    public function report_train(Request $request){
        $model = $this->report($request);
        $data['title'] = '';
        $data['request'] = $request->all();
        $data['employee'] = isset($request->employee_id)?ReportController::report_employee_detail($request->employee_id):'';
        try {
            if($request->report_type=='people'){
                $data['data'] = $model->orderBy('employee_experience.start_date','DESC')->get();
                $pdf = PDF::loadView('pdf.employee_train_people',$data);
                return @$pdf->stream('รายงานผลการฝึกอบรม(รายคน).pdf');
            }
            if($request->report_type=='list'){
                $data['data'] = $model->orderBy('employee.empcode','ASC')->get();
                $data['subject'] = $model->orderBy('employee_experience.start_date','DESC')->first()->subject;
                $pdf = PDF::loadView('pdf.employee_train_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
                return @$pdf->stream('รายงานผลการฝึกอบรม(หลักสูตร).pdf');
            }
            
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function report_warning(Request $request){
        $model = $this->query_warning($request);
        $data['title'] = '';
        $data['request'] = $request->all();
        $data['employee'] = isset($request->employee_id)?ReportController::report_employee_detail($request->employee_id):'';

        try {
            if($request->report_type=='people'){
                $data['data'] = $model->orderBy('employee_warning.start_date','DESC')->get();
                $pdf = PDF::loadView('pdf.employee_warn_people',$data);
                return @$pdf->stream('รายงานผลการเตือน(รายคน).pdf');
            }
            if($request->report_type=='list'){
                $data['data'] = $model->orderBy('employee.empcode','ASC')->get();
                $pdf = PDF::loadView('pdf.employee_warn_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
                return @$pdf->stream('รายงานผลการเตือน.pdf');
            }
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function report_position(Request $request){
        $model = $this->query_preferment($request);
        $data['title'] = '';
        $data['request'] = $request->all();
        $data['employee'] = isset($request->employee_id)?ReportController::report_employee_detail($request->employee_id):'';

        try {
            if($request->report_type=='people'){
                $data['data'] = $model->orderBy('employee_preferment.start_date','DESC')->get();
                $pdf = PDF::loadView('pdf.employee_position_people',$data);
                return @$pdf->stream('รายงานผลการฝึกอบรม(รายคน).pdf');
            }
            if($request->report_type=='list'){
                $data['data'] = $model->orderBy('employee.empcode','ASC')->get();
                $pdf = PDF::loadView('pdf.employee_train_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
                return @$pdf->stream('รายงานผลการฝึกอบรม(หลักสูตร).pdf');
            }
            
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Employeeexperience::insert($request->all())){
                    \DB::commit();
                    return "บันทึกสำเร็จ";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Employeeexperience::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            if($result = Employeeexperience::where('id',$id)->update($request->all())){
                \DB::commit();
                return "อัพเดทข้อมูลสำเร็จ";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Employeeexperience::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "ลบข้อมูลสำเร็จ";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}