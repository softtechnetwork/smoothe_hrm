<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Imports\ImportsEvaluation;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\Importevaluation;
use App\Models\Employeeevaluation;

class ImportController extends Controller
{
    public function get_evaluation(){
        $data['menu'] = 'นำเข้าข้อมูลการประเมิน';
        return view('admin.import.evalution')->with($data); // admin/import/evalution
    }

    public function post_evaluation(Request $request){
        $starttime = microtime(true);
        $truncate = Importevaluation::truncate();
        try {
            $import = Excel::import(new ImportsEvaluation, request()->file('file'));
            if(!$import){
                $endtime = microtime(true);
                $execution_time = ($endtime - $starttime);
                $duration = ceil($execution_time);
                $data['executetime'] = 'Total Execution Time : '.ROUND($duration).' second';
                $data['message'] = "รูปแบบไฟล์หรือข้อมูลไม่ถูกต้อง";
                $data['status'] = false;
                return $data;
            }
            $import_evaluation = Importevaluation::select([
                'e1.id as employee_id'
                ,'e2.id as employee_target_id'
                ,'import_evaluation.job'
                ,'import_evaluation.kpi'
                ,'import_evaluation.attitute'
            ])
            ->leftjoin(\DB::raw('employee e1'),'e1.empcode','import_evaluation.employee')
            ->leftjoin(\DB::raw('employee e2'),'e2.empcode','import_evaluation.employee_target')
            ->get();
            $check_insert_ee = false;
            if(count($import_evaluation)!=0){
                Employeeevaluation::truncate();
                $check_insert_ee = false;
                $array_result = array();
                if($import_evaluation){
                    foreach ($import_evaluation as $key => $value) {
                        if(isset($value->employee_id)&&isset($value->employee_target_id)){
                            $evaluation_ability = array();
                            if($value->job=='T'){
                                array_push($evaluation_ability,"1");
                            }
                            if($value->kpi=='T'){
                                array_push($evaluation_ability,"2");
                            }
                            if($value->attitute=='T'){
                                array_push($evaluation_ability,"3");
                            }
                            $item = array();
                            $item['employee_id'] = $value->employee_id;
                            $item['employee_target_id'] = $value->employee_target_id;
                            $item['evaluation_ability'] = json_encode($evaluation_ability);
                            $item['created_at'] = date('Y-m-d H:i:s');

                            $result = Employeeevaluation::insert($item);
                            if(!$result){
                                $check_insert_ee = false;
                            }
                        }
                    }
                }
            }
            $endtime = microtime(true);
            $execution_time = ($endtime - $starttime);
            $duration = ceil($execution_time);
            $data['executetime'] = 'Total Execution Time : '.ROUND($duration).' second';
            if( $check_insert_ee ){
                $data['message'] = "Import Complete!";
                $data['status'] = true;
                return $data;
            }else{
                $data['message'] = "Error!";
                $data['status'] = false;
                return $data;
            }
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}