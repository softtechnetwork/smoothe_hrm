<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController as API;
use App\Http\Controllers\FunctionController as FC;

use App\Models\Company;
use App\Models\Branch;
use App\Models\Groups;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Employeeregistration;
use App\Models\Employeeleave;
use App\Models\Employeeconfirm;
use App\Models\Employeeevaluation;
use App\Models\Leavetype;
use stdClass;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Shiftmanagement;
use App\Http\Controllers\Admin\EvaluationtypeController as ET;


use App\Models\Holiday;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;


use function GuzzleHttp\Promise\all;

class ReportController extends Controller
{
    public function attendance()
    {
        $data['employee'] = Employee::Active()->get();
        $data['menu'] = 'รายงานการเข้าออกงาน';
        return view('admin.report.attendance')->with($data); // admin/report/attendance
    }

    public function attendance_employee(Request $request)
    {
        $test = new \DateTime("$request->month-01 00:00:00");
        $month = date_format($test, 'Y-m');
        $request->date_start = date_format($test, 'Y-m-d');
        $request->date_end = $month.date_format($test, '-t');
        $data['employee'] = $this->report_employee_detail($request->employee_id);
        $registration =  $this->employee_registration($request);
        foreach ($registration as $key => $value) {
            $value->hr = \App\Http\Controllers\FunctionController::convert_minute_hr(intval($value->hr));
            $value->hr = ($value->hr==null) ? '-' : $value->hr;
            $data['registration'][] = $value;
        }
        $data['conclusion'] = $this->leave_conclusion($request);
        return $data;
    }

    public static function employee_registration(Request $request)
    {
        $test = new \DateTime("$request->month-01 00:00:00");
        $month = date_format($test, 'Y-m');
        $request->date_start = date_format($test, 'Y-m-d');
        $request->date_end = $month.date_format($test, '-t');

        $model = Employeeregistration::query();
        $model->leftjoin('employee', 'employee_registration.employee_id', 'employee.id');
        $model->select([
            \DB::raw("employee_registration.in_date as full_in_date"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.in_date),9,10) as in_date"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.in_time),1,8) as in_time"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.out_date),9,10) as out_date"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.out_time),1,8) as out_time"),
            \DB::raw("
                ( CASE WHEN (employee_registration.in_time <= CONVERT(time,'12:00:00') AND employee_registration.out_time >= CONVERT(time,'13:00:00'))
                THEN DATEDIFF(MINUTE,employee_registration.in_time,employee_registration.out_time)-60
                ELSE DATEDIFF(MINUTE,employee_registration.in_time,employee_registration.out_time)
                END ) as hr
            "),
            \DB::raw("CASE WHEN DATEDIFF( SECOND, '08:30:00',SUBSTRING(CONVERT(VARCHAR,employee_registration.in_time),1,8) ) > 0 THEN 'สาย' ELSE '' END AS status")
        ]);
        if (!empty($request->date_start) && !empty($request->date_end)) {
            $model->where('employee_registration.in_date', '>=', "$request->date_start");
            $model->where('employee_registration.in_date', '<=', "$request->date_end");
        }
        $model->where('employee_registration.employee_id', $request->employee_id);
        $result = $model->get();
        
        $model_el = Employeeleave::query();
        $model_el->where('leave_result', 'T');
        $model_el->leftjoin('leave_type', 'leave_type.id', 'employee_leave.leave_type_id');
        $model_el->select([
            \DB::raw("CONVERT(DATE,employee_leave.start_date) as full_in_date"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,CONVERT(DATE,employee_leave.start_date)),9,10) as in_date"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,CONVERT(DATE,employee_leave.end_date)),9,10) as out_date"),
            // \DB::raw("SUBSTRING(CONVERT(VARCHAR,CONVERT(DATE,employee_leave.start_date)),12,19) as in_time"),
            // \DB::raw("SUBSTRING(CONVERT(VARCHAR,CONVERT(DATE,employee_leave.end_date)),12,19) as out_time"),
            \DB::raw("'-' as in_time"),
            \DB::raw("'-' as out_time"),
            \DB::raw("0 as hr"),
            \DB::raw("leave_type.leave_name AS status"),
        ]);
        if (!empty($request->date_start) && !empty($request->date_end)) {
            $model_el->where('employee_leave.start_date', '>=', "$request->date_start");
            $model_el->where('employee_leave.end_date', '<=', "$request->date_end");
        }
        $model_el->where('employee_leave.employee_id', $request->employee_id);
        
        // $result = $model->union($model_el)->get();
        $get_leave_list = $model_el->get();
        
        $leave_list = array();
        foreach ($get_leave_list as $key => $value) {
            $start = (int)$value->in_date;
            $end = (int)$value->out_date;
            
            $full_in_date = $value->full_in_date;
            $plus = 0;
            for ($i=$start; $i<=$end; $i++) {
                $obj_leave = new stdClass();
                $full =  date('Y-m-d', strtotime($full_in_date." + $plus days"));
                $dayname = date("l", strtotime($full));
                $convert = FC::convert_minute_to_hr($value->hr);
                $obj_leave->full_in_date = $full;
                $obj_leave->in_date = sprintf("%02d", ($i));
                $obj_leave->in_time = '-';
                $obj_leave->out_time = '-';
                $obj_leave->hr = '00';
                $obj_leave->h = '00';
                $obj_leave->m = '00';
                $obj_leave->hm = '00:00';
                $obj_leave->status = $value->status;
                $obj_leave->dayname = FC::convert_day_thai($dayname);
                $obj_leave->date_th = $obj_leave->dayname;
                $leave_list[$i] = $obj_leave;
                $plus++;
            }
        }
        // return $leave_list;
        // $result = $model->union($model_el)->get();
        $return = array();
        $loops = (int)date('t', strtotime($request->date_start));
        foreach ($result as $key => $value) {
            $strcount = (int)date("j", strtotime($value->full_in_date));
            $value->dayname = date("l", strtotime($value->full_in_date));
            $return[$strcount-1] = $value;
        }
        $ym = date("Y-m-", strtotime($request->date_start));
        $return_resutl = array();

        $leave_list;

        for ($i=0; $i < $loops; $i++) {
            $obj = new stdClass();
            if (!empty($return[$i])) {
                $dayname = date("l", strtotime($return[$i]->full_in_date));
                $convert = FC::convert_minute_to_hr($return[$i]->hr);
                $obj->full_in_date = $return[$i]->full_in_date;
                $obj->in_date = $return[$i]->in_date;
                $obj->in_time = $return[$i]->in_time;
                $obj->out_time = $return[$i]->out_time;
                $obj->hr = ($return[$i]->hr)?trim($return[$i]->hr):'00';
                $obj->h = ($convert->h)?trim($convert->h):'00';
                $obj->m = ($convert->m)?trim($convert->m):'00';
                $obj->hm = $obj->h.":".$obj->m;
                $obj->status = $return[$i]->status;
                $obj->dayname = FC::convert_day_thai($dayname);
                $obj->date_th = $obj->dayname;
                // $return_result[$i] = $obj;
                $return_result[] = $obj;
            } else {
                if (!isset($leave_list[$i+1])) {
                    $fulldate = $ym.sprintf("%02d", ($i+1));
                    $dayname = date("l", strtotime($fulldate));
                    $obj->full_in_date = $fulldate;
                    $obj->in_date = sprintf("%02d", ($i+1));
                    $obj->in_time = '-';
                    $obj->out_time = '-';
                    $obj->hr = '00';
                    $obj->h = '00';
                    $obj->m = '00';
                    $obj->hm = '00:00';
                    $obj->status = '-';
                    $obj->dayname = FC::convert_day_thai($dayname);
                    $obj->date_th = $obj->dayname;
                    $return_result[] = $obj;
                }
            }
            if (isset($leave_list[$i+1])) {
                $return_result[] = $leave_list[$i+1];
            }
        }
        return $return_result;
    }

 
//     public static function leave_conclusion(Request $request)
//     {
//         if (!$request->has('month')) {
//             $request->request->add(['month' => now()->endOfYear()->format('Y-m')]);
//         }
//         $end = Carbon::make("$request->month-01 00:00:00")->endOfMonth()->format(Carbon::DEFAULT_TO_STRING_FORMAT);
//         $month = Carbon::make("$request->month-01 00:00:00")->endOfMonth()->format('Y-m');
//         $start = Carbon::make("$request->month-01 00:00:00")->endOfMonth()->format('Y');
//         $start = Carbon::make("$start-01-01");
//         $request->date_start = $start->format('Y-m-d');
//         $request->date_end = $end;

//         $leave_conclusion = [];

//         $startDate = Carbon::make($request->get('month'))->startOfYear()->format('Y-m-d');
//         $endDate = Carbon::make($request->get('month'))->endOfMonth()->format('Y-m-d');

//         $employeeLeaves = Employeeleave::query()
//             ->where('leave_result', 'T')
//             ->where('employee_id', $request->get('employee_id'))
//             ->where(
//                 function (Builder $query) use ($startDate, $endDate) {
//                     $query->where(
//                         function (Builder $query) use ($startDate, $endDate) {
//                             $query->where('start_date', '>=', $startDate)
//                                 ->where('start_date', '<=', "{$endDate} 23:59:59");
//                         }
//                     )
//                         ->orWhere(
//                             function (Builder $query) use ($startDate, $endDate) {
//                                 $query->where('end_date', '>=', $startDate)
//                                     ->where('end_date', '<=', "{$endDate} 23:59:59");
//                             }
//                         );
//                 }
//             )
//             ->get();


//         $shiftManagements = Shiftmanagement::query()
//             ->where('employee_id', $request->get('employee_id'))
//             ->whereBetween('date', [$startDate, $endDate])
//             ->with('shift')
//             ->get();


//         $leaveTypes = Leavetype::query()
//             ->where('status', 'T')
//             ->get();

//         $employeeLeaves->each(
//             function (Employeeleave $employeeLeave) use ($shiftManagements, $startDate, $endDate) {
//                 if ($employeeLeave->end_date > $endDate) {
//                     $date = Carbon::make($employeeLeave->end_date);
//                     $employeeLeave->true_end_date = Carbon::make($endDate)
//                         ->setTime($date->hour, $date->minute, $date->second)
//                         ->format('Y-m-d H:i:s');
//                 } else {
//                     $employeeLeave->true_end_date = $employeeLeave->end_date;
//                 }

//                 if ($employeeLeave->start_date < $startDate) {
//                     $date = Carbon::make($employeeLeave->start_date);
//                     $employeeLeave->true_start_date = Carbon::make($startDate)
//                         ->setTime($date->hour, $date->minute, $date->second)
//                         ->format('Y-m-d H:i:s');
//                 } else {
//                     $employeeLeave->true_start_date = $employeeLeave->start_date;
//                 }

//                 $trueStartDate = Carbon::make($employeeLeave->true_start_date);
//                 $trueEndDate = Carbon::make($employeeLeave->true_end_date);

//                 $diffMonth = ((int)$trueEndDate->month) - ((int)$trueStartDate->month);

//                 $endFrom = $trueEndDate->day;

//                 if ($diffMonth !== 0) {
//                     $endFrom = (clone $trueStartDate)->endOfMonth()->day;
//                 }

//                 $minutes = 0;
//                 if ($trueStartDate->diffInDays($trueEndDate) === 0) {
//                     $minutes += $trueStartDate->diffInMinutes($trueEndDate);
//                     $hasShiftManagement = self::getHasShiftManagement($shiftManagements, $trueStartDate);

//                     if ($hasShiftManagement) {
//                         $minutes = self::getSumMinuteDiffShiftManagement(
//                             $hasShiftManagement,
//                             $trueStartDate,
//                             $trueEndDate,
//                             $minutes
//                         );
//                     }

//                     if ($trueStartDate->hour < 12 && $trueEndDate->hour > 13) {
//                         $minutes -= 60;
//                     }
//                 } else {
//                     $startFrom = $trueStartDate->day;

//                     $month = $trueStartDate->format('Y-m');

//                     $sDate = (clone $trueStartDate);
//                     $eDate = (clone $trueEndDate);

//                     for ($round = $startFrom; $round <= $endFrom; $round++) {
//                         $minutes = self::sumMinuteWithMultipleLeave(
//                             $month,
//                             $round,
//                             $sDate,
//                             $eDate,
//                             $startFrom,
//                             $endFrom,
//                             $minutes,
//                             $shiftManagements
//                         );
//                     }


//                     //case diff month more than 1 month
//                     if ($diffMonth) {
//                         $startFrom = 0;
//                         $endFrom = $trueEndDate->day + 1;
//                         $month = (clone $trueStartDate)->addMonth()->startOfMonth()->format('Y-m');

//                         for ($round = 1; $round <= $endFrom; $round++) {
//                             $minutes = self::sumMinuteWithMultipleLeave(
//                                 $month,
//                                 $round,
//                                 $sDate,
//                                 $eDate,
//                                 $startFrom,
//                                 $endFrom,
//                                 $minutes,
//                                 $shiftManagements
//                             );
//                         }
//                     }
//                 }

//                 $employeeLeave->used_day = $minutes;
//             }
//         );

//         $leave_list = $leaveTypes->each(
//             function ($leaveType) use ($employeeLeaves) {
//                 $leaveType->amount_day = $leaveType->amount * 8 * 60;
//                 $leaveType->used_day = $employeeLeaves->filter(
//                     function ($employeeLeave) use ($leaveType) {
//                         return $employeeLeave->leave_type_id == $leaveType->id;
//                     }
//                 )->sum('used_day');
//             }
//         );

// //        $leave_list = \DB::select("
// //        SELECT
// //            lt.leave_name
// //            ,lt.id
// //            ,lt.detail as leave_detail
// //            ,(lt.amount * 8 * 60) as amount_day
// //            ,(
// //            SELECT
// //                    SUM(
// //                            CASE WHEN DATEDIFF(DAY, el.start_date , el.end_date)<=0
// //                            THEN
// //                                    (
// //                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
// //                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
// //                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
// //                                            END
// //                                    )
// //                            ELSE
// //                                    (
// //                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
// //                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
// //                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
// //                                            END
// //                                    )
// //                                    +
// //                                    (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8 * 60 )
// //                                    +
// //                                    (
// //                                            CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
// //                                            THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
// //                                            ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
// //                                            END
// //                                    )
// //                            END
// //                    )
// //                            FROM employee_leave el
// //                WHERE el.leave_type_id=lt.id
// //                AND el.employee_id = $request->employee_id
// //                AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday')
// //                AND el.leave_result='T'
// //                AND (el.start_date>='".$request->date_start."' OR el.end_date<='".$request->date_end."')
// //            ) as used_day
// //        FROM leave_type lt
// //        GROUP BY lt.id,lt.leave_name,lt.amount,lt.detail;
// //        ");

//         foreach ($leave_list as $key => $item) {
//             $amount_day = \App\Http\Controllers\FunctionController::convert_minute_day(intval($item->amount_day));
//             $used_day = \App\Http\Controllers\FunctionController::convert_minute_day(intval($item->used_day));
//             if ($item->amount_day <= $item->used_day) {
//                 $item->balance_day = '0วัน';
//             } else {
//                 $item->balance_day = \App\Http\Controllers\FunctionController::convert_minute_day(
//                     $item->amount_day - $item->used_day
//                 );
//             }
//             $item->amount_day = $amount_day;
//             $item->used_day = $used_day;
//             $leave_conclusion[] = $item;
//         }
//         return $leave_conclusion;
//     }

public static function leave_conclusion(Request $request)
{
    if (!$request->has('month')) {
        $request->request->add(['month' => now()->endOfYear()->format('Y-m')]);
    }
    $end = Carbon::make("$request->month-01 00:00:00")->endOfMonth()->format(Carbon::DEFAULT_TO_STRING_FORMAT);
    $month = Carbon::make("$request->month-01 00:00:00")->endOfMonth()->format('Y-m');
    $start = Carbon::make("$request->month-01 00:00:00")->endOfMonth()->format('Y');
    $start = Carbon::make("$start-01-01");
    $request->date_start = $start->format('Y-m-d');
    $request->date_end = $end;

    $leave_conclusion = [];

    $startDate = Carbon::make($request->get('month'))->startOfYear()->format('Y-m-d');
    $endDate = Carbon::make($request->get('month'))->endOfMonth()->format('Y-m-d');
   // ->where('leave_result', 'T')
    $employeeLeaves = Employeeleave::query()
      
        ->where('employee_id', $request->get('employee_id'))
        ->where('leave_result', 'T')
        ->where(
            function (Builder $query) use ($startDate, $endDate) {
                $query->where(
                    function (Builder $query) use ($startDate, $endDate) {
                        $query->where('start_date', '>=', $startDate)
                            ->where('start_date', '<=', "{$endDate} 23:59:59");
                    }
                )
                    ->orWhere(
                        function (Builder $query) use ($startDate, $endDate) {
                            $query->where('end_date', '>=', $startDate)
                                ->where('end_date', '<=', "{$endDate} 23:59:59");
                        }
                    );
            }
        )
        ->get();
         //echo 'emp id : '.$request->get('employee_id').' leave_result '.'T' .''.$startDate.' || '. $endDate;
         //    echo $employeeLeaves;exit();

    $shiftManagements = Shiftmanagement::query()
        ->where('employee_id', $request->get('employee_id'))
        ->whereBetween('date', [$startDate, $endDate])
        ->with('shift')
        ->get();


    $leaveTypes = Leavetype::query()
        ->where('status', 'T')
        ->get();
    
    // echo '<PRE>';
    // print_r($employeeLeaves);exit();
    $employeeLeaves->each(
        function (Employeeleave $employeeLeave) use ($shiftManagements, $startDate, $endDate) {
           
            if ($employeeLeave->end_date > $endDate) {
                $date = Carbon::make($employeeLeave->end_date);
                $employeeLeave->true_end_date = Carbon::make($endDate)
                    ->setTime($date->hour, $date->minute, $date->second)
                    ->format('Y-m-d H:i:s');
            } else {
                $employeeLeave->true_end_date = $employeeLeave->end_date;
            }

            if ($employeeLeave->start_date < $startDate) {
                $date = Carbon::make($employeeLeave->start_date);
                $employeeLeave->true_start_date = Carbon::make($startDate)
                    ->setTime($date->hour, $date->minute, $date->second)
                    ->format('Y-m-d H:i:s');
            } else {
                $employeeLeave->true_start_date = $employeeLeave->start_date;
            }

            $trueStartDate = Carbon::make($employeeLeave->true_start_date);
            $trueEndDate = Carbon::make($employeeLeave->true_end_date);

            $diffMonth = ((int)$trueEndDate->month) - ((int)$trueStartDate->month);
         
            $endFrom = $trueEndDate->day;

            if ($diffMonth !== 0) {
                $endFrom = (clone $trueStartDate)->endOfMonth()->day;
            }
          
            // if($employeeLeave->leave_type_id == 3){
            //   //  echo
            //     print_r($employeeLeave->true_start_date);
            //     print_r($employeeLeave->true_end_date);
            //     exit();
            // }
            $minutes = 0;
            if ($trueStartDate->diffInDays($trueEndDate) === 0) {
                $minutes += $trueStartDate->diffInMinutes($trueEndDate);
                
                $hasShiftManagement = self::getHasShiftManagement($shiftManagements, $trueStartDate);

                if ($hasShiftManagement) {
                    $minutes = self::getSumMinuteDiffShiftManagement(
                        $hasShiftManagement,
                        $trueStartDate,
                        $trueEndDate,
                        $minutes
                    );
                }

                if ($trueStartDate->hour < 12 && $trueEndDate->hour > 13) {
                    $minutes -= 60;
                }
            } else {
                $startFrom = $trueStartDate->day;

                $month = $trueStartDate->format('Y-m');

                $sDate = (clone $trueStartDate);
                $eDate = (clone $trueEndDate);
               // echo $eDate;exit();
                for ($round = $startFrom; $round <= $endFrom; $round++) {
                    $minutes = self::sumMinuteWithMultipleLeave(
                        $month,
                        $round,
                        $sDate,
                        $eDate,
                        $startFrom,
                        $endFrom,
                        $minutes,
                        $shiftManagements
                    );
                   // echo $minutes.'||';exit();
                }


                //case diff month more than 1 month
                if ($diffMonth) {
                    $startFrom = 0;
                    $endFrom = $trueEndDate->day + 1;
                    $month = (clone $trueStartDate)->addMonth()->startOfMonth()->format('Y-m');

                    for ($round = 1; $round <= $endFrom; $round++) {
                        $minutes = self::sumMinuteWithMultipleLeave(
                            $month,
                            $round,
                            $sDate,
                            $eDate,
                            $startFrom,
                            $endFrom,
                            $minutes,
                            $shiftManagements
                        );
                       
                    }
                }
            }

            $employeeLeave->used_day = $minutes;
        }
    );
   
    
    $leave_list = $leaveTypes->each(
        function ($leaveType) use ($employeeLeaves) {
            //echo $leaveType->amount.' id ->' . $leaveType->id.' ||';
            $leaveType->amount_day = $leaveType->amount * 8.50 * 60;
          
            $leaveType->used_day = $employeeLeaves->filter(
                function ($employeeLeave) use ($leaveType) {
                    
                    return $employeeLeave->leave_type_id == $leaveType->id;
                }
            )->sum('used_day');

        }
    );
    // echo '<PRE>';
    // print_r($leave_list);exit();
  
    // exit();
    //print_r($leave_list);exit();
//        $leave_list = \DB::select("
//        SELECT
//            lt.leave_name
//            ,lt.id
//            ,lt.detail as leave_detail
//            ,(lt.amount * 8 * 60) as amount_day
//            ,(
//            SELECT
//                    SUM(
//                            CASE WHEN DATEDIFF(DAY, el.start_date , el.end_date)<=0
//                            THEN
//                                    (
//                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00') AND el.end_date >= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'13:00:00'))
//                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) - 60)
//                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ))
//                                            END
//                                    )
//                            ELSE
//                                    (
//                                            CASE WHEN (el.start_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'12:00:00'))
//                                            THEN (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ) - 60)
//                                            ELSE (DATEDIFF(MINUTE, CONVERT(datetime,el.start_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.start_date))+' '+'17:00:00') ))
//                                            END
//                                    )
//                                    +
//                                    (DATEDIFF(DAY, CONVERT(datetime,el.start_date) , CONVERT(datetime,el.end_date) ) * 8 * 60 )
//                                    +
//                                    (
//                                            CASE WHEN (el.end_date <= CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'12:00:00'))
//                                            THEN DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') ) - 60
//                                            ELSE DATEDIFF(MINUTE, CONVERT(datetime,el.end_date) , CONVERT(datetime,CONVERT(varchar, CONVERT(date,el.end_date))+' '+'17:00:00') )
//                                            END
//                                    )
//                            END
//                    )
//                            FROM employee_leave el
//                WHERE el.leave_type_id=lt.id
//                AND el.employee_id = $request->employee_id
//                AND DATENAME(weekday, el.start_date) NOT IN (N'Saturday', N'Sunday')
//                AND el.leave_result='T'
//                AND (el.start_date>='".$request->date_start."' OR el.end_date<='".$request->date_end."')
//            ) as used_day
//        FROM leave_type lt
//        GROUP BY lt.id,lt.leave_name,lt.amount,lt.detail;
//        ");
    // echo '<PRE>';
    // print_r($leave_list);exit();
    foreach ($leave_list as $key => $item) {
        $amount_day = \App\Http\Controllers\FunctionController::convert_minute_day(intval($item->amount_day));
        //echo $amount_day.'||';
        $used_day = \App\Http\Controllers\FunctionController::convert_minute_day(intval($item->used_day));
      
        if ($item->amount_day <= $item->used_day) {
            $item->balance_day = '0วัน';
        } else {
            $item->balance_day = \App\Http\Controllers\FunctionController::convert_minute_day(
                $item->amount_day - $item->used_day
            );
        }

        if($item->id == 1){
            //echo $used_day;
            //echo $item->used_day;exit();
        }
        $item->amount_day = $amount_day;
        //echo $used_day;exit();
        $item->used_day = $used_day;
        $leave_conclusion[] = $item;
    }
    //print_r($leave_conclusion);exit();
    return $leave_conclusion;
}

    public static function check_confirm(Request $request)
    {
        $employee_id = $request->employee_id;
        $month = $request->month;
        $result = Employeeconfirm::where([
            ['employee_id',"=",$employee_id],
            ['month','=',$month]
        ])->first();
        if ($result) {
            return isset($result->created_at) ? "ยืนยันแล้ว" .' ('. \App\Http\Controllers\FunctionController::DateThai($result->created_at) .')' : 'ไม่ยืนยัน';
        } else {
            return 'ไม่ยืนยัน';
        }
    }

    public function attendance_all()
    {
        $data['company'] = Company::Active()->get();
        $data['branch'] = Branch::Active()->get();
        $data['group'] = Groups::Active()->get();
        $data['department'] = Department::Active()->get();
        $data['employee'] = Employee::Active()->get();
        $data['menu'] = 'รายงานการเข้าออกงาน(รวม)';
        return view('admin.report.attendance_all')->with($data); // admin/report/attendance_all
    }

    public function attendance_all_list(Request $request)
    {
        $model = $this->report_attendance($request);
        return  \DataTables::eloquent($model)
        ->addIndexColumn()
        ->toJson();
    }

    public function evaluation_check()
    {
        $data['evaluation'] = \App\Models\Evaluation::get();
        return view('admin.report.evaluation_check')->with($data); // admin/report/evalution
    }

    public function evaluation_check_list(Request $request)
    {
        return $query = Employeeevaluation::select([
            \DB::raw('e.empcode')
            ,\DB::raw("e.firstname+' '+e.lastname as name")
            ,\DB::raw('l.name as lname')
            ,\DB::raw('d.name as dname')
            ,\DB::raw('g.name as gname')
            ,\DB::raw('b.branch_name as bname')
            ,\DB::raw("COUNT(DISTINCT employee_evaluation.employee_target_id) as total")
            ,\DB::raw("(SELECT COUNT(DISTINCT er.employee_target_id) FROM evaluation_result er WHERE er.employee_id = employee_evaluation.employee_id AND er.evaluation_id=".$request->evaluation_id.") as assessed")
            ,\DB::raw("(
                COUNT(DISTINCT employee_evaluation.employee_target_id)
                    -
                (SELECT COUNT(DISTINCT er.employee_target_id) FROM evaluation_result er WHERE er.employee_id = employee_evaluation.employee_id)
            ) as balance")
            ,\DB::raw("
            CASE
                WHEN
                (COUNT(DISTINCT employee_evaluation.employee_target_id)-(SELECT COUNT(DISTINCT er.employee_target_id) FROM evaluation_result er WHERE er.employee_id = employee_evaluation.employee_id AND er.evaluation_id=".$request->evaluation_id."))
                >0 THEN 'F'
                ELSE 'T'
            END
            as status
            ")
        ])
        ->leftjoin(\DB::raw('employee e'), 'employee_id', 'e.id')
        ->leftjoin(\DB::raw('level l'), 'l.id', 'e.level_id')
        ->leftjoin(\DB::raw('department d'), 'd.id', 'e.department_id')
        ->leftjoin(\DB::raw('groups g'), 'g.id', 'e.group_id')
        ->leftjoin(\DB::raw('branch b'), 'b.id', 'e.branch_id')
        ->groupBy([
            'employee_evaluation.employee_id'
            ,'e.empcode'
            ,\DB::raw("(e.firstname+' '+e.lastname)")
            ,'l.name'
            ,'d.name'
            ,'g.name'
            ,'b.branch_name'
        ])
        ->get();
    }

    public static function report_employee_detail($employee_id)
    {
        return Employee::where('employee.id', $employee_id)->Active()
        ->leftjoin('company', 'company.id', 'employee.company_id')
        ->leftjoin('branch', 'branch.id', 'employee.branch_id')
        ->leftjoin('groups', 'groups.id', 'employee.group_id')
        ->leftjoin('level', 'level.id', 'employee.level_id')
        ->select(['employee.*','company.name as cname','groups.name as gname','branch.branch_name as bname','level.name as lname'])
        ->first();
    }

    public static function report_attendance(Request $request)
    {
        // $working_time_start = '08:00:00';
        if (isset($request->employee_id)) {
            $employee_id = $request->employee_id;
        }
        $model = Employeeregistration::query();
        $model->leftjoin('employee', 'employee_registration.employee_id', 'employee.id');
        $model->leftjoin('company', 'company.id', 'employee.company_id');
        $model->leftjoin('department', 'department.id', 'employee.department_id');
        $model->leftjoin('branch', 'branch.id', 'employee.branch_id');
        $model->leftjoin('groups', 'groups.id', 'employee.group_id');
        $model->leftjoin('level', 'level.id', 'employee.level_id');
        $model->select([
            \DB::raw("employee.firstname +' '+ employee.lastname as fullname"),
            \DB::raw("employee_registration.in_date as full_in_date"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.in_date),9,10) as in_date"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.in_time),1,8) as in_time"),
            \DB::raw("employee_registration.in_reason"),
            \DB::raw("SUBSTRING(CONVERT(VARCHAR,employee_registration.out_time),1,8) as out_time"),
            \DB::raw("employee_registration.out_reason"),
            \DB::raw("
            RIGHT('00' + 
                CONVERT(NVARCHAR,CASE WHEN (employee_registration.in_time < '12:00:00')
                THEN ROUND(CONVERT( INT , (DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time) - 60)/60 ),0)
                ELSE ROUND(CONVERT( INT , DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)/60 ),0)
                END)
            ,2)
            +':'+
            RIGHT('00' + 
                CONVERT(NVARCHAR,CASE WHEN employee_registration.in_time < '12:00:00' 
                THEN DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)-(ROUND(CONVERT( INT , DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)/60 ),0)*60)
                ELSE DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)-(ROUND(CONVERT( INT , DATEDIFF(MINUTE, employee_registration.in_time , employee_registration.out_time)/60 ),0)*60)
                END)
            ,2) as hr
            "),
            \DB::raw("CASE WHEN DATEDIFF( SECOND, branch.working_time_start,employee_registration.in_time ) > 0 THEN 'สาย' ELSE '' END AS status"),
            'company.name as cname',
            'branch.branch_name as bname',
            'department.name as dname',
            'groups.name as gname',
            'employee_registration.created_at as created_at'
        ]);
        if (!empty($request->date_start) && !empty($request->date_end)) {
            $model->whereBetween('employee_registration.in_date', [$request->date_start , $request->date_end]);
        }
        if (!empty($request->company_id)) {
            $model->where('employee.company_id', "$request->company_id");
        }
        if (!empty($request->branch_id)) {
            $model->where('employee.branch_id', $request->branch_id);
        }
        if (!empty($request->group_id)) {
            $model->where('employee.group_id', $request->group_id);
        }
        if (!empty($request->department_id)) {
            $model->where('employee.department_id', $request->department_id);
        }
        if (!empty($request->employee_id)) {
            $model->where('employee.id', $request->employee_id);
        }
        return $model;
    }

    public function report_evaluation_view(Request $request)
    {
        $data['evaluation'] = \App\Models\Evaluation::get();
        return view('admin.report.evaluation')->with($data); // admin/report/evalution
    }

    public function report_evaluation_data(Request $request)
    {
        $time_start = microtime(true);
        $query = Employee::select([
            'employee.id as employee_id'
            ,\DB::raw("'".url('/')."'+employee.picture_profile as picture_profile")
            ,'employee.empcode as employee_code'
            ,\DB::raw("employee.firstname+' '+employee.lastname as name")
            ,'employee_level.name as employee_level_name'
            ,'groups.name as group_name'
            ,'department.name as department_name'
            ,'level.name as position_name'
            ,'branch.branch_name'
            ,\DB::raw("
                (SELECT TOP 1 eo.prename+eo.firstname+' '+eo.lastname FROM employee eo WHERE eo.id = (SELECT s.employee_id FROM structure_pivot s WHERE s.box_id = structure_pivot.box_top_id)) as leader_name
            ")
        ]);
        $query->leftjoin('employee_level', 'employee.employee_level_id', 'employee_level.id');
        $query->leftjoin('groups', 'employee.group_id', 'groups.id');
        $query->leftjoin('department', 'employee.department_id', 'department.id');
        $query->leftjoin('level', 'employee.level_id', 'level.id');
        $query->leftjoin('branch', 'employee.branch_id', 'branch.id');
        $query->leftjoin('structure_pivot', 'structure_pivot.employee_id', 'employee.id');
        $query->orderBy('employee.id', 'ASC');
        $query->active();
        $results = $query->get();

        $result = array();
        foreach ($results as $key => $value){
            $items = ET::query_get_value($value->employee_id,$request['evaluation_id']);
            foreach ($items as $key => $item) {
                $value->{ strtolower(substr($item->name, 0, 1)) } = ROUND($item->score_float,2);
            }
            $value->score = ROUND(FC::total_score($items),2);
            $value->grade = FC::grade($value->score);
            $result[] = $value;
        }
        $data['data'] = $result;
        $time_end = microtime(true);
        $data['excutetime'] = ($time_end - $time_start);
        return $data;
    }

    public function report_evaluation_data_old(Request $request)
    {
        $time_start = microtime(true);
        $query = Employee::select([
            'employee.id as employee_id'
            ,\DB::raw("'".url('/')."'+employee.picture_profile as picture_profile")
            ,'employee.empcode as employee_code'
            ,\DB::raw("employee.firstname+' '+employee.lastname as name")
            ,'employee_level.name as employee_level_name'
            ,'groups.name as group_name'
            ,'department.name as department_name'
            ,'level.name as position_name'
            ,'branch.branch_name'
            ,\DB::raw("
                (SELECT TOP 1 eo.prename+eo.firstname+' '+eo.lastname FROM employee eo WHERE eo.id = (SELECT s.employee_id FROM structure_pivot s WHERE s.box_id = structure_pivot.box_top_id)) as leader_name
            ")
        ]);
        $query->leftjoin('employee_level', 'employee.employee_level_id', 'employee_level.id');
        $query->leftjoin('groups', 'employee.group_id', 'groups.id');
        $query->leftjoin('department', 'employee.department_id', 'department.id');
        $query->leftjoin('level', 'employee.level_id', 'level.id');
        $query->leftjoin('branch', 'employee.branch_id', 'branch.id');
        $query->leftjoin('structure_pivot', 'structure_pivot.employee_id', 'employee.id');
        $query->orderBy('employee.id', 'ASC');
        $query->active();
        $results = $query->get();

        $result = array();
        foreach ($results as $key => $value){
            $items = ET::calculate_result($employee_id=$value->employee_id, $request['evaluation_start'], $request['evaluation_end'], $request['evaluation_id']);
            foreach ($items['evaluation_type'] as $key => $item) {
                $value->{ strtolower(substr($item->name, 0, 1)) } = $item->score;
            }
            $value->grade =  $items['grade'];
            $value->score = $items['score'];
            $value->jka = $items['items'];
            $result[] = $value;
        }
        $data['data'] = $result;
        $time_end = microtime(true);
        $data['excutetime'] = ($time_end - $time_start);
        return $data;
    }


    /**
     * @param string $month
     * @param int $round
     * @param Carbon|null $sDate
     * @param Carbon|null $eDate
     * @param int $startFrom
     * @param int $endFrom
     * @param int $minutes
     * @param Collection $shiftManagements
     * @return float|int
     */
    protected static function sumMinuteWithMultipleLeave(
        string $month,
        int $round,
        ?Carbon $sDate,
        ?Carbon $eDate,
        int $startFrom,
        int $endFrom,
        int $minutes,
        Collection $shiftManagements
    ) {
        $day = sprintf('%02d', $round);
        $thisDay = "{$month}-{$day}";
        $workingStartDay = Carbon::make($thisDay);
        $workingEndDay = Carbon::make($thisDay);

        $hasShiftManagement = self::getHasShiftManagement($shiftManagements, $workingStartDay);

        $dayForCondition = Carbon::make($thisDay);
        $startDayForCondition = clone ($dayForCondition);
        $endDayForCondition = clone ($dayForCondition);
        $holidaies = Holiday::query()->where(['status' => 'T'])->whereBetween(
            'holiday_date',
            [
                $startDayForCondition->startOfYear()->format('Y-m-d'),
                $endDayForCondition->endOfYear()->format(
                    'Y-m-d'
                )
            ]
        )->pluck('holiday_date')->toArray();

        if (($dayForCondition->isWeekend() && $dayForCondition->year === 2020) && $hasShiftManagement->isEmpty()) {
            return $minutes;
        }

        if (in_array($thisDay, $holidaies)) {
            return $minutes;
        }
     

        $start = $workingStartDay->setTime($sDate->hour, $sDate->minute, $sDate->second);
        $end = $workingEndDay->setTime($eDate->hour, $eDate->minute, $eDate->second);
       // echo $sDate->hour.'||';exit();
//        dump("ruond: $round endfoen $endFrom");
        if ($round !== $startFrom && $round !== $endFrom) {
            $minutes += 8.50 * 60;
        } else {
            $minutes += $start->diffInMinutes($end);
            //echo $start;
            if ($start->hour < 12 && $end->hour > 13) {
                $minutes -= 60;
            }
           
        }

        //echo $minutes;exit();

        if ($hasShiftManagement) {
            $minutes = self::getSumMinuteDiffShiftManagement(
                $hasShiftManagement,
                $start,
                $end,
                $minutes
            );
        }
        return $minutes;
    }

      /**
     * @param Collection $shiftManagements
     * @param Carbon|null $trueStartDate
     * @return Collection
     */
    protected static function getHasShiftManagement(Collection $shiftManagements, ?Carbon $trueStartDate)
    {
        $hasShiftManagement = $shiftManagements->filter(
            function (Shiftmanagement $shiftManagement) use ($trueStartDate) {
                return $shiftManagement->date == $trueStartDate->format('Y-m-d');
            }
        );
        return $hasShiftManagement;
    }

    /**
     * @param $hasShiftManagement
     * @param Carbon|null $trueStartDate
     * @param Carbon|null $trueEndDate
     * @param int $minutes
     * @return int
     */
    protected static function getSumMinuteDiffShiftManagement(
        $hasShiftManagement,
        ?Carbon $trueStartDate,
        ?Carbon $trueEndDate,
        int $minutes
    ): int {
        $minutes -= $hasShiftManagement->each(
            function (Shiftmanagement $shiftManagement) use ($trueStartDate, $trueEndDate, $minutes) {
                if (!Arr::has($shiftManagement, 'shift')) {
                    $shiftManagement->minutes = 0;
                    return;
                }
                $manageShiftStartDate = Carbon::make("{$shiftManagement->date} {$shiftManagement->shift->time_start}");
                $manageShiftEndDate = Carbon::make("{$shiftManagement->date} {$shiftManagement->shift->time_end}");

                $min = 0;
                if ($manageShiftStartDate > $trueStartDate) {
                    $min += $manageShiftStartDate->diffInMinutes($trueStartDate);
                }
                if ($manageShiftEndDate < $trueEndDate) {
                    $min += $manageShiftEndDate->diffInMinutes($trueEndDate);
                }

                $shiftManagement->minutes = $min;
            }
        )->sum('minutes');
        return $minutes;
    }

}