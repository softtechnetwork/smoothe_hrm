var Employeeleave = (function () {
    var handleTables = function () {
      var table = $('#employeeleave').DataTable({
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": rurl + 'admin/employeeleave/list_employee',
        "language": { "url" : rurl + "assets/plugins/datatable_th.json" },
        "columns": [{
            "data": 'DT_RowIndex',
            "name": 'DT_RowIndex',
            orderable: false,
            searchable: false,
            className:"text-center"
          },
          {"data":"leave_name","name":"leave_type.leave_name"},
					{"data":"duration_name","name":"leave_duration.duration_name"},
          {"data":"start_date","name":"employee_leave.start_date"},
          {"data":"end_date","name":"employee_leave.end_date"},
          {"data":"ep1firstname","name":"ep1.firstname"},
          {"data":"ep1lastname","name":"ep1.lastname","visible":false},
					{"data":"remark","name":"employee_leave.remark"},
          {"data":"ep2firstname","name":"ep2.firstname"},
          {"data":"ep2lastname","name":"ep2.lastname","visible":false},
					{"data":"approved_at","name":"employee_leave.approved_at"},
					{"data":"leave_result","name":"employee_leave.leave_result"},
        ],
        "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
        "lengthMenu": [
            //[10, 25, 50, -1],
            //[10, 25, 50, "All"]
          [50, 100, 200, 300, 400, 1000],
          [50, 100, 200, 300, 400, 1000]
        ],
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-default btn-sm',
                text: '<i class="fas fa-file-excel"></i> Excel'
            },
            {
                extend: 'print',
                className: 'btn btn-default btn-sm',
                text: '<i class="fas fa-print"></i> Print',
                orientation: 'landscape'
            },
        ],
      });
    }
  
    var handleValidation = function () {
      var form = $('.validateForm');
      var btn = $('.validateForm [type="submit"]');

      form.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden input

        rules: {
					leave_type_id: {required: true},
					leave_duration_id: {required: true},
					start_date: {required: true},
					end_date: {required: true},

        },
  
        highlight: function (element) { // hightlight error inputs
          $(element)
            .closest('.form-group .form-control').addClass('is-invalid') // set invalid class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
          $(element)
            .closest('.form-group .form-control').removeClass('is-invalid') // set invalid class to the control group
            .closest('.form-group .form-control').addClass('is-valid')
        },
        errorPlacement: function (error, element) {
          if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            error.insertAfter(element.parent())
          } else {
            error.insertAfter(element)
          }
        },
        success: function (label) {
          label
            .closest('.form-group .form-control').removeClass('is-invalid') // set success class to the control group
        },
        submitHandler: function(element) {
          btn.prop('disabled', true)
          $.ajax({
            type: "post",
            url: rurl+'admin/employeeleave',
            data: $( element ).serialize(),
            dataType: "html",
            success: function (data) {
              btn.prop('disabled', false)
              $('[data-dismiss="modal"]').trigger('click');
              $('.validateForm').removeClass('formadd')
              $('.validateForm').removeClass('formedit')
              $("#employeeleave").DataTable().ajax.reload(null, false);
              swal('ยินดีด้วย!', data, "success")
            },
            error: function (data) {
              $('.validateForm').removeClass('formadd')
              $('.validateForm').removeClass('formedit')
              
            }
          });
        }
      })
    }

    var handleButton = function () {
        $(document).on('click', '.btn-delete', function(){
          $this = $(this)
          swal({
            title: "คุณแน่ใจไหม?",
            text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "ใช่, ยืนยัน!",
            cancelButtonText: "ยกเลิก",
            closeOnConfirm: false
          },
          function(){
            deleteUser($this);
          });
        })

        $(document).on('click', '.btn-add', function(){
          $('[name="id"]').val(null);

          //remove form class
          $('.validateForm').removeClass('formadd')
          $('.validateForm').removeClass('formedit')

          $('.validateForm').trigger('reset')
          $('.validateForm').addClass('formadd')
          $('.validateForm').removeAttr('data-id')

          //add value to form example
          $('.ls-select2').select2();
        })

        $(document).on('click', '.btn-edit', function(btn){
          //remove form class
          $('.validateForm').removeClass('formadd');
          $('.validateForm').removeClass('formedit');

          var id = $(this).data('id');
          $('[name="id"]').val(id);

          var selector = $('.validateForm');
          selector.addClass('formedit');
          selector.find('[type="submit"]').removeAttr('data-id');
          selector.find('[type="submit"]').attr('data-id',id);
          $.ajax({
            type: 'get',
            url: rurl+'admin/employeeleave/'+id,
            dataType: "json",
            success: function (data) {
              Object.entries(data).forEach(entry => {
                $('[name="'+entry[0]+'"]').val(entry[1]);
                if(entry[0]=='approved_at'){
                  var approved_at = (entry[1]).split(" ");
                  $('input[name="approved_at"]').val(approved_at[0]);
                  $('input[name="approved_time"]').val(approved_at[1]);
                  console.log(approved_at[0]);
                }
              });
              getApprovers(data.employee_id,data.approver_id);
              $('.ls-select2').select2();
              $('.modal').modal('show');
            },
            error: function (data) {
              swal("ไม่พบข้อมูลที่ท่านต้องการ!",data.responseTextta, "error");
            }
          })
        })

        $(document).on('click', '.formadd [type="submit"]', function(){
          handleValidation();
        })

        $(document).on('click', '.formedit [type="submit"]', function(e){
          handleValidation();
        })
    }

    var deleteUser = function (value) {
      var id = value.data('id');
      var token = value.data('token');

      $.ajax({
        url: rurl+'admin/employeeleave/'+id,
        type: 'DELETE',
        data: {
          _method: 'delete',
          _token: token,
          _id : id
        },
        success: function (data) {
          $("#employeeleave").DataTable().ajax.reload(null, false);
          swal('ยินดีด้วย!', data, "success");
        },
        error: function (data) {
          swal("พบข้อผิดผลาด!",data, "error");
        }
      })
    }

    $(document).ready(function () {
      $(document).on('change', '#employee_id', function(btn){
        getApprovers($(this).val(),null);
      });
    });

    function getApprovers(employee_id,approver_id=null){
      var id = employee_id;
      $.ajax({
        type: 'get',
        url: rurl+'admin/employeeleave/approvers/'+id,
        dataType: "JSON",
        success: function (data) {
          console.log(data);
          if(data!=0){
            $('select#approver_id').empty();
            $('<option value="">== อนุมัติโดย ==</option>').appendTo('select#approver_id');
            $.each(data, function (indexInArray, valueOfElement) { 
              $('select#approver_id').append('<option value="'+valueOfElement.id+'" '+((approver_id==valueOfElement.id)?"selected":"")+'>'+valueOfElement.firstname+' '+valueOfElement.lastname+'</option>');
            });
          }else{
            $('select#approver_id').empty();
            $('<option value="">== อนุมัติโดย ==</option>').appendTo('select#approver_id');
          }
        }
      });
    }
  
    return {
      // main function to initiate the module
      init: function () {
        handleTables();
        handleButton();
      }
    }
  })()
  
  jQuery(document).ready(function () {
    Employeeleave.init();
  })