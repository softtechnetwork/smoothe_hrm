var Employeeregistration = (function () {
    $('.ls-select2').select2();
    var handleTables = function () {
      var option_table = {

        "columnDefs": [
          { "width": "4%", "targets": 0 }, 
          { "width": "10%", "targets": 1 }, 
          { "width": "0%", "targets": 2 ,"visible":false}, 
          { "width": "6%", "targets": 3 }, 
          { "width": "6%", "targets": 4 }, 
          { "width": "7%", "targets": 5 }, 
          { "width": "8%", "targets": 6 }, 
          { "width": "7%", "targets": 7 }, 
          { "width": "6%", "targets": 8 }, 
          { "width": "6%", "targets": 9 }, 
          { "width": "7%", "targets": 10 }, 
          { "width": "9%", "targets": 11 }, 
          { "width": "7%", "targets": 12 }, 
          { "width": "0%", "targets": 13 ,"visible":false}, 
          { "width": "6%", "targets": 14 }
        ],
        oLanguage: {sProcessing: "<div id='loader'></div>"},

        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
          "url" :  rurl + 'admin/employeeregistration/list'
          ,"type" : "POST"
          ,"data" : {
            "company_id" : $('[name="company_id"]').val(),
            "branch_id" : $('[name="branch_id"]').val(),
            "level_id" : $('[name="level_id"]').val(),
            "group_id" : $('[name="group_id"]').val(),
            "department_id" : $('[name="department_id"]').val(),
            "employee_id" : $('[name="employee_id"]').val(),
            "in_date" : $('[name="in_date"]').val(),
            "out_date" : $('[name="out_date"]').val()
          }
        },
        "language": { "url" : rurl + "assets/plugins/datatable_th.json" },
        "order": [[ 13, "desc" ]],
        "columns": [
          {
            "data": 'DT_RowIndex',
            "name": 'DT_RowIndex',
            orderable: false,
            searchable: false,
            className:"text-center"
          },
          {"data":"firstname","name":"employee.firstname"},
          {"data":"lastname","name":"employee.lastname","visible":false},
					{"data":"in_date","name":"employee_registration.in_date"},
					{"data":"in_time","name":"employee_registration.in_time"},
					{"data":"in_latitude","name":"employee_registration.in_latitude"},
					{"data":"in_longitude","name":"employee_registration.in_longitude"},
					{"data":"in_reason","name":"employee_registration.in_reason"},
					{"data":"out_date","name":"employee_registration.out_date"},
					{"data":"out_time","name":"employee_registration.out_time"},
					{"data":"out_latitude","name":"employee_registration.out_latitude"},
					{"data":"out_longitude","name":"employee_registration.out_longitude"},
          {"data":"out_reason","name":"employee_registration.out_reason"},
          {"data":"created_at","name":"employee_registration.created_at","visible":false},
          {
            "data": "action",
            orderable: false,
            searchable: false,
            className:"text-center"
          }
        ],
        "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
        "lengthMenu": [
            //[10, 25, 50, -1],
            //[10, 25, 50, "All"]
            [100, 200, 300, 400, 1000, 5000],
            [100, 200, 300, 400, 1000, 5000]        ],
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-default btn-sm',
                text: '<i class="fas fa-file-excel"></i> Excel',
                orientation: 'landscape',
                title: 'รายการลงทะเบียนทำงาน'
            },
            {
                extend: 'print',
                className: 'btn btn-default btn-sm',
                text: '<i class="fas fa-print"></i> Print',
                orientation: 'landscape',
                title: 'รายการลงทะเบียนทำงาน'
            },
        ],
      };
      var table = $('#employeeregistration').DataTable(option_table);

      $('form#filter').submit(function (e) {
        e.preventDefault();
        new_option = {

        "columnDefs": [
          { "width": "4%", "targets": 0 }, 
          { "width": "10%", "targets": 1 }, 
          { "width": "0%", "targets": 2 ,"visible":false}, 
          { "width": "6%", "targets": 3 }, 
          { "width": "6%", "targets": 4 }, 
          { "width": "7%", "targets": 5 }, 
          { "width": "8%", "targets": 6 }, 
          { "width": "7%", "targets": 7 }, 
          { "width": "6%", "targets": 8 }, 
          { "width": "6%", "targets": 9 }, 
          { "width": "7%", "targets": 10 }, 
          { "width": "9%", "targets": 11 }, 
          { "width": "7%", "targets": 12 }, 
          { "width": "0%", "targets": 13 ,"visible":false}, 
          { "width": "6%", "targets": 14 }
        ],
        oLanguage: {sProcessing: "<div id='loader'></div>"},

          "responsive": true,
          "serverSide": true,
          "processing": true,
          "ajax": {
            "url" :  rurl + 'admin/employeeregistration/list'
            ,"type" : "POST"
            ,"data" : {
              "company_id" : $('[name="company_id"]').val(),
              "branch_id" : $('[name="branch_id"]').val(),
              "level_id" : $('[name="level_id"]').val(),
              "group_id" : $('[name="group_id"]').val(),
              "department_id" : $('[name="department_id"]').val(),
              "employee_id" : $('[name="employee_id"]').val(),
              "in_date" : $('[name="in_date"]').val(),
              "out_date" : $('[name="out_date"]').val()
            }
          },
          "language": { "url" : rurl + "assets/plugins/datatable_th.json" },
          "order": [[ 13, "desc" ]],
          "columns": [
            {
              "data": 'DT_RowIndex',
              "name": 'DT_RowIndex',
              orderable: false,
              searchable: false,
              className:"text-center"
            },
            {"data":"firstname","name":"employee.firstname"},
            {"data":"lastname","name":"employee.lastname","visible":false},
            {"data":"in_date","name":"employee_registration.in_date"},
            {"data":"in_time","name":"employee_registration.in_time"},
            {"data":"in_latitude","name":"employee_registration.in_latitude"},
            {"data":"in_longitude","name":"employee_registration.in_longitude"},
            {"data":"in_reason","name":"employee_registration.in_reason"},
            {"data":"out_date","name":"employee_registration.out_date"},
            {"data":"out_time","name":"employee_registration.out_time"},
            {"data":"out_latitude","name":"employee_registration.out_latitude"},
            {"data":"out_longitude","name":"employee_registration.out_longitude"},
            {"data":"out_reason","name":"employee_registration.out_reason"},
            {"data":"created_at","name":"employee_registration.created_at","visible":false},
            {
              "data": "action",
              orderable: false,
              searchable: false,
              className:"text-center"
            }
          ],
          "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
          "lengthMenu": [
              //[10, 25, 50, -1],
              //[10, 25, 50, "All"]
            [100, 200, 300, 400, 1000, 5000],
            [100, 200, 300, 400, 1000, 5000]          ],
          buttons: [
              {
                  extend: 'excel',
                  className: 'btn btn-default btn-sm',
                  text: '<i class="fas fa-file-excel"></i> Excel',
                  orientation: 'landscape',
                  title: 'รายการลงทะเบียนทำงาน'
              },
              {
                  extend: 'print',
                  className: 'btn btn-default btn-sm',
                  text: '<i class="fas fa-print"></i> Print',
                  orientation: 'landscape',
                  title: 'รายการลงทะเบียนทำงาน'
              },
          ],
        };
        $('#employeeregistration').DataTable().destroy();
        $('#employeeregistration').DataTable(new_option);
      });
    }
  
    var handleValidation = function () {
      var form = $('.validateForm');
      var btn = $('.validateForm [type="submit"]');

      form.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden input

        rules: {

        },
  
        highlight: function (element) { // hightlight error inputs
          $(element)
            .closest('.form-group .form-control').addClass('is-invalid') // set invalid class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
          $(element)
            .closest('.form-group .form-control').removeClass('is-invalid') // set invalid class to the control group
            .closest('.form-group .form-control').addClass('is-valid')
        },
        errorPlacement: function (error, element) {
          if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            error.insertAfter(element.parent())
          } else {
            error.insertAfter(element)
          }
        },
        success: function (label) {
          label
            .closest('.form-group .form-control').removeClass('is-invalid') // set success class to the control group
        },
        submitHandler: function(element) {
          btn.prop('disabled', true)
          $.ajax({
            type: "post",
            url: rurl+'admin/employeeregistration',
            data: $( element ).serialize(),
            dataType: "html",
            success: function (data) {
              btn.prop('disabled', false)
              $('[data-dismiss="modal"]').trigger('click');
              $('.validateForm').removeClass('formadd')
              $('.validateForm').removeClass('formedit')
              $("#employeeregistration").DataTable().ajax.reload(null, false);
              swal('ยินดีด้วย!', data, "success")
            },
            error: function (data) {
              $('.validateForm').removeClass('formadd')
              $('.validateForm').removeClass('formedit')
              
            }
          });
        }
      })
    }

    var handleButton = function () {
        $(document).on('click', '.btn-delete', function(){
          $this = $(this)
          swal({
            title: "คุณแน่ใจไหม?",
            text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "ใช่, ยืนยัน!",
            cancelButtonText: "ยกเลิก",
            closeOnConfirm: false
          },
          function(){
            deleteUser($this);
          });
        })

        $(document).on('click', '.btn-add', function(){
          $('[name="id"]').val(null);

          //remove form class
          $('.validateForm').removeClass('formadd')
          $('.validateForm').removeClass('formedit')

          $('.validateForm').trigger('reset')
          $('.validateForm').addClass('formadd')
          $('.validateForm').removeAttr('data-id')

          //add value to form example
          $('.ls-select2').select2();
        })

        $(document).on('click', '.btn-edit', function(btn){
          //remove form class
          $('.validateForm').removeClass('formadd');
          $('.validateForm').removeClass('formedit');

          var id = $(this).data('id');
          $('[name="id"]').val(id);

          var selector = $('.validateForm');
          selector.addClass('formedit');
          selector.find('[type="submit"]').removeAttr('data-id');
          selector.find('[type="submit"]').attr('data-id',id);
          $.ajax({
            type: 'get',
            url: rurl+'admin/employeeregistration/'+id,
            dataType: "json",
            success: function (data) {
              // console.log(data);
              Object.entries(data).forEach(entry => {
                $('[name="'+entry[0]+'"]').val(entry[1]);
              })
              $('.ls-select2').select2();
              $('.modal').modal('show');
            },
            error: function (data) {
              swal("ไม่พบข้อมูลที่ท่านต้องการ!",data.responseTextta, "error");
            }
          })
        })

        $(document).on('click', '.formadd [type="submit"]', function(){
          handleValidation();
        })

        $(document).on('click', '.formedit [type="submit"]', function(e){
          handleValidation();
        })
    }

    var deleteUser = function (value) {
      var id = value.data('id');
      var token = value.data('token');

      $.ajax({
        url: rurl+'admin/employeeregistration/'+id,
        type: 'DELETE',
        data: {
          _method: 'delete',
          _token: token,
          _id : id
        },
        success: function (data) {
          $("#employeeregistration").DataTable().ajax.reload(null, false);
          swal('ยินดีด้วย!', data, "success");
        },
        error: function (data) {
          swal("พบข้อผิดผลาด!",data, "error");
        }
      })
    }
  
    return {
      // main function to initiate the module
      init: function () {
        handleTables();
        handleButton();
      }
    }
  })()
  
  jQuery(document).ready(function () {
    Employeeregistration.init();
  })