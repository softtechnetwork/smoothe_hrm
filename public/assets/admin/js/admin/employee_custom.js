var EpyForm = (function () {
    function redrawelement(loop, form_id) {
        $('.ls-select2').select2('destroy');
        var container = $(form_id).closest('.tab-pane').find('.container-clone');
        $(form_id).find('.container-clone').empty();
        $.each(loop, function (index, value) {
            //clone
            var element = $(form_id).closest('.row').find('.d-none').clone().removeClass('d-none');
            element.attr("data-id", index);
            element.find('span.card-title-id').html('#' + (index + 1));
            if (form_id == "#form-evaluation") {
                var inputall = $(element).find(':input'); //array all input
                $.each(inputall, function (indexInArray, valueOfElement) {
                    var oldname = ($(valueOfElement).attr('name'));
                    var newname = oldname.split("[");
                    var lastname = newname[0];
                    if (newname[0] == 'evaluation_ability') {
                        var ea_value = value[newname[0]];
                        lastname = lastname + "[" + index + "][]";
                        $(valueOfElement).attr('name', lastname);
                        if (jQuery.inArray($(valueOfElement).val(), JSON.parse(ea_value)) != -1) {
                            $(valueOfElement).attr('checked', true);
                        }
                    } else {
                        lastname = lastname + "[" + index + "]";
                        $(valueOfElement).attr('name', lastname);
                        $(valueOfElement).val(value[newname[0]]);
                    }
                });
            } else {
                $.each(value, function (indexInArray, valueOfElement) {
                    //find and set value
                    if (element.find('[name="' + indexInArray + '[]"]').length > 0) {
                        element.find('[name="' + indexInArray + '[]"]').val(valueOfElement);
                    }
                });
            }
            container.prepend(element);
        });
        $('.ls-select2').select2();
    }

    $('#form-general').validate({
        rules: {
            // cid: {
            //     required: true
            // },
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            empcode: {
                required: true
            },
            sex: {
                required: true
            },
            // mobile: {
            //     required: true
            // }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            // btn.prop('disabled', true)
            var form = $('form#form-general');
            $.ajax({
                type: "post",
                url: rurl + 'admin/employee',
                data: form.serialize(),
                // dataType: "html",
                success: function (data) {
                    swal('ยินดีด้วย!', data.message, "success");
                    $('[name*="employee_id"]').val(data.employee_id);
                    // if (data.employee_id) {
                    //     $('[name*="employee_id"]').val(data.employee_id);
                    // }
                },
                error: function (data) {
                    swal('พบข้อผิดผลาด!', data.message, "error");
                }
            });
        }
    });

    $('#form-preferment').validate({
        // debug: true,
        rules: {
            branch_id: {
                required: true
            },
            department_id: {
                required: true
            },
            level_id: {
                required: true
            },
            start_date: {
                required: true
            },
            end_date: {
                required: true
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            if ($('#form-preferment').find('[name*="employee_id"]').val() == null && $('#form-preferment').find('[name*="employee_id"]').val() == undefined) {
                console.log($('#form-preferment').find('[name*="employee_id"]').val());
                swal('พบข้อผิดผลาด!', 'คุณต้องทำการบันทึกข้อมูลทั่วไปก่อน', "error");
            } else {
                $.ajax({
                    type: "POST",
                    url: rurl + 'admin/employee/preferment',
                    data: $(element).serializeArray(),
                    dataType: "json",
                    success: function (data) {
                        swal('ยินดีด้วย!', data.text, "success");
                        var loop = data.form_employee_preferment;
                        redrawelement(loop, "#form-preferment");
                    },
                    error: function (data) {
                        swal('พบข้อผิดผลาด!', data, "error");
                    }
                });
            }
        }
    });

    $('#form-work').validate({
        // debug: true,
        rules: {
            place: {
                required: true
            },
            level: {
                required: true
            },
            start_date: {
                required: true
            },
            end_date: {
                required: true
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            if ($('#form-work').find('[name*="employee_id"]').val() == null || $('#form-work').find('[name*="employee_id"]').val() == undefined) {
                swal('พบข้อผิดผลาด!', 'คุณต้องทำการบันทึกข้อมูลทั่วไปก่อน', "error");
            } else {
                $.ajax({
                    type: "POST",
                    url: rurl + 'admin/employee/experience',
                    data: $(element).serializeArray(),
                    // dataType: "JSON",
                    success: function (data) {
                        swal('ยินดีด้วย!', data.text, "success");
                        var loop = data.form_employee_experience;
                        redrawelement(loop, "#form-work");
                    },
                    error: function (data) {
                        // console.log(data);
                        swal('พบข้อผิดผลาด!', data, "error");
                    }
                });
            }
        }
    });

    $('#form-education').validate({
        // debug: true,
        rules: {
            place: {
                required: true
            },
            level: {
                required: true
            },
            start_date: {
                required: true
            },
            end_date: {
                required: true
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            if ($('#form-education').find('[name*="employee_id"]').val() == null || $('#form-education').find('[name*="employee_id"]').val() == undefined) {
                swal('พบข้อผิดผลาด!', 'คุณต้องทำการบันทึกข้อมูลทั่วไปก่อน', "error");
            } else {
                $.ajax({
                    type: "POST",
                    url: rurl + 'admin/employee/experience',
                    data: $(element).serializeArray(),
                    // dataType: "JSON",
                    success: function (data) {
                        console.log(data);
                        swal('ยินดีด้วย!', data.text, "success");
                        var loop = data.form_employee_experience;
                        redrawelement(loop, "#form-education");
                    },
                    error: function (data) {
                        // console.log(data);
                        swal('พบข้อผิดผลาด!', data, "error");
                    }
                });
            }
        }
    });

    $('#form-training').validate({
        rules: {
            place: {
                required: true
            },
            level: {
                required: true
            },
            start_date: {
                required: true
            },
            end_date: {
                required: true
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            if ($('#form-training').find('[name*="employee_id"]').val() == null || $('#form-training').find('[name*="employee_id"]').val() == undefined) {
                swal('พบข้อผิดผลาด!', 'คุณต้องทำการบันทึกข้อมูลทั่วไปก่อน', "error");
            } else {
                $.ajax({
                    type: "POST",
                    url: rurl + 'admin/employee/experience',
                    data: $(element).serializeArray(),
                    success: function (data) {
                        swal('ยินดีด้วย!', data.text, "success");
                        var loop = data.form_employee_experience;
                        redrawelement(loop, "#form-training");
                    },
                    error: function (data) {
                        swal('พบข้อผิดผลาด!', data, "error");
                    }
                });
            }
        }
    });

    $('#form-leave-approvers').validate({
        // debug: true,
        rules: {
            approvers: {
                required: true
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            // console.log($('#form-leave-approvers').find('[name="employee_id"]').val()!='');
            if($('#form-leave-approvers').find('[name="employee_id"]').val()==''){
                swal('พบข้อผิดผลาด!', 'คุณต้องทำการบันทึกข้อมูลทั่วไปก่อน', "error");
            }else{
                $.ajax({
                    type: "POST",
                    url: rurl + 'admin/employee/leaveapprovers',
                    data: $(element).serializeArray(),
                    // dataType: "JSON",
                    success: function (data) {
                        $('#form-leave-approvers').find('[name="id"]').val(data.result);
                        $('#form-leave-approvers').find('[name="id"]').length;
                        swal('ยินดีด้วย!', data.message, "success");
                    },
                    error: function (data) {
                        console.log(data);
                        swal('พบข้อผิดผลาด!', data.message, "error");
                    }
                });
            }
        }
    });

    $('#form-evaluation').validate({
        // debug: true,
        rules: {
            // approvers: {
            //     required: true
            // }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            if ($('#form-evaluation').find('[name*="employee_id"]').val() == null || $('#form-evaluation').find('[name*="employee_id"]').val() == undefined) {
                swal('พบข้อผิดผลาด!', 'คุณต้องทำการบันทึกข้อมูลทั่วไปก่อน', "error");
            } else {
                $.ajax({
                    type: "POST",
                    url: rurl + 'admin/employee/evaluation',
                    data: $(element).serialize(),
                    // dataType: "JSON",
                    success: function (data) {
                        swal('ยินดีด้วย!', data.text, "success");
                        var loop = data.form_employee_evaluation;
                        redrawelement(loop, "#form-evaluation");
                    },
                    error: function (data) {
                        // console.log(data);
                        swal('พบข้อผิดผลาด!', data, "error");
                    }
                });
            }
        }
    });

    $('#form-warning').validate({
        rules: {
            name: {
                required: true
            },
            level: {
                required: true
            },
            start_date: {
                required: true
            },
            end_date: {
                required: true
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            if ($('#form-warning').find('[name*="employee_id"]').val() == null || $('#form-warning').find('[name*="employee_id"]').val() == undefined) {
                swal('พบข้อผิดผลาด!', 'คุณต้องทำการบันทึกข้อมูลทั่วไปก่อน', "error");
            } else {
                $.ajax({
                    type: "POST",
                    url: rurl + 'admin/employee/warning',
                    data: $(element).serializeArray(),
                    // dataType: "JSON",
                    success: function (data) {
                        console.log(data.form_employee_warning);
                        swal('ยินดีด้วย!', data.text, "success");
                        var loop = data.form_employee_warning;
                        redrawelement(loop, "#form-warning");
                    },
                    error: function (data) {
                        // console.log(data);
                        swal('พบข้อผิดผลาด!', data, "error");
                    }
                });
            }
        }
    });

    $('#form-account').validate({
        // debug: true,
        rules: {
            approvers: {
                required: true
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `invalid-feedback` class to the error element
            error.addClass("invalid-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },

        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        tooltip_options: {
            thefield: {
                placement: 'top'
            }
        },
        submitHandler: function (element) {
            if ($('#form-account').find('[name*="employee_id"]').val() == '' || $('#form-account').find('[name*="employee_id"]').val() == undefined) {
                swal('พบข้อผิดผลาด!', 'คุณต้องทำการบันทึกข้อมูลทั่วไปก่อน', "error");
            } else {
                $.ajax({
                    type: "POST",
                    url: rurl + 'admin/employee/adminuser',
                    data: $(element).serializeArray(),
                    // dataType: "JSON",
                    success: function (data) {
                        // console.log(data);
                        //swal('ยินดีด้วย!', data.message, "success");
                        //$('.form-account').find('[name="id"]').val(data.adminuser_id);
                        if(data.status == 0){
                            swal('พบข้อผิดผลาด!', data.message, "error");
                        }else{
                            swal('ยินดีด้วย!', data.message, "success");
                            $('.form-account').find('[name="id"]').val(data.adminuser_id);
                        }
                    },
                    error: function (data) {
                        // console.log(data);
                        swal('พบข้อผิดผลาด!', data.message, "error");
                    }
                });
            }
        }
    });

    $(document).on('click', '.btn-newpassword', function () {
        $('.newpassword').toggleClass('d-none');
        console.log($('.newpassword.d-none').length);
        if ($('.newpassword.d-none').length == 1) {
            $('.newpassword input[type="password"]:first').removeAttr('name');
        } else {
            $('.newpassword input[type="password"]:first').attr('name', 'password');
        }
    });

    $(document).on('change', '.main_menu', function () {
        var checkbox = $(this);
        if (checkbox.is(':checked')) {
            $("[data-group='" + checkbox.val() + "']").prop("checked", true);
        } else {
            $("[data-group='" + checkbox.val() + "']").prop("checked", false);
        }
    });

    $('.ls-select2').select2();

    $('#lfm').filemanager('image');

    $(document).on('click', '.card-clone', function () {
        var id = $(this).closest('.tab-pane').find('.container-clone .card:first').data('id');
        var new_id = (id + 1);
        $('.ls-select2').select2('destroy');
        var element = $(this).closest('.card').clone();
        element.find('span.card-title-id').html('#' + (new_id + 1));
        element.attr('data-id', new_id).removeClass('d-none');
        element.find('[name*="id"]:first').val(null);
        if ($(this).closest('.row').find('form').attr('id') == 'form-evaluation') {
            element.find('[name*="id"]:first').attr('name','id['+new_id+']');
            element.find('[name*="employee_id"]:first').attr('name','employee_id['+new_id+']');
        }
        var container = $(this).closest('.tab-pane').find('.container-clone');
        container.prepend(element);
        if (element.find('[type="checkbox"]').length != 0) {
            element.find('select').attr('name', 'employee_target_id[' + (new_id) + ']');
            var multicheckbox = element.find('[type="checkbox"]');
            $.each(multicheckbox, function (indexInArray, valueOfElement) {
                var input_id = $(valueOfElement).attr('id');
                var input = input_id.split("-");
                input[1] = parseInt(new_id);
                var string = input.toString().replace(/\,/g, "-");
                $(valueOfElement).attr('id', string);
                $(valueOfElement).attr('name', 'evaluation_ability[' + (new_id) + '][]');
                $(valueOfElement).closest('.checkbox.check-success').find('label').attr('for', string);
            });
        }
        $('.ls-select2').select2();
    });

    $(document).on('click', '.card-add', function () {
        var id = $(this).closest('.tab-pane').find('.container-clone .card:first').data('id');
        $('.ls-select2').select2('destroy');
        if (id == undefined) {
            id = 0;
            var new_id = 0;
        } else {
            var new_id = (id + 1);
        }
        var element = $(this).closest('.tab-pane').find('.card.d-none').clone();
        element.find('span.card-title-id').html('#' + (new_id + 1));
        element.attr('data-id', new_id).removeClass('d-none');
        if ($(this).closest('.row').find('form').attr('id') == 'form-evaluation') {
            element.find('[name="id[]"]').attr('name', 'id[' + new_id + ']');
            element.find('[name="employee_id[]"]').attr('name', 'employee_id[' + new_id + ']');
        }

        var container = $(this).closest('.tab-pane').find('.container-clone');
        container.prepend(element);
        if (element.find('[type="checkbox"]').length != 0) {
            var multicheckbox = element.find('[type="checkbox"]');
            element.find('select').attr('name', 'employee_target_id[' + (new_id) + ']');
            $.each(multicheckbox, function (indexInArray, valueOfElement) {
                var input_id = $(valueOfElement).attr('id');
                var input = input_id.split("-");
                input[1] = parseInt(new_id);
                var string = input.toString().replace(/\,/g, "-");
                $(valueOfElement).attr('id', string);
                $(valueOfElement).attr('name', 'evaluation_ability[' + (new_id) + '][]');
                $(valueOfElement).closest('.checkbox.check-success').find('label').attr('for', string);
            });
        }

        $('.ls-select2').select2();
    });

    $(document).on('click', '.card-remove', function () {
        $this = $(this);
        swal({
                title: "คุณแน่ใจไหม?",
                text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "ใช่, ยืนยัน!",
                cancelButtonText: "ยกเลิก",
                closeOnConfirm: false
            },
            function () {
                deleteUser($this);
            });
    });

    $(document).on('change', '#cr_provinces', function(){
        $('select#cr_districts').empty();
        $('select#cr_districts').append('<option value="">== อำเภอ/เขต ==</option>');
        $('select#cr_subdistricts').empty();
        $('select#cr_subdistricts').append('<option value="">== ตำบล/แขวง ==</option>');
        $('#cr_zipcode').val(null);
        $.ajax({
            type: "get",
            url: rurl + 'getdistrict/' + this.value,
            dataType: "json",
            success: function (response) {
                $.each(response, function (index, value) {
                    $('select#cr_districts').append('<option value="' + value.id + '">' +
                        value.name_th + '</option>');
                });
                $('.ls-select2').select2();
            }
        });
    });
    
    $(document).on('change', '#cr_districts', function(){
        $('select#cr_subdistricts').empty();
        $('select#cr_subdistricts').append('<option value="">== ตำบล/แขวง ==</option>');
        $('#cr_zipcode').val(null);
        $.ajax({
            type: "get",
            url: rurl + 'getsubdistrict/' + this.value,
            dataType: "json",
            success: function (response) {
    
                $.each(response, function (index, value) {
                    $('select#cr_subdistricts').append('<option value="' + value.id + '">' +
                        value.name_th + '</option>');
                });
                $('.ls-select2').select2();
            }
        });
    });
    
    $(document).on('change', '#cr_subdistricts', function(){
        $.ajax({
            type: "get",
            url: rurl + 'getzipcode/' + this.value,
            dataType: "json",
            success: function (response) {
                console.log(response);
                $('#cr_zipcode').val(response);
            }
        });
    });

    var deleteUser = function (value) {
        var id = $(value).closest('.card').find('[name*="id"]:first').val();
        var table = $(value).closest('.card').data('table');

        if(id==''){
            $(value).closest('.card').remove();
            swal('ยินดีด้วย!','ลบฟอร์มข้อมูล สำเร็จ!', "success");
        }else{
            $.ajax({
                url: rurl + 'admin/employee/delete',
                type: 'POST',
                data: {
                    table: table,
                    id: id
                },
                success: function (data) {
                    $("#employee").DataTable().ajax.reload(null, false);
                    swal('ยินดีด้วย!', data, "success");
                    $(value).closest('.card').remove();
                },
                error: function (data) {
                    swal("พบข้อผิดผลาด!", data, "error");
                }
            });
        }
    }

    $('form.hawkeye').submit(function (e) {
        e.preventDefault();
        var form_data = $(this).serializeArray();
        $.each(form_data, function (indexInArray, valueOfElement){
            if(valueOfElement.name!="province"||valueOfElement.name!="district"||valueOfElement.name!="subdistrict"){
                
                if(valueOfElement.name=="gender"){
                    console.log($('input[name="gender"][value="'+valueOfElement.value+'"]'));
                    $('input[name="gender"][value="'+valueOfElement.value+'"]').prop("checked", true);
                }else if(valueOfElement.name=="picture_profile_scan"){
                    var str = valueOfElement.value;
                    $.ajax({
                        type: "POST",
                        url: rurl + 'convertImage',
                        data: { picture_profile_scan:str  },
                        success: function (data) {
                            $('#form-general [name="'+valueOfElement.name+'"]').val(data);
                            $('#form-general img#holder_scan').attr('src',data);
                        }
                    });
                }else{
                    $('#form-general [name="'+valueOfElement.name+'"]').val(valueOfElement.value);
                }
            }
        });
        $.ajax({
            type: "POST",
            url: rurl + 'getidaddress',
            data: $(this).serializeArray(),
            success: function (data) {
                redrawaddress(data.subdistrictsid);
                $('.ls-select2').select2();
                $('.modal').modal('hide');
            },
            error: function (data) {
                swal('พบข้อผิดผลาด!', data.message, "error");
            }
        });
    });

    $('input[name="picture_profile_scan"]').change(function (e) { 
        e.preventDefault();
        $('#form-general img#holder_scan').attr('src',this.value);
    });
});

jQuery(document).ready(function () {
    EpyForm();
});