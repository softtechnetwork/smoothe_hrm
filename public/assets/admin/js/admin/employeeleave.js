var Employeeleave = (function () {
  var handleTables = function () {

    var table = $('#employeeleave').DataTable({
      "destroy": true,
      "responsive": true,
      "serverSide": true,
      "processing": true,
      "ajax": {
        "url":rurl + 'admin/employeeleave/list',
        "type":'POST',
        "data":{
          "company_id" : $('[name="company_id"]').val(),
          "branch_id" : $('[name="branch_id"]').val(),
          "level_id" : $('[name="level_id"]').val(),
          "group_id" : $('[name="group_id"]').val(),
          "department_id" : $('[name="department_id"]').val(),
          "leave_type_id" : $('[name="leave_type_id"]').val(),
          "employee_id" : $('[name="employee_id"]').val(),
          "month" : $('[name="month"]').val(),
          "status" : $('[name="status"]').val()
        }
      },
      "language": {
        "url": rurl + "assets/plugins/datatable_th.json"
      },
      "order": [[ 1, 'desc' ]],
      "columns": [
        {
          "data": 'DT_RowIndex',
          "name": 'DT_RowIndex',
          orderable: false,
          searchable: false,
          className: "text-center"
        },
        {
          "data": "created_at",
          "name": "employee_leave.created_at",
          "visible":true
        },
        {
          "data": "start_date",
          "name": "employee_leave.start_date"
        },
        {
          "data": "end_date",
          "name": "employee_leave.end_date"
        },{
          "data": "leave_diff",
          orderable: false,
          searchable: false,
        },
        {
          "data": "ep1firstname",
          "name": "ep1.firstname"
        },
        {
          "data": "ep1lastname",
          "name": "ep1.lastname",
          "visible": false
        },
        {
          "data": "leave_name",
          "name": "leave_type.leave_name"
        },
        {
          "data": "duration_name",
          "name": "leave_duration.duration_name"
        },
        {
          "data": "remark",
          "name": "employee_leave.remark"
        },
        {
          "data": "ep2firstname",
          "name": "ep2.firstname"
        },
        {
          "data": "ep2lastname",
          "name": "ep2.lastname",
          "visible": false
        },
        {
          "data": "approved_at",
          "name": "employee_leave.approved_at"
        },
        {
          "data": "leave_result",
          "name": "employee_leave.leave_result"
        },
        {
          "data": "action",
          orderable: false,
          searchable: false
        }
      ],
      "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
      "lengthMenu": [
          //[10, 25, 50, 100 , 1000],
          //[10, 25, 50, 100 , 1000]
          [50, 100, 200, 300, 400, 1000],
          [50, 100, 200, 300, 400, 1000]
      ],
      buttons: [
          {
              extend: 'excel',
              className: 'btn btn-default btn-sm',
              text: '<i class="fas fa-file-excel"></i> Excel',
              orientation: 'landscape',
              title: 'รายงานการลา'
          },
          {
              extend: 'print',
              className: 'btn btn-default btn-sm',
              text: '<i class="fas fa-print"></i> Print',
              orientation: 'landscape',
              title: 'รายงานการลา'
          },
      ],
    });

    $('form#filter').submit(function (e) {
      e.preventDefault();
      handleTables();
    });
  }
  $('.ls-select2').select2();
  var handleValidation = function () {
    var form = $('.validateForm');
    var btn = $('.validateForm [type="submit"]');

    form.validate({
      errorElement: 'span', // default input error message container
      errorClass: 'help-block help-block-error', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: '', // validate all fields including form hidden input

      rules: {
        leave_type_id: {
          required: true
        },
        leave_duration_id: {
          required: true
        },
        start_date: {
          required: true
        },
        end_date: {
          required: true
        },

      },

      highlight: function (element) { // hightlight error inputs
        $(element)
          .closest('.form-group .form-control').addClass('is-invalid') // set invalid class to the control group
      },
      unhighlight: function (element) { // revert the change done by hightlight
        $(element)
          .closest('.form-group .form-control').removeClass('is-invalid') // set invalid class to the control group
          .closest('.form-group .form-control').addClass('is-valid')
      },
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
          error.insertAfter(element.parent())
        } else {
          error.insertAfter(element)
        }
      },
      success: function (label) {
        label
          .closest('.form-group .form-control').removeClass('is-invalid') // set success class to the control group
      },
      submitHandler: function (element) {
        btn.prop('disabled', true)

        // $.ajax({
        //   type: "post",
        //   url: rurl + 'admin/employeeleave',
          
        //   data: $(element).serialize(),
        //   dataType: "html",
        //   success: function (data) {
        //     $("#employeeleave").DataTable().ajax.reload(null, false);
        //       btn.prop('disabled', false)
        //     if (data == 'บันทึกข้อมูลสำเร็จ!' || data == 'คุณอัพเดทข้อมูลสำเร็จ!') {
        //       $('[data-dismiss="modal"]').trigger('click');
        //       $('.validateForm').removeClass('formadd');
        //       $('.validateForm').removeClass('formedit');
        //       swal('ยินดีด้วย!', data, "success");
        //     } else {
        //       swal('ผิดพลาด', data, "warning");
        //     }
        //   },
        //   error: function (data) {
        //     $('.validateForm').removeClass('formadd')
        //     $('.validateForm').removeClass('formedit')
        //   }
        // });

        //console.log($(element).serialize());
        var leave_duration_id = $('[name="leave_duration_id"]').val();
        var start_date = $('[name="start_date"]').val();
        var start_time = $('[name="start_time"]').val();
        var end_date = $('[name="end_date"]').val();
        var end_time = $('[name="end_time"]').val();
        var employee_id = $('[name="employee_id"]').val();
        var leave_type_id = $('[name="leave_type_id"]').val();
        var remark = $('[name="remark"]').val();
        var approver_id = $('[name="approver_id"]').val();
        var approved_at = $('[name="approved_at"]').val();
        var approved_time = $('[name="approved_time"]').val();
        var leave_result = $('[name="leave_result"]').val();
        

        const inpFile = document.getElementById("leave_pic_attach");
      
        var fd = new FormData();
        const file_data = $('input[type="file"]')[0].files; // for multiple files
        // for(var i = 0;i<file_data.length;i++){
        //     fd.append("file_"+i, file_data[i]);
        // }
        var i = 0;
        let fileArr = [];
        console.log(file_data);
        //alert($('input[type="file"]')[0].files.length);
        var data = new FormData();

        if(file_data.length  > 0){
          //fd.append("file[]" ,$('input[type="file"]')[0].files);
          //console.log($('input[type="file"]')[0].files);
          for(const file of file_data){
            //fileArr.push(file);
            fd.append("file[]" ,file);
            ++ i;
          }
          // fd.append("file[]" ,JSON.stringify(fileArr));
      }
      console.log(file_data);
      //alert('5');
        fd.append("max_file_count" , i);
        var other_data = $(element).serializeArray();
        $.each(other_data,function(key,input){
            fd.append(input.name,input.value);
        });
      
       //console.log(fileArr);
        var id = $('[name="id"]').val();
        // $.ajax({
      
        //   type: "POST",
        //   url: rurl + 'admin/employeeleave',
          
        //   data: fd,
        //   dataType: "html",
        //   success: function (data) {
        //     $("#employeeleave").DataTable().ajax.reload(null, false);
        //       btn.prop('disabled', false)
        //     if (data == 'บันทึกข้อมูลสำเร็จ!' || data == 'คุณอัพเดทข้อมูลสำเร็จ!') {
        //       $('[data-dismiss="modal"]').trigger('click');
        //       $('.validateForm').removeClass('formadd');
        //       $('.validateForm').removeClass('formedit');
        //       swal('ยินดีด้วย!', data, "success");
        //     } else {
        //       swal('ผิดพลาด', data, "warning");
        //     }
        //   },
        //   error: function (data) {
        //     $('.validateForm').removeClass('formadd')
        //     $('.validateForm').removeClass('formedit')
        //   }
        // });
        $.ajax({
      
          type: "POST",
          url: rurl + 'admin/employeeleave',
          
          data: fd,
          contentType: false,
          processData: false,
          success: function (data) {
            $("#employeeleave").DataTable().ajax.reload(null, false);
              btn.prop('disabled', false)
            if (data == 'บันทึกข้อมูลสำเร็จ!' || data == 'คุณอัพเดทข้อมูลสำเร็จ!') {
              $('[data-dismiss="modal"]').trigger('click');
              $('.validateForm').removeClass('formadd');
              $('.validateForm').removeClass('formedit');
              swal('ยินดีด้วย!', data, "success");
              location.reload();
            } else {
              swal('ผิดพลาด', data, "warning");
            }
          },
          error: function (data) {
            $('.validateForm').removeClass('formadd')
            $('.validateForm').removeClass('formedit')
          }
        });
      //   data:{
      //     id : id,
      //     leave_duration_id: leave_duration_id,
      //     start_date: start_date,
      //     start_time: start_time,
      //     end_date: end_date,
      //     end_time: end_time,
      //     employee_id: employee_id,
      //     leave_type_id: leave_type_id,
      //     remark: remark,
        
      //     approver_id: approver_id,
      //     approved_at: approved_at,
      //     approved_time: approved_time,
      //     leave_result: leave_result,
      //     files : files
      // },
       // enctype: 'multipart/form-data',
   
      }
    })
  }

  var handleButton = function () {
    $(document).on('click', '.btn-delete', function () {
      $this = $(this)
      swal({
          title: "คุณแน่ใจไหม?",
          text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "ใช่, ยืนยัน!",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false
        },
        function () {
          deleteUser($this);
        });
    })

    $(document).on('click', '.btn-add', function () {
      $('[name="id"]').val(null);

      //remove form class
      $('.validateForm').removeClass('formadd')
      $('.validateForm').removeClass('formedit')

      $('.validateForm').trigger('reset')
      $('.validateForm').addClass('formadd')
      $('.validateForm').removeAttr('data-id')
      $('#leave_picture').html('');
      //add value to form example
      $('.ls-select2').select2();
    })

    $(document).on('click', '.btn-edit', function (btn) {
      //remove form class
      $('.validateForm').removeClass('formadd');
      $('.validateForm').removeClass('formedit');

      var id = $(this).data('id');
      $('[name="id"]').val(id);

      $('#leave_picture').html('');
      $('div.picture_remove').html('');

      var selector = $('.validateForm');
      selector.addClass('formedit');
      selector.find('[type="submit"]').removeAttr('data-id');
      selector.find('[type="submit"]').attr('data-id', id);
      // clear file 
      $('#leave_pic_attach').val();
      $.ajax({
        type: 'get',
        url: rurl + 'admin/employeeleave/' + id,
        dataType: "json",
        success: function (data) {
          Object.entries(data).forEach(entry => {
            $('[name="' + entry[0] + '"]').val(entry[1]);
            if (entry[0] == 'approved_at' && entry[1] != null) {
              var approved_at = (entry[1]).split(" ");
              $('input[name="approved_at"]').val(approved_at[0]);
              $('input[name="approved_time"]').val(approved_at[1]);
            }
            // console.log(entry[0]);

            if (entry[0] == 'picture' && Array.isArray(jQuery.parseJSON(entry[1]))) {
              $.each(jQuery.parseJSON(entry[1]), function (indexInArray, valueOfElement) {
                $('#leave_picture').append('<div class="row container_leave_img"><a target="_blank" href="' + rurl + "photos/images_leave/" + valueOfElement + '"><img class="img-thumbnail leave_img" alt="' + valueOfElement + '" width="100%" height="auto" src="' + rurl + "photos/images_leave/" + valueOfElement + '" /></a><a href="#" class="btn btn-xs btn-rm-img topright" data-name="' + valueOfElement + '">x</a href="#"><input type="hidden" name="picture_current[]" value="' + valueOfElement + '"/></div>');
              });
            }
          });
          getApprovers(data.employee_id, data.approver_id);
          $('.ls-select2').select2();
          $('.modal').modal('show');
        },
        error: function (data) {
          swal("ไม่พบข้อมูลที่ท่านต้องการ!", data.responseTextta, "error");
        }
      })
    })

    $(document).on('click', '.formadd [type="submit"]', function () {
      handleValidation();
    })

    $(document).on('click', '.formedit [type="submit"]', function (e) {
      handleValidation();
    })
  }

  var deleteUser = function (value) {
    var id = value.data('id');
    var token = value.data('token');

    $.ajax({
      url: rurl + 'admin/employeeleave/' + id,
      type: 'DELETE',
      data: {
        _method: 'delete',
        _token: token,
        _id: id
      },
      success: function (data) {
        $("#employeeleave").DataTable().ajax.reload(null, false);
        swal('ยินดีด้วย!', data, "success");
      },
      error: function (data) {
        swal("พบข้อผิดผลาด!", data, "error");
      }
    })
  }

  $(document).ready(function () {

    function convert_to_time(date) {
      // e => Y-m-d H:i:s
      var dateSplite = date.split(".");
      return dateSplite[0];
    }

    $(document).on('change', '#employee_id', function (btn) {
      getApprovers($(this).val(), null);
    });

    $(document).on('change', '[name="leave_duration_id"]', function (e){
      var id = $(this).val();
      $.ajax({
        type: 'get',
        url: rurl + 'admin/leaveduration/'+id,
        dataType: "JSON",
        success: function (data) {
          $('[name="start_time"]').val(convert_to_time(data.duration_start));
          $('[name="end_time"]').val(convert_to_time(data.duration_end));
        }
      });
    });

    $(document).on('click', '.btn-rm-img', function () {
      var picture_remove_name = $(this).data('name');
      $('div.picture_remove').append('<input type="text" name="picture_remove[]" value="' + picture_remove_name + '"/>');
      $(this).closest('.container_leave_img').remove();
      removeFile(picture_remove_name);
    });



    function removeFile(e) {
      var file = (e);
      var file_attach = $('[name="file[]"]').get(0).files;
      consolel.log(file_attach);
      var file_attach_arr = Array.from(file_attach);
      
      for(var i=0;i<file_attach_arr.length;i++) {
        if(file_attach_arr[i].name == file) {
          console.log(delete file_attach_arr[i]);
          console.log(delete $('[name="file[]"]').get(0).files[i]);
          break;
        }
      }
      
    }

    $(document).on('change', '[name="file[]"]', function (e) {
      var files = e.target.files;
      for (var i = 0; i < files.length; ++i) {
        filePreview(files[i]);
      }
    });

  });

  $("#file").change(function () {
    filePreview(this);
  });

  function filePreview(input) {
    if (input) {
      var reader = new FileReader();
      reader.onload = function (e) {
        e.target.result;
        $('#leave_picture').append('<div class="row container_leave_img"><a target="_blank" href="' + rurl + "photos/images_leave/" + input.name + '"><img class="img-thumbnail leave_img" alt="' + input.name + '" width="100%" height="auto" src="'+e.target.result+'" /></a><a href="#" class="btn btn-xs btn-rm-img topright" data-name="' + input.name + '">x</a href="#"><input type="hidden" name="picture_current[]" value="' + input.name + '"/></div>');
      };
      reader.readAsDataURL(input);
    }
  }

  function getApprovers(employee_id, approver_id = null) {
    var id = employee_id;
    $.ajax({
      type: 'get',
      url: rurl + 'admin/employeeleave/approvers/' + id,
      dataType: "JSON",
      success: function (data) {
        if (data != 0) {
          $('select#approver_id').empty();
          $('<option value="">== อนุมัติโดย ==</option>').appendTo('select#approver_id');
          $.each(data, function (indexInArray, valueOfElement) {
            $('select#approver_id').append('<option value="' + valueOfElement.id + '" ' + ((approver_id == valueOfElement.id) ? "selected" : "") + '>' + valueOfElement.firstname + ' ' + valueOfElement.lastname + '</option>');
          });
        }
      }
    });
  }

  return {
    // main function to initiate the module
    init: function () {
      handleTables();
      handleButton();
    }
  }
})()

jQuery(document).ready(function () {
  Employeeleave.init();
})