var Importevaluation = (function () {
    var handleTables = function () {
      $('#importevaluation').append("<title style='title-side: bottom'>ข้อมูลวันที่ : {{date('Y-m-d H:i:s')}}</caption>");
      var table = $('#importevaluation').DataTable({
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": rurl + 'admin/importevaluation/list',
        "language": { "url" : rurl + "assets/plugins/datatable_th.json" },
        "columns": [{
            "data": 'DT_RowIndex',
            "name": 'DT_RowIndex',
            orderable: false,
            searchable: false,
            className:"text-center"
          },
					{"data":"employee","name":"import_evaluation.employee"},
					{"data":"employee_target","name":"import_evaluation.employee_target"},
					{"data":"job","name":"import_evaluation.job"},
					{"data":"kpi","name":"import_evaluation.kpi"},
					{"data":"attitute","name":"import_evaluation.attitute"},
					{"data":"created_at","name":"import_evaluation.created_at"},
          // {
          //   "data": "action",
          //   orderable: false,
          //   searchable: false
          // }
        ],
        "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
        "lengthMenu": [
            //[10, 25, 50, -1],
            //[10, 25, 50, "All"]
            [50, 100, 200, 300, 400, 1000],
            [50, 100, 200, 300, 400, 1000]
        ],
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-default btn-sm',
                text: '<i class="fas fa-file-excel"></i> Excel',
                title:'ข้อมูลความสัมพันธ์การประเมิน',
                exportOptions: {
                  columns: [ 1,2,3,4,5 ]
                }
            },
            {
                extend: 'print',
                className: 'btn btn-default btn-sm',
                text: '<i class="fas fa-print"></i> Print',
                title:'ข้อมูลความสัมพันธ์การประเมิน',
                orientation: 'landscape',
                exportOptions: {
                  columns: [ 1,2,3,4,5 ]
                }
            },
        ],
      });
    }
  
    var handleValidation = function () {
      var form = $('.validateForm');
      var btn = $('.validateForm [type="submit"]');

      form.validate({
        errorElement: 'span', // default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: '', // validate all fields including form hidden input

        rules: {

        },
  
        highlight: function (element) { // hightlight error inputs
          $(element)
            .closest('.form-group .form-control').addClass('is-invalid') // set invalid class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
          $(element)
            .closest('.form-group .form-control').removeClass('is-invalid') // set invalid class to the control group
            .closest('.form-group .form-control').addClass('is-valid')
        },
        errorPlacement: function (error, element) {
          if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
            error.insertAfter(element.parent())
          } else {
            error.insertAfter(element)
          }
        },
        success: function (label) {
          label
            .closest('.form-group .form-control').removeClass('is-invalid') // set success class to the control group
        },
        submitHandler: function(element) {
          btn.prop('disabled', true)
          $.ajax({
            type: "post",
            url: rurl+'admin/importevaluation',
            data: $( element ).serialize(),
            dataType: "html",
            success: function (data) {
              btn.prop('disabled', false)
              $('[data-dismiss="modal"]').trigger('click');
              $('.validateForm').removeClass('formadd')
              $('.validateForm').removeClass('formedit')
              $("#importevaluation").DataTable().ajax.reload(null, false);
              swal('ยินดีด้วย!', data, "success")
            },
            error: function (data) {
              $('.validateForm').removeClass('formadd')
              $('.validateForm').removeClass('formedit')
              
            }
          });
        }
      })
    }

    var handleButton = function () {
        $(document).on('click', '.btn-delete', function(){
          $this = $(this)
          swal({
            title: "คุณแน่ใจไหม?",
            text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "ใช่, ยืนยัน!",
            cancelButtonText: "ยกเลิก",
            closeOnConfirm: false
          },
          function(){
            deleteUser($this);
          });
        })

        $(document).on('click', '.btn-add', function(){
          $('[name="id"]').val(null);

          //remove form class
          $('.validateForm').removeClass('formadd')
          $('.validateForm').removeClass('formedit')

          $('.validateForm').trigger('reset')
          $('.validateForm').addClass('formadd')
          $('.validateForm').removeAttr('data-id')

          //add value to form example
          $('.ls-select2').select2();
        })

        $(document).on('click', '.btn-edit', function(btn){
          //remove form class
          $('.validateForm').removeClass('formadd');
          $('.validateForm').removeClass('formedit');

          var id = $(this).data('id');
          $('[name="id"]').val(id);

          var selector = $('.validateForm');
          selector.addClass('formedit');
          selector.find('[type="submit"]').removeAttr('data-id');
          selector.find('[type="submit"]').attr('data-id',id);
          $.ajax({
            type: 'get',
            url: rurl+'admin/importevaluation/'+id,
            dataType: "json",
            success: function (data) {
              Object.entries(data).forEach(entry => {
                $('[name="'+entry[0]+'"]').val(entry[1]);
              })
              $('.ls-select2').select2();
              $('.modal').modal('show');
            },
            error: function (data) {
              swal("ไม่พบข้อมูลที่ท่านต้องการ!",data.responseTextta, "error");
            }
          })
        })

        $(document).on('click', '.formadd [type="submit"]', function(){
          handleValidation();
        })

        $(document).on('click', '.formedit [type="submit"]', function(e){
          handleValidation();
        })
    }

    var deleteUser = function (value) {
      var id = value.data('id');
      var token = value.data('token');

      $.ajax({
        url: rurl+'admin/importevaluation/'+id,
        type: 'DELETE',
        data: {
          _method: 'delete',
          _token: token,
          _id : id
        },
        success: function (data) {
          $("#importevaluation").DataTable().ajax.reload(null, false);
          swal('ยินดีด้วย!', data, "success");
        },
        error: function (data) {
          swal("พบข้อผิดผลาด!",data, "error");
        }
      })
    }
  
    return {
      // main function to initiate the module
      init: function () {
        handleTables();
        handleButton();
      }
    }
  })()
  
  jQuery(document).ready(function () {
    Importevaluation.init();
  })