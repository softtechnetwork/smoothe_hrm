var Employeeleave = (function () {
  var handleTables = function () {
    var table = $('#employeeleave').DataTable({
      "responsive": true,
      "serverSide": true,
      "processing": true,
      "ajax": rurl + 'admin/employeeleave/list_approver',
      "language": {
        "url": rurl + "assets/plugins/datatable_th.json"
      },
      "columns": [{
          "data": 'DT_RowIndex',
          "name": 'DT_RowIndex',
          orderable: false,
          searchable: false,
          className: "text-center"
        },
        {
          "data": "start_date",
          "name": "employee_leave.start_date"
        },
        {
          "data": "end_date",
          "name": "employee_leave.end_date"
        },
        {
          "data": "ep1firstname",
          "name": "ep1.firstname"
        },
        {
          "data": "ep1lastname",
          "name": "ep1.lastname",
          "visible": false
        },
        {
          "data": "leave_name",
          "name": "leave_type.leave_name"
        },
        {
          "data": "duration_name",
          "name": "leave_duration.duration_name"
        },
        {
          "data": "remark",
          "name": "employee_leave.remark"
        },
        {
          "data": "ep2firstname",
          "name": "ep2.firstname"
        },
        {
          "data": "ep2lastname",
          "name": "ep2.lastname",
          "visible": false
        },
        {
          "data": "approved_at",
          "name": "employee_leave.approved_at"
        },
        {
          "data": "leave_result",
          "name": "employee_leave.leave_result"
        },
        {
          "data": "action",
          orderable: false,
          searchable: false
        }
      ],
      "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
      "lengthMenu": [
          //[10, 25, 50, -1],
          //[10, 25, 50, "All"]
          [50, 100, 200, 300, 400, 1000],
          [50, 100, 200, 300, 400, 1000]
      ],
      buttons: [
          {
              extend: 'excel',
              className: 'btn btn-default btn-sm',
              text: '<i class="fas fa-file-excel"></i> Excel'
          },
          {
              extend: 'print',
              className: 'btn btn-default btn-sm',
              text: '<i class="fas fa-print"></i> Print',
              orientation: 'landscape'
          },
      ],
    });
  }

  var handleButton = function () {
    $(document).on('click', '.btn-approve', function () {
      var id = $(this).data('id');
      $.ajax({
        type: 'get',
        url: rurl + 'admin/employeeleave/detail/' + id,
        dataType: "json",
        success: function (data) {
          $('.modal-approve').find('input[name="id"]').val(id);
          $('.modal-approve').find('#picture_profile').attr('data-src-retina', surl + data.employee.picture_profile).attr('data-src', surl + data.employee.picture_profile).attr('src', surl + data.employee.picture_profile);
          // $('.modal-approve').find('#picture_profile').attr('data-src', surl + data.employee.picture_profile);
          // $('.modal-approve').find('#picture_profile').attr('src', surl + data.employee.picture_profile);
          $('.modal-approve').find('#name').html(data.employee.firstname + ' ' + data.employee.lastname);
          $('.modal-approve').find('#branch_level').html(data.employee.bname + ' | ' + data.employee.lname);
          $('.modal-approve').modal('show');

          $('.modal-approve').find('td#tb-start').html(data.employeeleave.start_date);
          $('.modal-approve').find('td#tb-end').html(data.employeeleave.end_date);
          // $('.modal-approve').find('td#tb-name').html(data.employee.firstname + ' ' + data.employee.lastname);
          $('.modal-approve').find('td#tb-type').html(data.employeeleave.leave_name);
          $('.modal-approve').find('td#tb-duration').html(data.employeeleave.duration_name);
          $('.modal-approve').find('td#tb-remark').html(data.employeeleave.remark);
        },
        error: function (data) {
          swal("ไม่พบข้อมูลที่ท่านต้องการ!", data.responseText, "error");
        }
      });
    });

    $(document).on('click', '.set-approve', function () {
      var id = $('input[name="id"]').val();
      $.ajax({
        type: 'post',
        url: rurl + 'admin/employeeleave',
        data:{ id:id,leave_result:"T",approver_id:approver_id },
        success: function (data) {
          swal("ยินดีด้วย!", data, "success");
          $("#employeeleave").DataTable().ajax.reload(null, false);
          $('#modelId').modal('hide');
        },
        error: function (data) {
          swal("พบข้อผิดผลาด!", data, "error");
          $('#modelId').modal('hide');
        }
      });
    });

    $(document).on('click', '.set-notapprove', function () {
      var id = $('input[name="id"]').val();
      $.ajax({
        type: 'post',
        url: rurl + 'admin/employeeleave',
        data:{ id:id,leave_result:"F",approver_id:approver_id},
        success: function (data) {
          swal("ยินดีด้วย!", data, "success");
          $("#employeeleave").DataTable().ajax.reload(null, false);
          $('#modelId').modal('hide');
        },
        error: function (data) {
          swal("พบข้อผิดผลาด!", data, "error");
          $('#modelId').modal('hide');
        }
      });
    });

  }

  return {
    // main function to initiate the module
    init: function () {
      handleTables();
      handleButton();
    }
  }
})()

jQuery(document).ready(function () {
  Employeeleave.init();
})