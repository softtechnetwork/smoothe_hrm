var Evaluationresult = (function() {
  var handleTables = function() {
    var option = {
      responsive: true,
      serverSide: true,
      processing: true,
      ajax: {
        type: "POST",
        url: rurl + "admin/evaluationresult/list",
        data: {
          evaluation_id: $('[name="evaluation_id"]').val(),
          employee_id: $('[name="employee_id"]').val(),
          employee_target_id: $('[name="employee_target_id"]').val(),
        },
      },
      language: { url: rurl + "assets/plugins/datatable_th.json" },
      order: [[11, "desc"]],
      columns: [
        {
          data: "DT_RowIndex",
          name: "DT_RowIndex",
          orderable: false,
          searchable: false,
          className: "text-center",
        },
        { data: "ename", name: "evaluation.evaluation_name" },
        { data: "e1name", name: "e1.firstname", searchable: false },
        { data: "e1firstname", name: "e1.firstname", visible: false },
        { data: "e1lastname", name: "e1.lastname", visible: false },
        { data: "e2name", name: "e2.firstname", searchable: false },
        { data: "e2firstname", name: "e2.firstname", visible: false },
        { data: "e2lastname", name: "e2.lastname", visible: false },
        { data: "etname", name: "evaluation_type.name" },
        { data: "score", name: "evaluation_result.score" },
        { data: "reason", name: "evaluation_result.reason" },
        { data: "created_at", name: "evaluation_result.created_at" },
        { data: "action", searchable: false, ordering: false },
      ],
      dom:
        "<'row' <'col-9'lB> <'col-3'f> >" +
        "tr" +
        "<'row' <'col-6'i> <'col-6'p> >",
      lengthMenu: [
        //[10, 25, 50, 100, 1000, 5000, -1],
        //[10, 25, 50, 100, 1000, 5000, "All"],
        [50, 100, 200, 300, 400, 1000],
        [50, 100, 200, 300, 400, 1000]
      ],
      buttons: [
        {
          extend: "excel",
          className: "btn btn-default btn-sm",
          text: '<i class="fas fa-file-excel"></i> Excel',
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
          },
          title: "ประวัติการประเมินผล",
        },
        {
          extend: "print",
          className: "btn btn-default btn-sm",
          text: '<i class="fas fa-print"></i> Print',
          orientation: "landscape",
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
          },
          title: "ประวัติการประเมินผล",
        },
      ],
    };

    $(".btn-export").click(function(e) {
      e.preventDefault();
      $.ajax({
        type: "POST",
        url: rurl + "admin/evaluationresult/export",
        data: "data",
        dataType: $("#filter").serialize(),
        success: function(response) {
          console.log(response);
        },
      });
    });

    $("#filter").submit(function(e) {
      // console.log($('#filter').serialize());
      var new_option = {
        responsive: true,
        serverSide: true,
        processing: true,
        ajax: {
          type: "POST",
          url: rurl + "admin/evaluationresult/list",
          data: {
            evaluation_id: $('[name="evaluation_id"]').val(),
            employee_id: $('[name="employee_id"]').val(),
            employee_target_id: $('[name="employee_target_id"]').val(),
          },
        },
        language: { url: rurl + "assets/plugins/datatable_th.json" },
        columns: [
          {
            data: "DT_RowIndex",
            name: "DT_RowIndex",
            orderable: false,
            searchable: false,
            className: "text-center",
          },
          { data: "ename", name: "evaluation.evaluation_name" },
          { data: "e1name", name: "e1.firstname", searchable: false },
          { data: "e1firstname", name: "e1.firstname", visible: false },
          { data: "e1lastname", name: "e1.lastname", visible: false },
          { data: "e2name", name: "e2.firstname", searchable: false },
          { data: "e2firstname", name: "e2.firstname", visible: false },
          { data: "e2lastname", name: "e2.lastname", visible: false },
          { data: "etname", name: "evaluation_type.name" },
          { data: "score", name: "evaluation_result.score" },
          { data: "reason", name: "evaluation_result.reason" },
          { data: "created_at", name: "evaluation_result.created_at" },
          { data: "action", searchable: false, ordering: false },
        ],
        dom:
          "<'row' <'col-9'lB> <'col-3'f> >" +
          "tr" +
          "<'row' <'col-6'i> <'col-6'p> >",
        lengthMenu: [
          [10, 25, 50, 100, 1000, 5000, -1],
          [10, 25, 50, 100, 1000, 5000, "All"],
        ],
        buttons: [
          {
            extend: "excel",
            className: "btn btn-default btn-sm",
            text: '<i class="fas fa-file-excel"></i> Excel',
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
            },
            title: "ประวัติการประเมินผล",
          },
          {
            extend: "print",
            className: "btn btn-default btn-sm",
            text: '<i class="fas fa-print"></i> Print',
            orientation: "landscape",
            exportOptions: {
              columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
            },
            title: "ประวัติการประเมินผล",
          },
        ],
      };
      e.preventDefault();
      $("#evaluationresult")
        .DataTable()
        .destroy();
      $("#evaluationresult").DataTable(new_option);
    });
    var table = $("#evaluationresult").DataTable(option);
  };

  var handleValidation = function() {
    var form = $(".validateForm");
    var btn = $('.validateForm [type="submit"]');

    form.validate({
      errorElement: "span", // default input error message container
      errorClass: "help-block help-block-error", // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "", // validate all fields including form hidden input

      rules: {
        employee_id: { required: true },
        employee_target_id: { required: true },
        evaluation_type_id: { required: true },
      },

      highlight: function(element) {
        // hightlight error inputs
        $(element)
          .closest(".form-group .form-control")
          .addClass("is-invalid"); // set invalid class to the control group
      },
      unhighlight: function(element) {
        // revert the change done by hightlight
        $(element)
          .closest(".form-group .form-control")
          .removeClass("is-invalid") // set invalid class to the control group
          .closest(".form-group .form-control")
          .addClass("is-valid");
      },
      errorPlacement: function(error, element) {
        if (
          element.parent(".input-group").length ||
          element.prop("type") === "checkbox" ||
          element.prop("type") === "radio"
        ) {
          error.insertAfter(element.parent());
        } else {
          error.insertAfter(element);
        }
      },
      success: function(label) {
        label.closest(".form-group .form-control").removeClass("is-invalid"); // set success class to the control group
      },
      submitHandler: function(element) {
        btn.prop("disabled", true);
        $.ajax({
          type: "post",
          url: rurl + "admin/evaluationresult",
          data: $(element).serialize(),
          dataType: "html",
          success: function(data) {
            btn.prop("disabled", false);
            $('[data-dismiss="modal"]').trigger("click");
            $(".validateForm").removeClass("formadd");
            $(".validateForm").removeClass("formedit");
            $("#evaluationresult")
              .DataTable()
              .ajax.reload(null, false);
            swal("ยินดีด้วย!", data, "success");
          },
          error: function(data) {
            $(".validateForm").removeClass("formadd");
            $(".validateForm").removeClass("formedit");
          },
        });
      },
    });
  };

  var handleButton = function() {
    $(document).on("click", ".btn-delete", function() {
      $this = $(this);
      swal(
        {
          title: "คุณแน่ใจไหม?",
          text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "ใช่, ยืนยัน!",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false,
        },
        function() {
          deleteUser($this);
        }
      );
    });

    $(document).on("click", ".btn-add", function() {
      $('[name="id"]').val(null);

      //remove form class
      $(".validateForm").removeClass("formadd");
      $(".validateForm").removeClass("formedit");

      $(".validateForm").trigger("reset");
      $(".validateForm").addClass("formadd");
      $(".validateForm").removeAttr("data-id");

      //add value to form example
      $(".ls-select2").select2();
    });

    $(document).on("click", ".btn-edit", function(btn) {
      //remove form class
      $(".validateForm").removeClass("formadd");
      $(".validateForm").removeClass("formedit");

      var id = $(this).data("id");
      $('[name="id"]').val(id);

      var selector = $(".validateForm");
      selector.addClass("formedit");
      selector.find('[type="submit"]').removeAttr("data-id");
      selector.find('[type="submit"]').attr("data-id", id);
      $.ajax({
        type: "get",
        url: rurl + "admin/evaluationresult/" + id,
        dataType: "json",
        success: function(data) {
          Object.entries(data).forEach((entry) => {
            $('[name="' + entry[0] + '"]').val(entry[1]);
          });
          $(".ls-select2").select2();
          $(".modal").modal("show");
        },
        error: function(data) {
          swal("ไม่พบข้อมูลที่ท่านต้องการ!", data.responseTextta, "error");
        },
      });
    });

    $(document).on("click", '.formadd [type="submit"]', function() {
      handleValidation();
    });

    $(document).on("click", '.formedit [type="submit"]', function(e) {
      handleValidation();
    });
  };

  var deleteUser = function(value) {
    var id = value.data("id");
    var token = value.data("token");

    $.ajax({
      url: rurl + "admin/evaluationresult/" + id,
      type: "DELETE",
      data: {
        _method: "delete",
        _token: token,
        _id: id,
      },
      success: function(data) {
        $("#evaluationresult")
          .DataTable()
          .ajax.reload(null, false);
        swal("ยินดีด้วย!", data, "success");
      },
      error: function(data) {
        swal("พบข้อผิดผลาด!", data, "error");
      },
    });
  };

  $(".ls-select2").select2();

  return {
    // main function to initiate the module
    init: function() {
      handleTables();
      handleButton();
    },
  };
})();

jQuery(document).ready(function() {
  Evaluationresult.init();
});
