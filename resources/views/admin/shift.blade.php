@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/shift.js')}}"></script>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + {{ isset($menu) ? $menu : '' }}
        </button>
    </div>
    <div class="card-body">
        <table id="shift" class="table table-xs table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>กะทำงาน</th>
                    <th>เริ่มงาน</th>
                    <th>เลิกงาน</th>
                    <th>เริ่มพัก</th>
                    <th>เลิกพัก</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form class="validateForm">
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5>{{ isset($menu) ? $menu : '' }}</h5>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">กะทำงาน</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" placeholder="name" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="time_start" class="col-sm-2 col-form-label">เริ่มงาน</label>
                            <div class="col-sm-10">
                                <input type="time" name="time_start" placeholder="time_start"
                                    class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="time_end" class="col-sm-2 col-form-label">เลิกงาน</label>
                            <div class="col-sm-10">
                                <input type="time" name="time_end" placeholder="time_end" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="time_break_start" class="col-sm-2 col-form-label">เริ่มพัก</label>
                            <div class="col-sm-10">
                                <input type="time" name="time_break_start" placeholder="time_break_start"
                                    class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="time_break_end" class="col-sm-2 col-form-label">เลิกพัก</label>
                            <div class="col-sm-10">
                                <input type="time" name="time_break_end" placeholder="time_break_end"
                                    class="form-control input-sm">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop