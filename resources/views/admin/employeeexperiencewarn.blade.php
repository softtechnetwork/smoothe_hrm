@extends('admin.layouts.app')

@section('style')
<link rel="stylesheet" href="{{asset('template/condensed/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}">
@stop

@section('script')
<script src="{{asset('template/condensed/assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('template/condensed/assets/plugins/moment/moment-with-locales.min.js')}}"></script>
<script src="{{asset('template/condensed/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>

<script src="{{asset('assets/admin/js/admin/employeeexperiencewarn.js')}}"></script>
<script>
	$('[name="start_date"]').daterangepicker({
        timePicker: false,
        timePickerIncrement: 30,
        format: 'YYYY-MM-DD',
        setDate:"{{date('Y-m-d')}}"
    },function(start, end, label){
        $('[name="start_date"]').val(start.format('YYYY-MM-DD'));
        $('[name="end_date"]').val(end.format('YYYY-MM-DD'));
    });
</script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<div class="card-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseHistory">
				<h5>ประวัติ{{ isset($menu) ? $menu : '' }}</h5>
			</a>
		</div>
	</div>
	<div class="card-body">
		<form id="employeeexperiencewarn">
			<input type="hidden" name="experience_type" value="w">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<label for="leves">การเตือน</label>
					<select class="form-control ls-select2" name="warning_id" id="warning_id" tabindex="-1" aria-hidden="true">
						<option value="">== การเตือน ==</option>
						@foreach ($warning as $item)
						<option value="{{$item->id}}">{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4 col-sm-6">
					<label for="date_start">ช่วงวันที่</label>
					<input type="text" name="start_date" id="date_start" class="form-control">
				</div>
				<div class="col-md-4 col-sm-6">
					<label for="date_end">ถึงวันที่</label>
					<input type="text" name="end_date" id="date_end" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<label for="leves">พนักงาน</label>
					<select class="form-control ls-select2" name="employee_id" id="employee_id" tabindex="-1" aria-hidden="true">
						<option value="">== พนักงาน ==</option>
						@foreach ($employee as $item)
						<option value="{{$item->id}}">{{ $item->prename.$item->firstname." ".$item->lastname }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4 col-sm-6">
					<label for=""></label>
					<button type="submit" class="btn btn-primary btn-block">ค้นหา</button>
				</div>
				<div class="col-md-4 col-sm-6">
					<label for=""></label>
					<a class="btn btn-default btn-block btn-report-people">พิมพ์รายงาน</a>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="card">
	<div class="card-body" id="collapseHistory">
		<table id="employeeexperience"
			class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0"
			width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>ประเภท</th>
					<th>เริ่มต้น</th>
					<th>สิ้นสุด</th>
					<th>ชื่อ</th>
					<th>ชื่อ</th>
					<th>นามสกุล</th>
					<th>รายละเอียด</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="firstname" class="col-sm-2 col-form-label">firstname</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="employee_id">
									<option value="">== firstname ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{$item->id}}">{{$item->firstname}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="experience_type" class="col-sm-2 col-form-label">experience_type</label>
							<div class="col-sm-10">
								<input type="text" name="experience_type" placeholder="experience_type"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="place" class="col-sm-2 col-form-label">place</label>
							<div class="col-sm-10">
								<input type="text" name="place" placeholder="place" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="level" class="col-sm-2 col-form-label">level</label>
							<div class="col-sm-10">
								<input type="text" name="level" placeholder="level" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="start_date" class="col-sm-2 col-form-label">start_date</label>
							<div class="col-sm-10">
								<input type="date" name="start_date" placeholder="start_date"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="end_date" class="col-sm-2 col-form-label">end_date</label>
							<div class="col-sm-10">
								<input type="date" name="end_date" placeholder="end_date" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="subject" class="col-sm-2 col-form-label">subject</label>
							<div class="col-sm-10">
								<input type="text" name="subject" placeholder="subject" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="name" class="col-sm-2 col-form-label">name</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="education_level_id">
									<option value="">== name ==</option>
									@foreach ($educationlevel as $key => $item)
									<option value="{{$item->id}}">{{$item->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="name" class="col-sm-2 col-form-label">name</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="institute_id">
									<option value="">== name ==</option>
									@foreach ($institutes as $key => $item)
									<option value="{{$item->id}}">{{$item->name}}</option>
									@endforeach
								</select>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop