@extends('admin.layouts.app')

@section('script')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>
<script src="{{asset('assets/admin/js/admin/employeeleave_approver.js')}}"></script>
<script>
	var approver_id = {{\AUTH::guard('admin')->user()->employee_id}};
</script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
	</div>
	<div class="card-body">
		<table id="employeeleave" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>ลาวันที่</th>
					<th>ถึงวันที่</th>
					<th>ชื่อ - นามสกุล</th>
					<th></th>
					<th>ประเภทการลา</th>
					<th>ช่วงเวลา</th>
					<th>หมายเหตุ</th>
					<th>อนุมัติโดย</th>
					<th></th>
					<th>อนุมัติวันที่</th>
					<th>สถานะ</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<!-- Modal -->
<div class="modal slide-up fade modal-approve" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content-wrapper">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">อนุมัติการลา</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div class="social-user-profile col-xs-height text-center col-top">
								<div class="thumbnail-wrapper d48 circular bordered b-white">
									<img id="picture_profile" alt="Avatar">
								</div>
							</div>
							<div class="col-xs-height p-l-20">
								<input type="hidden" name="id">
								<h3 id="name" class="no-margin p-b-5"></h3>
								<p id="branch_level" class="no-margin fs-16"></p>
							</div>
							<table class="table">
								<thead>
									<tr>
										<th>ลาวันที่</th>
										<th>ถึงวันที่</th>
										{{-- <th>ชื่อ - นามสกุล</th> --}}
										<th>ประเภทการลา</th>
										<th>ช่วงเวลา</th>
										<th>หมายเหตุ</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td id="tb-start"></td>
										<td id="tb-end"></td>
										{{-- <td id="tb-name"></td> --}}
										<td id="tb-type"></td>
										<td id="tb-duration"></td>
										<td id="tb-remark"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger set-notapprove">
						<i class="far fa-times-circle"></i> ไม่อนุมัติ
					</button>
					<button type="button" class="btn btn-complete set-approve">
						<i class="far fa-check-circle"></i> อนุมัติ
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
@stop