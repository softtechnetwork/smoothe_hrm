@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/leavetype.js')}}"></script>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + {{ isset($menu) ? $menu : '' }}
        </button>
    </div>
    <div class="card-body">
        <table id="leavetype" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ประเภท</th>
                    <th>จำนวน</th>
                    <th>แจ้งก่อน</th>
                    <th>รายละเอียด</th>
                    <th>สถานะ</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form class="validateForm" id="form">
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5>{{ isset($menu) ? $menu : '' }}</h5>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
                        <div class="form-group row">
                            <label for="leave_name" class="col-sm-2 col-form-label">ประเภท</label>
                            <div class="col-sm-10">
                                <input type="text" name="leave_name" placeholder="ประเภท" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="leave_name" class="col-sm-2 col-form-label">เพศ</label>
                            <div class="col-sm-10">
                                <div class="form-check">
                                  <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="sex[]" value="M">
                                    ชาย
                                  </label>
                                  <br>
                                  <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="sex[]" value="F">
                                    หญิง
                                  </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="before_date" class="col-sm-2 col-form-label">แจ้งก่อน</label>
                            <div class="col-sm-7">
                                <input type="text" name="before_date" placeholder="แจ้งก่อน x วัน"
                                    class="form-control input-sm">
                            </div>
                            <div class="col-sm-3">
                                <select name="before_unit" class="ls-select2">
                                    <option value="">หน่วยนับ</option>
                                    <option value="d">วัน</option>
                                    <option value="h">ชั่วโมง</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="before_date" class="col-sm-2 col-form-label">จำนวนลาขั้นต่ำ</label>
                            <div class="col-sm-7">
                                <input type="text" name="minimum_leave" placeholder="จำนวนลาขั้นต่ำ"
                                    class="form-control input-sm">
                            </div>
                            <div class="col-sm-3">
                                <select name="minimum_leave_unit" class="ls-select2">
                                    <option value="">หน่วยนับ</option>
                                    <option value="d">วัน</option>
                                    <option value="h">ชั่วโมง</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="amount" class="col-sm-2 col-form-label">จำนวน(วัน)</label>
                            <div class="col-sm-10">
                                <input type="text" name="amount" placeholder="จำนวน(วัน)" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="amount" class="col-sm-2 col-form-label">อายุงาน(เดือน)</label>
                            <div class="col-sm-10">
                                <input type="text" name="longevity" placeholder="อายุงาน(เดือน)" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="detail" class="col-sm-2 col-form-label">รายละเอียด</label>
                            <div class="col-sm-10">
                                <textarea name="detail" class="form-control" cols="5" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="in_out_distance" class="col-sm-2 col-form-label">การใช้งาน</label>
                            <div class="col-sm-10">
                                <select class="ls-select2" name="status">
                                    <option value="">== สถานะ ==</option>
                                    <option value="T">เปิด</option>
                                    <option value="F">ปิด</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop