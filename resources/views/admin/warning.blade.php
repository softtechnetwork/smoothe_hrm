@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/warning.js')}}"></script>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + {{ isset($menu) ? $menu : '' }}
        </button>
    </div>
    <div class="card-body">
        <table id="warning" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                        <th>การเตือน</th>
                        <th>สถานะ</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form class="validateForm">
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                        </button>
                        <h5>{{ isset($menu) ? $menu : '' }}</h5>
                        {{-- <p class="p-b-10"></p> --}}
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
<div class="form-group row">
                			<label for="name" class="col-sm-2 col-form-label">การเตือน</label>
                			<div class="col-sm-10">
                    			<input type="text" name="name" placeholder="การเตือน" class="form-control input-sm">
                			</div>
            			</div>
						<div class="form-group row">
						    <label for="status" class="col-sm-2 col-form-label">สถานะ</label>
						    <div class="col-sm-10">
						    <select class="ls-select2" name="status">
						        <option value="">== สถานะ ==</option>
						        <option value="T">เปิดใช้งาน</option>
						        <option value="F">ยกเลิก</option>
							</select>
							</div>
						</div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop