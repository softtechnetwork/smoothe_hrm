<table class="table">
    <thead>
        <tr role="row">
            <th>#</th>
            <th>การประเมิน</th>
            <th>ผู้ประเมิน</th>
            <th>ผู้ถูกประเมิน</th>
            <th>ประเภทการประเมิน</th>
            <th>คะแนน</th>
            <th>เหตุผล</th>
            <th>วันที่ประเมิน</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($evaluation_result as $key => $item)
        <tr>
            <td>{{ $key }}</td>
            <td>{{ $item->ename }}</td>
            <td>{{ $item->e1name }}</td>
            <td>{{ $item->e2name }}</td>
            <td>{{ $item->etname }}</td>
            <td>{{ $item->score }}</td>
            <td>{{ $item->reason }}</td>
            <td>{{ $item->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>