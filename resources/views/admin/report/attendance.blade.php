@extends('admin.layouts.app')

@section('style')
<link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
@endsection

@section('script')
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}"></script>
<script>
    $('.ls-select2').select2();

    $('#datepicker-component1, #datepicker-component2').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        language: 'th'
    });
    
    $('#month').datepicker({
        format: 'yyyy-mm',
        autoclose: true,
        language: 'th',
        startView: "months", 
        minViewMode: "months"
    });

    $('#report').submit(function (e) {
        $('.btn-print-report').attr('href',rurl + 'export/employee_conclusion?'+$(this).serialize());
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: rurl + 'admin/report/attendance/employee',
            data: $(this).serialize(),
            dataType: "JSON",
            success: function (data) {
                console.log(data);
                var emp = data.employee;
                if (emp != null) {
                    $.each(emp, function (indexInArray, valueOfElement) {
                        if (valueOfElement != null) {
                            if(indexInArray=="picture_profile"){
                                $('#' + indexInArray).attr('src',rurl+'/'+valueOfElement);
                            }else{
                                $('#' + indexInArray).html(valueOfElement);
                            }
                        }
                    });
                }
                if (data.registration != null) {
                    $('tbody#attendance').html('');
                    $.each(data.registration, function (indexInArray, valueOfElement) {
                        $('tbody#attendance').append(
                            `<tr>
                                <td class="text-center">`+ valueOfElement.full_in_date +`</td>
                                <td class="text-center">`+ valueOfElement.dayname +`</td>
                                <td class="text-center">`+ valueOfElement.in_time +`</td>
                                <td class="text-center">`+ valueOfElement.out_time +`</td>
                                <td class="text-center">`+ valueOfElement.hr +`</td>
                                <td class="text-center">`+ valueOfElement.status +`</td>
                            </tr>`
                        );
                    });
                }
                if (data.conclusion != null) {
                    $('tbody#conclusion').html('');
                    $.each(data.conclusion, function (indexInArray, valueOfElement) {
                        $('tbody#conclusion').append(
                            `<tr>
                                <td class="text-center">`+ valueOfElement.leave_name +`</td>
                                <td class="text-center">`+ valueOfElement.amount_day +`</td>
                                <td class="text-center">`+ valueOfElement.used_day +`</td>
                                <td class="text-center">`+ valueOfElement.balance_day +`</td>
                            </tr>`
                        );
                    });
                }
                $('.card.d-none').removeClass('d-none');
            },
            error: function (data) {}
        });
    });

    $('input[name="month"]').change(function () {
        var date_start = $('input[name="month"]').val()+'-01';
        var date_end = $('input[name="month"]').val()+'-{{date("t")}}';
        $('input[name="date_start"]').val(date_start);
        $('input[name="date_end"]').val(date_end);
    });
</script>
@endsection

@section('content')
<!-- START card -->
<div class="card card-default">
    <div class="card-header ">
        <div class="card-title text-center">
            <h5>รายงานการลงเวลา</h5>
        </div>
    </div>
    <div class="card-body">
        <form class="form" id="report" action="" method="post">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-input-group">
                        <label>พนักงาน</label>
                        <select name="employee_id" class="ls-select2 form-control" required>
                            <option value="">== เลือก ==</option>
                            @foreach ($employee as $item)
                            <option value="{{$item->id}}">{{$item->firstname." ".$item->lastname}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="col-lg-4">
                    <div class="form-input-group">
                        <label>เดือน</label>
                        <input id="month" name="month" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-input-group">
                        <label></label>
                        <div class="clearfix" style="margin-top: 7px;"></div>
                        <button class="btn btn-block btn-primary" type="submit">ค้นหา</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END card -->

<!-- START card -->
<div class="card card-default d-none">
    <div class="card-body">
        <div class="row">
            <div class="col-3 text-center">
                <img id="picture_profile" class="m-t-5 img-thumbnail" width="120px" height="auto">
            </div>
            <div class="col-3">
                <p class="margin-bottom-20 margin-top-10">
                    <strong>รหัสพนักงาน</strong>&nbsp;
                    <span id="empcode"></span><br>
                    <br>
                    <strong>บริษัท</strong>&nbsp;
                    <span id="cname"></span><br>
                    <br>
                    <strong>ตำแหน่ง</strong>&nbsp;
                    <span id="lname"></span><br>
                </p>
            </div>
            <div class="col-3">
                <p class="margin-bottom-20 margin-top-10">
                    <strong>ชื่อ</strong>&nbsp;
                    <span id="firstname"></span><br>
                    <br>
                    <strong>สังกัด</strong>&nbsp;
                    <span id="bname"></span><br>
                    <br>
                    <strong>เวลาเข้างาน</strong>&nbsp;
                    08:30น.<br>

                </p>
            </div>
            <div class="col-3">
                <p class="margin-bottom-20 margin-top-10">
                    <strong>นามสกุล</strong>&nbsp;
                    <span id="lastname"></span><br>
                    <br>
                    <strong>ฝ่าย</strong>&nbsp;
                    <span id="gname"></span><br>
                    <br>
                    <strong>เวลาออกงาน</strong>&nbsp;
                    18:00น.<br>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="card card-default d-none">
    <div class="card-body">
        <table class="table table-hover table-xs table-condensed dataTable no-footer">
            <thead>
                <tr>
                    <th class="text-center">วันที่</th>
                    <th class="text-center">วัน</th>
                    <th class="text-center">เข้า</th>
                    <th class="text-center">ออก</th>
                    <th class="text-center">เวลาทำงาน</th>
                    <th class="text-center">สถานะ</th>
                </tr>
            </thead>
            <tbody id="attendance"></tbody>
        </table>
    </div>
</div>

<div class="card card-default d-none">
    <div class="card-body">
        <table class="table table-hover table-xs table-condensed dataTable no-footer">
            <thead>
                <tr>
                    <th class="text-center">ประเภท</th>
                    <th class="text-center">ลาได้(วัน)</th>
                    <th class="text-center">ลาไปแล้ว(วัน)</th>
                    <th class="text-center">คงเหลือ(วัน)</th>
                </tr>
            </thead>
            <tbody id="conclusion"></tbody>
        </table>
    </div>
</div>
<div class="card card-default d-none">
    <div class="card-body">
        <a class="btn btn-primary pull-right btn-print-report" target="_blank" style="color:white;">พิมพ์รายงาน</a>
    </div>
</div>
<!-- END card -->
@endsection