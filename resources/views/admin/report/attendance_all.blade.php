@extends('admin.layouts.app')

@section('style')
<link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css"media="screen">
@endsection

@section('script')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript">
</script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js')}}"></script>
<script>
    $('.ls-select2').select2();
    $('#datepicker-component1, #datepicker-component2').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        language: 'th'
    });

    var option_datatable = {
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
            "url": rurl + 'admin/report/attendance_all',
            "type": "POST",
            "data": {
                "company_id": $('[name="company_id"]').val(),
                "branch_id": $('[name="branch_id"]').val(),
                "group_id": $('[name="group_id"]').val(),
                "department_id": $('[name="department_id"]').val(),
                "date_start": $('input[name="date_start"]').val(),
                "date_end": $('input[name="date_end"]').val(),
            }
        },
        "language": {
            "url": rurl + "assets/plugins/datatable_th.json"
        },
        "columns": [{
                    "data": 'DT_RowIndex',
                    "name": 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    className: "text-center"
                },
                {
                    "data": "full_in_date",
                    "name": "employee_registration.in_date",
                    "className": "text-center"
                },
                {
                    "data": "in_time",
                    "name": "employee_registration.in_time",
                    "className": "text-center"
                },
                {
                    "data": "in_reason",
                    "name": "employee_registration.in_reason",
                    "className": "text-center"
                },
                {
                    "data": "out_time",
                    "name": "employee_registration.out_time",
                    "className": "text-center"
                },
                {
                    "data": "out_reason",
                    "name": "employee_registration.out_reason",
                    "className": "text-center"
                },
                {
                    "data": "fullname",
                    "name": "employee.firstname",
                    "className": "text-center"
                },
                {
                    "data": "cname",
                    "name": "company.name",
                    "className": "text-center"
                },
                {
                    "data": "bname",
                    "name": "branch.branch_name",
                    "className": "text-center"
                },
                {
                    "data": "gname",
                    "name": "groups.name",
                    "className": "text-center"
                },
                {
                    "data": "dname",
                    "name": "department.name",
                    "className": "text-center"
                },
                {
                    "data": "hr",
                    "className": "text-center",
                    orderable: false,
                    searchable: false
                },
                {
                    "data": "status",
                    "className": "text-center",
                    orderable: false,
                    searchable: false
                },
            ],
        "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
        "lengthMenu": [
            //[10, 25, 50, -1],
            //[10, 25, 50, "All"]
            [50, 100, 200, 300, 400, 1000],
            [50, 100, 200, 300, 400, 1000]
        ],
        buttons: [
            {
                extend: 'excel',
                className: 'btn btn-default btn-sm',
                text: '<i class="fas fa-file-excel"></i> Excel'
            },
            {
                extend: 'print',
                className: 'btn btn-default btn-sm',
                text: '<i class="fas fa-print"></i> Print',
                orientation: 'landscape'
            },
        ],
    };

    var table = $('#attendance_all').DataTable(option_datatable);

    $('#report').submit(function (e) {
        var new_option = {
            "responsive": true,
            "serverSide": true,
            "processing": true,
            "ajax": {
                "url": rurl + 'admin/report/attendance_all',
                "type": "POST",
                "data": {
                    "company_id": $('[name="company_id"]').val(),
                    "branch_id": $('[name="branch_id"]').val(),
                    "group_id": $('[name="group_id"]').val(),
                    "department_id": $('[name="department_id"]').val(),
                    "date_start": $('input[name="date_start"]').val(),
                    "date_end": $('input[name="date_end"]').val(),
                }
            },
            "language": {
                "url": rurl + "assets/plugins/datatable_th.json"
            },
            "columns": [{
                    "data": 'DT_RowIndex',
                    "name": 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    className: "text-center"
                },
                {
                    "data": "full_in_date",
                    "name": "employee_registration.in_date",
                    "className": "text-center"
                },
                {
                    "data": "in_time",
                    "name": "employee_registration.in_time",
                    "className": "text-center"
                },
                {
                    "data": "in_reason",
                    "name": "employee_registration.in_reason",
                    "className": "text-center"
                },
                {
                    "data": "out_time",
                    "name": "employee_registration.out_time",
                    "className": "text-center"
                },
                {
                    "data": "out_reason",
                    "name": "employee_registration.out_reason",
                    "className": "text-center"
                },
                {
                    "data": "fullname",
                    "name": "employee.firstname",
                    "className": "text-center"
                },
                {
                    "data": "cname",
                    "name": "company.name",
                    "className": "text-center"
                },
                {
                    "data": "bname",
                    "name": "branch.branch_name",
                    "className": "text-center"
                },
                {
                    "data": "gname",
                    "name": "groups.name",
                    "className": "text-center"
                },
                {
                    "data": "dname",
                    "name": "department.name",
                    "className": "text-center"
                },
                {
                    "data": "hr",
                    "className": "text-center",
                    orderable: false,
                    searchable: false
                },
                {
                    "data": "status",
                    "className": "text-center",
                    orderable: false,
                    searchable: false
                },
            ],
            "dom": "<'row' <'col-6'lB> <'col-6'f> >" + "rt" + "<'row' <'col-6' i><'col-6'p> >",
            "lengthMenu": [
                //[10, 25, 50, -1],
                //[10, 25, 50, "All"]
            	[50, 100, 200, 300, 400, 1000],
            	[50, 100, 200, 300, 400, 1000]
            ],
            buttons: [
                {
                    extend: 'excel',
                    className: 'btn btn-default btn-sm',
                    text: '<i class="fas fa-file-excel"></i> Excel',
                    orientation: 'landscape',
                    title: 'รายงานการลงเวลา'
                },
                {
                    extend: 'print',
                    className: 'btn btn-default btn-sm',
                    text: '<i class="fas fa-print"></i> Print',
                    orientation: 'landscape',
                    title: 'รายงานการลงเวลา'
                },
            ],
        };
        e.preventDefault();
        $('#attendance_all').DataTable().destroy();
        $('#attendance_all').DataTable(new_option);
        $('.card.d-none').removeClass('d-none');
    });
</script>
@endsection

@section('content')
<!-- START card -->
<title>รายงานการลงเวลา</title>
<div class="card card-default">
    <div class="card-header ">
        <div class="card-title text-center">
            <h5>รายงานการลงเวลา</h5>
        </div>
    </div>
    <div class="card-body">
        <form class="form" id="report" action="" method="post">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-input-group">
                        <label>บริษัท</label>
                        <select name="company_id" class="ls-select2 form-control">
                            <option value="">== บริษัท ==</option>
                            @foreach ($company as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-input-group">
                        <label>สาขา</label>
                        <select name="branch_id" class="ls-select2 form-control">
                            <option value="">== สาขา ==</option>
                            @foreach ($branch as $item)
                            <option value="{{$item->id}}">{{$item->branch_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-input-group">
                        <label>ฝ่าย</label>
                        <select name="group_id" class="ls-select2 form-control">
                            <option value="">== ฝ่าย ==</option>
                            @foreach ($group as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-input-group">
                        <label>แผนก</label>
                        <select name="department_id" class="ls-select2 form-control">
                            <option value="">== แผนก ==</option>
                            @foreach ($department as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-input-group">
                        <label>วันที่</label>
                        <input name="date_start" type="text" class="form-control" id="datepicker-component1" required>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-input-group">
                        <label>ถึง</label>
                        <input name="date_end" type="text" class="form-control" id="datepicker-component2" required>
                    </div>
                </div>
                <div class="col-lg-3"></div>
                <div class="col-lg-3">
                    <div class="form-input-group">
                        <label></label>
                        <div class="clearfix" style="margin-top: 7px;"></div>
                        <button class="btn btn-block btn-primary" type="submit">ค้นหา</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END card -->

<!-- START card -->
<div class="card card-default">
    <div class="card-body">
        <table id="attendance_all" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>วันที่</th>
                    <th>เข้า</th>
                    <th>เหตุผล</th>
                    <th>ออก</th>
                    <th>เหตุผล</th>
                    <th>ชื่อ</th>
                    <th>บริษัท</th>
                    <th>สาขา</th>
                    <th>ฝ่าย</th>
                    <th>แผนก</th>
                    <th>ชั่วโมง:นาที</th>
                    <th>สถานะ</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<!-- END card -->
@endsection