@extends('admin.layouts.app')

@section('script')
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>
<script src="{{asset('assets/admin/js/admin/project.js')}}"></script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
		<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
			+ {{ isset($menu) ? $menu : '' }}
		</button>
		<div class="pull-right">
			( หมายเหตุ : N = In progress , P = Pending , U= Uncompleted , C = Complete , H = Hold )
			&nbsp;&nbsp;&nbsp;&nbsp;
		</div>
	</div>
	<div class="card-body">
		<table id="project" class="table table-xs table-hover table-bordered table-striped dataTable no-footer"
			cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>ชื่อโปรเจค</th>
					<th width="20%">รายละเอียด</th>
					<th>วันเสร็จงาน</th>
					<th>ผู้ดูแลโปรเจค</th>
					<th>firstname</th>
					<th>lastname</th>
					<th width="15%">ผู้ที่ได้รับมอบหมาย</th>
					<th>สถานะ</th>
					<th>วันที่สร้างโปรเจค</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal modal-project fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
						{{-- <p class="p-b-10"></p> --}}
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="name" class="col-sm-2 col-form-label">ชื่อโปรเจค</label>
							<div class="col-sm-10">
								<input type="text" name="name" placeholder="name" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="detail" class="col-sm-2 col-form-label">รายละเอียด</label>
							<div class="col-sm-10">
								<input type="text" name="detail" placeholder="detail" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="due_date" class="col-sm-2 col-form-label">วันเสร็จงาน</label>
							<div class="col-sm-10">
								<input type="date" name="due_date" placeholder="due_date" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="status" class="col-sm-2 col-form-label">สถานะ</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="status">
									<option value="">== สถานะ ==</option>
									<option value="N">In progress</option>
									<option value="P">Pending</option>
									<option value="U">Uncompleted</option>
									<option value="C">Complete</option>
									<option value="H">Hold</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="firstname" class="col-sm-2 col-form-label">ผู้ดูแลโปรเจค</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="created_by">
									<option value="">== ผู้ดูแลโปรเจค ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{$item->id}}">{{$item->firstname.' '.$item->lastname}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form class="manage-people">
	<div class="modal modal-manage-people fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-full">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
					</div>
					<div class="modal-body">
						<div class="form-group row">
							<label for="status" class="col-sm-2 col-form-label">สถานะ</label>
							<input type="hidden" name="project_id">
							<div class="col-sm-10">
								<select class="ls-select2" name="assign_people[]" multiple>
									<option value="">== พนักงาน ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{ $item->id }}" data-picture-image="{{ $item->picture_profile }}">{{ $item->firstname.' '.$item->lastname }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-12" id="project_assign_people">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop